<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Project;
use App\TaskPeople;
use App\TaskPeopleGroup;
use App\ProjectStructure;
use App\Absensi;
use Excel;
use Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{
    public function index()
    {
        $data = DB::table('view_project')->orderBy('id','desc')->paginate(10);
        return view('TaskList.index_data',compact('data'));
    }

    public function getTaskProject($id)
    {
         $jumlahData = 10;
         $project = Project::where('id',$id)->first();
         $structure = ProjectStructure::where('project_id',$id)->first();
         $job = DB::table('view_project_vacancy')->where('project_id',$id)->get();
         $task_group = DB::table('view_task_group')->where('project_id',$id)->get();
         $outlet = DB::table('outlet')->orderBy('id','desc')->get();

         $structure = ProjectStructure::where('project_id',$id)->first();
          $m_district = array();
         if($structure == NULL){
           $m_district = [];
         }else{
             $m_district = DB::table('structure_area_step2')->where('title_structure_id',$structure->structure_id)->distinct('district')->get();
         }
        
         $member = DB::table('task_people_project')->where('project_id',$id)->where('status',1)->distinct('employee_id')->paginate($jumlahData);
    
        
         return view('TaskList.task_project',compact('project','member','job','outlet','task_group','m_district','jumlahData'));
    }

    public function getAddTaskPeople($id)
    {
        
        $member = DB::table('task_people_project')->where('id',$id)->first();   
        $project = Project::where('id',$member->project_id)->first();
        $m_outlet = DB::table('outlet')->where('outlet_city',$member->distict)->get();

        $data = DB::table('view_task_people2')->where('project_id',$project->id)->where('people_id',$member->employee_id)->orderBy('id','desc')->get();

        
        return view('TaskList.task_people',compact('project','member','m_outlet','data'));
    }

    public function postAddTaskPeople(Request $request)
    {
        $outlet = $request->outlet_id;
        $count = DB::table('task_people')->where('people_id', $request->people_id)->get();
        $next_sequence = (int)$count->count() + 1;
        $user = Auth::user();
        for ($i=0; $i < count($outlet); $i++) { 
            $task = array();
            $task['people_id'] = $request->people_id;
            $task['project_id'] = $request->project_id;
            $task['outlet_id'] = $request->outlet_id[$i];
            $task['schedule_date'] = date('Y-m-d', strtotime($request->schedule));
            $task['task_start'] = date('Y-m-d', strtotime($request->task_start));
            $task['task_end'] = date('Y-m-d', strtotime($request->task_end));
            $task['description'] = $request->desc;
            $task['task_type'] = $request->task_type;
            $task['task_number'] = "T".date('Ymd').$request->people_id.$next_sequence;
            $task['active_flag'] = '1';
            $task['status'] = '1';
            $task['status_flag'] = '0';
            $task['delete_flag'] = '1';
            $task['created_by'] = $user->name;
            $task['updated_by'] = 'system';

            TaskPeople::create($task);
        }


        return redirect()->back();
    }

    public function postAddTaskGroup(Request $request)
    {
        $user = Auth::user();
        $task_group = new  TaskPeopleGroup();
        $task_group->project_id = $request->project_id;
        $task_group->outlet_id = $request->outlet_id;
        $task_group->interes_id = $request->interes_id;
        $task_group->task_start = date('Y-m-d', strtotime($request->task_start));
        $task_group->task_end = date('Y-m-d', strtotime($request->task_end));
        $task_group->description = $request->desc;
        $task_group->status = '1';
        $task_group->active_flag = '1';
        $task_group->delete_flag = '1';
        $task_group->created_by = $user->name;
        $task_group->updated_by = 'system';

        $task_group->save();

        return redirect()->back();
    }

    public function getViewTaskProject($id)
    {
		$jumlahData = 10;
		$active = 'job_vacancy';
		if(Input::get('data') == 'task_people'){
			$active = 'job_vacancy';
		}elseif(Input::get('data') == 'absensi'){
			$active = 'personal_data';
		}
        
		$people_id = 'all';
		$people_id_tasklist = 'all';
		$dateFrom = '';
		$dateTo = '';
		$dateFromTasksList = '';
		$dateToTasksList = '';
        $project = Project::where('id',$id)->first();
        $task = DB::table('task_people')->where('project_id',$id)->get();
        foreach ($task as $key) {
            $key->qty_task = DB::table('task_people')->where('project_id',$id)->where('people_id',$key->people_id)->count();

        }
        $task_people = DB::table('view_task_project_test')->where('project_id',$id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
        $absensi = DB::table('absensi_people_view')->where('project_id',$id)->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
        $member = DB::table('view_project_member')->where('project_id',$id)->orderBy('first_name')->get();
		
		$task_people->appends(['data' => 'task_people']);
		$task_people->appends(['absensi' => $absensi->currentPage()]);
		
		$absensi->appends(['data' => 'absensi']);
		$absensi->appends(['task_people' => $task_people->currentPage()]);
		
        return view('TaskList.view_task_project',compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList', 'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData'));
    }

    public function getDetailTaskProject($id,$people,$date)
    {

        $project = Project::where('id',$id)->first();
        $detail_task = DB::table('view_detail_task')->where('project_id',$id)->where('people_id',$people)->where('schedule_date',$date)->get();

        return view('TaskList.detail_view_task',compact('project','detail_task'));
    }

    public function getAbsensiProject($people,$schedule,$project)
    {
        $project = Project::where('id',$project)->first();
        $absensi = Absensi::where('people_id',$people)->get();
        $absensi_outlet = DB::table('detail_absensi_outlet')->where('people_id',$people)->where('task_number',$schedule)->get();
        return view('TaskList.absensi_tasklist',compact('project','absensi','absensi_outlet'));
    }

    

        public function getFilterAbsensi2(Request $request)
    {
        if($request->submit == 'download'){
            $people = $request->people;
            $from = date('Y-m-d', strtotime($request->dateFrom));
            $to = date('Y-m-d', strtotime($request->dateTo));
            $active = 'personal_data';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();
            if($project_id != null){
                if($people == 'all' && $request->dateFrom == null && $request->dateTo == null){
                     $absensi = DB::table('data_report_excel')->orderBy('first_name')->orderBy('date_absensi','desc')->get();
                    return Excel::create('laravelcode', function($excel) use ($absensi) {
                        $excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
                               $sheet->loadView('TaskList.absensi', array(
                                      'absensi' => $absensi
                               ));
                         });
                     })->export('xls');

                }elseif($people == 'all' && $request->dateFrom != null && $request->dateTo != null){
                    $absensi = DB::table('data_report_excel')->WhereBetween('date_absensi',array($from,$to))->orderBy('first_name')->orderBy('date_absensi','desc')->get();
                    return Excel::create('laravelcode', function($excel) use ($absensi) {
                        $excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
                               $sheet->loadView('TaskList.absensi', array(
                                      'absensi' => $absensi
                               ));
                         });
                     })->export('xls');
                }elseif($people != 'all' && $request->dateFrom != null && $request->dateTo != null){
                     $absensi = DB::table('data_report_excel')->where('people_id',$people)->WhereBetween('date_absensi',array($from,$to))->orderBy('first_name')->orderBy('date_absensi','desc')->get();
                    return Excel::create('laravelcode', function($excel) use ($absensi) {
                        $excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
                               $sheet->loadView('TaskList.absensi', array(
                                      'absensi' => $absensi
                               ));
                         });
                     })->export('xls');

                }elseif($people != 'all'){
                 
                     $absensi = DB::table('data_report_excel')->where('people_id',$people)->orderBy('first_name')->orderBy('date_absensi','desc')->get();
                    return Excel::create('laravelcode', function($excel) use ($absensi) {
                        $excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
                               $sheet->loadView('TaskList.absensi', array(
                                      'absensi' => $absensi
                               ));
                         });
                     })->export('xls');

                }else{
                   
                    $absensi = DB::table('data_report_excel')->where('people_id',$people)->orderBy('first_name')->orderBy('date_absensi','desc')->WhereBetween('date_absensi',array($from,$to))->get();
                     return Excel::create('laravelcode', function($excel) use ($absensi) {
                        $excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
                               $sheet->loadView('TaskList.absensi', array(
                                      'absensi' => $absensi
                               ));
                         });
                     })->export('xls');
                }
                return view('TaskList.view_task_project',compact('project','task_people','task','absensi','member','active'));
            }
        }else{
			$jumlahData = 10;
            $people = $request->people;
            $from = date('Y-m-d', strtotime($request->dateFrom));
            $to = date('Y-m-d', strtotime($request->dateTo));
            $active = 'personal_data';
			if(Input::get('data') == 'task_people'){
				$active = 'job_vacancy';
			}elseif(Input::get('data') == 'absensi'){
				$active = 'personal_data';
			}
            $people_id = $request->people;
            $people_id_tasklist = 'all';
            $dateFrom = '';
            $dateTo = '';
            $dateFromTasksList = '';
            $dateToTasksList = '';
            $project_id = $request->project_id;
            
            if($project_id != null){
				
				$project = Project::where('id',$project_id)->first();
				$task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
				$member = DB::table('view_project_member')->where('project_id',$project_id)->get();
				
                if($people == 'all' && $request->dateFrom == null && $request->dateTo == null){
                    
                    $absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }elseif($people == 'all' && $request->dateFrom != null && $request->dateTo != null){
                    
                    $dateFrom = date('d-m-Y', strtotime($request->dateFrom));
                    $dateTo = date('d-m-Y', strtotime($request->dateTo));
                    $absensi = DB::table('absensi_people_view')->whereBetween('date_absensi', [$from,$to])->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');                    
                    
                }elseif($people != 'all' && $request->dateFrom != null && $request->dateTo != null){
                    
                    $dateFrom = date('d-m-Y', strtotime($request->dateFrom));
                    $dateTo = date('d-m-Y', strtotime($request->dateTo));
                    $absensi = DB::table('absensi_people_view')->where('people_id',$people)->whereBetween('date_absensi', [$from,$to])->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }elseif($people != 'all'){
                    
                    $absensi = DB::table('absensi_people_view')->where('people_id',$people)->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }else{

                    $absensi = DB::table('absensi_people_view')->where('people_id',$people)->whereBetween('date_absensi',array($from,$to))->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }
				
        				$task_people->appends(['data' => 'task_people']);
        				$task_people->appends(['absensi' => $absensi->currentPage()]);
        				$task_people->appends($request->only('project_id'));
        				$task_people->appends($request->only('people'));
        				
        				$absensi->appends(['data' => 'absensi']);
        				$absensi->appends(['task_people' => $task_people->currentPage()]);
				
                $absensi->appends($request->only('people'));
                $absensi->appends($request->only('project_id'));
                $absensi->appends($request->only('dateFrom'));
                $absensi->appends($request->only('dateTo'));
                
                 return view('TaskList.view_task_project',compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList', 'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData'));
               
            }
        }

    }

		
		public function getFilterTaskList2(Request $request){
			
			$jumlahData = 10;
			$people = $request->people_id_tasklist;
            $from = date('Y-m-d', strtotime($request->dateFromTasksList));
            $to = date('Y-m-d', strtotime($request->dateToTasksList));
            $active = 'job_vacancy';
			if(Input::get('data') == 'task_people'){
				$active = 'job_vacancy';
			}elseif(Input::get('data') == 'absensi'){
				$active = 'personal_data';
			}
			$people_id = 'all';
			$people_id_tasklist = $request->people_id_tasklist;
			$dateFrom = '';
			$dateTo = '';
			$dateFromTasksList = '';
			$dateToTasksList = '';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();

            if($project_id != null){
				
				$absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                $member = DB::table('view_project_member')->where('project_id',$project_id)->get();
				
                if($people == 'all' && $request->dateFromTasksList == null && $request->dateToTasksList == null){
					
					$task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }elseif($people == 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){
					
    					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
    					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
    					$task_people = DB::table('view_task_project_test')->whereBetween('schedule_date', [$from,$to])->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }elseif($people != 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){

          					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
          					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->whereBetween('schedule_date',array($from,$to))->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
	
					
                }elseif($people != 'all'){
					
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');

					
                }else{

                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }
				
				$absensi->appends(['data' => 'absensi']);
				$absensi->appends(['task_people' => $task_people->currentPage()]);
				$absensi->appends($request->only('people_id_tasklist'));
				$absensi->appends($request->only('project_id'));
				
				$task_people->appends(['data' => 'task_people']);
				$task_people->appends(['absensi' => $absensi->currentPage()]);
				
				$task_people->appends($request->only('people_id_tasklist'));
				$task_people->appends($request->only('project_id'));
				$task_people->appends($request->only('dateFromTasksList'));
				$task_people->appends($request->only('dateToTasksList'));
				
				return view('TaskList.view_task_project', compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList' ,'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData'));
               
			}
		}
	
    // Ajax get Outlet from city
    public function getOutlet(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Outlet -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('outlet')->select('id', 'outlet_name')->where('outlet_city',$request->data_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->outlet_name.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
	
	//ajax get team from city
	public function getTeam(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Team -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('project_team')->select('id', 'name_team')->where('city_id',$request->data_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->name_team.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
	
	public function getMember(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $village = DB::table('view_project_member')->select('employee_id', 'first_name','middle_name','surname')->where('team_id',$request->data_id)->get();
            return json_encode($village);
        }
    }
	
	//ajax get member from team
	public function getMember2(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Team -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('project_member')->select('id', 'firstname')->where('team_id',$request->data_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->firstname.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }

    public function postInsertGroupTaskList(Request $request)
    {
        $project_id = $request->project_id;
        $outlet_id = $request->outlet_id;
        $people_id = $request->people_id;
         
        $tim = $request->team;
        if($people_id == null ){
          
            $data_member = DB::table('view_project_member')->where('team_id',$tim)->where('status_user',1)->get();
            foreach($data_member as $mem){
                $count = DB::table('task_people')->where('project_id', $project_id)->get();
                $next_sequence = (int)$count->count() + 1;
                $user = Auth::user();
                $task = array();
                $task['people_id']      = $mem->employee_id;
                $task['project_id']     = $project_id;
                $task['outlet_id']      = $outlet_id;
                $task['schedule_date']  = date('Y-m-d', strtotime($request->schedule_date));
                $task['task_start']     = date('Y-m-d', strtotime($request->task_start));
                $task['task_end']       = date('Y-m-d', strtotime($request->task_end));
                $task['description']    = $request->desc;
                $task['task_type']      = $request->task_type;
                $task['task_number']    = "T".date('Ymd').$next_sequence;
                $task['active_flag']    = '1';
                $task['status']         = '1';
                $task['status_flag']    = '0';
                $task['delete_flag']    = '1';
                $task['created_by']     = $user->name;
                $task['updated_by']     = 'system group all';

                TaskPeople::create($task);
            }

        }else{
          
          $count = DB::table('task_people')->where('project_id', $project_id)->get();
          $next_sequence = (int)$count->count() + 1;
          $user = Auth::user();
          for ($i=0; $i < count($people_id); $i++) { 
              $task = array();
              $task['people_id']      = $people_id[$i];
              $task['project_id']     = $project_id;
              $task['outlet_id']      = $outlet_id;
              $task['schedule_date']  = date('Y-m-d', strtotime($request->schedule_date));
              $task['task_start']     = date('Y-m-d', strtotime($request->task_start));
              $task['task_end']       = date('Y-m-d', strtotime($request->task_end));
              $task['description']    = $request->desc;
              $task['task_type']      = $request->task_type;
              $task['task_number']    = "T".date('Ymd').$next_sequence;
              $task['active_flag']    = '1';
              $task['status']         = '1';
              $task['status_flag']    = '0';
              $task['delete_flag']    = '1';
              $task['created_by']     = $user->name;
              $task['updated_by']     = 'system group';

              TaskPeople::create($task);

           }
       }
        return redirect()->back()->with('message','Success !!!');

  }
 
    
}
