           
var myVar;

var date = new Date();
date.setDate(date.getDate());


// $(document).ready(function(){
//     for(var i = 0; i < 20; i++) {
//         $('#detail'+i).click(function(){
//             var an = $('#aktivitas_name'+i).text();
//             $('.aktivitas').val(an);
//         }); 
//     }   
// });

$(document).ready(function(){
    $("#datepicker").datepicker({ 
        autoclose: true, 
        startDate: date, 
        todayHighlight: true
    }).datepicker('update'); 

    $("#b-date").datepicker({ 
        autoclose: true,
        startDate: date, 
        todayHighlight: true
    }).datepicker('update');

    $("#d-filter").datepicker({ 
        autoclose: true
    }).datepicker('update');
	 $("#d-filter2").datepicker({ 
        autoclose: true
    }).datepicker('update');
});

function loader() {
    myVar = setTimeout(showPage, 0);
    
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  $('main').removeClass('bg-white'); 
}
 
$( document ).ready(function() {
    

    $('#home-tab').click(function(){
        $('#mapBox').removeClass('d-none');
        $('#mapBox2').addClass('d-none');
    });

    $('#profile-tab').click(function(){
        $('#mapBox2').removeClass('d-none');
        $('#mapBox').addClass('d-none');
    });

    $('.btn-show').click(function(){
        $(this).addClass('btn-primary');
        $('.btn-hide').removeClass('btn-primary');
        $('.hiding').removeClass('d-none');
    });

    $('.btn-hide').click(function(){
        $(this).addClass('btn-primary');
        $('.btn-show').removeClass('btn-primary');
        $('.hiding').addClass('d-none');
    });

    $(".alert").fadeTo(1000, 500).slideUp(500, function(){
        $(this).slideUp(500);
    });

    $('#input[name="spesifik"]').click(function(){
        var $radio = $(this);
        
        // if this was previously checked
        if ($radio.data('waschecked') == true)
        {
            $radio.prop('checked', false);
            $radio.data('waschecked', false);
        }
        else
            $radio.data('waschecked', true);
        
        // remove was checked from other radios
        $radio.siblings('input[name="spesifik"]').data('waschecked', false);
    });

$('.del-button').click( function(){
    
    var href = $(this).attr('href');
    $('.del-confirm').attr("href", href);

    console.log(href);
})

$( "#distributor" ).change(function() {
    var str = "";
    $( "#distributor option:selected" ).each(function() {
      str += $( this ).text() + " ";
    });
    console.log("a"+str+"a");
    if (str == "-- ") {
        $('#distributor1').attr('disabled', true);
    } else{
        $('#distributor1').attr('disabled', false);        
    }
  }).trigger( "change" );

  $(".tes").change(function(){
      console.log($(this).val());
  })

  $( "#provinsi" ).change(function() {
    var str = "";
    $( "#provinsi option:selected" ).each(function() {
      str += $( this ).val() + "";
    });
    console.log("a"+str+"a");
    if (str == "1") {
        $('#kota-2').removeClass('d-block');
        $('#kota-3').removeClass('d-block');
        $('#kab_kota2').val('');
        $('#kab_kota3').val('');
        
        $('#kota-1').addClass('d-block');
        $('#kota-0').addClass('d-none');
    } else if (str == "2"){
        $('#kota-1').removeClass('d-block');
        $('#kota-3').removeClass('d-block');
        $('#kab_kota1').val('');
        $('#kab_kota3').val('');
        
        $('#kota-2').addClass('d-block');
        $('#kota-0').addClass('d-none');                
    } else if (str == "3"){
        $('#kota-1').removeClass('d-block');
        $('#kota-2').removeClass('d-block');
        $('#kab_kota2').val('');
        $('#kab_kota1').val('');
        
        $('#kota-3').addClass('d-block');
        $('#kota-0').addClass('d-none');                
    } else{
        $('#kota-1').removeClass('d-block');
        $('#kota-2').removeClass('d-block');
        $('#kota-3').removeClass('d-block');
        $('#kota-0').removeClass('d-none');
    }
  }).trigger( "change" );



// var cN = $('.count-notif').html();

// if (cN == 0) {
//     $('.count-notif').addClass('d-none');
// } else {
//     $('.count-notif').removeClass('d-none');
//     $('.count-notif').addClass('d-block');        
// }

$('.list-notif').hover(function(){
    $(this).addClass('aktif-list');
}, function(){
    $(this).removeClass('aktif-list');    
});

$('[data-toggle="tooltip"]').tooltip();

$('#tab').DataTable( {
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});

$('#tab1').DataTable( {
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});

$('#tab2').DataTable( {
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});

$('#tab3').DataTable( {
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});

$('#tab4').DataTable( {
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});

$('#tab-n').DataTable( {
    "columnDefs": [
        { "orderable": false, "targets": 1 }
      ],
    "lengthMenu": [ 5, 10, 25, 50, 75, 100 ],
    "bPaginate": true,
    paging: true,
    responsive: true,
    stateSave: true
});


// var ctx = document.getElementById("chart-1");
// var chart1 = new Chart(ctx, {
//     type: 'line',
//     data: {
//         labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agt", "Sep", "Okt", "Nov", "Des"],
//         datasets: [{
//             label: '# Penjualan 2018',
//             data: [12, 19, 3, 5, 2, 3, 7],
//             backgroundColor: [
//                 'rgba(255, 99, 132, 0.2)'
//             ],
//             borderColor: [
//                 'rgba(255,99,132,1)'
//             ],
//             borderWidth: 1
//         }]
//     },
//     options: {
//         responsive: true,
//         maintainAspectRatio: false,
//         scaleShowVerticalLines: false,
//         scales: {
//             xAxes: [{
//               gridLines: {
//                 drawOnChartArea: false
//               }
//             }]
//           }
//     }
// });

var ctx1 = document.getElementById("chart1").getContext("2d");
    var data1 = {
        labels: ["January", "February", "March", "April", "May", "June", "July","August","September","October","November","Desember"],
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(34,186,160,0.2)",
                strokeColor: "rgba(34,186,160,1)",
                pointColor: "rgba(34,186,160,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(18,175,203,1)",
                data: [28, 48, 40, 19, 86, 27, 90]
            }
        ]
    };

      var ctx2 = document.getElementById("chart1").getContext("2d");
    var data2 = {
        labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"],
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(34,186,160,0.2)",
                strokeColor: "rgba(34,186,160,1)",
                pointColor: "rgba(34,186,160,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(18,175,203,1)",
                data: [18, 48, 30, 19, 65, 45, 70]
            }
        ]
    };
     var ctx3 = document.getElementById("chart1").getContext("2d");
    var data3 = {
        labels: ["2018", "2019", "2020", "2021"],
        datasets: [
            {
                label: "My Second dataset",
                fillColor: "rgba(34,186,160,0.2)",
                strokeColor: "rgba(34,186,160,1)",
                pointColor: "rgba(34,186,160,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(18,175,203,1)",
                data: [800, 500, 200, 400]
            }
        ]
    };
     var chart1 = new Chart(ctx2,{
        type: "line",
        data: data2,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        responsive: true,
        maintainAspectRatio: false,
        scaleShowVerticalLines: false,
        scales: {
            xAxes: [{
              gridLines: {
                  display: false
              }
            }]
          }
    });  

$('#category_faq').change(function(){
    var val = $(this).val();
    if(val==1)
    {
        chart1.destroy();
        chart1 = new Chart(ctx2, {
        type: "line",
        data: data2,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        responsive: true,
        responsive: true,
        maintainAspectRatio: false,
        scaleShowVerticalLines: false,
        scales: {
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
    });  
    }
    else if(val==2)
    {
        chart1.destroy();
        chart1 = new Chart(ctx1, {
        type: "line",
        data: data1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        responsive: true,
        maintainAspectRatio: false,
        scaleShowVerticalLines: false,
        scales: {
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
    });
    }
    else if(val==3)
    {
        chart1.destroy();
        chart1 = new Chart(ctx3, {
        type: "line",
        data: data3,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 2,
        datasetFill : true,
        responsive: true,
        maintainAspectRatio: false,
        scaleShowVerticalLines: false,
        scales: {
            xAxes: [{
              gridLines: {
                display: false
              }
            }]
          }
    });
    }
})

});