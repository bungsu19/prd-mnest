<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('testing', 'Api\TestingController@test');
Route::get('test','Api\LoginController@test');
//app
Route::get('timeszone','Api\TaskListApiController@getTimesZone');
Route::get('app-android','Api\TaskListApiController@getAppAndroid');
//user
Route::post('login','Api\LoginController@getLogin');
//task
Route::post('task-list','Api\TaskListApiController@getTasklist');
Route::post('absensi-in','Api\TaskListApiController@getAbsensiIn');
Route::post('absensi-out','Api\TaskListApiController@getAbsensiOut');
Route::post('absensi-in-outlet','Api\TaskListApiController@getAbsensiOutletIn');
Route::post('absensi-out-outlet','Api\TaskListApiController@getAbsensiOutletOut');
Route::post('task-log','Api\TaskListApiController@postUpdateTask');
Route::post('task-open','Api\TaskListApiController@postTaskOpen');
Route::post('task-open-update','Api\TaskListApiController@postUpdateTaskOpen');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
