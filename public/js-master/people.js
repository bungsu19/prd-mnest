 function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#profile-img-tag').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#profile-img").change(function(){
    readURL(this);
});

var $statusfilter = 0;
$(document).ready(function(){
    $('.filterbtn').click(function(){
    if($statusfilter == 0){
        $('#formfilter').show(999);
        $statusfilter = 1;
    } else {
        $('#formfilter').hide(999);
        $statusfilter = 0;
    }
     });
});


 $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

    $('#province2').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency2 select').html(obj.data);
                        $('.regency2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency2 select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict2 select').html(obj.data);
                        $('.subdistrict2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict2 select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village2 select').html(obj.data);
                        $('.village2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village2 select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
        autocomplete(document.getElementById("myInput"), countries);
    
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });