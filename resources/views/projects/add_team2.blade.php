@extends('layout-menu.app')

@section('content')

   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header">
                            <font size="4" color="black"><b>Team Project -> {{ $city->city_id }}</b></font>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="text-align: left;">
                                            <label> Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <i>{{ $project->project_name }}</i></label><br>
                                            <label> Project Description : <i>{{ $project->project_desc }}</i></label>
                                        
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">New Team</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow"> Team Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> Description</font> </td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Vacancy</font></td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach($team as $tim)
                                                        <tr>
                                                            <td align="center">{{ $no }}</td>
                                                            <td>{{ $tim->name_team }}</td>
                                                            <td>{{ $tim->description }}</td>
                                                            <td align="center"> <a href="{{url('/all-project/team-criteria')}}/{{ $tim->id }}" class="btn bg-blue waves-effect m-r-20">+ Vacancy</a></td>
                                                            <td align="center">
                                                                 <a href="#" data-toggle="tooltip" title="Edit Data">
                                                                <i class="material-icons">mode_edit</i>
                                                            </a>
                                                            <a onclick="return confirm('Delete this Data ?')" href="{{url('/all-project/delete-team')}}/{{ $tim->id }}" data-toggle="tooltip" title="Delete Data">
                                                                <i class="material-icons">delete_forever</i>
                                                            </a>
                                                            </td>
                                                        </tr>
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="{{url('/all-project/edit-structure')}}/{{ $project->id }}"  class="btn btn-block bg-blue waves-effect" ><font size="3">BACK</font></a>
                                               <!-- <button type="button" class="btn btn-block bg-blue waves-effect" onclick="javascript:history.back()"><font size="3">BACK</font></button>
                                            </div> -->
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>

     <form class="form-signin" method="POST" action="{{url('/all-project/get-team')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Team</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Team Name</label>
                                    <input type="text" name="name_team" class="form-control" placeholder="Name Team" required>
                                <br>
                             </div>
                             <br>
                             <div class="col-sm-12">
                                <br>
                                <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="Description Job Title"></textarea>
                                <input type="hidden" name="project_id" value="{{ $project->id }}">
                                <input type="hidden" name="structure_id" value="{{ $structure->structure_id }}">
                                <input type="hidden" name="city_id" value="{{ $city->id }}">
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

    

</section>
    

@endsection
