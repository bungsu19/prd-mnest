@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
		
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif	
		
             <div class="card">
                 <div class="header">
                            <font size="4" color="black"><b>Team Project  -> {{ $city->city_id }}</b></font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="text-align: left;">
                                            <label> Project Name q &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <i>{{ $project->project_name }}</i></label><br>
                                            <label> Project Description : <i>{{ $project->project_desc }}</i></label>
                                        
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">New Team</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow"> Team Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> Description</font> </td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Vacancy</font></td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach($team as $tim)
                                                        <tr>
                                                            <td align="center">{{ $no }}</td>
                                                            <td>{{ $tim->name_team }}</td>
                                                            <td>{{ $tim->description }}</td>
                                                             <td align="center"> <a href="{{url('/all-project/edit-team-criteria')}}/{{ $tim->id }}" class="btn bg-blue waves-effect m-r-20">+ Vacancy</a></td>
                                                            <td align="center">
															
															<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete name team '{{ $tim->name_team }}' ?" data-href="{{url('/all-project/delete-team')}}/{{ $tim->id }}" title="Delete Data">
																<i class="material-icons">delete_forever</i>
															</a>
															
                                                            </td>
                                                        </tr>
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                
                                            </div>
                                            <div class="col-sm-6">
                                               <a href="{{url('/all-project/edit-structure')}}/{{ $project->id }}"  class="btn btn-block bg-blue waves-effect" ><font size="3">BACK</font></a>
                                              <!--  <button type="button" class="btn btn-block bg-blue waves-effect" onclick="javascript:history.back()"><font size="3">BACK</font></button> -->
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>

     <form class="form-signin" method="POST" action="{{url('/all-project/get-team')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Team</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Team Name <span class="mandatory">*</span></label>
                                    <input type="text" name="name_team" id="name_team" class="form-control" placeholder="Name Team" maxlength="30" >
                                    <span class="text-error" id="txt_name_team"></span>
                             </div>
                                 
                             <br>
                             <div class="col-sm-12">
                                <br>
                                <label>Description <span class="mandatory">*</span></label>
                                <textarea name="description" id="description" maxlength="150" class="form-control" placeholder="Description Job Title" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;"></textarea>
                                  <span class="text-error" id="txt_description"></span>
                                <input type="hidden" name="project_id" value="{{ $project->id }}">
                                <input type="hidden" name="structure_id" value="{{ $structure->structure_id }}">
                                <input type="hidden" name="city_id" value="{{ $city->id }}">
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim"> Save</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="send"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });

           $('#name_team').keypress(function (e) {
                    var startPos = e.currentTarget.selectionStart;
                    if (e.which === 32 && startPos==0){
                        e.preventDefault();
                    }else if($('#name_team').val().length > 0){
                        $('#name_team').val($('#name_team').val().replace(/\s{2,}/g,' '));
                    }
              });
          
              /*handle paste with extra space on description*/
              $(document).on('paste', '#name_team', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 200));
              });
              
                //description cannot long space on input
              $('#description').keypress(function (e) {
                    var startPos = e.currentTarget.selectionStart;
                    if (e.which === 32 && startPos==0){
                        e.preventDefault();
                    }else if($('#description').val().length > 0){
                        $('#description').val($('#description').val().replace(/\s{2,}/g,' '));
                    }
              });
          
              /*handle paste with extra space on description*/
              $(document).on('paste', '#description', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 200));
              });
              
              $("#send").click(function(){
                  
                      if ($('#name_team').val().length < 1) {
                            $('#txt_name_team').text('Name Team not is empty');
                      }else{
                            $('#txt_name_team').text('');
                      }
                      
                      if ($('#description').val().length < 1) {
                            $('#txt_description').text('Description not is empty');
                      }else{
                            $('#txt_description').text('');
                      }
                      
                      if($('#name_team').val().length > 0 &&
                         $('#description').val().length > 0){
                              $("#kirim").click();
                          }
                  
                });
				
				$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
        });

    </script>  
     

    

</section>
    

@endsection
