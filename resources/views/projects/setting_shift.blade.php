@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">

    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h1 align="left">Project Information</h1>
							               <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								                  <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							             <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                       
                                </div>
								<div class="row clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

     <div class="row clearfix">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                <div class="header">
                            <font size="5" color="black"><b>Outlet</b></font>
                             
							<div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             
                                             </div>
                                        </div>
                                    </div>
                </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover ">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow">Outlet Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> Address</font></td>
                                                        <td class="bg-column"><font color="yellow">Phone Number</font></td>
                                                        <td class="bg-column"><font color="yellow">City</font></td>
                                                        <td class="bg-column"><font color="yellow">Shift</font></td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                   <?php for($i = 0; $i < count($outlet); $i++) { ?>
                                                    <tr>
                                                        <td align="center"><?php echo $i + 1; ?></td>
                                                        <td><?php echo $outlet[$i]['outlet_name']; ?></td>
                                                        <td><?php echo $outlet[$i]['outlet_address1']; ?></td>
                                                        <td><?php echo $outlet[$i]['outlet_phone1']; ?></td>
                                                        <td><?php echo $outlet[$i]['outlet_city']; ?></td>
                                                        <td align="center"><?php if($outlet[$i]['jumlah'] > 0){ echo $outlet[$i]['jumlah']; } ?></td>  
                                                      <td align="center"><a class="btn bg-blue" href="{{url('/detail-setting-shift')}}/{{ $outlet[$i]['id'] }}/{{ $project->id }}">setting shift</a> </td>
                                                    </tr>
                                                   <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                       </div> 
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    
                                </div>
                                </form>
                        </div>
                    </div>


                    <!-- #END# form add prodct -->
            </div>
             

    

    


    

        
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
    	  
    			$("#send_add_structure_project").click(function(){
    				  if ($('#structure_id').val().length < 1) {
    						$('#txt_structure_id').text('Structure not selected');
    				  }else{
    						$('#txt_structure_id').text('');
    				  }
    				  
    				  if($('#structure_id').val().length > 0 ){
    						  $("#kirim_add_structure_project").click();
					  }
  			  
  			});
		  
      		   //start_age maximum length 2
      		  $('#start_age').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#start_age').val().length == 2)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on start_age*/
      			  $(document).on('paste', '#start_age', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 2));
      		   });
      		   
      		   //end_age maximum length 2
      		  $('#end_age').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#end_age').val().length == 2)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on end_age*/
      			  $(document).on('paste', '#end_age', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 2));
      		   });
      		   
      		    //qty maximum length 2
      		  $('#qty').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#qty').val().length == 3)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on qty*/
      			  $(document).on('paste', '#qty', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 3));
      		   });
      		   
      		    //description cannot long space on input
      		  $('#description').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if (e.which === 32 && startPos==0){
      					e.preventDefault();
      				}else if($('#description').val().length > 0){
      					$('#description').val($('#description').val().replace(/\s{2,}/g,' '));
      				}
      		  });
      	  
      	      /*handle paste with extra space on description*/
      		  $(document).on('paste', '#description', function(e) {
      			  e.preventDefault();
      			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      			  var text = $(this).val()+withoutSpaces;
      			  $(this).val(text.substring(0, 200));
      		  });
      		  
      		  $("#send_add_vacancy").click(function(){
      			  
      				  if ($('#job_title').val().length < 1) {
      						$('#txt_job_title').text('Job title not selected');
      				  }else{
      						$('#txt_job_title').text('');
      				  }
      				  
      				  if ($('#gender').val().length < 1) {
      						$('#txt_gender').text('Gender not selected');
      				  }else{
      						$('#txt_gender').text('');
      				  }
      				  
      				  if ($('#last_education').val().length < 1) {
      						$('#txt_last_education').text('Last education not selected');
      				  }else{
      						$('#txt_last_education').text('');
      				  }
      				  
      				  if ($('#start_age').val().length < 1) {
      						$('#txt_start_age').text('Start age is empty');
      				  }else if ($('#start_age').val() == 0) {
      						$('#txt_start_age').text('Start age not entered');
      				  }else{
      						$('#txt_start_age').text('');
      				  }
      				  
      				  if ($('#end_age').val().length < 1) {
      						$('#txt_end_age').text('End age is empty');
      				  }else if ($('#end_age').val() == 0) {
      						$('#txt_end_age').text('End age not entered');
      				  }else{
      						$('#txt_end_age').text('');
      				  }
      				  
      				  if ($('#qty').val().length < 1) {
      						$('#txt_qty').text('Qty is empty');
      				  }else if ($('#qty').val() == 0) {
      						$('#txt_qty').text('Qty not entered');
      				  }else{
      						$('#txt_qty').text('');
      				  }
      				  
      				  if ($('#description').val().length < 1) {
      						$('#txt_description').text('Description is empty');
      				  }else{
      						$('#txt_description').text('');
      				  }
      				  
      				  if($('#job_title').val().length > 0 &&
      				     $('#gender').val().length > 0 &&
      					 $('#last_education').val().length > 0 &&
      					 ($('#start_age').val().length > 0 && $('#start_age').val() != '0') &&
      					 ($('#end_age').val().length > 0 && $('#end_age').val() != '0') &&
      					 ($('#qty').val().length > 0 && $('#qty').val() != '0') &&
      					 $('#description').val().length > 0){
      						  $("#kirim_add_vacancy").click();
      					  }
      			  
      			});
      		   
      	  
			$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
		  
          }); 
    </script>  

    

</section>
    

@endsection
