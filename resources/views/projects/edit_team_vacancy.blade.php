@extends('layout-menu.app')

@section('content')

   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header">
                            <font size="4" color="black"><b>Vacancy Team -></b> <small><i>{{ $team->name_team }}</i></small></font>
                            
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="text-align: left;">
                                            <label> Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <i>{{ $project->project_name }}</i></label><br>
                                            <label> Project Description : <i>{{ $project->project_desc }}</i></label>
                                        
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">New Vacancy</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                      <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow">Job Title Name</font></td>
                                                        <td class="bg-column"><font color="yellow">Qty People</font> </td>

                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach($team_criteria as $crite)
                                                        <tr>
                                                            <td align="center">{{ $no }}</td>
                                                            <td>{{ $crite->job_title_name }}</td>
                                                            <td>{{ $crite->qty_people }}</td>
                                                            <td align="center">
                                                            <a href="#" data-toggle="tooltip" title="Edit Data">
                                                                <i class="material-icons">mode_edit</i>
                                                            </a>
                                                            <a onclick="return confirm('Delete this Data ?')" href="#" data-toggle="tooltip" title="Delete Data">
                                                                <i class="material-icons">delete_forever</i>
                                                            </a>
                                                        </td>
                                                        </tr>
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                
                                            </div>
                                            <div class="col-sm-6">
                                               <a onclick="javascript:history.back()" class="btn bg-blue btn-block bg-gray waves-effect" ><font size="4"> Back</font></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>

     <form class="form-signin" method="POST" action="{{url('/all-project/team-criteria')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Vacancy Team</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                             <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <label>Job Name Title</label>
                                           <select name="job_name_title" class="form-control">
                                            <option>-select-</option>
                                            @foreach($vacancy as $vac)
                                                <option>{{ $vac->job_name }}</option>
                                            @endforeach
                                               
                                           </select>
                                        <br>
                                     </div>
                                     <br>
                                 </div>
                                 <br>
                              <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <label>Qty People</label>
                                           <input type="number" name="qty" class="form-control" placeholder="Qty people">
                                        <br>
                                     </div>
                                     <br>
                                 </div>
                                 <br>
                              
                             <input type="hidden" name="project_id" value="{{ $project->id }}">
                            <input type="hidden" name="structure_id" value="{{ $structure->structure_id }}">
                            <input type="hidden" name="city_id" value="{{ $city->id }}">
                             <input type="hidden" name="team_id" value="{{ $team->id }}">
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

    

</section>
    

@endsection
