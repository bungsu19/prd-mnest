@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
    
    .mandatory
    {
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -25px;">
		
			@if(session()->has('failed'))
            <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('failed') }}
            </div>
            @endif

             @if(session()->has('message'))
            <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('message') }}
            </div>
            @endif
		
             <div class="card">
                 <div class="header">
                            <font size="4" color="black"><b>Vacancy Team -></b> <small><i>{{ $team->name_team }}</i></small></font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="text-align: left;">
                                            <label> Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <i>{{ $project->project_name }}</i></label><br>
                                            <label> Project Description : <i>{{ $project->project_desc }}</i></label>
                                        
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">New Vacancy</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover ">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow">Job Title Name</font></td>
                                                        <td class="bg-column"><font color="yellow">Qty People</font> </td>

                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1; ?>
                                                    @foreach($team_criteria as $crite)
                                                        <tr>
                                                            <td align="center">{{ $no }}</td>
                                                            <td>{{ $crite->job_title_name }} </td>
                                                            <td>{{ $crite->qty_people }}</td>
                                                           <td align="center">
														   
														    <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete jo vacancy '{{ $crite->job_title_name }}' ?" data-href="{{url('/post-delete-team-vacancy')}}/{{ $crite->id }}" title="Delete Data">
																<i class="material-icons">delete_forever</i>
															</a>
                                                        </td>
                                                        </tr>
                                                        <?php $no++; ?>
                                                    @endforeach
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="{{url('/all-project/edit-team')}}/{{ $team->city_id }}/{{ $project->id }}"  class="btn btn-block bg-blue waves-effect" ><font size="3">BACK</font></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>

     <form class="form-signin" method="POST" action="{{url('/all-project/team-criteria')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Vacancy Team</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                             <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <label>Job Name Title</label>
                                           <select name="job_name_title" id="job_name_title" class="form-control">
                                            <option value="">-select-</option>
                                            @foreach($vacancy as $vac)
                                                <option value="{{ $vac->job_name }}">{{ $vac->job_name }} ({{ $vac->qty_people }})</option>
                                            @endforeach
                                               
                                           </select>
                                           <span class="text-error" id="txt_job_name_title"></span>
                                        <br>
                                     </div>
                                     <br>
                                 </div>
                                 <br>
                              <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <label>Qty People</label>
                                           
                                            <input type="text" name="qty" class="form-control" placeholder="Qty " id="qty" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="5">
                                           <span class="text-error" id="txt_qty"></span>
                                        <br>
                                     </div>
                                     <br>
                                 </div>
                                 <br>
                              
                             <input type="hidden" name="project_id" value="{{ $project->id }}">
                            <input type="hidden" name="structure_id" value="{{ $structure->structure_id }}">
                            <input type="hidden" name="city_id" value="{{ $city->id }}">
                             <input type="hidden" name="team_id" value="{{ $team->id }}">
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim"> Save</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="send"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });

          
              
                //description cannot long space on input
              $('#qty').keypress(function (e) {
                    var startPos = e.currentTarget.selectionStart;
                    if (e.which === 32 && startPos==0){
                        e.preventDefault();
                    }else if($('#qty').val().length > 0){
                        $('#qty').val($('#qty').val().replace(/\s{2,}/g,' '));
                    }
              });
          
              /*handle paste with extra space on description*/
              $(document).on('paste', '#qty', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 200));
              });
              
              $("#send").click(function(){
                  
                      if ($('#job_name_title').val().length < 1) {
                            $('#txt_job_name_title').text('Job Name Title not selected');
                      }else{
                            $('#txt_job_name_title').text('');
                      }
                      
                      if ($('#qty').val().length < 1) {
                            $('#txt_qty').text('Qty is empty');
                      }else{
                            $('#txt_qty').text('');
                      }
                      
                      if($('#job_name_title').val().length > 0 &&
                         $('#qty').val().length > 0){
                              $("#kirim").click();
                          }
                  
                });
				
				$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
        });

    </script>   

    

</section>
    

@endsection
