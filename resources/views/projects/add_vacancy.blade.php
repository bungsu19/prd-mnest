@extends('layout-menu.app')

@section('content')

   
<section class="content">
    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                Project Information
                            </h1>
                            <label>Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_name }} </i></small> <br>
                            <label>Project Description &nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_desc }} </i></small><br>

                            <label>Contract Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_contract_no }} </i></small>  <br>

                            <label>Periode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;({{ $project->project_start }}) To ({{ $project->project_end }}) </i></small> <br>
                            <label>Project Value &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;<b>Rp.</b><i>{{ number_format($project->project_value,0) }}</i> </i></small>  
                            
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header">
                            <font size="5" color="black"><b>Job Vacancy Qualifications</b></font>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">Add Vacancy</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                       <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                <thead>
                                                   <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow"> Job Title Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> Gender</font></td>
                                                        <td class="bg-column"><font color="yellow">Last Education</font></td>
                                                        <td class="bg-column"><font color="yellow">Age</font></td>
                                                        <td class="bg-column"><font color="yellow">Qty People</font></td>
                                                        <td class="bg-column"><font color="yellow"> Description</font> </td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                  <tbody>
                                                    <?php $no = 1 ?>
                                                    @foreach($vacancy as $vac)
                                                    <tr>
                                                        <td align="center">{{ $no }}</td>
                                                        <td>{{ $vac->job_name }}</td>
                                                         <td>{{ $vac->gender }}</td>
                                                          <td>{{ $vac->last_education }}</td>
                                                           <td>{{ $vac->start_age }} - {{ $vac->end_age }}</td>
                                                            <td>{{ $vac->qty_people }}</td>
                                                        <td>{{ $vac->description }}</td>
                                                        <td align="center">
                                                            <a href="#" data-toggle="tooltip" title="Edit Data">
                                                                <i class="material-icons">mode_edit</i>
                                                            </a>
                                                            <a onclick="return confirm('Delete this Data ?')" href="#" data-toggle="tooltip" title="Delete Data">
                                                                <i class="material-icons">delete_forever</i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="#" class="btn btn-block bg-gray waves-effect" ><font size="4"> Cancel</font></a>
                                            </div>
                                            <div class="col-sm-3">
                                                <a href="{{url('/add-structure')}}/{{ $project->id }}"  class="btn btn-block bg-blue waves-effect" ><font size="4">Next</font></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>

    <form class="form-signin" method="POST" action="{{url('/add-vacancy')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Add Job Vacancy</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Job Title Name</label>
                                    <select name="job_title" class="form-control">
                                        <option value=" ">-select-</option>
                                        @foreach($job as $jb)
                                            <option value="{{ $jb->id }}">{{ $jb->job_name }}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Gender</label>
                                    <select name="gender" class="form-control">
                                        <option>-select-</option>
                                        <option>All</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Last Education</label>
                                    <select name="last_education" class="form-control">
                                        <option>-select-</option>
                                        <option>SD</option>
                                        <option>SMP</option>
                                        <option>SMA/SMK</option>
                                        <option>D1</option>
                                        <option>D2</option>
                                        <option>D3</option>
                                        <option>D4</option>
                                        <option>S1</option>
                                        <option>S2</option>
                                        <option>S3</option>
                                    </select>
                                    <br>
                                 </div>
                             </div>

                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Start Age</label>
                                    <input type="number" name="start_age" class="form-control" placeholder="Age Start">
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Start Age</label>
                                    <input type="number" name="end_age" class="form-control" placeholder="Age End">
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Qty</label>
                                    <input type="number" name="qty" class="form-control" placeholder="Qty People">
                                    <br>
                                 </div>
                             </div>

                             <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <br>
                                        <label>Description</label>
                                        <textarea name="description" class="form-control" placeholder="Description Job Title"></textarea>
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                        <br>
                                        <br>
                                         
                                    </div>
                                </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

    

</section>
    

@endsection
