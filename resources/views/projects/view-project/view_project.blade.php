@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
    
    .mandatory
    {
            font-size: 15px;
            color: red;
    }
</style>
   
			<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				@if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif

				@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
				@endif
                    <div class="card">
                        <div class="header">
                            <h1 align="left">Project Information</h1>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
                                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
                                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
                                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
                                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
                                <div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                            <div class="row clearfix"></div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Member Project</a>
                                        </li>
                                        <li role="presentation" >

                                            <a href="#job_vacancy" data-toggle="tab" class="btn bg-blue">JoB Vacanccy</a>
                                        </li>
                                        
                                        <li role="presentation">
                                            <a href="#structure_project" class="btn bg-blue" data-toggle="tab">Structure Project</a>
                                           
                                        </li>
                                        <li role="presentation">
                                                 <a href="#messages_animation_2" class="btn bg-blue" data-toggle="tab">Team Project</a>
                                        </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="job_vacancy">
                                         <div class="table-responsive">
                                                <table class="table table-hover dashboard-task-infos">
                                                    <thead>
                                                        <tr>
                                                            <td class="bg-column"><font color="yellow"> No</font></td>
                                                            <td class="bg-column"><font color="yellow">Job Title Name</font></td>
                                                            <td class="bg-column"><font color="yellow">Gender</font></td>
                                                            <td class="bg-column"><font color="yellow">Last Education</font></td>
                                                            <td class="bg-column"><font color="yellow">Age</font></td>
                                                            <td class="bg-column"><font color="yellow">Qty People</font></td>
                                                            <td class="bg-column"><font color="yellow">Description</font></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach($vacancy as $vac)
                                                        <tr>
                                                            <td>{{ $no }}</td>
                                                            <td>{{ $vac->job_name }}</td>
                                                            <td>{{ $vac->gender }}</td>
                                                            <td>{{ $vac->last_education }}</td>
                                                            <td>{{ $vac->start_age }}  To  {{ $vac->end_age }}</td>
                                                            <td>{{ $vac->qty_people }}</td>
                                                            <td>{{ $vac->description }}</td>
                                                           
                                                        </tr>
                                                        <?php $no++; ?>
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="structure_project">
                                            <div class="table-responsive">
                                            <table class="table table-hover dashboard-task-infos">
                                                <thead>
                                                            <tr>
                                                                <td class="bg-column" align="center" width="20"><font color="yellow">No</font></td>
                                                                <td class="bg-column"><font color="yellow">Area Name</font></td>
                                                                <td class="bg-column"><font color="yellow">District/city</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $no = 1 ?>

                                                            @foreach($dt_structure as $det)
                                                            <?php $count = explode("|", $det->area_city); ?>
                                                            <?php $team = explode("|", $det->id_team); ?>
                                                            <tr>
                                                                <td rowspan="{{ count($count)+1 }}" align="center">{{ $no }}</td>
                                                                <td rowspan="{{ count($count)+1 }}">{{ $det->name_area }}</td>
                                                            </tr>
                                                                       
                                                                        @for ($i = 0; $i < count($count ); $i++)
                                                                        <tr >
                                                                             <!-- <td style="border: none;"><a href="{{url('all-project/get-team')}}/{{ $team[$i] }}/{{ $project->id }}"> {{ $count[$i] }}</a></td> -->
                                                                             <td><a href="{{url('/all-project/edit-team')}}/{{ $team[$i] }}/{{ $project->id }}"> {{ $count[$i] }}</a></td>
                                                                        </tr>
                                                                            @endfor
                                                            </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                     <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">City</font></td>
                                                    <td class="bg-column"><font color="yellow">Name Team</font></td>
                                                    <td class="bg-column"><font color="yellow">Job Vacancy</font></td><!-- 
                                                    <td class="bg-column"><font color="yellow">Qty People</font></td> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 1; ?>
                                                @foreach($dt_team as $tim)
                                                 <?php $count = explode("|", $tim->job_title_name); ?>
                                                 <?php $qty = explode("|", $tim->qty_people); ?>

                                                <tr>
                                                    <td rowspan="{{ count($count)+1 }}" align="center">{{ $no }}</td>
                                                    <td rowspan="{{ count($count)+1 }}">{{ $tim->city_name }}</td>
                                                    <td rowspan="{{ count($count)+1 }}">{{ $tim->name_team }}</td>
                                                    @for ($i = 0; $i < count($count ); $i++)
                                                    <tr >
                                                         <td> {{ $count[$i] }} </td>
                                                    </tr>
                                                     @endfor

                                                    
                                                   
                                                </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>

                         
                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                
                                    <div class="header">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                                 <div class="row clearfix">
                                                    <form class="form-signin" method="GET" action="{{url('/all-project/filter-view-project-member')}}" enctype="multipart/form-data" autocomplete="off">
                                                    {{ csrf_field() }}
                                                    <div class="col-sm-3">
                                                        <select name="search" class="form-control show-tick">
                                                                <option value="2">All</option>
                                                                <option value="0">Name</option>
                                                                <option value="1">Job Vacancy</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-5">
                                                        <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                        <input type="hidden" name="id" class="form-control" value="{{ $project->id }}">
                                                    </div> 
                                                    <div class="col-sm-2">
                                                        <button type="submit" class="btn bg-blue waves-effect m-r-20">Search</button>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="row clearfix"></div>
                                        </div>
                                
                                   <div class="table-responsive" style="margin-top: -20px;">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">Name People</font></td>
                                                    <td class="bg-column"><font color="yellow">Name Team</font></td>
                                                    <td class="bg-column"><font color="yellow">Gender</font></td>
                                                    <td class="bg-column"><font color="yellow">District</font></td>
                                                    <td class="bg-column"><font color="yellow">Job</font></td>
                                                    <td class="bg-column"><font color="yellow">Status</font></td>
                                                    <td class="bg-column"><font color="yellow">Reason</font></td>
                                                    <td class="bg-column"><font color="yellow">Allow Task Open</font></td>
                                                    <td class="bg-column"><font color="yellow">Action</font></td>
                                                    <td class="bg-column"><font color="yellow">Set Team</font></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $no = 0;
                                                    if($member->currentPage() == 1){
                                                        $no = 1;
                                                    }else{
                                                      $no = ($member->currentPage()-1) * $jumlahData + 1;
                                                    }
                                                ?>
                                                @foreach($member as $mem)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td><a href="{{url('data-people/view-data/')}}/{{ $mem->employee_id }}">{{ $mem->first_name }} {{ $mem->middle_name }} {{ $mem->surname }} </a></td>
                                                        <td>{{ $mem->name_team }}</td>
                                                        <td>{{ $mem->gender }}</td>
                                                        <td>{{ $mem->district }}</td>
                                                        <td>{{ $mem->job_name }}</td>
                                                        <td>
                                                           
                                                            @if($mem->status == 1)
                                                                <span class="label label-success"> Active</span>
                                                            @elseif($mem->status == 2)
                                                                <span class="label label-warning">Resign</span>
                                                            @elseif($mem->status == 3)
                                                               <span class="label label-danger">Fired</span>
                                                            @else
                                                                <span class="label label-danger">Project Done</span>
                                                            @endif
                                                        </td>
                                                        <td>{{ $mem->reason }}</td>
                                                        <td>
                                                            @if($mem->status == 3)

                                                            @elseif($mem->status == 2)

                                                            @elseif($mem->status == 1)
                                                                @if($mem->status_user == 1)
                                                                <form class="form-signin" method="POST" action="{{url('/access-task-open')}}" enctype="multipart/form-data">
                                                                      {{ csrf_field() }}
                                                                      <input type="hidden" name="people_id" value="{{ $mem->employee_id }}">
                                                                       <input type="hidden" name="stausUser" value="{{ $mem->status_user }}">
                                                                     <button type="submit" class="btn bg-blue" > Active</button>
                                                                </form>
                                                                @else
                                                                 <form class="form-signin" method="POST" action="{{url('/access-task-open')}}" enctype="multipart/form-data">
                                                                      {{ csrf_field() }}
                                                                     <input type="hidden" name="people_id" value="{{ $mem->employee_id }}">
                                                                       <input type="hidden" name="stausUser" value="{{ $mem->status_user }}">
                                                                     <button type="submit" class="btn bg-blue" > Non Active</button>
                                                                    </form>
                                                                @endif
                                                            @else

                                                            @endif
                                                            
                                                            
                                                       
                                                        </td>
                                                        <td>
                                                            @if($mem->status == 3)

                                                            @elseif($mem->status == 2)

                                                            @elseif($mem->status == 1)
                                                                 <a href="#" class="btn bg-blue" data-toggle="modal" data-target="#ModalInvite{{ $mem->id }}"> Action</a>
                                                            @else

                                                            @endif
                                                           
                                                        </td>
                                                        <td> @if($mem->interes_id == 17 )
                                                             <a href="{{url('/setting-team')}}/{{ $mem->employee_id }}/{{ $project->id }}" class="btn bg-blue">Setting</a>
                                                             @else
															 <a href="#" class="btn bg-blue" data-toggle="modal" data-target="#ModalOutTeam" data-people_name="{{ $mem->first_name }} {{ $mem->middle_name }} {{ $mem->surname }}" data-people_id="{{ $mem->employee_id }}" data-team_id="{{ $mem->team_id }}" data-name_team="{{ $mem->name_team }} ( {{ $mem->district }} )" data-project_id="{{ $project->id }}"> Change</a>
                                                             @endif
                                                        </td>

                                                    </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <h1><b></b></h1>
                                            </div>
                                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                {{ $member->links('paginator.default') }}
                                            </div>
                                    </div>
                                </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        @foreach($member as $aa)
        <form class="form-signin" method="POST" action="{{url('/project-member-update')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalInvite{{ $aa->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Update Status</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Action</label>
                                <select name="status" id="status" class="form-control">
                                    <option value="">-select-</option>
                                    <option value="2">Resign</option>
                                    <option value="3">Fired</option>
                                </select>
                                 <span class="text-error" id="txt_status"></span>
                                <br>
                             </div>
                             <div class="col-sm-12">
                                <label>Reason</label>
                               <textarea name="reason" id="reason" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" placeholder="Reason" maxlength="150"></textarea>
                                <span class="text-error" id="txt_reason"></span>
                                  
                                
                                <br>
                             </div>
                             <br>
                             <div class="col-sm-12">
                                <?php $todayDate = date("Y-m-d")?>
                               <input type="hidden" name="id" value="{{ $aa->id }}">
                               <input type="hidden" name="people_id" value="{{ $aa->employee_id }}">
                                <input type="hidden" name="pm_end" value="{{ $todayDate }}">
                                <br>
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <!--  <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim_add_vacancy"> Update</button> -->
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="send_add_vacancy"> Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
@endforeach

        <form class="form-signin" method="POST" action="{{url('/postdistrict/update/move-team')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalOutTeam" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Move Team</font></h2>
                        </div>
                       
                        <div class="modal-body">
							 <div class="col-sm-12">
                                <label>From</label>
								<input name="from" class="form-control" disabled>
                                <br>
                             </div>
							 <div class="col-sm-12">
                                <label>To</label>
                                <div class="team_id">
									<select name="team_id" id="team_id" class="form-control">
										<option value="">- Select Team -</option>
									</select>
								</div>
                                 <span class="text-error" id="txt_team_id"></span>
                                <br>
                             </div>
                             <div class="col-sm-12">
							    <input type="hidden" name="project_id">
                                <input type="hidden" name="people_id">
                                <input type="hidden" name="id_team_old">
								<input type="hidden" name="name_team">
                                <br>
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim_update_team_member"> Update</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="send_update_team_member"> Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
          
                //description cannot long space on input
              $('#reason').keypress(function (e) {
                    var startPos = e.currentTarget.selectionStart;
                    if (e.which === 32 && startPos==0){
                        e.preventDefault();
                    }else if($('#reason').val().length > 0){
                        $('#reason').val($('#reason').val().replace(/\s{2,}/g,' '));
                    }
              });
          
              /*handle paste with extra space on description*/
              $(document).on('paste', '#reason', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 200));
              });
              
              $("#send_add_vacancy").click(function(){
                  
                      if ($('#status').val().length < 1) {
                            $('#txt_status').text('Job title not selected');
                      }else{
                            $('#txt_status').text('');
                      }
                    
                      
                      if ($('#reason').val().length < 1) {
                            $('#txt_reason').text('Description is empty');
                      }else{
                            $('#txt_reason').text('');
                      }
                      
                      if($('#status').val().length > 0 &&
                         $('#reason').val().length > 0){
                              $("#kirim_add_vacancy").click();
                          }
                  
                });
               
			   $('#ModalOutTeam').on('show.bs.modal', function(e) {
				    var project_id = $(e.relatedTarget).data('project_id');
					var people_name = $(e.relatedTarget).data('people_name');
					var people_id = $(e.relatedTarget).data('people_id');
					var name_team = $(e.relatedTarget).data('name_team');
					var team_id = $(e.relatedTarget).data('team_id');
	
					$(e.currentTarget).find('input[name="project_id"]').val(project_id);
					$(e.currentTarget).find('input[name="from"]').val(name_team);
					$(e.currentTarget).find('input[name="people_id"]').val(people_id);
					$(e.currentTarget).find('input[name="name_team"]').val(name_team);
					$(e.currentTarget).find('input[name="id_team_old"]').val(team_id);
					
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
					$.ajax({
						url:"{{ url('/postdistrict/get/move-team/') }}",
						type:"POST",
						data:{ people_id: people_id, team_id : team_id, _token: CSRF_TOKEN },
						success:function(response) {
							var obj = JSON.parse(response);
							if(obj.hasil == 'OK'){
								$('.team_id select').html(obj.data);
								$('.team_id select').selectpicker('refresh');
								return false;
							}
						},
						error:function(){
							alert("error");
						}
					});
					
				}).on('hidden.bs.modal', function(e) {
				  $(e.currentTarget).find('#txt_team_id').text('');
			    });
				
				$("#send_update_team_member").click(function(){
                  
                      if ($('#team_id').val().length < 1) {
                            $('#txt_team_id').text('Name team not selected');
                      }else{
                            $('#txt_team_id').text('');
                      }
                      
                      if($('#team_id').val().length > 0){
                              $("#kirim_update_team_member").click();
                          }
                  
                });
          
          }); 
    </script>  

    </section>
    

@endsection
