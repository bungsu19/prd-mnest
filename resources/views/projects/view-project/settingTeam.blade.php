@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
    
    .mandatory
    {
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif

				@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif
					
                    <div class="card">
                        <div class="header">
                            <h1 align="left">Project Information</h1>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
							<div class="row clearfix"></div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Setting Team</a>
                                        </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        

                                       
                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                   <div class="table-responsive">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                               <h1><b>Setting Team</b></h1>
                            </div><br>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                
                                 <div class="row clearfix">
                                    <div class="col-sm-3">
                                    </div>
                                    <div class="col-sm-7">
                                        <form action="{{ url('/post-setting-team') }}" method="POST" enctype="multipart/form-data" class="form-inline">
                                        {{ csrf_field() }}
                                            <div class="col-xs-12 col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-10">
                                                        <select class="form-control" data-placeholder="Choose an option please" multiple="multiple" name="team_id[]" id="chosen"> 
                                                            @foreach($team as $tm)
                                                                <option value="{{ $tm->id }}">{{ $tm->name_team }} ( {{ $tm->district }} )</option>
                                                            @endforeach
                                                        </select>
                                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                                        <input type="hidden" name="people_id" value="{{ $people->id }}">

                                                    </div>

                                                    <div class="col-md-2">
                                                        <button class="btn bg-blue waves-effect m-r-20" type="submit">Add</button>     
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            
                            
                            </div>

                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">Team Name</font></td>
                                                    <td class="bg-column"><font color="yellow">District</font></td>
                                                    <td class="bg-column"><font color="yellow">Action</font></td>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $no = 0;
                                                    if($leader->currentPage() == 1){
                                                        $no = 1;
                                                    }else{
                                                      $no = ($leader->currentPage()-1) * $jumlahData + 1;
                                                    }
                                                ?>
                                                @foreach($leader as $led)
                                                    <tr>
                                                        <td> {{ $no }}</td>
                                                        <td> {{ $led->name_team }}</td>
                                                        <td> {{ $led->district }}</td>
                                                        <td align="center"> <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete area structure '{{ $led->name_team }}' ?" data-href="{{url('/get-delete-setting-team')}}/{{ $led->id }}" title="Delete Data">
                                                        <i class="material-icons">delete_forever</i>
                                                    </a></td>
                                                    </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <h1><b></b></h1>
                                            </div>
                                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                            {{ $paginator->links('paginator.default') }}
                                        </div>
                                    </div>
                                </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br><br>
                        <a class="btn-ok"> 
                            <button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
                        </a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>

       
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
          
                //description cannot long space on input
              $('#reason').keypress(function (e) {
                    var startPos = e.currentTarget.selectionStart;
                    if (e.which === 32 && startPos==0){
                        e.preventDefault();
                    }else if($('#reason').val().length > 0){
                        $('#reason').val($('#reason').val().replace(/\s{2,}/g,' '));
                    }
              });
          
              /*handle paste with extra space on description*/
              $(document).on('paste', '#reason', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 200));
              });
              
              $("#send_add_vacancy").click(function(){
                  
                      if ($('#status').val().length < 1) {
                            $('#txt_status').text('Job title not selected');
                      }else{
                            $('#txt_status').text('');
                      }
                    
                      
                      if ($('#reason').val().length < 1) {
                            $('#txt_reason').text('Description is empty');
                      }else{
                            $('#txt_reason').text('');
                      }
                      
                      if($('#status').val().length > 0 &&
                         $('#reason').val().length > 0){
                              $("#kirim_add_vacancy").click();
                          }
                  
                });
               
          
          }); 
    </script> 

    </script>  
    <script type="text/javascript">
        $(document).ready(function() {
          $("#chosen").chosen({
                disable_search_threshold: 10,
                no_results_text: "Oops, nothing found!",
                width: "100%"
            });

          $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
          
        }); 
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script> 

    </section>
    

@endsection
