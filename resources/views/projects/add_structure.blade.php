@extends('layout-menu.app')

@section('content')

   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header">
                            <font size="5" color="black"><b>Structure Project </b></font>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                            <label> Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <i>{{ $project->project_name }}</i></label><br>
                                            <label> Project Description : <i>{{ $project->project_desc }}</i></label>
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                              <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalStructure">Add Structure</button>
                                         </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" width="20"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow"> Area Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> District/City</font> </td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <tr>
                                                        
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a onclick="javascript:history.back()" class="btn btn-block bg-gray waves-effect" ><font size="4"> Back</font></a>
                                            </div>
                                            <div class="col-sm-3">
                                                <button href="#"  class="btn btn-block bg-blue waves-effect" ><font size="4">Finish</font></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
<form class="form-signin" method="POST" action="{{url('/add-structure')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalStructure" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Add Structure Project</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Structure Name</label>
                                <select name="structure_id" class="form-control">
                                    <option>-select-</option>
                                   @foreach($structure as $str)
                                    <option value="{{ $str->id }}">{{ $str->title_structure }}</option>
                                    @endforeach
                                </select>
                                <br>
                             </div>
                             <br>
                             <div class="col-sm-12">
                                <br>
                               <!--  <label>Description</label>
                                <textarea name="description" class="form-control" placeholder="Description Job Title"></textarea> -->
                                <input type="hidden" name="project_id" value="{{ $project->id }}">
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>


   
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

    

</section>
    

@endsection
