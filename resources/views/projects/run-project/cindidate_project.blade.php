@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <!-- <div class="block-header">
                <h2>
                   All Product
                </h2>
                 
            </div> -->
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                Project Information
                            </h1>
                            <label>Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_name }} </i></small> <br>
                            <label>Project Description &nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_desc }} </i></small><br>

                            <label>Contract Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_contract_no }} </i></small>  <br>

                            <label>Periode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;({{ $project->project_start }}) To ({{ $project->project_end }}) </i></small> <br>
                            <label>Project Value &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;<b>Rp.</b><i>{{ number_format($project->project_value,0) }}</i> </i></small>  
                             
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        
                                        <li role="presentation" class="active">

                                            <a href="#job_vacancy" data-toggle="tab" class="btn bg-blue">Register</a>
                                        </li>
                                        
                                       

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="job_vacancy">
                                         <div class="table-responsive">
                                                <table class="table table-hover dashboard-task-infos">
                                                    <thead>
                                                        <tr>
                                                            <td class="bg-column"><font color="yellow"> No</font></td>
                                                            <td class="bg-column"><font color="yellow">Name Candidate</font></td>
                                                            <td class="bg-column"><font color="yellow">Gender</font></td>
                                                            <td class="bg-column"><font color="yellow">Last Education</font></td>
                                                            <td class="bg-column"><font color="yellow">Phone Number</font></td>
                                                            <td class="bg-column"><font color="yellow">Interesting Job</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach($people as $peop)
                                                            <tr>
                                                                <td>{{ $no }}</td>
                                                                <td>{{ $peop->first_name }} {{ $peop->middle_name }} {{ $peop->surname }}</td>
                                                                <td> {{ $peop->gender }}</td>
                                                                <td> {{ $peop->last_education }}</td>
                                                                <td> {{ $peop->phone1 }}</td>
                                                                <td>{{ $peop->interesting_job_1 }},{{ $peop->interesting_job_2 }}, {{ $peop->interesting_job_3 }}</td>
                                                                <td align="center">
                                                                     <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                                                                        <label class="custom-control-label filterbtn" for="defaultUnchecked"></label>
                                                                    </div>
                                                                  </td>
                                                            </tr>
                                                        <?php $no++; ?>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="#" class="btn btn-block bg-gray waves-effect" ><h5>Cancel</h5></a>
                                            </div>
                                            <div class="col-sm-5">
                                                <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" ><h5>Submit</h5></button>
                                            </div>
                                        </div>
                                    </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

    

       
    </section>
    

@endsection
