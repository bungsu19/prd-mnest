@extends('layout-menu.app')

@section('content')

<style>
	.nav-tabs li.active a{
		color: #337ab7 !important; 
		background-color: white !important; 
	}
	
	.nav > li > a:hover {
		color: #337ab7 !important; 
		background-color: white !important; 
	}
	
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
<br>
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -50px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                     <div class="card">
                        <div class="header">
                            <h1 align="left" >Project Information</h1>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
						
                        </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Candidate</a>
                                        </li>
										
                                        <li role="presentation" >
                                            <a href="#Interview" data-toggle="tab" class="btn bg-blue">Interview</a>
                                        </li>
                                        
                                        <li role="presentation">
                                            <a href="#Candidate" class="btn bg-blue" data-toggle="tab">Employee</a>
                                        </li>
										
										<!-- <li role="presentation">
                                            <a href="#Member" class="btn bg-blue" data-toggle="tab">Member</a>
                                        </li> -->
                                    </ul>

                                    <!-- Tab panes -->
                               
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="Interview">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn2"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                       <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/all-project/filter-running-project')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter2" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2">All</option>
                                                                        <option value="0">Name</option>
                                                                        <option value="1">Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                                    <input type="hidden" name="tab" value="Interview">
                                                                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>          <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
										<br>

                         
                                     </div>
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
																<td class="bg-column"><font color="yellow"> No</font></td>
																<td class="bg-column"><font color="yellow">Name People</font></td>
																<td class="bg-column"><font color="yellow">Gender</font></td>
																<td class="bg-column"><font color="yellow">Last Education</font></td>
																<td class="bg-column"><font color="yellow">Address People</font></td>
																<td class="bg-column"><font color="yellow">Age</font></td>
																<td class="bg-column"><font color="yellow">Job Vacancy</font></td>
																<td class="bg-column"><font color="yellow">Interview Date</font></td>
																<td class="bg-column" align="center"><font color="yellow">Action</font></td>
															</tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($data->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($interview as $pp)
                                                            <tr>
                                                                    <td>{{ $no }}</td>
                                                                    <td><a href="{{url('data-people/view-data/')}}/{{ $pp['id_people'] }}">{{ $pp['first_name'] }} {{ $pp['middle_name'] }} {{ $pp['surname'] }}</a></td>
                                                                    <td>{{ $pp['gender'] }}</td>
                                                                    <td>{{ $pp['last_education'] }}</td>
                                                                    <td>
                                                                    @if($pp['dom_district'] == NULL)
                                                                        {{ $pp['ktp_district'] }}
                                                                    @else

                                                                        {{ $pp['dom_district'] }}
                                                                   @endif
                                                                    

                                                                </td>
                                                                    <td>{{ $pp['date_part'] }}</td>
                                                                    <td>{{ $pp['job_name'] }}</td>
                                                                    <td>{{ $pp['date_confirm'] }}    {{ $pp['jam_confirm'] }}</td>
                                                                    <td align="center">
                                                                 @if($pp->status_project == 401)
                                                                        <a href="#" data-toggle="modal" data-target="#ModalBankEdit" data-id="{{ $pp['id_people'] }}" data-project="{{ $project->id }}" title="Edit Data">
                                                                            <i class="material-icons">done_all</i>
                                                                        </a>

                                                                    <!-- <a href="#" data-toggle="modal" data-target="#add_candidate" data-message="are you sure want to add CANDIDATE name  ?" data-href="{{url('confirmation-candidate-project')}}/{{ $pp->id_people }}" title="Approve">
                                                                        <i class="material-icons">done_all</i>
                                                                    </a> -->
                                                                @elseif($pp->status_project == 402)
                                                                     <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $pp['id_people'] }}" title="Send Interview">
                                                                            <i class="material-icons">send</i>
                                                                        </a>
                                                                @else
                                                                    <span class="text-error">waiting confirmation</span>
                                                                @endif
                                                                        <!-- <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $pp['id_people'] }}" title="Send Interview">
                                                                            <i class="material-icons">send</i>
                                                                        </a> -->

                                                                        
                                                                       
                                                                    </div>
                                                                      </td>
                                                                </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $interview->links('paginator.default') }}
                                                    </div>
                                                </div>
                                             </div>
                                    </div>     
                                </div>
								
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="Candidate">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn3"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                       <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/all-project/filter-running-project')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter3" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2">All</option>
                                                                        <option value="0">Name</option>
                                                                        <option value="1">Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                                    <input type="hidden" name="tab" value="Candidate">
                                                                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>          <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div><br>

                         
                                     </div>
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
																<td class="bg-column"><font color="yellow"> No</font></td>
																<td class="bg-column"><font color="yellow">Name People</font></td>
																<td class="bg-column"><font color="yellow">Gender</font></td>
																<td class="bg-column"><font color="yellow">Last Education</font></td>
																<td class="bg-column"><font color="yellow">Address People</font></td>
																<td class="bg-column"><font color="yellow">Age</font></td>
																<td class="bg-column"><font color="yellow">Job Vacancy</font></td>
																<td class="bg-column"><font color="yellow">Area Project</font></td>
																<td class="bg-column" align="center"><font color="yellow">Action</font></td>
															</tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($data->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($candidate as $pp)
                                                            <tr>
                                                                    <td>{{ $no }}</td>
                                                                    <td><a href="{{url('data-people/view-data/')}}/{{ $pp['id_people'] }}">{{ $pp['first_name'] }} {{ $pp['middle_name'] }} {{ $pp['surname'] }}</a></td>
                                                                    <td>{{ $pp['gender'] }}</td>
                                                                    <td>{{ $pp['last_education'] }}</td>
                                                                    <td>
                                                                    @if($pp['dom_district'] == NULL)
                                                                        {{ $pp['ktp_district'] }}
                                                                    @else

                                                                        {{ $pp['dom_district'] }}
                                                                   @endif
                                                                    

                                                                </td>
                                                                    <td>{{ $pp['date_part'] }}</td>
                                                                    <td>{{ $pp['job_name'] }}</td>
                                                                    <td>{{ $pp['district'] }}</td>
                                                                    <td align="center">
                                                                        <!-- <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $pp['id_people'] }}" title="Send Interview">
                                                                            <i class="material-icons">send</i>
                                                                        </a> -->
                                                                        <button type="submit" 
                                                                            class="btn bg-blue waves-effect m-r-20" 
                                                                            data-toggle="modal"
                                                                            data-target="#ModalInvite"
                                                                            data-project_id="{{ $pp['project_id'] }}" 
                                                                            data-people_id="{{ $pp['id_people'] }}"
                                                                            data-timid="{{ $pp['team_id'] }}" 
                                                                            data-district="{{ $pp['district'] }}"
                                                                            data-interes_id="{{ $pp['interes_id'] }}"
                                                                            data-job_name="{{ $pp['job_name'] }}"> + Invite</button>
                                                                       
                                                                    </div>
                                                                      </td>
                                                                </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $candidate->links('paginator.default') }}
                                                    </div>
                                                </div>

                                             </div>
                                    </div>     
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="Member">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn4"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                       <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/all-project/filter-running-project')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter4" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2">All</option>
                                                                        <option value="0">Name</option>
                                                                        <option value="1">Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                                    <input type="hidden" name="tab" value="Member">
                                                                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>          <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div><br>

                         
                                     </div>
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
                                                                <td class="bg-column"><font color="yellow"> No</font></td>
                                                                <td class="bg-column"><font color="yellow">Name People</font></td>
                                                                <td class="bg-column"><font color="yellow">Gender</font></td>
                                                                <td class="bg-column"><font color="yellow">Last Education</font></td>
                                                                <td class="bg-column"><font color="yellow">Address People</font></td>
                                                                <td class="bg-column"><font color="yellow">Age</font></td>
                                                                <td class="bg-column"><font color="yellow">Job Vacancy</font></td>
                                                                <td class="bg-column"><font color="yellow">Area Project</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($data->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($member as $pp)
                                                            <tr>
                                                                    <td>{{ $no }}</td>
                                                                    <td><a href="{{url('data-people/view-data/')}}/{{ $pp['id_people'] }}">{{ $pp['first_name'] }} {{ $pp['middle_name'] }} {{ $pp['surname'] }}</a></td>
                                                                    <td>{{ $pp['gender'] }}</td>
                                                                    <td>{{ $pp['last_education'] }}</td>
                                                                    <td>
                                                                    @if($pp['dom_district'] == NULL)
                                                                        {{ $pp['ktp_district'] }}
                                                                    @else

                                                                        {{ $pp['dom_district'] }}
                                                                   @endif
                                                                    

                                                                </td>
                                                                    <td>{{ $pp['date_part'] }}</td>
                                                                    <td>{{ $pp['job_name'] }}</td>
                                                                    <td>{{ $pp['district'] }}</td>
                                                                    <td align="center">
                                                                        <!-- <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $pp['id_people'] }}" title="Send Interview">
                                                                            <i class="material-icons">send</i>
                                                                        </a> -->
                                                                       
                                                                    </div>
                                                                      </td>
                                                                </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $candidate->links('paginator.default') }}
                                                    </div>
                                                </div>

                                             </div>
                                    </div>     
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
										<br>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                        <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/all-project/filter-running-project')}}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2">All</option>
                                                                        <option value="0">Name</option>
                                                                        <option value="1">Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                                    <input type="hidden" name="tab" value="personal_data">
                                                                    <input type="hidden" name="project_id" value="{{ $project->id }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>          <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
										<br>
                         
                                     </div>
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
																<td class="bg-column"><font color="yellow"> No</font></td>
																<td class="bg-column"><font color="yellow">Name People</font></td>
																<td class="bg-column"><font color="yellow">Gender</font></td>
																<td class="bg-column"><font color="yellow">Last Education</font></td>
																<td class="bg-column"><font color="yellow">Address People</font></td>
																<td class="bg-column"><font color="yellow">Age</font></td>
																<td class="bg-column"><font color="yellow">Job Vacancy</font></td>
																<td class="bg-column"><font color="yellow">Area Project</font></td>
																<td class="bg-column" align="center"><font color="yellow">Interview</font></td>
															</tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($data->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($data as $pp)
                                                            <tr>
																	<td>{{ $no }}</td>
																	<td><a href="{{url('data-people/view-data/')}}/{{ $pp['id_people'] }}">{{ $pp['first_name'] }} {{ $pp['middle_name'] }} {{ $pp['surname'] }}</a></td>
																	<td>{{ $pp['gender'] }}</td>
																	<td>{{ $pp['last_education'] }}</td>
																	<td>
																	@if($pp['dom_district'] == NULL)
																		{{ $pp['ktp_district'] }}
																	@else

																		{{ $pp['dom_district'] }}
																   @endif
																	

																</td>
																	<td>{{ $pp['date_part'] }}</td>
																	<td>{{ $pp['job_name'] }}</td>
																	<td>{{ $pp['district'] }}</td>
																	<td align="center">
																		<a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $pp['id_people'] }}" title="Send Interview">
																			<i class="material-icons">send</i>
																		</a>
																	   
																	</div>
																	  </td>
																</tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                   <h1><b></b></h1>
                                                    </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $data->links('paginator.default') }}
                                                    </div>
                                                </div>

                                             </div>
                                    </div>


                                    
                                </div>


                                    </div>
                                </div>
                            </div>
                    </div> 
                </div>
            </div>

        <form class="form-signin" name="formubahbank" method="POST" action="{{url('reject-member-project')}}"  autocomplete="off">
            {{ csrf_field() }}
            <div class="modal fade" id="ModalBankEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Edit Data Bank</font></h2>
                        </div>
                        
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label> select <span class="mandatory">*</span> </label>
                                <select class="form-control" name="name_select" id="name_select">
                                    <option value="0">-select-</option>
                                    <option value="1">Accept </option>
                                    <option value="2">Reject</option>
                                </select>
                                <span class="text-error" id="txt_name_select"></span>
                                <br>
                            </div>

                            <div class="col-sm-12">
                                <label> Reason <span class="mandatory">*</span> </label>
                                <textarea class="form-control" name="reason"></textarea>
                                <span class="text-error" id="txt_name_select"></span>
                                <br>
                            </div>
                             
                             <div class="col-sm-12">
                                
                                <br>
                                <br>
                                <input type="hidden" name="people_id" id="id">
                                <input type="hidden" name="project_id" id="project_id">
                                
                             </div>
                            
                        </div>
                        
                        <div class="modal-footer">
                            <br>
                                <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimubahbank" > Update</button>
                                <!-- <button type="button" class="btn btn-link bg-blue waves-effect" id="sendubahbank"> Update</button> -->
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" action="{{url('/project-member')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalInvite" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Invite People</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Position</label>
                                <select name="interes_id" class="form-control"id="interes_id" ></select>
                                <br>
                             </div>
                             <div class="col-sm-12">
                                <label>Team Name</label>
								<div class="team_id">
									<select name="team_id" class="form-control">
										<option>-select-</option>
									</select>
								</div>
                                <br>
                             </div>
                             <br>
                             <div class="col-sm-12">
                                <?php $todayDate = date("Y-m-d")?>
                                <input type="hidden" name="project_id" >
                                <input type="hidden" name="people_id">
                                <input type="hidden" name="pm_start" value="{{ $todayDate }}">
                                <input type="hidden" name="pm_end" value=" ">
                                <br>
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
		
		<form class="form-signin" method="POST" action="{{url('/all-project/add-interview-member-project')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="modalInTerview" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Add Date Interview</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">

                             <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Message <span class="mandatory">*</span></label>
                                   <div class="form-group">
                                        <div class="form-line">
                                                <textarea name="message" class="form-control" placeholder=" Input message" id="message" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200">{{ $project->project_name }} sedang mencari kandidat2 yang berpengalaman sebagai : 
@foreach($messageJob as $qwa) {{ $qwa->job_name }}, @endforeach dll untuk periode pekerjaan "{{ $project->project_start }} - {{ $project->project_end }}", dan berdasarkan database kami, anda terpilih sebagai calon pekerja pada proyek tersebut. Untuk itu kami mengundang Sdr/i melakukan wawancara pada : 

                                      </textarea>
                                        </div>
                                        <span class="text-error" id="txt_message"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                    <label>Date 1 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="text" name="date1" class="form-control datespicker" placeholder="Date 1 ..." id="date_1">
                                        </div>
                                        <span class="text-error" id="txt_date_1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 1 <span class="mandatory">*</span></label>
                                   <div class="form-line">
                                                <input type="time" name="jam1" class="form-control " placeholder="Hours 1 ..." id="jam_1">
                                        </div>
                                        <span class="text-error" id="txt_jam_1"></span>
                                    
                                </div>
                            </div>
                            <div class="row clearfix">
    							<div class="col-sm-8">
                                    <label>Date 2 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="text" name="date2" class="form-control datespicker2" placeholder="Date 2 ..." id="date_2">
                                        </div>
                                        <span class="text-error" id="txt_date_2"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 2 <span class="mandatory">*</span></label>
                                   <div class="form-line">
                                                <input type="time" name="jam2" class="form-control " placeholder="Hours 2 ..." id="jam_2">
                                        </div>
                                        <span class="text-error" id="txt_jam_2"></span>
                                   
                                </div>
                            </div>
                            <div class="row clearfix">
    							<div class="col-sm-8">
                                    <label>Date 3 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                            <div class="form-line">
                                                <input type="text" name="date3" class="form-control datespicker3" placeholder="Date 3 ..." id="date_3">
                                            </div>
                                            <span class="text-error" id="txt_date_3"></span>
                                        </div>
    									<input type="hidden" name="people_id" id="id">
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 3 <span class="mandatory">*</span></label>
                                     <div class="form-line">
                                                <input type="time" name="jam3" class="form-control " placeholder="Hours 3 ..." id="jam_3">
                                        </div>
                                        <span class="text-error" id="txt_jam_3"></span>
                                    
                                </div>
                            </div>
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Loaction <span class="mandatory">*</span></label>
                                   <div class="form-line">
                                                <input type="text" name="lokasi" class="form-control " placeholder="Location Interview"" id="lokasi">
                                        </div>
                                        <span class="text-error" id="txt_lokasi"></span>
                                    
                                </div>

                            </div>
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Meet <span class="mandatory">*</span></label>
                                     <div class="form-line">
                                                <input type="text" name="ketemu" class="form-control " placeholder="Meet You Interview"" id="Ketemu">
                                        </div>
                                        <span class="text-error" id="txt_ketemu"></span>
                                    
                                </div>

                            </div>

                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Event <span class="mandatory">*</span></label>
                                     <div class="form-line">
                                                <input type="text" name="event" class="form-control " placeholder="Event"" id="event">
                                        </div>
                                        <span class="text-error" id="txt_event"></span>
                                    
                                </div>

                            </div>
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Address <span class="mandatory">*</span></label>
                                   <div class="form-group">
                                        <div class="form-line">
                                                <textarea name="address" class="form-control" placeholder=" Input Address" id="address" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200"></textarea>
                                        </div>
                                        <span class="text-error" id="txt_address"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimInterview" style="display: none;">Send</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="sendInterview">Send</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
		
        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>
        <div class="modal fade" id="add_candidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <b class="message"></b><br>
                        </div>
                        <div class="modal-footer">
                             <a class="btn-ok"> 
                                <button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
                            </a>
                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
    
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
              activaTab('<?php echo $active; ?>');
                  function activaTab(tab){
                    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
                  };
            }); 
        </script> 
        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script type = "text/javascript">
            $(document).ready(function () {

                $('#ModalBankEdit').on('show.bs.modal', function(e) { 
                    var id = $(e.relatedTarget).data('id');
                    var project_id = $(e.relatedTarget).data('project');
                    $(e.currentTarget).find('#id').val(id);  
                    $(e.currentTarget).find('#project_id').val(project_id);  
                });
				
				$('#modalInTerview').on('show.bs.modal', function(e) {
				  
					var id = $(e.relatedTarget).data('id');
					$(e.currentTarget).find('#id').val(id);  
				
					  $(e.currentTarget).find('.datespicker').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate:0,
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
					  
					  $(e.currentTarget).find('.datespicker2').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate:0,
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
					  
					  $(e.currentTarget).find('.datespicker3').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate: '+1',
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
				});
				
				$('#modalInTerview').on('hidden.bs.modal', function(e) {
				  
					$(e.currentTarget).find('#date_1').val(''); 
					$(e.currentTarget).find('#date_2').val(''); 
					$(e.currentTarget).find('#date_3').val('');
                    $(e.currentTarget).find('#jam1').val(''); 
                    $(e.currentTarget).find('#jam2').val(''); 
                    $(e.currentTarget).find('#jam3').val(''); 
                    $(e.currentTarget).find('#lokasi').val(''); 
                    $(e.currentTarget).find('#ketemu').val(''); 
                    $(e.currentTarget).find('#address').val(''); 
                    $(e.currentTarget).find('#event').val(''); 
					
					$(e.currentTarget).find('#txt_date_1').text('');
					$(e.currentTarget).find('#txt_date_2').text('');
					$(e.currentTarget).find('#txt_date_3').text('');
                    $(e.currentTarget).find('#txt_jam_1').text('');
                    $(e.currentTarget).find('#txt_jam_2').text('');
                    $(e.currentTarget).find('#txt_jam_3').text('');
                    $(e.currentTarget).find('#txt_address').text('');
                    $(e.currentTarget).find('#txt_ketemu').text('');
                    $(e.currentTarget).find('#txt_lokasi').text('');
                    $(e.currentTarget).find('#txt_event').text('');
				});
				
				var $statusfilter = 0;
				var $statusfilter2 = 0;
				var $statusfilter3 = 0;
                var $statusfilter4 = 0;
				
				$('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('.filterbtn2').click(function(){
            if($statusfilter2 == 0){
                $('#formfilter2').show(999);
                $statusfilter2 = 1;
            } else {
                $('#formfilter2').hide(999);
                $statusfilter2 = 0;
            }
             });

             $('#add_candidate').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
			 
			 $('.filterbtn3').click(function(){
                if($statusfilter3 == 0){
                    $('#formfilter3').show(999);
                    $statusfilter3 = 1;
                } else {
                    $('#formfilter3').hide(999);
                    $statusfilter3 = 0;
                }
             });

              $('.filterbtn4').click(function(){
                if($statusfilter4 == 0){
                    $('#formfilter4').show(999);
                    $statusfilter4 = 1;
                } else {
                    $('#formfilter4').hide(999);
                    $statusfilter4 = 0;
                }
             });

                $(".my_cb").on("click", function () {
                    var val = $(this).val();
                    var checkboxes = $("input[type=checkbox][name='list[]'][value='"+val+"']");
                    var checkedState = this.checked;
                    checkboxes.each(function () {
                        this.checked = checkedState;
                    });

                });
				
				$('#ModalInvite').on('show.bs.modal', function(e) {
					var project_id = $(e.relatedTarget).data('project_id');
					var people_id = $(e.relatedTarget).data('people_id');
					var district = $(e.relatedTarget).data('district');
					var interes_id = $(e.relatedTarget).data('interes_id');
					var job_name = $(e.relatedTarget).data('job_name');
					
					$(e.currentTarget).find('#interes_id').append($('<option>', {
						value: interes_id,
						text: job_name
					}));
					$(e.currentTarget).find('input[name="project_id"]').val(project_id);
					$(e.currentTarget).find('input[name="people_id"]').val(people_id);
					
					var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
					$.ajax({
						url:"{{ url('/postdistrict/get/team/') }}",
						type:"POST",
						data:{ data_id: district, project_id : project_id, _token: CSRF_TOKEN },
						success:function(response) {
							var obj = JSON.parse(response);
							if(obj.hasil == 'OK'){
								$('.team_id select').html(obj.data);
								$('.team_id select').selectpicker('refresh');
								return false;
							}
						},
						error:function(){
							alert("error");
						}
					});
					
				});

                $("#sendInterview").click(function(){
                
                    if ($('#date_1').val().length < 1) {
                        $('#txt_date_1').text('Date 1 is empty');
                    }else{
                        $('#txt_date_1').text('');
                    }
                    
                    if ($('#date_2').val().length < 1) {
                        $('#txt_date_2').text('Date 2 is empty');
                    }else{
                        $('#txt_date_2').text('');
                    }
                    
                    if ($('#date_3').val().length < 1) {
                        $('#txt_date_3').text('Date 3 is empty');
                    }else{
                        $('#txt_date_3').text('');
                    }

                    // if ($('#jam1').val().length < 1) {
                    //     $('#txt_jam_1').text('Jam 1 is empty');
                    // }else{
                    //     $('#txt_jam_1').text('');
                    // }
                    // if ($('#jam2').val().length < 1) {
                    //     $('#txt_jam_2').text('jam 2 is empty');
                    // }else{
                    //     $('#txt_jam_2').text('');
                    // }
                    // if ($('#jam3').val().length < 1) {
                    //     $('#txt_jam_3').text('jam 3 is empty');
                    // }else{
                    //     $('#txt_jam_3').text('');
                    // }
                    // if ($('#lokasi').val().length < 1) {
                    //     $('#txt_lokasi').text('Location is empty');
                    // }else{
                    //     $('#txt_lokasi').text('');
                    // }
                    // if ($('#ketemu').val().length < 1) {
                    //     $('#txt_ketemu').text('Meet you is empty');
                    // }else{
                    //     $('#txt_ketemu').text('');
                    // }

                    // if ($('#event').val().length < 1) {
                    //     $('#txt_event').text('event is empty');
                    // }else{
                    //     $('#txt_event').text('');
                    // }

                    // if ($('#address').val().length < 1) {
                    //     $('#txt_address').text('address is empty');
                    // }else{
                    //     $('#txt_address').text('');
                    // }
                    
                    if(
                        $('#date_1').val().length > 0 &&
                        $('#date_2').val().length > 0 &&
                        $('#date_3').val().length > 0 
                        
                    )
                    {
                        $("#kirimInterview").click();  
                    }
                
                });
				 
            });
			
			setTimeout(function(){
           $("div.alert").remove();
        }, 5000 );
        </script>
		
    </section>
    

@endsection
