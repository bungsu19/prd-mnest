@extends('layout-menu.app')

@section('content')
<style type="text/css">
  .text-error
  {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
      margin-top: 5px;
    }
</style>
<br>
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -50px;">
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow">New Project</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('/add-project')}}" enctype="multipart/form-data" autocomplete="off" name="comboForm" >
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Project Name<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="project_name" class="form-control" placeholder="Project Name" id="project_name" maxlength="30">
                                                </div>
                                                <span class="text-error" id="txt_project_name"></span>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Client<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                   <select name="client_id" class="form-control" id="client_id">
                                                     <option value="">-select-</option>
                                                     @foreach($client as $cl)
                                                        <option value="{{ $cl->client_id }}"> {{ $cl->company_name }}</option>
                                                    @endforeach
                                                   </select>
                                                </div>
                                                <span class="text-error" id="txt_client_id"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                   <select name="branch_id" class="form-control" id="branch_id">
                                                     <option value="">-select-</option>
                                                     @foreach($branch as $br)
                                                    <option value="{{ $br->id }}">{{ $br->branch_name }}</option>
                                                    @endforeach
                                                   </select>
                                                </div>
                                                <span class="text-error" id="txt_branch_id"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contract Number<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                  <input type="text" name="contact_number" class="form-control" placeholder="Contract Number" id="contact_number" maxlength="50">
                                                </div>
                                                <span class="text-error" id="txt_contact_number"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Periode<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="input-daterange input-group" id="bs_datepicker_range_container">
                        												<div class="form-line">
                        													<input type="text" name="project_start" class="form-control" placeholder="Project Start.." id="project_start">
                        												</div>
                        												<span class="text-error" id="txt_project_start"></span>
                        												<span class="input-group-addon" style="margin-left:10px;">to</span>
                        												<div class="form-line">
                        													<input type="text" name="project_end" class="form-control" placeholder="Project End..." id="project_end">
                        												</div>
                        												<span class="text-error" id="txt_project_end"></span>
                        											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Project Value<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                  <input type="text" name="project_value" class="form-control" placeholder="Project Value" id="rupiah" maxlength="50" >
                                                </div>
                                                <span class="text-error" id="txt_rupiah"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Description<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                  <textarea name="description" class="form-control" placeholder=" Description Project" id="description" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" maxlength="150" required></textarea>
                                                </div>
                                                <span class="text-error" id="txt_description"></span>
                                            </div>
                                        </div>
                                    </div>
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="#" class="btn btn-block bg-gray waves-effect" ><font size="4"> Cancel</font></a>
                                            </div>
                                            <div class="col-sm-5">
                                                <button href="#" type="button" id="send" class="btn btn-block bg-blue waves-effect" ><font size="4">Next</font></button>
                                                 <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Next</h4></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
  
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  
	<script type="text/javascript">
     $(function(){
		$("#bs_datepicker_range_container").datepicker({
			  format: 'dd-mm-yyyy',
			  autoclose: true,
			  todayHighlight: false,
		  });
	});
    </script>
	

    <script type="text/javascript">
     $(function(){
      
       $('#project_name').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
            }else if($('#project_name').val().length > 0){
                    $('#project_name').val($('#project_name').val().replace(/\s{2,}/g,' '));
                }
            });

        $('#contact_number').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
            }else if($('#contact_number').val().length > 0){
                    $('#contact_number').val($('#contact_number').val().replace(/\s{2,}/g,' '));
                }
            });

                $('#description').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
            }else if($('#description').val().length > 0){
                    $('#description').val($('#description').val().replace(/\s{2,}/g,' '));
                }
            });





       $("#send").click(function(){
        if ($('#project_name').val().length < 1) {
                $('#txt_project_name').text('Project Name not empty');
          }else{
                $('#txt_project_name').text('');
          }

          if ($('#contact_number').val().length < 1) {
                $('#txt_contact_number').text('Contract Number not empty');
          }else{
                $('#txt_contact_number').text('');
          }

          if ($('#project_start').val().length < 1) {
                $('#txt_project_start').text('Project Start not empty');
          }else{
                $('#txt_project_start').text('');
          }

             if ($('#project_end').val().length < 1) {
                $('#txt_project_end').text('Project End not empty');
          }else{
                $('#txt_project_end').text('');
          }

        if ($('#client_id').val().length < 1) {
                $('#txt_client_id').text('Client not selected');
          }else{
                $('#txt_client_id').text('');
          }
      
        if ($('#branch_id').val().length < 1) {
                $('#txt_branch_id').text('Client not selected');
          }else{
                $('#txt_branch_id').text('');
          }

          if ($('#rupiah').val().length < 1) {
                $('#txt_rupiah').text('project Value not empty');
          }else{
                $('#txt_rupiah').text('');
          }

          if ($('#description').val().length < 1) {
                $('#txt_description').text('Description not empty');
          }else{
                $('#txt_description').text('');
          }

      if( $('#client_id').val().length > 0 && $('#branch_id').val().length > 0 ){
         $("#kirim").click();  
       }
         
      
    });


     });
    </script>
    <script type="text/javascript">
    
        var rupiah = document.getElementById('rupiah');
        rupiah.addEventListener('keyup', function(e){
          // tambahkan 'Rp.' pada saat form di ketik
          // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
          rupiah.value = formatRupiah(this.value, '');
        });
     
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          // tambahkan titik jika yang di input sudah menjadi angka ribuan
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
  </script>

    <script Language="JavaScript">
      function validateR(){
      var selectedCombobox=(comboForm.branch_id.value);
      if (selectedCombobox=="-1") {
          alert("Please Select Branch");
          return false;
      }
         return true;
      }
    </script>

</section>
    

@endsection
