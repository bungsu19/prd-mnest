@extends('layout-menu.app')

@section('content')

   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:-30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card" >
                        <div class="header">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>Projects</b></h1>
                            </div><br>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             <a href="{{url('/add-project')}}" class="btn bg-blue waves-effect m-r-20">New Project</a>
                              <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            
                            </div>
                            <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->
                            <form class="form-signin" method="GET" action="{{url('/filter-project')}}" autocomplete="off">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-3">
                                                    <label>Filter by</label>
                                                    <select name="filterby" class="form-control show-tick">
                                                        <option value="0" <?php if($filterBy == '0'){ echo "selected='selected'";} ?>>All</option>
                                                        <option value="1" <?php if($filterBy == '1'){ echo "selected='selected'";} ?>>Name</option>
														<option value="2" <?php if($filterBy == '2'){ echo "selected='selected'";} ?>>Client</option>
                                                    </select>
                                                </div>
												<div class="col-sm-6">
                                                    <label>Value</label>
                                                    <input type="text" name="data" class="form-control" placeholder="Value" value="<?php echo $value; ?>">
                                                </div>
												<div class="col-sm-3">
                                                     <button type="submit" name="search" class="btn bg-blue waves-effect m-r-20" style="margin-top:25px;" value="search-project">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                       <tr>
                                            <td class="bg-column" align="center" width="3%"><font color="yellow"> No</font></td>
                                            <td class="bg-column"><font color="yellow" width="10%">Status</font></td>
                                            <td class="bg-column"><font color="yellow"width="10%">Name</font></td>
                                            <td class="bg-column"><font color="yellow"width="20%">Client</font></td>
                                            <td class="bg-column" style="width: 110px;"><font color="yellow" width="21%">Start Date</font></td>
                                            <td class="bg-column" style="width: 110px;"><font color="yellow" width="21%">End Date</font></td>
                                            <td class="bg-column" align="center" width="15%"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
											$no = 0;
											if($data->currentPage() == 1){
												$no = 1;
											}else{
											  $no = ($data->currentPage()-1) * $jumlahData + 1;
											}
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td>
												<?php 
													$todayDate = date("Y-m-d");
													$datetime1 = new DateTime($todayDate);
													$datetime2 = new DateTime($da->project_end);
													$datetime2->add(new DateInterval('P1D'));
													$interval = $datetime1->diff($datetime2);
													$count = $interval->format('%R%a');
													$countDown = '';
													
													if($count > 7){
														$countDown = '';
													}elseif($count >= 0 && $count <= 7){
														$countDown = '('.$count.' days)';
													}else{
														$countDown = '';
													}								
												?>
                                                @if($todayDate >= $da->project_start && $todayDate < $da->project_end)
													
                                                    @if($count > 0 && $count <= 7)
                                                        <span class="label" style="background-color: #FA8072">End in to <?php echo $countDown; ?> </span>
                                                    @else
                                                        <span class="label label-success">On-Going </span>
                                                    @endif
													
                                                @elseif($todayDate >= $da->project_start && $todayDate >= $da->project_end)
												
                                                    @if($da->active_flag == 1)
														<span class="label" style="background-color: #FF0000">Finish </span>
													@else
                                                        <span class="label" style="background-color: #FF0000">Done</span>
                                                    @endif
													
                                                @else
													
                                                    <span class="label label-warning">Ready</span>
													
                                                @endif
                                                    
                                                </td>
                                                <td> <span class="span-dots-project">{{ $da->project_name }} </span> </td>
                                                <td>{{ $da->client }}</td>
                                                <td>{{ $da->project_start }}</td>
                                                <td>{{ $da->project_end }}</td>
                                                <td>
                                                    @if($todayDate >= $da->project_start && $todayDate < $da->project_end)
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/run-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Execute">
                                                                    <i class="material-icons">play_circle_filled</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/view-project')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                                  <i class="material-icons">remove_red_eye</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/edit-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                                    <i class="material-icons">mode_edit</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2"></div>
                                                    @elseif( $todayDate >= $da->project_start && $todayDate >= $da->project_end)
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/view-project')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                                  <i class="material-icons">remove_red_eye</i>
                                                                </a>
                                                            </div>
                                                        
                                                            @if($da->active_flag == 1)
                                                                <div class="col-sm-2">
                                                                <a href="{{url('/all-project/edit-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                                    <i class="material-icons">mode_edit</i>
                                                                </a>
                                                            </div>
                                                                <div class="col-sm-2">
																	<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="Confirmation End Project ?" data-href="{{url('/confirmation-end-project')}}/{{ $da->id }}" title="Confirmation End Project">
																		<i class="material-icons">done_all</i>
																	</a>
                                                                </div>
                                                            @else
                                                                <div class="col-sm-2"></div>
                                                            @endif
                                                    @else
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/run-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Execute">
                                                                    <i class="material-icons">play_circle_filled</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/view-project')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                                  <i class="material-icons">remove_red_eye</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <a href="{{url('/all-project/edit-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                                    <i class="material-icons">mode_edit</i>
                                                                </a>
                                                            </div>
                                                            <div class="col-sm-2">
																<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete project '{{ $da->project_name }}' ?" data-href="{{url('/delete-project')}}/{{ $da->id }}" title="Delete Data">
																	<i class="material-icons">delete_forever</i>
																</a>
															</div>
                                                                                                                        
                                                    @endif
                                                 </td>
                                                </tr>
                                            <?php $no++; ?>
                                            @endforeach
                                    </tbody>
                                </table>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
                    </div>
                </div>
          
            <!-- #END# Basic Examples -->

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                     <h3>Delete</h3>
                </div>
                <div class="modal-body">
                    <p>You are about to delete.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                  <a href="#" id="btnYes" class="btn danger">Yes</a>
                  <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
                </div>
            </div>
        
			<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>

    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
        });
</script>

<script type="text/javascript">
    $('#myModal').on('show', function() {
    var id = $(this).data('id'),
        removeBtn = $(this).find('.danger');
    });

    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();

        var id = $(this).data('id');
        $('#myModal').data('id', id).modal('show');
    });

    $('#btnYes').click(function() {
        // handle deletion here
        var id = $('#myModal').data('id');
        $('[data-id='+id+']').remove();
        $('#myModal').modal('hide');
    });
	
	setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );

</script>
@endsection
    

@endsection
