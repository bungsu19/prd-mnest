@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">

    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
					
					@if(session()->has('gagal'))
					<div class="alert bg-secondary alert-dismissible" role="alert" style="height: 110px; overflow-y: auto;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
						  <?php $pesan = session()->get('gagal'); ?>
					   <?php for ($i=0; $i < count($pesan) ; $i++) { 
						   echo $pesan[$i].'<br>';
					   }
					   ?>
					</div>
					@endif
					
                    <div class="card">
                        <div class="header">
                            <h1 align="left">Project Information</h1>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                       
                                </div>
								<div class="row clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -10px;">
             <div class="card">
                <div class="header">
                            <h1 align="left">Outlet</h1>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Outlet Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $outlet->outlet_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $outlet->outlet_address1 }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                     <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalStructure">Setting Shift</button>  
                                </div>   
                            </div>
							<div class="row clearfix"></div>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover ">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> No</font></td>
                                                        <td class="bg-column"><font color="yellow"> Shift</font></td>
                                                        <td class="bg-column"><font color="yellow">Hour In</font></td>
                                                        <td class="bg-column"><font color="yellow">Hour Out</font></td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1 ?>
                                                    @foreach($shift as $sh)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                      <td>@if($sh->code_shift == 1)
                                                            Shift I
                                                          @elseif($sh->code_shift == 2)
                                                            Shift II
                                                          @else
                                                            Shift III
                                                          @endif  
                                                      </td>
                                                      <td>{{ $sh->hour_in }}</td>
                                                      <td>{{ $sh->hour_out }}</td>
                                                      <td align="center">
														<a href="#" data-toggle="modal" data-target="#ModalShiftEdit" 
															data-id="{{ $sh->id  }}" 
															data-shift="{{ 'Shift '.$sh->code_shift }}" 
															data-hour_in="{{ $sh->hour_in }}" 
															data-hour_out="{{ $sh->hour_out }}"
															data-outlet_id="{{ $sh->outlet_id }}"	
															data-project_id="{{ $sh->project_id }}"title="Edit Data">
																<i class="material-icons">mode_edit</i>
														</a>
													
														<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete shift '{{ $sh->code_shift }}' ?" data-href="{{url('delete-setting-shift-project')}}/{{ $sh->id }}" title="Delete Data">
															<i class="material-icons">delete_forever</i>
														</a>
														
													  </td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                       </div> 
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                        </div>
                    </div>
            </div>
		</div>
	</div>
	
	<form class="form-signin" method="POST" action="{{url('post-setting-shift-project')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalStructure" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Setting Shift</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                             <div class="col-sm-4">
                                <label>Shift <span class="mandatory">*</span></label>
							</div>
							<div class="col-sm-8">
								<select id="number" multiple="multiple" name="shift[]" class="form-control">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
								</select>
                             </div>
                           </div>
                             <div class="col-sm-12">
                                <input type="hidden" name="project_id" value="{{ $project->id }}">
                                <input type="hidden" name="outlet_id" value="{{ $outlet->id }}">
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    

    


		<form class="form-signin" method="POST" action="{{url('post-update-setting-shift-project')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalShiftEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Setting Shift</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                             <div class="col-sm-12">
                                <label>Shift Type <span class="mandatory">*</span></label>
                                <input type="text" name="shift" class="form-control" placeholder="Hour In" id="shift" disabled>
                                <br>
                             </div>
                           </div>
                            <div class="row clearfix">
                                   <br>
                                  <div class="col-sm-6">
                                    <label>Hour In <span class="mandatory">*</span></label>
                                    <input type="text" name="hour_in" class="form-control" placeholder="Hour In" id="hour_in">
                                       <span class="text-error" id="txt_hour_in"></span>
                                 </div>

                                 <div class="col-sm-6">
                                    <label>Hour Out <span class="mandatory">*</span></label>
                                    <input type="text" name="hour_out" class="form-control" placeholder="Hour In" id="hour_out">
                                       <span class="text-error" id="txt_hour_out"></span>
                                 </div>
                             </div>
                             <div class="col-sm-12">
							 <input type="hidden" name="id" id="id">
                                <input type="hidden" name="project_id" id="project_id">
                                <input type="hidden" name="outlet_id" id="outlet_id">
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
		 $("#number").select2({
                placeholder: 'Select a shift',
				width: '100%'
         });
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
    	  
    			$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
				
			$('#ModalShiftEdit').on('show.bs.modal', function(e) {

				var id = $(e.relatedTarget).data('id');
				var shift = $(e.relatedTarget).data('shift');
				var hour_in = $(e.relatedTarget).data('hour_in');
				var hour_out = $(e.relatedTarget).data('hour_out');
				var outlet_id = $(e.relatedTarget).data('outlet_id');
				var project_id = $(e.relatedTarget).data('project_id');

				$(e.currentTarget).find('#id').val(id);
				$(e.currentTarget).find('#shift').val(shift);
				
				$(e.currentTarget).find('#hour_in').val(hour_in); 
				var input = $(e.currentTarget).find('#hour_in');
				input.clockpicker({
					autoclose: true
				});
				
				$(e.currentTarget).find('#hour_out').val(hour_out); 
				var input2 = $(e.currentTarget).find('#hour_out');
				input2.clockpicker({
					autoclose: true
				});
				
				$(e.currentTarget).find('#outlet_id').val(outlet_id);   
				$(e.currentTarget).find('#project_id').val(project_id);   

			});
			
			$('#ModalStructure').on('hidden.bs.modal', function(e) {
				$(e.currentTarget).find('#number').val([]).trigger("change");
			});
		  
          }); 
    </script>  

    

</section>
    

@endsection
