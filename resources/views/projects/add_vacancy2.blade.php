@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">

    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h1 align="left">Project Information</h1>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> Rp. {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <a href="{{url('/edit-project')}}/{{ $project->id }}" class="btn bg-blue waves-effect m-r-20">Edit Project</a>
                                             </div>
                                        </div>
                                </div>
								<div class="row clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>

     <div class="row clearfix">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                <div class="header">
                            <font size="5" color="black"><b>Job Vacancy Qualifications</b></font>
							<div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalVacancy">Add Vacancy</button>
                                             </div>
                                        </div>
                                    </div>
                </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                     <div class="row clearfix">
                                        <div class="table-responsive">
                                        <div class="col-sm-12">
                                            <table class="table table-bordered table-striped table-hover ">
                                                <thead>
                                                    <tr>
                                                        <td class="bg-column" align="center"><font color="yellow"> NO</font></td>
                                                        <td class="bg-column"><font color="yellow"> Job Title Name</font></td>
                                                        <td class="bg-column"><font color="yellow"> Gender</font></td>
                                                        <td class="bg-column"><font color="yellow">Last Education</font></td>
                                                        <td class="bg-column"><font color="yellow">Age</font></td>
                                                        <td class="bg-column"><font color="yellow">Number Of Person</font></td>
                                                        <td class="bg-column" align="center"><font color="yellow"> Action</font></td>
                                                    </tr>
                                                </thead> 
                                                <tbody>
                                                    <?php $no = 1 ?>
                                                    @foreach($vacancy as $vac)
                                                    <tr>
                                                        <td align="center">{{ $no }}</td>
                                                        <td>{{ $vac->job_name }}</td>
                                                         <td>{{ $vac->gender }}</td>
                                                          <td>{{ $vac->last_education }}</td>
                                                           <td>{{ $vac->start_age }} - {{ $vac->end_age }}</td>
                                                            <td>{{ $vac->qty_people }}</td>
                                                        <td align="center">
														
															<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete data '{{ $vac->job_name }}' ?" data-href="{{url('/post-delete-project-vacancy')}}/{{ $vac->id }}" title="Delete Data">
																<i class="material-icons">delete_forever</i>
															</a>
                                                        </td>
                                                    </tr>
                                                    <?php $no++; ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                       </div> 
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                        </div>
                    </div>


                    <!-- #END# form add prodct -->
            </div>
             <div class="row clearfix">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="card">
                   <div class="header">
                            <font size="5" color="black"><b>Structure Area Projects</b></font>
							<div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <?php $valid = count($dt_struc); ?>
                                                @if($valid == 0 )
                                                <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalStructure">Add Structure</button>
                                                @else
                                                  
                                                @endif
                                             </div>
                                        </div>
                                    </div>
                </div>

                      <!-- form add product -->
                       <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                      {{ csrf_field() }}
                      <div class="row clearfix">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row clearfix">
                                         <div class="table-responsive">
                                          <div class="col-sm-12">
                                              <table class="table table-bordered table-striped table-hover ">
                                              <thead>
                                                
                                                  <tr>
                                                      <td class="bg-column" align="center" width="20"><font color="yellow">No</font></td>
                                                      <td class="bg-column" align="center"><font color="yellow">Area Name</font></td>
                                                      <td class="bg-column" align="center"><font color="yellow">District/city</font></td>
                                                      <td class="bg-column" align="center"><font color="yellow">Action  </font></td>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php $no = 1 ?>

                                                  @foreach($dt_struc as $det)
                                                  <?php $count = explode("|", $det->area_city); ?>
                                                  <?php $team = explode("|", $det->id_team); ?>
                                                  <tr>
                                                      <td rowspan="{{ count($count)+1 }}" align="center">{{ $no }}</td>
                                                      <td rowspan="{{ count($count)+1 }}" align="center">{{ $det->name_area }}</td>
												  </tr>
                                                             
                                                              @for ($i = 0; $i < count($count ); $i++)
                                                              <tr >
                                                                   
                                                                   <td align="center" > {{ $count[$i] }}</td>
                                                                   <td align="center"><a class="btn bg-blue" href="{{url('/all-project/edit-team')}}/{{ $team[$i] }}/{{ $project->id }}"> + Team</a> </td>
                                                              </tr>
                                                              @endfor
                                                         
                                                  <?php $no++; ?>
                                                   @endforeach
                                              </tbody>
                                          </table>
                                          </div>
                                          </div>
                                      </div>
                                      
                                   
                                       <div class="row clearfix">
                                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                           
                                          
                                          </div>
                                      <div class="row clearfix">
                                           
                                      </div>
                                      
                                  </div>
                                  </form>
                          </div>
        </div>
    </div>

    <div class="row clearfix">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <div class="card">
                   <div class="header">
                            <font size="5" color="black"><b>Setting Shift</b></font>
              <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="text-align: right;">
                                            
                                        </div>
                                    </div>
                </div>

                      <!-- form add product -->
                       <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                      {{ csrf_field() }}
                      <div class="row clearfix">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="row clearfix">
                                         <div class="table-responsive">
                                          <div class="col-sm-12">
                                              <table class="table table-bordered table-striped table-hover ">
                                              <thead>
                                                
                                                  <tr>
                                                      <td class="bg-column" align="center" width="20"><font color="yellow">No</font></td>
                                                      <td class="bg-column" align="center"><font color="yellow">District/city</font></td>
                                                      <td class="bg-column" align="center"><font color="yellow">Action  </font></td>
                                                  </tr>
                                              </thead>
                                              <tbody>
                                                  <?php $no = 1 ?>

                                                  @foreach($dt_struc as $det)
                                                  <?php $count = explode("|", $det->area_city); ?>
                                                  <?php $team = explode("|", $det->id_team); ?>
                                                  <tr>
                                                      <td rowspan="{{ count($count)+1 }}" align="center">{{ $no }}</td>
                                                      
                                                  </tr>
                                                             
                                                    @for ($i = 0; $i < count($count ); $i++)
                                                              <tr >
                                                                   
                                                                   <td align="center" > {{ $count[$i] }}</td>
                                                                   <td align="center"><a class="btn bg-blue" href="{{url('/setting-shift')}}/{{ $count[$i] }}/{{ $project->id }}"> -></a> </td>
                                                              </tr>
                                                              @endfor
                                                         
                                                  <?php $no++; ?>
                                                   @endforeach
                                              </tbody>
                                          </table>
                                          </div>
                                          </div>
                                      </div>
                                      
                                   
                                       <div class="row clearfix">
                                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                           
                                          
                                          </div>
                                      <div class="row clearfix">
                                           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                              <div class="col-sm-5">
                                                  <a onclick="javascript:history.back()" class="btn btn-block bg-gray waves-effect" ><font size="4"> Back</font></a>
                                              </div>
                                              <div class="col-sm-5">
                                                   <a href="{{url('/all-project')}}"  class="btn btn-block bg-blue waves-effect" ><font size="4">Finish</font></a>
                                              </div>
                                          </div>
                                      </div>
                                      
                                  </div>
                                  </form>
                          </div>
        </div>
    </div>

    <form class="form-signin" method="POST" action="{{url('/add-structure')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalStructure" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Add Structure Project</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Structure Name <span class="mandatory">*</span></label>
                                <select name="structure_id" class="form-control" id="structure_id">
                                    <option value="">-select-</option>
                                   @foreach($structure as $str)
                                    <option value="{{ $str->id }}">{{ $str->title_structure }}</option>
                                    @endforeach
                                </select>
							                   	<span class="text-error" id="txt_structure_id"></span>
                                <br>
                             </div>
                             <br>
                             <div class="col-sm-12">
                                <br>
                                <input type="hidden" name="project_id" value="{{ $project->id }}">
                                <br>
                                <br>
                                 
                            </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer"> 
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim_add_structure_project"> Save</button>
						              <button type="button" class="btn btn-link bg-blue waves-effect" id="send_add_structure_project"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>


     <form class="form-signin" method="POST" action="{{url('/add-vacancy')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancy" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Add Job Vacancy</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Job Title Name <span class="mandatory">*</span></label>
                                    <select name="job_title" class="form-control" id="job_title">
                                        <option value="">-select-</option>
                                        @foreach($job as $jb)
                                            <option value="{{ $jb->id }}">{{ $jb->job_name }}</option>
                                        @endforeach
                                    </select>
									                   <span class="text-error" id="txt_job_title"></span>
                                 </div>
                             </div>
                             <div class="row clearfix">
							                   <br>
                                  <div class="col-sm-12">
                                    <label>Gender <span class="mandatory">*</span></label>
                                    <select name="gender" class="form-control" id="gender">
                                        <option value="">-select-</option>
                                        <option>All</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
								                    	<span class="text-error" id="txt_gender"></span>
                                 </div>
                             </div>
                             <div class="row clearfix">
							                    <br>
                                  <div class="col-sm-12">
                                    <label>Last Education <span class="mandatory">*</span></label>
                                    <select name="last_education" class="form-control" id="last_education">
                                        <option value="">-select-</option>
                                        <option>SD</option>
                                        <option>SMP</option>
                                        <option>SMA/SMK</option>
                                        <option>D1</option>
                                        <option>D2</option>
                                        <option>D3</option>
                                        <option>D4</option>
                                        <option>S1</option>
                                        <option>S2</option>
                                        <option>S3</option>
                                    </select>
									                   <span class="text-error" id="txt_last_education"></span>
                                 </div>
                             </div>

                             <div class="row clearfix">
							                     <br>
                                  <div class="col-sm-12">
                                    <label>Minimum Age <span class="mandatory">*</span></label>
                                    <input type="text" name="start_age" class="form-control" placeholder="Age Start" id="start_age" maxlength="2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="17">
									                     <span class="text-error" id="txt_start_age"></span>
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Maximum Age <span class="mandatory">*</span></label>
                                    <input type="text" name="end_age" class="form-control" placeholder="Age End" id="end_age" maxlength="2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="50">
								                      	<span class="text-error" id="txt_end_age"></span>
                                    <br>
                                 </div>
                             </div>
                             <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <label>Number Of Person <span class="mandatory">*</span></label>
                                    <input type="text" name="qty" class="form-control" placeholder="Qty People" id="qty" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
									                     <span class="text-error" id="txt_qty"></span>
                                 </div>
                             </div>

                             <div class="row clearfix">
                                     <div class="col-sm-12">
                                        <br>
                                        <label>Description <span class="mandatory">*</span></label>
                                        <textarea name="description" class="form-control" placeholder="Description Job Title" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" id="description" maxlength="200"></textarea>
                                        <span class="text-error" id="txt_description"></span>
									                       	<input type="hidden" name="project_id" value="{{ $project->id }}">
                                        <br>
                                        <br>
                                         
                                    </div>
                                </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim_add_vacancy"> Save</button>
						                  <button type="button" class="btn btn-link bg-blue waves-effect" id="send_add_vacancy"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        @foreach($vacancy as $vaca)
         <form class="form-signin" method="POST" action="{{url('/post-edit-vacancy')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalVacancyEdit{{ $vaca->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Edit Job Vacancy</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Job Title Name <span class="mandatory">*</span></label>
                                    <select name="job_title" class="form-control">
                                        <option value=" ">-select-</option>
                                        @foreach($job as $jb)
                                            <option value="{{ $jb->id }}" @if($vaca->interes_id === $jb->id) selected='selected' @endif>{{ $jb->job_name }}</option>
                                        @endforeach
                                    </select>
                                 </div>
                             </div>
                             <div class="row clearfix">
							                   <br>
                                  <div class="col-sm-12">
                                    <label>Gender <span class="mandatory">*</span></label>
                                    <select name="gender" class="form-control">
                                        <option>{{ $vaca->gender }}</option>
                                        <option>All</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                    </select>
                                 </div>
                             </div>
                             <div class="row clearfix">
					                 		 <br>
                                  <div class="col-sm-12">
                                    <label>Last Education <span class="mandatory">*</span></label>
                                    <select name="last_education" class="form-control">
                                        <option>{{ $vaca->last_education }}</option>
                                        <option>SD</option>
                                        <option>SMP</option>
                                        <option>SMA/SMK</option>
                                        <option>D1</option>
                                        <option>D2</option>
                                        <option>D3</option>
                                        <option>D4</option>
                                        <option>S1</option>
                                        <option>S2</option>
                                        <option>S3</option>
                                    </select>
                                 </div>
                             </div>

                             <div class="row clearfix">
						                 	 <br>
                                  <div class="col-sm-12">
                                    <label>Minimum Age <span class="mandatory">*</span></label>
                                    <input type="number" name="start_age" class="form-control" value="{{ $vaca->start_age }}" placeholder="Age Start">
                                 </div>
                             </div>
                             <div class="row clearfix">
						                	 <br>
                                  <div class="col-sm-12">
                                    <label>Maximum Age <span class="mandatory">*</span></label>
                                    <input type="number" name="end_age" value="{{ $vaca->end_age }}" class="form-control" placeholder="Age End">
                                 </div>
                             </div>
                             <div class="row clearfix">
						                	 <br>
                                  <div class="col-sm-12">
                                    <label>Number Of Person <span class="mandatory">*</span></label>
                                    <input type="number" name="qty" value="{{ $vaca->qty_people }}" class="form-control" placeholder="Qty People">
                                 </div>
                             </div>

                             <div class="row clearfix">
							                  <br>
                                     <div class="col-sm-12">
                                        <label>Description <span class="mandatory">*</span></label>
                                        <textarea name="description" class="form-control" placeholder="Description Job Title" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;">{{ $vaca->description }}</textarea>
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                        <input type="hidden" name="id" value="{{ $vaca->id }}">
                                        <br>
                                        <br>
                                         
                                    </div>
                                </div>
                            
                        </div>
                         
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        @endforeach
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>         
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
     <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
    	  
    			$("#send_add_structure_project").click(function(){
    				  if ($('#structure_id').val().length < 1) {
    						$('#txt_structure_id').text('Structure not selected');
    				  }else{
    						$('#txt_structure_id').text('');
    				  }
    				  
    				  if($('#structure_id').val().length > 0 ){
    						  $("#kirim_add_structure_project").click();
					  }
  			  
  			});
		  
      		   //start_age maximum length 2
      		  $('#start_age').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#start_age').val().length == 2)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on start_age*/
      			  $(document).on('paste', '#start_age', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 2));
      		   });
      		   
      		   //end_age maximum length 2
      		  $('#end_age').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#end_age').val().length == 2)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on end_age*/
      			  $(document).on('paste', '#end_age', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 2));
      		   });
      		   
      		    //qty maximum length 2
      		  $('#qty').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if ($('#qty').val().length == 3)
      					e.preventDefault();
      		  });
      		  
      		  /*handle paste with extra space on qty*/
      			  $(document).on('paste', '#qty', function(e) {
      				  e.preventDefault();
      				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      				  var text = $(this).val()+withoutSpaces;
      				  $(this).val(text.substring(0, 3));
      		   });
      		   
      		    //description cannot long space on input
      		  $('#description').keypress(function (e) {
      				var startPos = e.currentTarget.selectionStart;
      				if (e.which === 32 && startPos==0){
      					e.preventDefault();
      				}else if($('#description').val().length > 0){
      					$('#description').val($('#description').val().replace(/\s{2,}/g,' '));
      				}
      		  });
      	  
      	      /*handle paste with extra space on description*/
      		  $(document).on('paste', '#description', function(e) {
      			  e.preventDefault();
      			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
      			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
      			  var text = $(this).val()+withoutSpaces;
      			  $(this).val(text.substring(0, 200));
      		  });
      		  
      		  $("#send_add_vacancy").click(function(){
      			  
      				  if ($('#job_title').val().length < 1) {
      						$('#txt_job_title').text('Job title not selected');
      				  }else{
      						$('#txt_job_title').text('');
      				  }
      				  
      				  if ($('#gender').val().length < 1) {
      						$('#txt_gender').text('Gender not selected');
      				  }else{
      						$('#txt_gender').text('');
      				  }
      				  
      				  if ($('#last_education').val().length < 1) {
      						$('#txt_last_education').text('Last education not selected');
      				  }else{
      						$('#txt_last_education').text('');
      				  }
      				  
      				  if ($('#start_age').val().length < 1) {
      						$('#txt_start_age').text('Start age is empty');
      				  }else if ($('#start_age').val() == 0) {
      						$('#txt_start_age').text('Start age not entered');
      				  }else{
      						$('#txt_start_age').text('');
      				  }
      				  
      				  if ($('#end_age').val().length < 1) {
      						$('#txt_end_age').text('End age is empty');
      				  }else if ($('#end_age').val() == 0) {
      						$('#txt_end_age').text('End age not entered');
      				  }else{
      						$('#txt_end_age').text('');
      				  }
      				  
      				  if ($('#qty').val().length < 1) {
      						$('#txt_qty').text('Qty is empty');
      				  }else if ($('#qty').val() == 0) {
      						$('#txt_qty').text('Qty not entered');
      				  }else{
      						$('#txt_qty').text('');
      				  }
      				  
      				  if ($('#description').val().length < 1) {
      						$('#txt_description').text('Description is empty');
      				  }else{
      						$('#txt_description').text('');
      				  }
      				  
      				  if($('#job_title').val().length > 0 &&
      				     $('#gender').val().length > 0 &&
      					 $('#last_education').val().length > 0 &&
      					 ($('#start_age').val().length > 0 && $('#start_age').val() != '0') &&
      					 ($('#end_age').val().length > 0 && $('#end_age').val() != '0') &&
      					 ($('#qty').val().length > 0 && $('#qty').val() != '0') &&
      					 $('#description').val().length > 0){
      						  $("#kirim_add_vacancy").click();
      					  }
      			  
      			});
      		   
      	  
			$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
		  
          }); 
    </script>  

    

</section>
    

@endsection
