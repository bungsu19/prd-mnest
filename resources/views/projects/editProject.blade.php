@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
</style>
<br>
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -50px;">
		
			@if(session()->has('failed'))
            <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('failed') }}
            </div>
            @endif

             @if(session()->has('message'))
            <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('message') }}
            </div>
            @endif
		
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow">Edit Project</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('/post-edit-project')}}" enctype="multipart/form-data" autocomplete="off" name="comboForm" >
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Project Name<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                    <input type="text" name="project_name" class="form-control" placeholder="Project Name" value="{{ $project->project_name }}" id="project_name" maxlength="50">
                                                </div>
												
												<span class="text-error" id="txt_project_name"></span>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Client<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                   <select name="client_id" class="form-control" id="client_id">
                                                     @foreach($client as $cl)
                                                        <option value="{{ $cl->client_id }}" @if($project->client_id === $cl->client_id) selected='selected' @endif> {{ $cl->company_name }}</option>
                                                    @endforeach
                                                   </select>
                                                </div>
												
												<span class="text-error" id="txt_client_id"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                   <select name="branch_id" class="form-control" id="branch_id">
                                                     @foreach($branch as $br)
                                                       <option value="{{ $br->id }}" @if($project->branch_id === $br->id) selected='selected' @endif > {{ $br->branch_name }}</option>
                                                    @endforeach
                                                   </select>
                                                </div>
												
												<span class="text-error" id="txt_branch_id"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contact Number<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                  <input type="text" name="contact_number" class="form-control" placeholder="Contract Number" value="{{ $project->project_contract_no }}" id="contact_number" maxlength="15" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                </div>
												
												<span class="text-error" id="txt_contact_number"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Periode<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="input-daterange input-group" id="bs_datepicker_range_container">
                        												<div class="form-line">
                        													<input type="text" name="project_start" class="form-control" placeholder="Project Start.." value="<?php echo date('d-m-Y', strtotime($project->project_start)); ?>" id="project_start">
                        												</div>
                        												<span class="text-error" id="txt_project_start"></span>
                        												<span class="input-group-addon" style="margin-left:10px;">to</span>
                        												<div class="form-line">
                        													<input type="text" name="project_end" class="form-control" placeholder="Project End..." value="<?php echo date('d-m-Y', strtotime($project->project_end)); ?>" id="project_end">
                        												</div>
                        												<span class="text-error" id="txt_project_end"></span>
                        											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Project Value<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                  <input type="text" name="project_value" class="form-control" placeholder="Project Value" id="project_value" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="14">
                                                </div>
												
												<span class="text-error" id="txt_project_value"></span>
												
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Description<font color="red"> *</font></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                  <textarea name="description" class="form-control"  placeholder=" Description Project" id="description" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200">{{ $project->project_desc }}</textarea>
                                                  <input type="hidden" name="id" value="{{ $project->id }}">
                                                </div>
												
												<span class="text-error" id="txt_description"></span>
												
                                            </div>
                                        </div>
                                    </div>
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('all-project/edit-project')}}/{{ $project->id }}" class="btn btn-block bg-gray waves-effect" ><font size="4">Back</font></a>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="submit" class="btn bg-blue waves-effect" style="display: none;" id="kirim"><font size="4">Update</font></button>
												<button type="button" class="btn bg-blue waves-effect" id="send"><font size="4">Update</font></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
	<script type="text/javascript">
		$(document).ready(function() {
		  $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  return false;
			}
		  });
		}); 
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>      
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
     $(function(){
		 var idr = '{{ $project->project_value }}';
	  $("#project_value").val(formatRupiah(idr, ''));
	  
      
	  
	  function formatRupiah(angka, prefix){
			  var number_string = angka.replace(/[^,\d]/g, '').toString(),
			  split       = number_string.split(','),
			  sisa        = split[0].length % 3,
			  rupiah        = split[0].substr(0, sisa),
			  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
		 
			  if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			  }
		 
			  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			  return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
			}
	  
     });
    </script>

     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
	  
	  $("#bs_datepicker_range_container").datepicker({
			  format: 'dd-mm-yyyy',
			  minDate:0,
			  autoclose: true,
			  todayHighlight: false,
		  });
	  
    }); 
    </script>  
	
	<script type="text/javascript">
     $(function(){
		  
		  //project_name cannot long space on input
		  $('#project_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#project_name').val().length > 0){
					$('#project_name').val($('#project_name').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on project_name*/
		  $(document).on('paste', '#project_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  //contact_number maximum length 15
		  $('#contact_number').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#contact_number').val().length == 15)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on contact_number*/
			  $(document).on('paste', '#contact_number', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  $(this).val(text.substring(0, 15));
		   });
		   
		    //project_value maximum length 14
		   $('#project_value').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#project_value').val().length == 14)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on project_value*/
			  $(document).on('paste', '#project_value', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  var text = $(this).val()+withoutSpaces;
				  $(this).val(formatRupiah(text.substring(0, 14))); 
		   });
		   
		   $('#project_value').keyup(function() {
				$('#project_value').val(formatRupiah($('#project_value').val(), ''));
		  });
		   
		   function formatRupiah(angka, prefix){
			  var number_string = angka.replace(/[^,\d]/g, '').toString(),
			  split       = number_string.split(','),
			  sisa        = split[0].length % 3,
			  rupiah        = split[0].substr(0, sisa),
			  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
		 
			  if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			  }
		 
			  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			  return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
			}
			
			//description cannot long space on input
		  $('#description').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#description').val().length > 0){
					$('#description').val($('#description').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space on description*/
			  $(document).on('paste', '#description', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  $(this).val(text.substring(0, 200));
		   });
		   
		   $("#send").click(function(){
			  if ($('#project_name').val().length < 1) {
					$('#txt_project_name').text('Project name is empty');
			  }else{
					$('#txt_project_name').text('');
			  }
			  
			  if ($('#client_id').val().length < 1) {
					$('#txt_client_id').text('Client not selected');
			  }else{
					$('#txt_client_id').text('');
			  }
			  
			  if ($('#branch_id').val().length < 1) {
					$('#txt_branch_id').text('Branch not selected');
			  }else{
					$('#txt_branch_id').text('');
			  }
			  if ($('#contact_number').val().length < 1) {
					$('#txt_contact_number').text('Contact number is empty');
			  }else if ($('#contact_number').val() == 0) {
					$('#txt_contact_number').text('Contact number not entered');
			  }else{
					$('#txt_contact_number').text('');
			  }
			  
			  if ($('#project_start').val().length < 1) {
					$('#txt_project_start').text('Project start is empty');
			  }else{
					$('#txt_project_start').text('');
			  }
			  
			  if ($('#project_end').val().length < 1) {
					$('#txt_project_end').text('Project end is empty');
			  }else{
					$('#txt_project_end').text('');
			  }
			  
			  if ($('#project_value').val().length < 1) {
					$('#txt_project_value').text('Project value is empty');
			  }else if ($('#project_value').val() == 0) {
					$('#txt_project_value').text('Project value not entered');
			  }else{
					$('#txt_project_value').text('');
			  }
			  
			  if ($('#description').val().length < 1) {
					$('#txt_description').text('Description is empty');
			  }else{
					$('#txt_description').text('');
			  }
			  
			  
			  if(
				 $('#project_name').val().length > 0 &&
				 $('#client_id').val().length > 0 &&
				 $('#branch_id').val().length > 0 &&
				 ($('#contact_number').val().length > 0 && $('#contact_number').val() != '0') &&
				 $('#project_start').val().length > 0 &&
				 $('#project_end').val().length > 0 &&
				 $('#project_value').val().length > 0 &&
				 $('#description').val().length > 0){
					  $("#kirim").click();
				  }
				  
			});
		  
	 });
	 </script>

</section>
    

@endsection
