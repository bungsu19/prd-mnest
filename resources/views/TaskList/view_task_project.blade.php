@extends('layout-menu.app')

@section('content')
<style>
.nav-tabs li.active a{
		color: #337ab7 !important; 
		background-color: white !important; 
	}
	
	.nav > li > a:hover {
		color: #337ab7 !important; 
		
</style>

   <br>
   
<section class="content">
            
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -50px;">
                    <div class="card" >
                        <div class="header">
                            <h3 align="left" style="margin-top: -5px;">
                                <u>Task Project Information</u>
                            </h3>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix" style="margin-bottom: -10px;">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                        </div>
						
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                       
                                        <li role="presentation" id="tasklist">

                                            <a href="#job_vacancy" data-toggle="tab" class="btn bg-blue">Task Lisk Project</a>
                                        </li>
                                        <li role="presentation" id="absensi">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Attandance Time</a>
                                          
                                        </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="job_vacancy">
										<div class="body">
											  <div class="row">
													<div class="col-sm-4">
														<h4>Task List Project</h4>
                                                    </div>
											  </div>
                                              <div class="row clearfix">
                                                 <form class="form-signin" method="GET" action="{{url('/filter-tasklist')}}" autocomplete="off">
                                                    {{ csrf_field() }}
                                                    <div class="col-sm-2">
														<select name="people_id_tasklist" class="form-control">
                                                            <option value="all" {{ ( $people_id_tasklist == 'all') ? 'selected' : '' }} >All</option>
															<option value="name" {{ ( $people_id_tasklist == 'name') ? 'selected' : '' }} >Name</option>
															<option value="task_list" {{ ( $people_id_tasklist == 'task_list') ? 'selected' : '' }} >Task Number</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <input type="text" name="value" class="form-control" placeholder="Value" id="project_start" value="{{ $value }}">
                                                    </div>
                                                    <div class="col-sm-4">
                      														<div class="input-daterange input-group" id="bs_datepicker_range_container">
                      															<div class="form-line">
                      																<input type="text" name="dateFromTasksList" class="form-control" placeholder="Project Start.." id="project_start" value="{{ $dateFromTasksList }}">
                      															</div>
                      															<span class="input-group-addon" style="margin-left:10px;">to</span>
                      															<div class="form-line">
                      																<input type="text" name="dateToTasksList" class="form-control" placeholder="Project End..." id="project_end" value="{{ $dateToTasksList }}">
                      															</div>
                      														</div>
                      														<input type="hidden" name="project_id" value="{{ $project->id }}">
													                                                       													
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <h4></h4>
                                                         <button type="submit" value="filter" name="submit"  class="btn bg-blue waves-effect m-r-20">Search Task List</button>
                                                    </div>
                                                </form>
                                              </div>
                                            </div>
                                         <div class="table-responsive">
                                                 <table class="table table-bordered table-striped table-hover ">
                                                    <thead>
                                                        <tr>
                                                            <td class="bg-column" width="40"><font color="yellow"> No</font></td>
                                                            <td class="bg-column" ><font color="yellow">People</font></td>
                                                            <td class="bg-column" ><font color="yellow">Shift</font></td>
                                                            <td class="bg-column" ><font color="yellow">Outlet Name</font></td>
                                                            <td class="bg-column" ><font color="yellow"> Sent Date</font></td>
                                                            <td class="bg-column" ><font color="yellow">Status Task</font></td>
                                                            <td class="bg-column" ><font color="yellow">Task Info</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
														<?php 
														$no = 0;
														if($task_people->currentPage() == 1){
															$no = 1;
														}else{
														  $no = ($task_people->currentPage()-1) * $jumlahData + 1;
														}
														?>
                                    @foreach($task_people as $task)
                                     <?php $count = explode("|", $task->outlet_name); ?>
                                     <?php $outlet = explode("|", $task->outlet_id); ?>
                                      <?php $number = explode("|", $task->task_number); ?>
                                        <tr>
                                            <td  align="center" rowspan="{{ count($number)+1 }}"  >{{ $no }}</td>
                                            <td rowspan="{{ count($number)+1 }} ><a href="{{url('data-people/view-data')}}/{{ $task->people_id }}">{{ $task->first_name }} {{ $task->middle_name }} {{ $task->surname }}</a></td>
                                            <td rowspan="{{ count($number)+1 }}">  @if($task->code_shift == 1)
                                              Shift I
                                            @elseif($task->code_shift == 2)
                                              Shift II
                                            @elseif($task->code_shift == 3)
                                              Shift III
                                            @else

                                            @endif
                                          </td>
                                             <td rowspan="{{ count($number)+1 }}  align="center" >{{ $task->outlet_name }}</td>
                                             <td rowspan="{{ count($number)+1 }} align="center">{{ date('d-M-Y', strtotime($task->schedule_date)) }}</td>
                                             
                                             <td rowspan="{{ count($number)+1 }} align="center">
                                              @if($task->delete_flag == 1)
                                                Aktif
                                              @else
                                                Non Aktif
                                              @endif
                                               </td>
                                              
                                              

                                              @for ($i = 0; $i < count($number ); $i++)
                                                    <tr >
                                                         <td style="border: none;"> {{ $number[$i] }}</td>
                                                         <td align="center">
														                               <a href="{{url('absensi-project')}}/{{ $task->id_task }}/{{ $task->schedule_date }}/{{ $task->project_id }}" title="View Task list"><i class="material-icons">remove_red_eye</i>
                                                          </a>
                                                          @if($task->delete_flag == 1)
                                                            <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete Task List ?" data-href="{{url('delete-tasklist')}}/{{ $task->id_task }}" title="Delete Data">
                                                        <i class="material-icons">delete_forever</i>
                                                      </a>
                                                          @else

                                                          @endif
                                                          
                                                        </td>
                                                    </tr>
                                                        @endfor
                                      
                                            
                                           
                                            
                                        </tr>
                                        <?php $no++; ?>
                                    @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                               <h1><b></b></h1>
                                                </div>
                                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
													 {{ $paginator->links('paginator.default') }}
													 
                                                </div>
                                                
                                            </div>
                                         </div>

                                         <div role="tabpanel" class="tab-pane animated fadeInRight" id="personal_data">
                                            <div class="body">
											<div class="row">
													<div class="col-sm-4">
														<h4>Attandance Time</h4>
                                                    </div>
											  </div>
                                              <div class="row clearfix">
                                                 <form class="form-signin" method="GET" action="{{url('/filter-absensi')}}" autocomplete="off">
                                                    {{ csrf_field() }}
                                                    <div class="col-sm-2">

                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select name="people" class="form-control">
                                                            <option value="all" @if($people_id == 'all') 'selected="selected"' @endif>-All-</option>
                                                            @foreach($member as $me)
                                                            <option value="{{ $me->employee_id }}" {{ ( $me->employee_id == $people_id) ? 'selected' : '' }} >{{ $me->first_name }} {{ $me->middle_name }} {{ $me->surname }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
													<div class="col-sm-4">
														<div class="input-daterange input-group" id="bs_datepicker_range_container2">
															<div class="form-line">
																<input type="text" name="dateFrom" class="form-control" placeholder="Project Start.." id="project_start" value="{{ $dateFrom }}">
															</div>
															<span class="input-group-addon" style="margin-left:10px;">to</span>
															<div class="form-line">
																<input type="text" name="dateTo" class="form-control" placeholder="Project End..." id="project_end" value="{{ $dateTo }}">
															</div>
														</div>
														<input type="hidden" name="project_id" value="{{ $project->id }}">
													                                                       													
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <h4></h4>
                                                         <button type="submit" value="filter" name="submit"  class="btn bg-blue waves-effect m-r-20">Search People</button>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <h4></h4>
                                                        <button type="submit" value="download" name="submit" class="btn bg-blue waves-effect m-r-20">Download</button>
                                                    </div>
                                                </form>
                                              </div>
                                            </div>
                                               <div class="table-responsive">
                                                     <table class="table">
                                                        <thead>
                                                            <tr>
                                                               <td class="bg-column" width="20"><font color="yellow"> No</font></td>
                                                               <td class="bg-column" align="center"><font color="yellow">Employe Name</font></td>
                                                               <td class="bg-column" align="center"><font color="yellow">Date</font></td>
                                                               <td class="bg-column" align="center"><font color="yellow">Check In</font></td>
                                                               <td class="bg-column" align="center"><font color="yellow">Check Out</font></td>
															   <td class="bg-column" align="center"><font color="yellow">Image Absensi In</font></td>
                                                               <td class="bg-column" align="center"><font color="yellow">Maps</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
															<?php 
															$no = 0;
															if($absensi->currentPage() == 1){
																$no = 1;
															}else{
															  $no = ($absensi->currentPage()-1) * $jumlahData + 1;
															}
															?>
                                                            @foreach($absensi as $absen)
                                                            <tr>
                                                                <td align=" center"> {{ $no }}</td>
                                                                <td align="center"><a href="{{url('data-people/view-data')}}/{{ $absen->people_id }}"> {{ $absen->first_name }} {{ $absen->middle_name }} {{ $absen->surname }} </a></td>
                                                                <td align="center">{{ $absen->date_absensi }}</td>
                                                                <td align="center">{{ $absen->absensi_in }}</td>
                                                                <td align="center">{{ $absen->absensi_out }}</td> 
																<td align="center"><img id="myImg" class="p-image" src="{{ URL::to('absensi_people/'.$absen->image) }}" alt="" width="50px"> </img></td>
                                                                <td align="center"><a href="#ModalImage" data-toggle="modal" data-maps-in-latt="{{ $absen->in_latitude }}" data-maps-in-long="{{ $absen->in_langitude }}" data-maps-out-latt="{{ $absen->out_latitude }}" data-maps-out-long="{{ $absen->out_langitude }}"> <i class="material-icons">pin_drop</i></a>
                                                                  
                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                            @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                   <h1><b></b></h1>
                                                    </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
														 <!--{{ $absensi->appends(['task_people' => $task_people->currentPage()])->links() }}-->
														 {{ $absensi->links('paginator.default') }}
														 													  
                                                    </div>
                                                    </div>
                                         </div>

                                    </div>

                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

            <div class="modal fade" id="ModalImage" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Maps</font></h2>
                        </div>
                        
                          <div class="body">
                           <div id="map" class="gmap"></div>
                            </div>
                        <div class="modal-footer">
                            <br>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
          <div class="modal-footer">
             <a class="btn-ok"> 
              <button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
            </a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
          </div>
                </div>
            </div>
        </div>
			

    
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
		<script type="text/javascript">
     $(function(){
	
	$("#bs_datepicker_range_container").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
	  
	  $("#bs_datepicker_range_container2").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
	});
    </script>
		<script>
		$(document).ready(function() {
			$('#ModalImage').on('show.bs.modal', function(e) {
				var inLatt = $(e.relatedTarget).data('maps-in-latt');
				var inLong = $(e.relatedTarget).data('maps-in-long');
				var outLatt = $(e.relatedTarget).data('maps-out-latt');
				var outLong = $(e.relatedTarget).data('maps-out-long');
			
				var myLatLng = {lat: inLatt, lng: inLong };
				var map = new google.maps.Map(document.getElementById('map'), {
				  zoom: 10,
				  center: myLatLng,
               styles: [ 
                      {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                      {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#38414e'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#212a37'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#9ca5b3'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                      },
                      {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                      },
                      {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#17263c'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#515c6d'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#17263c'}]
                      }
                  ]
            });
			
			
            var markerin, markerout;
  
            markerin = new google.maps.Marker({
            position: new google.maps.LatLng(inLatt, inLong),
            title : 'Absensi Masuk',
            map: map
            });
  
            markerout = new google.maps.Marker({
            position: new google.maps.LatLng(outLatt, outLong),
            title : 'Absensi Pulang',
            map: map
            });
         
			
			});
		}); 
		</script>
		
		<script type="text/javascript">
		$(document).ready(function() {
		  activaTab('<?php echo $active; ?>');
			  function activaTab(tab){
				$('.nav-tabs a[href="#' + tab + '"]').tab('show');
			  };
		}); 
		</script> 
		
        <script>
            var modal = document.getElementById('myModal');
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            var span = document.getElementsByClassName("close")[0];
            span.onclick = function() { 
                modal.style.display = "none";
            }

        </script>
		
         @section('script')
         <script type="text/javascript">
         $(function(){
          $(".datepicker1").datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              todayHighlight: true,
          });
              });
        </script>

         <script type="text/javascript">
         $(function(){
          $(".datepicker").datepicker({
              format: 'dd-mm-yyyy',
              autoclose: true,
              todayHighlight: true,
          });
              });
        </script>
		
		<script type="text/javascript">
			$(function(){
				  $(".datepickerFrom").datepicker({
					  format: 'dd-mm-yyyy',
					  autoclose: true,
					  todayHighlight: true,
				  });
				  
				  $(".datepickerTo").datepicker({
					  format: 'dd-mm-yyyy',
					  autoclose: true,
					  todayHighlight: true,
				  });

          $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
          var message = $(e.relatedTarget).data('message');
          $(e.currentTarget).find('.message').text(message);
          $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
			});
        </script>

        <script src="http://maps.googleapis.com/maps/api/js"></script>
       

         <script async defer
                 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHCxvsKpe_XnzcU5UxBH_Hfyv3Gg8iFhg&callback=initMap">
        </script>

        @endsection
        
    </section>
    

@endsection
