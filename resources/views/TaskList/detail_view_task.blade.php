@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <!-- <div class="block-header">
                <h2>
                   All Product
                </h2>
                 
            </div> -->
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                Detail Task Project
                            </h1>
                            <label>Project Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_name }} </i></small> <br>
                            <label>Project Description &nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_desc }} </i></small><br>

                            <label>Contract Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $project->project_contract_no }} </i></small>  <br>

                            <label>Periode &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;({{ $project->project_start }}) To ({{ $project->project_end }}) </i></small> <br>
                            <label>Project Value &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;<b>Rp.</b><i>{{ number_format($project->project_value,0) }}</i> </i></small>  
                            
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                       
                                        <li role="presentation" class="active">

                                            <a href="#job_vacancy" data-toggle="tab" class="btn bg-blue">Task Lisk Project</a>
                                        </li>
                                        
                                       

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="job_vacancy">
                                         <div class="table-responsive">
                                                 <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                                    <thead>
                                                        <tr>
                                                            <td class="bg-column" width="20"><font color="yellow"> No</font></td>
                                                            <td class="bg-column"><font color="yellow">Outlet Name </font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Start Task</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">End Task</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">IN</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Out</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach($detail_task as $detail)
                                                            <tr>
                                                                <td align="center">{{ $no }}</td>
                                                                <td> {{ $detail->outlet_name }}</td>
                                                                <td align="center">{{ $detail->task_start }}</td>
                                                                <td align="center">{{ $detail->task_end }}</td>
                                                                <td></td>
                                                                <td></td>
                                                                <td align="center"> <a href=""><i class="material-icons">pin_drop</i></a></td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="structure_project">
                                            <div class="table-responsive">
                                            <table class="table table-hover dashboard-task-infos">
                                                <thead>
                                                            <tr>
                                                                <td class="bg-column" align="center" width="20"><font color="yellow">No</font></td>
                                                                <td class="bg-column"><font color="yellow">Area Name</font></td>
                                                                <td class="bg-column"><font color="yellow">District/city</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                     <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">City</font></td>
                                                    <td class="bg-column"><font color="yellow">Name Team</font></td>
                                                    <td class="bg-column"><font color="yellow">Job Vacancy</font></td><!-- 
                                                    <td class="bg-column"><font color="yellow">Qty People</font></td> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                
                                            </tbody>
                                        </table>
                                    </div>
                            </div>

                         
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="personal_data">
                                   <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">Name People</font></td>
                                                    <td class="bg-column"><font color="yellow">Name Team</font></td>
                                                    <td class="bg-column"><font color="yellow">Gender</font></td>
                                                    <td class="bg-column"><font color="yellow">District</font></td>
                                                    <td class="bg-column"><font color="yellow">Job</font></td>
                                                    <td class="bg-column"><font color="yellow">Status</font></td>
                                                    <td class="bg-column"><font color="yellow">Reason</font></td>
                                                    <td class="bg-column"><font color="yellow">Action</font></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

     

    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
    </section>
    

@endsection
