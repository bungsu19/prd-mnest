<?php
 
 header("Content-type: application/vnd.ms-excel");
 
 header("Content-Disposition: attachment; filename=Report Absensi.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
<body style="border-style: ridge; border-width: 0.6px; border-color: #dcdcdc;">
<table style="border: 0.6px solid black;">
    <tr style="border: 0.6px solid black;">
        <th bgcolor="#FFFF00" height="40" >No</th>
        <th bgcolor="#FFFF00" height="40" >Nama People</th>
        <th bgcolor="#FFFF00" height="40" >Shift</th>
        <th bgcolor="#FFFF00" height="40" >Default Masuk</th>
        <th bgcolor="#FFFF00" height="40" >Area Project</th>
        <th bgcolor="#FFFF00" height="40" >Nama Team</th>
        <th bgcolor="#FFFF00" height="40" >Tanggal Absensi</th>
        <th bgcolor="#FFFF00" height="40" >Absen Masuk</th>
        <th bgcolor="#FFFF00" height="40" >Absen Pulang</th>
        <th bgcolor="#FFFF00" height="40" >Total Jam Kerja</th>
        <th bgcolor="#FFFF00" height="40" >Total Telat</th>
        <th bgcolor="#FFFF00" height="40" >Nama Outlet</th>
        <th bgcolor="#FFFF00" height="40" >Masuk Outlet</th>
        <th bgcolor="#FFFF00" height="40" >Keluar Outlet</th>
        <th bgcolor="#FFFF00" height="40" >Status Absensi</th>
        
    </tr>
    
    <?php $no=1; ?>
    @foreach($absensi as $post)

    <?php $outlet = explode('|', $post->outlet_name); ?>
    
    <?php $in_outlet = explode('|', $post->in_outlet); ?>
    <?php $out_outlet = explode('|', $post->out_outlet); ?>
    <?php $jam_masuk = explode('|', $post->hour_in); ?>
        <tr style="border: 0.6px solid black;">
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $no }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->first_name }} {{ $post->middle_name }}  {{ $post->surname }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->code_shift }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->hour_in }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->district_project }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->name_team }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->date_absensi }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->in_absensi }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">{{ $post->out_absensi }}</td>
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">
                    <?php if($post->out_absensi != null){
                        $a = strtotime($post->in_absensi);
                        $b = strtotime($post->out_absensi);
                        $total = $b - $a;
                        $masuk = floor($b / (60 * 60));
                        $hours = floor($total / (60 * 60));
                        $minutes = $total - $hours * (60 * 60);
                        // echo $hours.' Jam '.floor( $minutes / 60 ).' menit '; 
                        if ($post->code_shift != 3) {
                            echo $hours.' Jam '.floor( $minutes / 60 ).' menit '; 
                        }else{
                            echo ($hours + 24).' Jam '.floor( $minutes / 60 ).' menit '; 
                        }
                    } ?>
                
            </td>

            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">
                    <?php if($post->code_shift != null){
                        $masuk = strtotime($post->in_absensi);
                        $telat = strtotime($post->hour_in);
                        $total_telet = $masuk- $telat;
                        $hours = floor($total_telet / (60 * 60));
                        $minutes = $total_telet - $hours * (60 * 60);
                        //echo $hours.' Jam '.floor( $minutes / 60 ).' menit '; 
                        if ($hours > 0) {
                            echo $hours.' Jam '.floor( $minutes / 60 ).' menit '; 
                        }else{
                            echo ''; 
                        }
                    } ?>
                
            </td>
          
            <td>
				<table style="border: 0.6px solid black;">
                        <?php for ($i = 0; $i < count($outlet); $i++){ ?>
                          
                            
                            <tr style="border: 0.6px solid black;">
                                <td> <?php echo  $outlet[$i];  ?></td>
                            </tr> 
                           
                        <?php } ?>  
				</table>
            </td>
			<td>
				<table style="border: 0.6px solid black;">
                        <?php for ($i = 0; $i < count($in_outlet); $i++){ ?>
                          
                            
                            <tr style="border: 0.6px solid black;">
                                <td> <?php if($in_outlet[$i] == "00:00:00"){
                                        echo "";
                                    }else{
                                        echo $in_outlet[$i]; 
                                    }    ?>
							</td>
                            </tr> 
                           
                        <?php } ?>  
				</table>
            </td>
			<td>
				<table style="border: 0.6px solid black;">
                        <?php for ($i = 0; $i < count($out_outlet); $i++){ ?>
                          
                            
                            <tr style="border: 0.6px solid black;">
                                <td> 
                                    <?php if($out_outlet[$i] == "00:00:00"){
                                        echo "";
                                    }else{
                                        echo $out_outlet[$i]; 
                                    }    ?>
                                            
                                </td>
                            </tr> 
                           
                        <?php } ?>  
				</table>
            </td>
            
            <td rowspan="@if(count($outlet) > 1) count($outlet) - 1; @else count($outlet); @endif">
                @if($post->in_absensi != null && $post->out_absensi != null)
                    1
                @else
                    0
                @endif
            </td>

             
            

        </tr>
        <?php $no++; ?>
    @endforeach
</table>
</body>