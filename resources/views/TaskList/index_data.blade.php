@extends('layout-menu.app')

@section('content')

   
<section class="content">
                @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card" style="margin-top: -30px;">
                        <div class="header">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h3 style="margin-top: -5px;"><b> Task Projects</b></h3>
                            </div>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             <!-- <a href="{{url('/add-project')}}" class="btn bg-blue waves-effect m-r-20">New Project</a> -->
                              <!--<a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>-->
                            
                            </div>
                            <div class="row clearfix"></div>
						</div>
						<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                       <tr>
                                            <td class="bg-column" align="center"><font color="yellow"> No</font></td>
                                            <td class="bg-column"><font color="yellow">Status</font></td>
                                            <td class="bg-column"><font color="yellow">Name</font></td>
                                            <td class="bg-column"><font color="yellow">Client</font></td>
                                            <td class="bg-column" style="width: 110px;"><font color="yellow">Start</font></td>
                                            <td class="bg-column" style="width: 110px;"><font color="yellow">End</font></td>
                                            <td class="bg-column" align="center" style="width: 100px;"><font color="yellow" >Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                            $no = 0;
                                            if($data->currentPage() == 1){
                                                $no = 1;
                                            }else{
                                              $no = ($data->currentPage()-1) * $jumlahData + 1;
                                            }
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td>
                                                <?php 
                                                    $todayDate = date("Y-m-d");
                                                    $datetime1 = new DateTime($todayDate);
                                                    $datetime2 = new DateTime($da->project_end);
													$datetime2->add(new DateInterval('P1D'));
                                                    $interval = $datetime1->diff($datetime2);
                                                    $count = $interval->format('%R%a');
                                                    $countDown = '';
                                                    
                                                    if($count > 7){
                                                        $countDown = '';
                                                    }elseif($count >= 0 && $count <= 7){
                                                        $countDown = '('.$count.' days)';
                                                    }else{
                                                        $countDown = '';
                                                    }                               
                                                ?>
                                                @if($todayDate >= $da->project_start && $todayDate < $datetime2)
                                                    
                                                    @if($count > 0 && $count <= 7)
                                                        <span class="label" style="background-color: #FA8072">End in to <?php echo $countDown; ?> </span>
                                                    @else
                                                        <span class="label label-success">On-Going</span>
                                                    @endif
                                                    
                                                @elseif( $todayDate >= $da->project_start && $todayDate >= $datetime2)
                                                
                                                    @if($da->active_flag == 1)
                                                        <span class="label" style="background-color: #FF0000">Finish </span>
                                                    @else
                                                        <span class="label" style="background-color: #FF0000">Done</span>
                                                    @endif
                                                    
                                                @else
                                                    
                                                    <span class="label label-warning">Ready</span>
                                                    
                                                @endif
                                                    
                                                </td>
                                                <td>{{ $da->project_name }}</td>
                                                <td>{{ $da->client }}</td>
                                                <td>{{ date('d-M-Y', strtotime($da->project_start)) }}</td>
                                                <td>{{ date('d-M-Y', strtotime($da->project_end)) }}</td>
                                                <td align="center">
													@if( $todayDate >= $da->project_start && $todayDate <= $datetime2)
														@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
                                                            @if($da->active_flag == 1)
    															<a href="{{url('task-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Create Task List">
    															   <i class="material-icons">assignment</i>
    															</a>
                                                            @else

                                                            @endif
														@endif
                                                     <a href="{{url('view-task-people')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                      <i class="material-icons">remove_red_eye</i>
                                                    </a>
                                                    @else
													@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
                                                        @if($da->active_flag == 1)	
    														<a href="{{url('task-project')}}/{{ $da->id }}" data-toggle="tooltip" title="Create Task List">
    														   <i class="material-icons">assignment</i>
    														</a>
                                                        @else

                                                        @endif
													@endif
                                                    <a href="{{url('view-task-people')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                      <i class="material-icons">remove_red_eye</i>
                                                    </a>

													@endif
                                                   
                                                 </td>
                                                </tr>
                                            <?php $no++; ?>
                                            @endforeach
                                    </tbody>
                                </table>
                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                   <h1><b></b></h1>
                                    </div>
                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                        {{ $paginator->links('paginator.default') }}
                                    </div>
                               
                            </div>
					</div>
				</div>
			</div>
          
            <!-- #END# Basic Examples -->

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                     <h3>Delete</h3>
                </div>
                <div class="modal-body">
                    <p>You are about to delete.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                  <a href="#" id="btnYes" class="btn danger">Yes</a>
                  <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
                </div>
            </div>
        


    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
</script>

<script type="text/javascript">
    $('#myModal').on('show', function() {
    var id = $(this).data('id'),
        removeBtn = $(this).find('.danger');
    });

    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();

        var id = $(this).data('id');
        $('#myModal').data('id', id).modal('show');
    });

    $('#btnYes').click(function() {
        // handle deletion here
        var id = $('#myModal').data('id');
        $('[data-id='+id+']').remove();
        $('#myModal').modal('hide');
    });

</script>
@endsection
    

@endsection
