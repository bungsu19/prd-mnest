@extends('layout-menu.app')

@section('content')
<style type="text/css">
  .text-error
  {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
      margin-top: 5px;
    }
</style>

<br>
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -40px;">
                    <div class="card" style="margin-top: -10px;">
                        <div class="header">
                            <h3 align="left" style="margin-top: -5px;">
                                <u>Project Information</u>
                            </h3>
                            <div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix" style="margin-bottom: -10px;">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                    <div class="card">
						<div class="header">
                             <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                               <h1><b>Task List People</b></h1>
                            </div>
                           <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" style="text-align: right;">
								<br>
								@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
									<button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalInvite">New Task Group</button>
                                @endif
								<a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->
								<form class="form-signin" method="GET" action="{{url('/filter-task-project')}}" autocomplete="off">
								{{ csrf_field() }}
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
										<div class="card" style="margin: 0px;">
											
											<div class="body">
												<div class="row clearfix">
													
													<div class="col-sm-4">
														<label>Filter by</label>
														<select name="filterby" class="form-control show-tick">
															<option value="0">All</option>
															<option value="1">Name People</option>
															<option value="2">Job Name</option>
														</select>
													</div>
													<div class="col-sm-6">
														<label>Value</label>
														<input type="text" name="data" class="form-control" placeholder="Value" >
														<input type="hidden" name="id" value="{{ $project->id }}">
													</div>
													<div class="col-sm-2">
														 <button type="submit" name="search" class="btn bg-blue waves-effect m-r-20" style="margin-top:25px;" value="search-project">Search</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</form>
                        </div>
						
						@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4 || Auth::user()->hak_access == 5)
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <td class="bg-column"><font color="yellow"> No</font></td>
                                                    <td class="bg-column"><font color="yellow">Name People</font></td>
                                                    <td class="bg-column"><font color="yellow">Name Team</font></td>
                                                    <td class="bg-column"><font color="yellow">Gender</font></td>
                                                    <td class="bg-column"><font color="yellow">District</font></td>
                                                    <td class="bg-column"><font color="yellow">Job</font></td>
                                                    <td class="bg-column"><font color="yellow">Outlet Name</font></td>
                                                   
                                                    <td class="bg-column"><font color="yellow">Action</font></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php 
                                                    $no = 0;
                                                    if($member->currentPage() == 1){
                                                        $no = 1;
                                                    }else{
                                                      $no = ($member->currentPage()-1) * $jumlahData + 1;
                                                    }
                                                ?>
                                                @foreach($member as $mem)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td><a href="{{url('data-people/view-data/')}}/{{ $mem->employee_id }}">{{ $mem->first_name }} {{ $mem->middle_name }} {{ $mem->surname }} </a></td>
                                                        <td>{{ $mem->name_team }}</td>
                                                        <td>{{ $mem->gender }}</td>
                                                        <td>{{ $mem->distict }}</td>
                                                        <td>{{ $mem->job_name }}</td>
                                                        <td>{{ $mem->outlet_name }}</td>
                                                       
                                                        <td>
                                                            <a href="{{url('task-people')}}/{{ $mem->id }}" class="btn bg-blue"> Action</a>
                                                        </td>

                                                    </tr>
                                                <?php $no++; ?>
                                                @endforeach
                                               
                                            </tbody>
                                        </table>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <h1><b></b></h1>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                {{ $paginator->links('paginator.default') }}
                                        </div>
                            </div>
						@endif
                    </div>

         <form class="form-signin" method="POST" action="{{url('post-insert-task-team')}}" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
                    <div class="modal fade" id="ModalInvite" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-header">
                                    <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Task People Group</font></h2>
                                </div>
                                
                                
                                <div class="modal-body">
                                     <div class="col-sm-12">
                                        <label>City</label>
                                        <select name="distict" class="form-control" id="city_id">
                                            <option value="">-select-</option>
                                            @foreach($m_district as $city)
                                                <option value="{{ $city->city_id."+".$city->id }}">{{ $city->city_id }}</option>
                                            @endforeach
                                         
                                        </select>
											                   <span class="text-error" id="txt_city_id"></span>
                                        <br>
                                     </div>
                                     <br>
                                     <div class="col-sm-12">
                                        <label>Outlet</label>
                      										<div class="outlet_id">
                      											<select name="outlet_id" class="form-control" id="outlet_id">
                      												<option value="">-select-</option>
                      											</select>
                      										</div>
                                        <span class="text-error" id="txt_outlet_id"></span>
                                        <br>
                                     </div>
                                     <br>

                                    <div class="col-sm-12">
                                        <label>Shift</label>
                                      <div class="shift">
                                          <select name="shift" class="form-control" id="shift">
                                            <option value="">-select-</option>
                                          </select>
                                      </div>
                                        <span class="text-error" id="txt_shift"></span>
                                        <br>
                                    </div>
									                 <div class="col-sm-12">
                                        <label>Team</label>
                    									<div class="team">
                    											<select name="team" class="form-control" id="team">
                    												<option value="">-select-</option>
                    											</select>
                    									</div>
                                        <span class="text-error" id="txt_team"></span>
                                        <br>
                                    </div>
                                     <br>
									                   <div class="col-sm-12">
                                        <label>Member</label>
                      										<div class="member">
                      											<select id="member" multiple="multiple" name="people_id[]"></select>
                      										</div>
                                        <span class="text-error" id="txt_member"></span>
                                        <br>
                                     </div>
                                     <br>
									 <div class="col-sm-12">
                                        <label>Schedule Date</label>
												<div class="form-line">
                                                    <input type="text" name="schedule_date" class="form-control" placeholder="Schedule date" id="schedule_date">
                                                </div>
											<span class="text-error" id="txt_schedule_date"></span>
											<br>
                                     </div>
                                     <br>
									 <div class="col-sm-12">
                                         <div class="input-daterange input-group" id="bs_datepicker_range_container">
												<div class="form-line">
													<label>Task Start</label>
													<input type="text" name="task_start" class="form-control" placeholder="Task Start.." id="task_start">
												</div>
												<span class="text-error" id="txt_task_start"></span>
												<br>
												<span class="input-group-addon" style="margin-left:10px;">to</span>
												<div class="form-line">
												<label>Task End</label>
													<input type="text" name="task_end" class="form-control" placeholder="Task End..." id="task_end">
												</div>
												<span class="text-error" id="txt_task_end"></span>
												<br>
											</div>
                                     </div>
                                     <br>
                                     <div class="col-sm-12">
                                        <label>Task Type</label>
                                         <select name="task_type" class="form-control" id="task_type">
                                            <option value="">-select-</option>
                                            <option>Visit</option>
                                            <option>Offer</option>
                                            <option>Promotion</option>
                                             
                                         </select>
                                         <span class="text-error" id="txt_task_type"></span>
										 <br>
                                     </div>
                                     <div class="col-sm-12">
                                        <label>Description</label>
                                         <textarea name="desc" id="desc" class="form-control" placeholder="Decription" style="height: 60px; max-height: 60px; min-height: 80px; max-width: 100%; min-width: 100%;"></textarea>
										
                                      <span class="text-error" id="txt_desc"></span>
									 </div>
                                     <br>
                                     <div class="col-sm-12">
                                        <?php $todayDate = date("Y-m-d")?>
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                        <br>
                                         
                                    </div>
                                    
                                </div>
                                 
                                
                                <div class="modal-footer">
                                    
                                    <button href="#" type="button" id="send" class="btn btn-link bg-blue waves-effect" >Save</button>
                                    <button href="#" type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirim">Save</button>
                                    <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

     

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
    <script src="{{ asset('js-master/people.js') }}"></script>
		<script type="text/javascript">
     $(function(){
		$("#bs_datepicker_range_container").datepicker({
			  format: 'dd-mm-yyyy',
			  startDate: new Date(),
			  minDate:0,
			  autoclose: true,
			  todayHighlight: false,
		  });
		  
		  $("#schedule_date").datepicker({
			  format: 'dd-mm-yyyy',
			  startDate: new Date(),
			  minDate:0,
			  autoclose: true,
			  todayHighlight: false,
		  });
	});
    </script>
        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script type="text/javascript">
             $(function(){
              $(".datepicker").datepicker({
                  format: 'dd-mm-yyyy',
                  autoclose: true,
                  todayHighlight: true,
              });
             });



         </script>
		 <script type="text/javascript">
                $(document).ready(function() {
                  $("#member").select2({
                        placeholder: 'Select a team',
						width: '100%'
                    });
					
					$('#ModalInvite').on('hidden.bs.modal', function(e) { 
						$(e.currentTarget).find('#txt_city_id').text('');
						$(e.currentTarget).find('#txt_outlet_id').text('');
						$(e.currentTarget).find('#txt_team').text('');
						$(e.currentTarget).find('#txt_member').text('');
						$(e.currentTarget).find('#txt_schedule_date').text('');
						$(e.currentTarget).find('#txt_task_start').text('');
						$(e.currentTarget).find('#txt_task_end').text('');
						$(e.currentTarget).find('#txt_task_type').text('');
						$(e.currentTarget).find('#txt_desc').text('');
					});
            	  
                }); 
                </script>
          <script type="text/javascript">
        $(document).ready(function() {
          $(window).keydown(function(event){
            if(event.keyCode == 13) {
              event.preventDefault();
              return false;
            }
          });
        }); 
        </script> 
		<script type="text/javascript">
       

			$('#city_id').change(function(){
				
				var city_id = $(this).val();
				var arr = city_id.split('+');
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				
				$.ajax({
					url:"{{ url('/postcity_id/get/outlet/') }}",
					type:"POST",
					data:{ data_id: arr[0],_token: CSRF_TOKEN },
					success:function(response) {
						var obj = JSON.parse(response);
						if(obj.hasil == 'OK'){
							$('.outlet_id select').html(obj.data);
							$('.outlet_id select').selectpicker('refresh');
							return false;
						}
					},
					error:function(){
						alert("error");
					}
				});
				
				$.ajax({
					url:"{{ url('/postcity_id/post/team/') }}",
					type:"POST",
					data:{ data_id: arr[1],_token: CSRF_TOKEN, project_id: {{ $project->id }} },
					success:function(response) {
						var obj = JSON.parse(response);
						if(obj.hasil == 'OK'){
							$('.team select').html(obj.data);
							$('.team select').selectpicker('refresh');
							return false;
						}
					},
					error:function(){
						alert("error");
					}
				});

        $('#outlet_id').change(function(){
        
        var id = $(this).val();
          $.ajax({
          url:"{{ url('/postcity_id/post/shift/') }}",
          type:"POST",
          data:{ data_id: arr[1],_token: CSRF_TOKEN, project_id: {{ $project->id }}, outlet_id: id },
          success:function(response) {
            var obj = JSON.parse(response);
            if(obj.hasil == 'OK'){
              $('.shift select').html(obj.data);
              $('.shift select').selectpicker('refresh');
              return false;
            }
          },
          error:function(){
            alert("error");
          }
        });

        });

        
				
			});	


		</script> 


		
		<script type="text/javascript">
		 $(document).ready(function() {
			$('#team').change(function(){
			 	var team = $(this).val();
				var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
				
				$.ajax({
					url:"{{ url('/postcity_id/post/member/') }}",
					type:"POST",
					data:{ data_id: team, _token: CSRF_TOKEN },
					success:function(response) {
						
						var $select = $('#member');

						var options = $select.data('select2').options.options;

						$select.html('');

						var items = [];
						var datas = JSON.parse(response);
						if(datas.length > 0){
							$select.append("<option value='ALL'>ALL</option>");
							for (var i = 0; i < datas.length; i++) {

								items.push({
									"id": datas[i].employee_id,
									"text": datas[i].first_name,
									"middle": datas[i].middle_name,
                  "surname": datas[i].surname
								});

								$select.append("<option value=\"" + datas[i].employee_id + "\">" + datas[i].first_name + " " + datas[i].middle_name + " " + datas[i].surname  + "</option>");
							}	
						}

						options.data = items;
						$select.select2(options);
					},
					error:function(){
						alert("error");
					}
				});
				});
			});
		</script>

        <script Language="JavaScript">
          function validateR(){
          var selectedCombobox=(comboForm.branch_id.value);
          if (selectedCombobox=="-1") {
              alert("Please Select Branch");
              return false;
          }
             return true;
          }
        </script>

         <script type="text/javascript">
           $(function(){
			   
		   //outlet name cannot long space on input
		  $('#desc').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#desc').val().length > 0){
					$('#desc').val($('#desc').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on outlet name*/
		  $(document).on('paste', '#desc', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });

              $("#send").click(function(){
               
                if ($('#city_id').val().length < 1) {
                        $('#txt_city_id').text('City not selected');
                  }else{
                        $('#txt_city_id').text('');
                  }

                 if ($('#outlet_id').val().length < 1) {
                        $('#txt_outlet_id').text('outlet  not selected');
                  }else{
                        $('#txt_outlet_id').text('');
                  }
				  
				  if ($('#team').val().length < 1) {
                        $('#txt_team').text('Team not selected');
                  }else{
                        $('#txt_team').text('');
                  }
				  
				  if ($('#schedule_date').val().length < 1) {
                        $('#txt_schedule_date').text('Schedule date is empty');
                  }else{
                        $('#txt_schedule_date').text('');
                  }
				  
				  if ($('#task_start').val().length < 1) {
                        $('#txt_task_start').text('Task start date is empty');
                  }else{
                        $('#txt_task_start').text('');
                  }

					if ($('#task_end').val().length < 1) {
                        $('#txt_task_end').text('Task end date is empty');
                  }else{
                        $('#txt_task_end').text('');
                  }
				  
				  if ($('#task_type').val().length < 1) {
                        $('#txt_task_type').text('Task type not selected');
                  }else{
                        $('#txt_task_type').text('');
                  }

                  if ($('#desc').val().length < 1) {
                        $('#txt_desc').text('Description is empty');
                  }else{
                        $('#txt_desc').text('');
                  }
              

                 if($('#city_id').val().length > 0 && 
          					$('#outlet_id').val().length > 0 && 
          					$('#desc').val().length > 0 &&
          					$('#team').val().length > 0 &&
          					$('#schedule_date').val().length > 0 &&
          					$('#task_start').val().length > 0 &&
          					$('#task_type').val().length > 0 &&
          					$('#task_end').val().length > 0
          					){
                 $("#kirim").click();  
               }
                 
              
            });

          });
        </script>

    </section>
    

@endsection
