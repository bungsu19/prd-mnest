@extends('layout-menu.app')

@section('content')

   
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h3 align="left" style="margin-top: -5px;">
                                <u>Absensi Outlet</u>
                            </h3>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix" style="margin-bottom: -10px;">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                        </div>
						
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        

                                         <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Absensi Outlet</a>
                                          
                                        </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                           <div class="table-responsive">
                                                 <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <td class="bg-column" width="20"><font color="yellow"> No</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Date</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Outlet Name</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Outlet In</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Outlet Out</font></td>
                                                            <td class="bg-column" align="center"><font color="yellow">Maps</font></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $no = 1; ?>
                                                        @foreach($absensi_outlet as $outlet)
                                                            <tr>
                                                                <td align="center">{{ $no }}</td>
                                                                <td align="center">{{ $outlet->date_absensi }}</td>
                                                                <td align="center"><a href="{{url('/all-outlet/detail-outlet')}}/{{ $outlet->outlet_id }}"> {{ $outlet->outlet_name }}</a></td>
                                                                <td align="center">{{ $outlet->absensi_in }}</td>
                                                                <td align="center">{{ $outlet->absensi_out }}</td>
                                                                <td align="center"><a href="#" data-toggle="modal" data-target="#ModalImage" data-in-lattitude="{{ $outlet->in_latitude }}" data-in-longitude="{{ $outlet->in_langitude }}" data-out-lattitude="{{ $outlet->out_latitude }}"  data-out-longitude="{{ $outlet->out_langitude }}"> <i class="material-icons">pin_drop</i></a></td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                        @endforeach
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>


        <div class="modal fade" id="ModalImage" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Maps</font></h2>
                        </div>
                        
                          <div class="body">
                           <div id="map" class="gmap"></div>
                            </div>
                        <div class="modal-footer">
                            <br>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
		<script>
		$(document).ready(function() {
			$('#ModalImage').on('show.bs.modal', function(e) {
				var inLatt = $(e.relatedTarget).data('in-lattitude');
				var inLong = $(e.relatedTarget).data('in-longitude');
				var outLatt = $(e.relatedTarget).data('out-lattitude');
				var outLong = $(e.relatedTarget).data('out-longitude');
			
				var myLatLng = {lat: inLatt, lng: inLong };
				var map = new google.maps.Map(document.getElementById('map'), {
				  zoom: 10,
				  center: myLatLng,
               styles: [ 
                      {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                      {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#38414e'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#212a37'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#9ca5b3'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                      },
                      {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                      },
                      {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#17263c'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#515c6d'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#17263c'}]
                      }
                  ]
            });
			
			
            var markerin, markerout;
  
            markerin = new google.maps.Marker({
				position: new google.maps.LatLng(inLatt, inLong),
				title : 'Absensi Masuk',
				map: map
            });
  
            markerout = new google.maps.Marker({
				position: new google.maps.LatLng(outLatt, outLong),
				title : 'Absensi Pulang',
				map: map
            });
         
			
			});
		}); 
		</script>
		
		<script src="http://maps.googleapis.com/maps/api/js"></script>
		
		<script async defer
                 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHCxvsKpe_XnzcU5UxBH_Hfyv3Gg8iFhg&callback=initMap">
        </script>
       
    </section>
    

@endsection
