@extends('layout-menu.app')

@section('content')
<style type="text/css">
  .text-error
  {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
      margin-top: 5px;
    }
</style>



<section class="content">
        <div class="container-fluid">
            @if(session()->has('failed'))
            <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('failed') }}
            </div>
            @endif

             @if(session()->has('message'))
            <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('message') }}
            </div>
            @endif

            @if(session()->has('gagal'))
            <div class="alert bg-secondary alert-dismissible" role="alert" style="height: 400px; overflow-y: auto;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
                  <?php $pesan = session()->get('gagal'); ?>
               <?php for ($i=0; $i < count($pesan) ; $i++) { 
                   echo $pesan[$i].'<br>';
               }
               ?>
            </div>
            @endif

            @if(session()->has('gagal2'))
            <div class="alert bg-secondary alert-dismissible" role="alert" style="height: 400px; overflow-y: auto;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
                  <?php $pesan2 = session()->get('gagal2'); ?>
               <?php for ($i=0; $i < count($pesan2) ; $i++) { 
                   echo $pesan2[$i].'<br>';
               }
               ?>
            </div>
            @endif
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card" style="margin-top: -10px;">
                        <div class="header">
                            <h3 align="left" style="margin-top: -5px;">
                                <u>Project Information</u>
                            </h3>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Name</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_name }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Project Description</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_desc }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Contract Number</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ $project->project_contract_no }}</label>
                                </div>
                            </div>
							<div class="row clearfix">
                                <div class="col-sm-2">
                                    <label>Periode</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> ({{ $project->project_start }}) To ({{ $project->project_end }})</label>
                                </div>
                            </div>
							<div class="row clearfix" style="margin-bottom: -10px;">
                                <div class="col-sm-2">
                                    <label>Project Value</label>
                                </div>
								<div class="col-sm-5">
                                    <label style="margin-left: -30px;"><span style="margin-right:10px;">:</span> {{ number_format($project->project_value,0) }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>
                                Task List People 
                            </b></h2>
                            <small>Name Member &nbsp;&nbsp; : {{ $member->first_name }} {{ $member->middle_name }} {{ $member->surname }} </small><br>
                            <small>Job &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $member->job_name }}</small><br>
                            <small> Name Team &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{ $member->name_team }}</small><br>
                            <small>Area Project &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : {{ $member->distict }} </small>

                                <h2 align="right">
									@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
										<button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalInvite">New Task</button>
									@endif
								</h2>
                        </div>
						
							@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4 || Auth::user()->hak_access == 5)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" width="20"><font color="yellow"> No</font></td>
                                            <td class="bg-column"><font color="yellow">Name Outet</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Schedule Date</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Start</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">End</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Description</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                                    $no = 0;
                                                    if($data->currentPage() == 1){
                                                        $no = 1;
                                                    }else{
                                                      $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                    }
                                                ?>
                                        @foreach($data as $datas)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td>{{ $datas->outlet_name }}</td>
                                                <td align="center">{{ $datas->schedule_date }}</td>
                                                <td align="center">{{ $datas->task_start }}</td>
                                                <td align="center">{{ $datas->task_end }}</td>
                                                <td align="center">{{ $datas->description }}</td>
                                            </tr>
                                            <?php $no++; ?>
                                        @endforeach

                                    </tbody>
                                </table>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                           <h1><b></b></h1>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                {{ $paginator->links('paginator.default') }}
                                        </div>
                            </div>
							@endif
                    </div>
                </div>
            </div>

        </div>

        <form class="form-signin" method="POST" action="{{url('task_people/add')}}" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}
                    <div class="modal fade" id="ModalInvite" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-header">
                                    <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Assign Task To ({{ $member->first_name }} {{ $member->middle_name }} {{ $member->surname }}) </font></h2>
                                </div>
                                
                                <div class="modal-body">
                                     <div class="col-sm-12">
                                        <label>Outlet</label>
                    					<select class="form-control" data-placeholder="Choose an option please" multiple="multiple" name="outlet_id[]" id="chosen">				
                    						@foreach($m_outlet as $outlets)
                    							<option value="{{ $outlets->id}}">{{ $outlets->outlet_name }} ( {{ $outlets->outlet_address1 }}  )</option>
                    						@endforeach
                    					</select>
                                        <span class="text-error" id="txt_chosen"></span>
                                        <br>
                                     </div>
                                     <br>

                                     <div class="col-sm-12">
                                        <br>
                                        <label>Shift</label>
                                      <div class="shift">
                                          <select name="shift" class="form-control" id="shift">
                                            <option value="">-select-</option>
                                            <option value="1">Shift I</option>
                                            <option value="2">Shift II</option>
                                            <option value="3">Shift III</option>
                                          </select>
                                      </div>
                                        <span class="text-error" id="txt_shift"></span>
                                    </div>
                                     <div class="col-sm-12"><br>
                                        <label>Schedule</label>
                                         <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="schedule" id="schedule" class="form-control datepicker" placeholder="Schedule">
                                                </div>
												<span class="text-error" id="txt_schedule"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                     </div>
                                     <div class="col-sm-12">
                                        <label>Start Task</label>
                                         <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="task_start" id="task_start" class="form-control datepicker" placeholder="Start Task">
                                                </div>
												<span class="text-error" id="txt_task_start"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                     </div>
                                     <br>
                                     <div class="col-sm-12">
                                        <label>End Task</label>
                                         <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="task_end" class="form-control datepicker" placeholder="End Task" id="task_end">
                                                </div>
												<span class="text-error" id="txt_task_end"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                     </div>
                                     <br>
                                     <div class="col-sm-12">
                                        <label>Task Type</label>
                                         <select name="task_type" class="form-control" id="task_type">
                                            <option value="">-select-</option>
                                            <option>Visit</option>
                                            <option>Offer</option>
                                            <option>Promotion</option>
                                             
                                         </select>
                                         <span class="text-error" id="txt_task_type"></span>
                                     </div>
                                     <div class="col-sm-12">
									                   	<br>
                                        <label>Description</label>
                                        <textarea name="desc" class="form-control" placeholder="Decription" style="height: 55px; max-height: 55px; min-height: 55px; max-width: 100%; min-width: 100%;" id="desc"></textarea>
										<span class="text-error" id="txt_desc"></span>
									 </div>
                                         
                                     <br>
                                     <div class="col-sm-12">
                                        <?php $todayDate = date("Y-m-d")?>
                                        <input type="hidden" name="project_id" value="{{ $project->id }}">
                                  <input type="hidden" name="people_id" value="{{ $member->employee_id }}">
                                  <input type="hidden" name="team_id" value="{{ $member->team_id }}">
                                        <br>
                                        <br>
                                         
                                    </div>
                                    
                                </div>
                                 
                                
                                <div class="modal-footer">
                                    <br>
                                   <button type="button" id="send" class="btn bg-blue waves-effect"> SAVE</button>
                                   <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                    <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
     

    
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
    <script src="{{ asset('js-master/people.js') }}"></script>  
        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
        <script type="text/javascript">
             $(function(){
              $(".datepicker").datepicker({
                  format: 'dd-mm-yyyy',
                  autoclose: true,
                  todayHighlight: true,
              });
			  
					$('#ModalInvite').on('hidden.bs.modal', function(e) { 
						$(e.currentTarget).find('#txt_chosen').text('');
						$(e.currentTarget).find('#txt_schedule').text('');
						$(e.currentTarget).find('#txt_task_start').text('');
						$(e.currentTarget).find('#txt_task_end').text('');
						$(e.currentTarget).find('#txt_task_type').text('');
						$(e.currentTarget).find('#txt_desc').text('');
					});
			  
             });

            setTimeout(function(){
               $("div.alert").remove();
            }, 5000 );
         </script>
          <script type="text/javascript">
            $(document).ready(function() {
              $(window).keydown(function(event){
                if(event.keyCode == 13) {
                  event.preventDefault();
                  return false;
                }
              });
            }); 
            </script>  
            	<script type="text/javascript">
                $(document).ready(function() {
                  $("#chosen").chosen({
                        disable_search_threshold: 10,
                        no_results_text: "Oops, nothing found!",
                        width: "100%"
                    });
            	  
                }); 
                </script>

        <script type="text/javascript">
           $(function(){

             $('#desc').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#desc').val().length > 0){
                    $('#desc').val($('#desc').val().replace(/\s{2,}/g,' '));
                }
            });
			  
                  /*handle paste with extra space on nick name*/
              $(document).on('paste', '#desc', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  $(this).val(text.substring(0, 50));
           });

              $("#send").click(function(){
               
                if ($('#task_end').val().length < 1) {
                        $('#txt_task_end').text('Task End is Empty');
                  }else{
                        $('#txt_task_end').text('');
                  }

                  if ($('#task_start').val().length < 1) {
                        $('#txt_task_start').text('Task Start is Empty');
                  }else{
                        $('#txt_task_start').text('');
                  }

                  if ($('#task_type').val().length < 1) {
                        $('#txt_task_type').text('Task Type not Selected');
                  }else{
                        $('#txt_task_type').text('');
                  }

                  if ($('#desc').val().length < 1) {
                        $('#txt_desc').text('Description is Empty');
                  }else{
                        $('#txt_desc').text('');
                  }

                  if ($('#schedule').val().length < 1) {
                        $('#txt_schedule').text('Schedule is Empty');
                  }else{
                        $('#txt_schedule').text('');
                  }

                  if ($("select[name='outlet_id[]'] option:selected").length < 1) {
                        $('#txt_chosen').text('Outlet not Selected');
                  }else{
                        $('#txt_chosen').text('');
                  }

                 
              

                 if( $('#task_end').val().length > 0 && $('#task_start').val().length > 0 && $('#task_type').val().length > 0  && $('#desc').val().length > 0 && $('#schedule').val().length > 0 && $("select[name='outlet_id[]'] option:selected").length > 0 ){
					 $("#kirim").click();  
				   }
                 
              
            });

          });
        </script>
    </section>
    

@endsection
