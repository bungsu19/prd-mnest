@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
            <form class="form-signin" method="POST" action="{{url('/structure-area')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
             <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
						
							@if(session()->has('message'))
							<div class="alert bg-green alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								 {{ session()->get('message') }}
							</div>
							@endif

							@if(session()->has('failed'))
							<div class="alert bg-red alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								 {{ session()->get('failed') }}
							</div>
							@endif
						
                            <div class="card">
                                <div class="header">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        
                                          <font size="5">  <b>Area Name -> <i>{{ $data->structure_name }}</i></b></font>
                                       
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             <a href="#" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">New City</a>
                              <!-- <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a> -->
                            
                            </div>
                            <div class="row clearfix">
                            <form class="form-signin" method="POST" action="{{url('/data-people/filter-data')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All People</option>
                                                        <option value="1"> People Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </form>
            <!-- #END# Select -->
                            
                        </div>
                                   
                                    <div class="row clearfix">
                                    	 <!-- Basic Table -->
                                    
                                                <div class="body table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <td class="bg-column" ><font color="yellow"> NO</font></td>
                                                                <td class="bg-column" ><font color="yellow">Province</font></td>
                                                                <td class="bg-column" ><font color="yellow">District/City</font></td>
                                                                <td class="bg-column"><font color="yellow">Action</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($dt_step2->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($dt_step2->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                                ?>
                                                                 @foreach($dt_step2 as $da)
                                                                    <tr>
                                                                        <th scope="row">{{ $no }}</th>
                                                                        <td >{{ $da->provinsi_id }}</td>
                                                                        <td>{{ $da->city_id }}</td>
                                                                        <td>
																			<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete province '{{ $da->provinsi_id }}' ?" data-href="{{ url('/structure-area/delete-area') }}/{{ $da->id }}" title="Delete Data">
																				<i class="material-icons">delete_forever</i>
																			</a>
                                                                        </td>
                                                                        
                                                                    </tr>
                                                                 <?php $no++; ?>
                                                          @endforeach
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                       <h1><b></b></h1>
                                                        </div>
                                                       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                            {{ $paginator->links('paginator.default') }}
                                                        </div>
                                                </div>
                                    <!-- #END# Basic Table -->

                              
                                 
                                  

                                   
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">

                                         
                                        
                                        </div><br>
                                   
                                         <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 right">
                                            <div class="col-sm-6">
                                                <a href="{{url('structure-title/view')}}/{{ $data2->title_structure_id }}" class="btn btn-block bg-blue waves-effect" ><font size="4" color="white"> Back</font></a>
                                            </div>
                                            <div class="col-sm-6">

                                                <a href="{{url('structure-area-data')}}" class="btn btn-block bg-blue waves-effect" ><font size="4" color="white"> Finish</font></a>
                                            </div>
                                        </div>
                                   
                                    
                                
                                </div><br>
                        </div>
                    </div>
                </form>
           
        
        </div>

        <!-- Default Size -->
         <form class="form-signin" method="POST" action="{{url('/structure-area-step2')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"><font size="4" color="yellow"> Add New City</font> </h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12">
                               
                                 <label>Province <span class="mandatory">*</span>
</label>
                                <select name="provinsi_id" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province">
                                        <option value="">- Select Province -</option>
                                        @foreach($province as $result)
                                        <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                        @endforeach
                                    </select>
                               <span class="text-error" id="txt_province"></span>
                            </div>
                            <div class="col-sm-12">
							<br>
                                <label>District <span class="mandatory">*</span>
</label>
                                <div class="regency">
                                <select name="city_id" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="district">
                                            <option value="">- Select Regency/City -</option>
                                        </select>
                                        <span class="text-error" id="txt_district"></span>
                                </div>
                                <input type="hidden" name="stucture_id" value="{{ $data->id }}">
                                <input type="hidden" name="title_id" value="{{ $data2->title_structure_id }}">
                               <input type="hidden" name="kecamatan" value="NULL">
                               <input type="hidden" name="village" value="NULL">
                               <br>
                               <br>
                            </div>
                           
                        </div>
                        <div class="modal-footer">
                            <br>
                            <button type="button" class="btn bg-blue waves-effect" id="send">SAVE</button>
                            <button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim">SAVE</button>
                            <button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
    </section>

     @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
</script>

 <script type="text/javascript">
        $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>

    <script type="text/javascript">

         $("#send").click(function(){
             if ($('#province').val().length < 1) {
                $('#txt_province').text('Province not selected');
          }else{
                $('#txt_province').text('');
          }
          
          if ($('#district').val().length < 1) {
                $('#txt_district').text('District not selected');
          }else{
                $('#txt_district').text('');
          }

          if( $('#province').val().length > 0 &&
             $('#district').val().length > 0){
                 $("#kirim").click();    
             }
              
          });
          
      


    </script>
     <script type="text/javascript">
    $(document).ready(function() {
		
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
	  
	  $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
			var message = $(e.relatedTarget).data('message');
			$(e.currentTarget).find('.message').text(message);
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
	  
    }); 
	
	setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>  
    @endsection

@endsection
