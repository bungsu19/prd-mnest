@extends('layout-menu.app')

@section('content')
   
<section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <form class="form-signin" method="POST" action="/structure-area-add-team" enctype="multipart/form-data">
                    {{ csrf_field() }}
             <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                <h1>
                                    <b> {{ $area->structure_name }} ->{{ $data->city_id }} </b>
                                </h1>
            <!-- #END# Select -->
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <label> </label>
                                            <button type="button" class="btn btn-block bg-red waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add Team </button>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                         <!-- Basic Table -->
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card">
                                                    <div class="header">
                                                        
                                                        <ul class="header-dropdown m-r--5">
                                                            <li class="dropdown">
                                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="material-icons">more_vert</i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li><a href="javascript:void(0);">Action</a></li>
                                                                    <li><a href="javascript:void(0);">Another action</a></li>
                                                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="body table-responsive">
                                                        <table class="table">
                                                            <thead>
                                                                <tr>
                                                                    <th bgcolor="#808080">NO</th>
                                                                    <th bgcolor="#808080">Kota</th>
                                                                    <th bgcolor="#808080">Nama Team</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php $no = 1 ?>
                                                                    @foreach($dt_team as $da)
                                                                    <tr>
                                                                        <th scope="row">{{ $no }}</th>
                                                                        <td >{{ $da->structure_area_step2_id }}</td>
                                                                        <td>{{ $da->name_team }}</td>
                                                                    </tr>
                                                                 <?php $no++; ?>
                                                          @endforeach
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- #END# Basic Table -->

                                    </div>
                              
                                 
                                  

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <a href="/templete" type="submit" class="btn btn-block bg-blue waves-effect">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
           
        
        </div>

        <!-- Default Size -->
         <form class="form-signin" method="POST" action="/structure-area-add-team" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel"> Add Data </h4>
                        </div>
                        <div class="modal-body">
                            
                            <div class="col-sm-7">
                                <label>Nama Team</label>
                                <input type="hidden" class="form-control" name="step2_id" value="{{ $data-> id }}">
                                <input type="text" class="form-control" name="name_team">
                                <input type="hidden" name="title_id" value="{{ $data->title_structure_id }}">
                               
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn bg-red waves-effect" >ADD</button>
                            <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
    </section>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  
    

@endsection
