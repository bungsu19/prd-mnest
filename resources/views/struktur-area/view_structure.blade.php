@extends('layout-menu.app')

@section('content')

   
<section class="content">
              @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif

            @if(session()->has('failed'))
                <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('failed') }}
                </div>
                @endif
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                             <h1 align="left" >
                                <u>Area Information</u>
                            </h1>
                             
                            <span class="span-dots-area"><font size="3" ><b> Structure</b>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font></span><span class="span-dots-area"><font size="4" > {{ $data->title_structure }}</font></span><br>
                            <span class="span-dots-area"><font size="3" ><b> Scope Project</b>  &nbsp;&nbsp;&nbsp; :</font></span><span class="span-dots-area"><font size="4" > 
                                @if($data->national == "IND")
                                        NATIONAL
                                    @else
                                        LOCAL
                                    @endif </font></span><br>
                            <span class="span-dots-area"><font size="3" ><b>Desription</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font></span>
                            
                            <span class="span-dots-area" title="{{ $data->description }}"><font size="3" >{{ $data->description }}</font></span>
                             
                        </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Area Information</a>
                                        </li>
                                        
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                           <div class="table-responsive">
                                                        <table class="table">
															<thead>
																<tr>
																	<td class="bg-column" align="center"><font color="yellow">No</font></td>
																	<td class="bg-column"><font color="yellow">Area Name</font></td>
																	<td class="bg-column"><font color="yellow">District/City</font></td>
																</tr>
															</thead>
															<tbody>
															
																<?php $no = 1; ?>
																@foreach($det_data as $det)
																<?php $count = explode("|", $det->area_city); ?>
																
																<tr>
																	<td rowspan="{{ count($count)+1 }}">{{ $no }}</td>
																	<td rowspan="{{ count($count)+1 }}">{{ $det->name_area }}</td>
																</tr>
																
																@for ($i = 0; $i < count($count ); $i++)
																	<tr><td> {{ $count[$i] }}</td></tr>
																@endfor
																
																<?php $no++; ?>
																 @endforeach
																 
															</tbody>
														</table>

														
														<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
															<br><br><br>
															<div >
																<a href="{{url('/structure-area-data')}}" class="btn bg-blue waves-effect m-r-20" ><font size="4" color="white">Back</font></a>
															</div>
														<br>
																
													</div>
                                            </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="maps">
                                          <div class="body">
                                                 <div id="map" class="gmap"></div>
                                          </div>
                                         </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        
    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>


    </section>
    

@endsection
