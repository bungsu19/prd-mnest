@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <form class="form-signin" method="POST" action="/structure-area" enctype="multipart/form-data">
                    {{ csrf_field() }}
             <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                            <h2>
                                <b> Add Struktur Area</b>
                            </h2>
            <!-- #END# Select -->
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <label> Nasional</label>
                                            <input type="text" class=" form-control" name="national" placeholder="NASIONAL" disabled>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <label> Structure Name</label>
                                            <input type="text" class=" form-control" name="Structure" placeholder="Input Name Structure">
                                           
                                        </div>
                                    </div>
                              
                                 
                                  

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <a href="#" class="btn btn-block bg-blue waves-effect" >Cancel</a>
                                        </div>
                                        <div class="col-sm-3">
                                            <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" >Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
           
        
        </div>
    </section>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

@endsection
