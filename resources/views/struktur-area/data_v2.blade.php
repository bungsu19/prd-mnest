@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
            <div class="block-header">
               <!--  <h2>
                   All Master Struktur Area
                </h2> -->
                 
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                 <font size="5" color="black"><b>Structure Name -> <i>{{ $title->title_structure }}</i></b></font>
                            </div>
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             <a href="#" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">New Area</a>
                              <!-- <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a> -->
                            
                            </div>
                   <div class="row clearfix">
                            <form class="form-signin" method="POST" action="{{url('/data-people/filter-data')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All People</option>
                                                        <option value="1"> People Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
						
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" ><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Area Name</font></td>
                                            <td class="bg-column"><font color="yellow">Description</font></td>
                                            <td class="bg-column"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <th scope="row">{{ $no }}</th>
                                                <td> {{ $da->structure_name }}</td>
                                                <td><span class="span-dots" data-toggle="tooltip" title="{{ $da->description }}"> {{ $da->description }}</span> </td>
                                                <td align="center">
													<a href="#" data-toggle="modal" data-target="#defaultModalEdit" data-id="{{ $da->id }}" data-area_name="{{ $da->structure_name }}" data-description="{{ $da->description }}">
														 <i class="material-icons">rate_review</i>
													</a>
													   
													<a href="{{url('structure-area-data/view/'. $da->id)}}" data-toggle="tooltip" title="Edit Data">
														<i class="material-icons">mode_edit</i>
													</a>
													<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete structure area '{{ $da->structure_name }}' ?" data-href="{{ url('/structure-area/delete-structure')}}/{{ $da->id }}" title="Delete Data">
														<i class="material-icons">delete_forever</i>
													</a>
                                            </td>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                    <div >
                                        <a href="{{url('/structure-area-data')}}" class="btn bg-blue waves-effect m-r-20" ><font size="4" color="white">Back</font></a>
                                    </div>
                                <br>
                                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->

        <form class="form-signin" method="POST" action="{{url('/structure-area-v2')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"><font size="4" color="yellow"> Add New Area </font> </h4>
                        </div>
                        <div class="modal-body">

                            
                            <div class="col-sm-12">
                                    <input type="hidden" name="national" value="IND" >
                            
                            <br>
                                <label>Area Name <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="title_structure" placeholder="Area Name" id="title_structure" maxlength="30">
                                <span class="text-error" id="txt_title_structure"></span>
                                <input type="hidden" name="title_id" value="{{ $title->id }}">
                                
                            </div>
                             <div class="col-sm-12">
							 <br>
                                <label>Description <span class="mandatory">*</span></label>
                                <textarea name="description" id="description" class="form-control" placeholder="Description" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" maxlength="100"></textarea>
                                <span class="text-error" id="txt_description"></span>
                                <br>
                                <br>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-blue waves-effect" id="send" >SAVE</button>
                            <button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim">SAVE</button>
                            <button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">CANCEL</button>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </form>
		
        
        <form class="form-signin" method="POST" action="{{url('/structure-area-v2/update')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModalEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <font size="5" color="yellow"> Edit New Area </font>
                        </div>
                        <div class="modal-body">
                            
                            <div class="col-sm-12">
                                    <input type="hidden" name="national" value="IND" >
                            
                            <br>
                                <label>Area Name <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="title_structure" id="title_structure_data" maxlength="30">
								<span class="text-error" id="txt_title_structure_data"></span>
                                <input type="hidden" name="title_id" value="{{ $title->id }}">
                                
                            </div>
                             <div class="col-sm-12">
							 <br>
                                <label>Description <span class="mandatory">*</span></label>
                                 <textarea name="description" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" id="description_data" maxlength="100"></textarea>
                                <input type="hidden" name="id" id="id">
								<span class="text-error" id="txt_description_data"></span>
                                <br>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-blue waves-effect" id="send_data">Update</button>
							<button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim_data">UPDATE</button>
                            <button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
      
    </section>

           @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>

<script type="text/javascript">
     $(function(){
         $('#title_structure').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#title_structure').val().length > 0){
					$('#title_structure').val($('#title_structure').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#title_structure', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,30));
		  });
		  
		  $('#description').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#description').val().length > 0){
					$('#description').val($('#description').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#description', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,100));
		  });
          
          $("#send").click(function(){
              if ($('#title_structure').val().length < 1) {
                    $('#txt_title_structure').text('Area Name is empty');
              }else{
                    $('#txt_title_structure').text('');
              }
              
              if ($('#description').val().length < 1) {
                    $('#txt_description').text('Description is empty');
              }else{
                    $('#txt_description').text('');
              }
              
              if($('#title_structure').val().length > 0 && $('#description').val().length > 0){
                  $("#kirim").click();
              }
              
          });
		  
     });


</script>

<script type="text/javascript">
$(function(){
			$('#title_structure_data').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#title_structure_data').val().length > 0){
					$('#title_structure_data').val($('#title_structure_data').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#title_structure_data', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,30));
		  });
		  
		  $('#description_data').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#description_data').val().length > 0){
					$('#description_data').val($('#description_data').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#description_data', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,100));
		  });
		  
		  $("#send_data").click(function(){
              if ($('#title_structure_data').val().length < 1) {
                    $('#txt_title_structure_data').text('Area Name is empty');
              }else{
                    $('#txt_title_structure_data').text('');
              }
              
              if ($('#description_data').val() < 1) {
                    $('#txt_description_data').text('Description is empty');
              }else{
                    $('#txt_description_data').text('');
              }
              
              if($('#title_structure_data').val().length > 0 && $('#description_data').val().length > 0){
                  $("#kirim_data").click();
              }
              
          });
		  
		  });
</script>
 <script type="text/javascript">
    $(document).ready(function() {
		
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
	  
	  $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
			var message = $(e.relatedTarget).data('message');
			$(e.currentTarget).find('.message').text(message);
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
	  
	  $('#defaultModalEdit').on('show.bs.modal', function(e) {

			var id = $(e.relatedTarget).data('id');
			var area_name = $(e.relatedTarget).data('area_name');
			var description = $(e.relatedTarget).data('description');
			

			$(e.currentTarget).find('#id').val(id);
			$(e.currentTarget).find('#title_structure_data').val(area_name); 
			$(e.currentTarget).find('#description_data').val(description);		
        });
	  
    }); 
    </script>  
@endsection
    

@endsection
