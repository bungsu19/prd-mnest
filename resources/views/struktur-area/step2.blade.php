@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <form class="form-signin" method="POST" action="/structure-area" enctype="multipart/form-data">
                    {{ csrf_field() }}
             <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                            <h2>
                                <b>Step 2</b>
                            </h2>
            <!-- #END# Select -->
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                        	<label> </label>
                                            <button type="button" class="btn btn-block bg-red waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add Data</button>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                    	 <!-- Basic Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <!-- <h2>
                                BASIC TABLES
                                <small>Basic example without any additional modification classes</small>
                            </h2> -->
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>NO</th>
                                        <th>Provinsi</th>
                                        <th>Kota</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"></th>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Table -->

                                    </div>
                              
                                 
                                  

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <button href="#" type="submit" class="btn btn-block bg-blue waves-effect">Finish</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
           
        
        </div>

        <!-- Default Size -->
         <form class="form-signin" method="POST" action="/structure-area-step2" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-4">
                                <label>Provinsi</label>
                                <select class="form-control show-tick" name="provinsi_id">
                                    <option value="">-- All Project --</option>
                                    <option value="10">project 1</option>
                                    <option value="20">project 2</option>
                                    <option value="30">project 3</option>
                                </select>
                               
                            </div>
                            <div class="col-sm-4">
                                <label>Kota</label>
                                <select class="form-control show-tick" name="city_id">
                                    <option value="">-- All Project --</option>
                                    <option value="10">project 1</option>
                                    <option value="20">project 2</option>
                                    <option value="30">project 3</option>
                                </select>
                                <input type="hidden" name="stucture_id" value="{{ $data->id }}">
                               
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn bg-red waves-effect" >ADD</button>
                            <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
    </section>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

@endsection
