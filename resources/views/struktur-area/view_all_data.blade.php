@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-header">
                            <font size="5" color="yellow">Structure Name :</font><font size="4" color="white"> {{ $data->title_structure }}</font><br>
                            <font size="5" color="yellow">Scope Project  &nbsp;&nbsp;&nbsp; :</font><font size="4" color="white"> @if($data->national == "IND")
                                                        NATIONAL
                                                    @else
                                                        LOCAL
                                                    @endif </font><br>
                            <font size="5" color="yellow">Desription &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</font>
                            <font size="4" color="white">{{ $data->description }}</font>
                            
                            
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>

                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2><b>Area Information</b></h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                      
                       <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Area Name</font></td>
                                            <td class="bg-column"><font color="yellow">Description</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                         @foreach($name_struk as $da)
                                            <tr>
                                                <th scope="row">{{ $no }}</th>
                                                <td> {{ $da->structure_name }}</td>
                                                <td><span class="span-dots" data-toggle="tooltip" title="{{ $da->description }}"> {{ $da->description }}</span></td>
                                                
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2><b>Districts/Cities Information</b></h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                       
                    <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Area Name</font></td>
                                            <td class="bg-column"><font color="yellow">Province</font></td>
                                            <td class="bg-column"><font color="yellow">District/City</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <?php $no = 1 ?>
                                         @foreach($step2 as $da1)
                                            <tr>
                                                <th scope="row">{{ $no }}</th>
                                                <td>{{ $da1->structure_name }}</td>
                                                <td> {{ $da1->provinsi_id }}</td>
                                                <td> {{ $da1->city_id }}</td>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                

                </div>
            </div>

             
    </section>
    

@endsection
