@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
			
			<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <font size="5" color="black"><b>Area Structure For Projects</b></font>
                           </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
								@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
									<a href="#" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">New Structure</a>
								@endif
							</div>
                     <div class="row clearfix">
                            
            <!-- #END# Select -->
                            <form class="form-signin" method="POST" action="{{url('/data-people/filter-data')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All People</option>
                                                        <option value="1"> People Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
						
						@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" width="15px" align="center" ><font color="yellow">No</font></td>
                                            <td class="bg-column" width="200"><font color="yellow">Structure Name</font></td>
                                             <td class="bg-column" width="300" ><font color="yellow">Description</font></td>
                                            <td class="bg-column" width="200"><font color="yellow">Scope Project</font></td>
                                           
                                            <td class="bg-column" width="200" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td > {{ $da->title_structure }}</td>
                                                <td>{{ $da->description }}</td>
                                                <td > @if($da->national == "IND")
                                                        NATIONAL
                                                    @else
                                                        LOCAL
                                                    @endif</td>
                                                
                                                <td align="center">
                                                   @if(Auth::user()->hak_access == 1)
													<a href="#" data-toggle="modal" data-target="#defaultModalEdit{{ $da->id }}" >
														 <i class="material-icons">rate_review</i>
													</a>
													<a href="{{url('/structure-area/view-sturcture')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
														  <i class="material-icons">remove_red_eye</i>
													</a>
													<a href="{{url('/structure-title/view')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
														<i class="material-icons">mode_edit</i>
													</a>
													<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete area structure '{{ $da->title_structure }}' ?" data-href="{{ url('/structure-title/delete') }}/{{ $da->id }}" title="Delete Data">
														<i class="material-icons">delete_forever</i>
													</a>
                                                    @elseif(Auth::user()->hak_access == 2)
                                                        <a href="#" data-toggle="modal" data-target="#defaultModalEdit{{ $da->id }}" >
                                                         <i class="material-icons">rate_review</i>
                                                        </a>
                                                        <a href="{{url('/structure-area/view-sturcture')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                              <i class="material-icons">remove_red_eye</i>
                                                        </a>
                                                        <a href="{{url('/structure-title/view')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                            <i class="material-icons">mode_edit</i>
                                                        </a>
                                                        <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete area structure '{{ $da->title_structure }}' ?" data-href="{{ url('/structure-title/delete') }}/{{ $da->id }}" title="Delete Data">
                                                            <i class="material-icons">delete_forever</i>
                                                        </a>
                                                    @else

                                                    @endif
                                            </td>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
							
							@endif
                    </div>
                </div>
            <!-- #END# Basic Examples -->
        </div>
        <form class="form-signin" method="POST" action="{{url('/structure-title')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"><font size="4" color="yellow"> New Structure</font> </h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Scope Project National</label>
                                 <div class="switch">
                                    <label>OFF<input type="checkbox" name="national" value="IND" checked><span class="lever"></span>ON</label>
                            </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label>Name Structure <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="tite_structure" id="tite_structure" maxlength="30">
								<span class="text-error" id="txt_tite_structure"></span>
                                <br>
                               
                               
                            </div>

                            <div class="col-sm-12">
                                <label>Structure Description <span class="mandatory">*</span></label>
                                <textarea name="description" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" id="description"></textarea>
                                <span class="text-error" id="txt_description"></span>
								<br>
                                <br>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <br>
                            <br>
								<button type="button" class="btn bg-blue waves-effect" id="send">Save</button>
								<button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim">Save</button>
								<button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
		
        @foreach($data as $dada)
        <form class="form-signin" method="POST" action="{{url('structure-title/update-title')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModalEdit{{ $dada->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"><font size="4" color="yellow"> Edit Structure </font></h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Scope Project National</label>
                                 <div class="switch">
                                    <label>OFF<input type="checkbox" name="national" value="{{ $dada->national }}" checked><span class="lever"></span>ON</label>
                            </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label>Name Structure <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" value="{{ $dada->title_structure }}" name="tite_structure" id="tite_structure_data" maxlength="10" required>
								<span class="text-error" id="txt_tite_structure_data"></span>
                                <input type="hidden" name="id" value="{{ $dada->id }}">
                                <br>
                               
                               
                            </div>

                            <div class="col-sm-12">
                                <label>Structure Description <span class="mandatory">*</span></label>
                                <textarea name="description" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" id="description_data" maxlength="100" required>{{ $dada->description }}</textarea>
								<span class="text-error" id="txt_description_data"></span>
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <br>
                            <br>
								<button type="submit" class="btn bg-blue waves-effect" id="send_data">Update</button>
								<!-- <button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim_data">UPDATE</button> -->
								<button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
        @endforeach
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br><br>
                        <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
		
    </section>
       @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 
			$('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>

<script type="text/javascript">
     $(function(){
		  $('#tite_structure').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#tite_structure').val().length > 0){
					$('#tite_structure').val($('#tite_structure').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#tite_structure', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,30));
		  });
		  
		  $('#description').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();  
				}else if($('#description').val().length > 0){
					$('#description').val($('#description').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#description', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,100));
		  });
		  
		  $("#send").click(function(){
			  if ($('#tite_structure').val().length < 1) {
					$('#txt_tite_structure').text('Title structure is empty');
			  }else{
					$('#txt_tite_structure').text('');
			  }
			  
			  if ($('#description').val().length < 1) {
					$('#txt_description').text('Description is empty');
			  }else{
					$('#txt_description').text('');
			  }
			  
			  if($('#tite_structure').val().length > 0 && $('#description').val().length > 0){
				  $("#kirim").click();
			  }
			  
		  });
		  
		  $('#tite_structure_data').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#tite_structure_data').val().length > 0){
					$('#tite_structure_data').val($('#tite_structure_data').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#tite_structure_data', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,30));
		  });
		  
		  $('#description_data').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();  
				}else if($('#description_data').val().length > 0){
					$('#description_data').val($('#description_data').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space*/
		  $(document).on('paste', '#description_data', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0,100));
		  });

            // handle max desc
            $('#description').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#description').val().length == 150)
                e.preventDefault();
            });
		  
		  $("#send_data").click(function(){
			  if ($('#tite_structure_data').val().length < 1) {
					$('#txt_tite_structure_data').text('Title structure is empty');
			  }else{
					$('#txt_tite_structure_data').text('');
			  }
			  
			  if ($('#description_data').val().length < 1) {
					$('#txt_description_data').text('Description is empty');
			  }else{
					$('#txt_description_data').text('');
			  }
			  
			  if($('#tite_structure_data').val().length > 0 && $('#description_data').val().length > 0){
				  $("#kirim_data").click();
			  } 
			  
		  });
	 });
	 </script>
    @endsection

@endsection
