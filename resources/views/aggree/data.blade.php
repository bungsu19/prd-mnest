@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
			
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif	
			
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow"><b>Aggree</b></font>
                        </div>

                    <!-- form add product -->
                     <form class="form-signin" method="POST" action="{{url('post-aggree') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>judul <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-line">
                                            <input type="text" name="title_aggree" class="form-control" placeholder="judul" value="{{ $data->title_aggree }}">
                                        </div>
										<span class="text-error" id="txt_judul"></span>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>isi <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-line">
                                             <textarea name="value_aggree" class="form-control ckeditor"
                                     placeholder="Isi Berita" rows="5">{{ $data->value_aggree }} </textarea>
                                              
                                            </select>
                                            <input type="hidden" name="id_aggree" value="{{ $data->id }}">
                                        </div>
                                        <span class="text-error" id="txt_branch"></span>
                                           
                                        </div>
                                    </div>

                                   
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('home')}}" class="btn bg-gray waves-effect" ><font size="4">Cancel</font></a>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="submit" class="btn bg-blue waves-effect" id="send"><font size="4">Save</font></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                    
                            </div>
                                    
                         </div>
                                    
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  

       @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    

</section>
    @endsection
    

@endsection
