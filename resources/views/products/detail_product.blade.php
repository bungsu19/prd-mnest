@extends('layout-menu.app')

@section('content')

   
<section class="content">
    
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow"> <b>Product Information</b></font>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    <!-- form add product -->
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Image Product </label>
                                        </div>
                                        <div class="col-sm-8">
                                            : <img src="{{ asset('upload/imageProduct') }}/{{ $data->image }}" id="profile-img-tag" width="200px" />
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Name Product </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->product_name }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Name Client </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:
                                           @foreach($client as $cl)
                                           
                                                @if($data->client_id === $cl->client_id) 
                                                    {{ $cl->company_name }}
                                                 @endif 
                                                
                                            @endforeach
                                             </label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Name Branch </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:
                                           @foreach($branch as $br)
                                           
                                                @if($data->branch_id === $br->id) 
                                                    {{ $br->branch_name }}
                                                 @endif 
                                                
                                            @endforeach
                                             </label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Product Type </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->product_type }}</label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Product SKU</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->product_sku }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Product Color </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->color }}</label>
                                           
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Weight</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->weight }}</label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Height</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->height }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Packaging</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:
                                              @foreach($pakcaging as $pak)
                                           
                                                @if($data->packaging_id === $pak->id) 
                                                    {{ $pak->type }}
                                                 @endif 
                                                
                                            @endforeach
                                        </label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Dimention</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->dimension }}</label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Product Capacity </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->product_capacity }}</label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Product Price </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>: Rp.{{ $data->product_price }}</label>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Production Date </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->production_date }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Expired Date</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <label>:{{ $data->expiry_date }}</label>
                                           
                                        </div>
                                    </div>

                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-8">
                                                <a href="{{url('product-data')}}" class="btn btn-block bg-gray waves-effect" ><font size="4">Back</font></a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                    

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
       <!--  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow"> <b>Location Branch</b></font>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
            <div class="body">
                <div id="mymap" class="gmap"></div>
            </div>
        </div>
        </div> -->
    </div>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

         
            <script src="http://maps.google.com/maps/api/js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

             <script type="text/javascript">


                var locations = <?php print_r(json_encode($data)) ?>;


                var mymap = new GMaps({
                  el: '#mymap',
                  lat: 21.170240,
                  lng: 72.831061,
                  zoom:6
                });


                $.each( locations, function( index, value ){
                    mymap.addMarker({
                      lat: value.lat,
                      lng: value.lng,
                      title: value.city,
                      click: function(e) {
                        alert('This is '+value.city+', gujarat from India.');
                      }
                    });
               });


              </script>
            



            
       
    </section>
    

@endsection
