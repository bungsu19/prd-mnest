@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow"><b>Add Product</b></font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('/product-data/add-product') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    

                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													 <select name="branch_id" class="form-control" id="branch_id">
														<option value="">-select-</option>
														@foreach($branch as $br)
															<option value="{{ $br->id }}">{{ $br->branch_name }}</option>
														@endforeach
														 
													 </select>
												</div>
												<span class="text-error" id="txt_branch_id"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Client <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													 <select name="client_id" class="form-control" id="client_id">
														<option value="">-select-</option>
														@foreach($client as $cl)
															<option value="{{ $cl->client_id }}">{{ $cl->company_name }}</option>
														@endforeach
														 
													 </select>
												</div>
												<span class="text-error" id="txt_client_id"></span>
                                         </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Product Type <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">

													 <input type="text" name="product_type" class="form-control" placeholder=" Product Type" id="product_type">
												</div>
												<span class="text-error" id="txt_product_type"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Product SKU <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
                                                 <!-- <select name="product_sku" class="form-control">
                                                    <option value=" ">-select-</option>
                                                    <option>SKU</option>
                                                 </select> -->
													<input type="text" name="product_sku" class="form-control" placeholder="input product Sku" id="product_sku">
												</div>
												<span class="text-error" id="txt_product_sku"></span>
											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> Product Name <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="text" class=" form-control" name="product_name" placeholder="Input Name Product" id="product_name">
												</div>
												<span class="text-error" id="txt_product_name"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Color <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<select name="color" class="form-control" id="color">
														<option value="">-select-</option>
														<option>Red</option>
														<option>Green</option>
														<option>Blue</option>
														<option>Black</option>
													</select>
												</div>
												<span class="text-error" id="txt_color"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Weight <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="number" class=" form-control" name="weight" placeholder="Input weight" id="weight" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
												</div>
												<span class="text-error" id="txt_weight"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Height <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="number" class="form-control" name="height" placeholder="Input Height" id="height" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
												</div>
												<span class="text-error" id="txt_height"></span>
											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Packaging <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<select name="packaging_id" class="form-control" id="packaging_id">
														<option value="">-select-</option>
													   @foreach($pakcaging as $pak)
														<option value="{{ $pak->id }}"> {{ $pak->type }}</option>
													   @endforeach
													</select>
												</div>
												<span class="text-error" id="txt_packaging_id"></span>
											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Dimension <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="text" class=" form-control" name="dimension" placeholder="Dimention" id="dimension">
												</div>
												<span class="text-error" id="txt_dimension"></span>
											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Product Capacity <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="text" class=" form-control" name="product_capacity" placeholder="Product Capacity" id="product_capacity">
												</div>
												<span class="text-error" id="txt_product_capacity"></span>
											</div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Product Price <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
														<input type="text" class=" form-control" name="price" placeholder="Product Price" id="price" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
												</div>
												<span class="text-error" id="txt_price"></span>
											</div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Production Date <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="production_date" class="form-control datepicker" placeholder="Production date..." id="production_date">
                                                </div>
												<span class="text-error" id="txt_production_date"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Expired Date <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="expired_date" class="form-control datepicker" placeholder="Expired date..." id="expired_date">
                                                </div>
												<span class="text-error" id="txt_expired_date"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Image<span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <input type="file" name="image" class="form-control" id="profile-img" >
											 <span class="text-error" id="txt_profile-img"></span>
                                             <br>
                                             <img src="{{ asset('default.png') }}" id="profile-img-tag" width="200px" />
                                        </div>
                                           
                                    </div>

                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-3">
                                                <a href="{{url('data-people')}}" class="btn bg-gray waves-effect" ><h5>Cancel</h5></a>
                                            </div>
                                            <div class="col-sm-3">
											    <button type="button" class="btn bg-blue waves-effect" id="send"><h4>Save</h4></button>
                                                <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                            </div>
                                        </div>
                                    </div>

                                    </form>
                                    
                                    

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
        
    </div>
            



            
       
    </section>
     @section('script')
     <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
	  
	  $('#product_type').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0)
				e.preventDefault();
      });
	  
	  $('#product_sku').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0)
				e.preventDefault();
      });
	  
	  $('#product_name').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0)
				e.preventDefault();
      });
	  
	  $('#dimension').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0)
				e.preventDefault();
      });
	  
	  $('#product_capacity').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0)
				e.preventDefault();
      });
		
	  $('#price').keyup(function() {
			$('#price').val(formatRupiah($('#price').val(), ''));
	  });
	  
	  function formatRupiah(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }

	  
	  $("#send").click(function(){
	      if ($('#branch_id').val().length < 1) {
                $('#txt_branch_id').text('Branch not selected');
          }else{
                $('#txt_branch_id').text('');
          }
		  
		  if ($('#client_id').val().length < 1) {
                $('#txt_client_id').text('Client not selected');
          }else{
                $('#txt_client_id').text('');
          }
		  
		  if ($('#product_type').val().length < 1) {
                $('#txt_product_type').text('Product type is empty');
          }else{
                $('#txt_product_type').text('');
          }
		  
		  if ($('#product_sku').val().length < 1) {
                $('#txt_product_sku').text('Product sku is empty');
          }else{
                $('#txt_product_sku').text('');
          }
		  
		  if ($('#product_name').val().length < 1) {
                $('#txt_product_name').text('Product name is empty');
          }else{
                $('#txt_product_name').text('');
          }
		  
		  if ($('#color').val().length < 1) {
                $('#txt_color').text('Color not selected');
          }else{
                $('#txt_color').text('');
          }
		  
		  if ($('#weight').val().length < 1) {
                $('#txt_weight').text('Weight is empty');
          }else if ($('#weight').val() ==  0) {
                $('#txt_weight').text('Weight not entered');
          }else{
                $('#txt_weight').text('');
          }
		  
		  if ($('#height').val().length < 1) {
                $('#txt_height').text('Height is empty');
          }else if ($('#height').val() == 0) {
                $('#txt_height').text('Height not entered');
          }else{
                $('#txt_height').text('');
          }
		  
		  if ($('#packaging_id').val().length < 1) {
                $('#txt_packaging_id').text('Packaging not selected');
          }else{
                $('#txt_packaging_id').text('');
          }
		  
		  if ($('#dimension').val().length < 1) {
                $('#txt_dimension').text('Dimension is empty');
          }else{
                $('#txt_dimension').text('');
          }
		  
		  if ($('#product_capacity').val().length < 1) {
                $('#txt_product_capacity').text('Product capacity is empty');
          }else{
                $('#txt_product_capacity').text('');
          }
		  
		  if ($('#price').val().length < 1) {
                $('#txt_price').text('Price product is empty');
          }else if ($('#price').val() ==  0) {
                $('#txt_price').text('Price product not entered');
          }else{
                $('#txt_price').text('');
          }
		  
		  if ($('#production_date').val().length < 1) {
                $('#txt_production_date').text('Production date is empty');
          }else{
                $('#txt_production_date').text('');
          }
		  
		  if ($('#expired_date').val().length < 1) {
                $('#txt_expired_date').text('Expired date is empty');
          }else{
                $('#txt_expired_date').text('');
          }
		  
		  if ($('#profile-img').val().length < 1) {
                $('#txt_profile-img').text('Profile image is empty');
          }else{
                $('#txt_profile-img').text('');
          }
		  
		  if($('#branch_id').val().length > 0 && $('#client_id').val().length > 0 && 
			 $('#product_type').val().length > 0 && $('#product_sku').val().length > 0 &&
			 $('#product_name').val().length > 0 && $('#color').val().length > 0 &&
			 $('#weight').val().length > 0 && $('#height').val().length > 0 && 
			 $('#packaging_id').val().length > 0 && $('#dimension').val().length > 0 && 
			 $('#product_capacity').val().length > 0 && $('#price').val().length > 0 && 
			 $('#production_date').val().length > 0 && $('#expired_date').val().length > 0 &&$('#profile-img').val().length > 0 ){
				 $( "#kirim" ).click();
			 }
				 
		  
	  });
	  
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  
    @endsection

@endsection
