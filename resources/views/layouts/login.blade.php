<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login | MNEST - Login page</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('plugins/login/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('plugins/login/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('plugins/login/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ asset('plugins/login/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle" style="height: 100%;">
				<div class="auth-box" style="height: 100%;">
					<div class="left">
						<div class="overlay"></div>
						<div class="content text">
							<img src="{{ url('/sdm-logo.png') }}" class="mylogo" alt="SDM Logo">
							<img src="{{ url('login-bg.png') }}" class="imgPeople" alt="image login background">
							<h1 class="heading">We make things easy for our job seekers, we provide the jobs and you apply via our simple and quick application process.</h1>
							<p>by Bisnis Integrasi Global</p>
						</div>
					</div>
					<div class="right">
						@yield('content')
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>