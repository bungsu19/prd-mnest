@if (isset($paginator2) && $paginator2->lastPage() > 1)

    <ul class="pagination">

        <?php
        $interval = isset($interval) ? abs(intval($interval)) : 2 ;
        $from = $paginator2->currentPage() - $interval;
        if($from < 1){
            $from = 1;
        }

        $to = $paginator2->currentPage() + $interval;
        if($to > $paginator2->lastPage()){
            $to = $paginator2->lastPage();
        }
        ?>

        <!-- first/previous -->
        @if($paginator2->currentPage() > 1)
            <li>
                <a href="{{ $paginator2->url(1) }}" aria-label="First">
                    <span aria-hidden="true">First</span>
                </a>
            </li>

            <li>
                <a href="{{ $paginator2->url($paginator2->currentPage() - 1) }}" aria-label="Previous">
                    <span aria-hidden="true">Prev</span>
                </a>
            </li>
        @endif

        <!-- links -->
        @for($i = $from; $i <= $to; $i++)
            <?php 
            $isCurrentPage = $paginator2->currentPage() == $i;
            ?>
            <li class="{{ $isCurrentPage ? 'active' : '' }}">
                <a href="{{ !$isCurrentPage ? $paginator2->url($i) : '#' }}">
                    {{ $i }}
                </a>
            </li>
        @endfor

        <!-- next/last -->
        @if($paginator2->currentPage() < $paginator2->lastPage())
            <li>
                <a href="{{ $paginator2->url($paginator2->currentPage() + 1) }}" aria-label="Next">
                    <span aria-hidden="true">Next</span>
                </a>
            </li>

            <li>
                <a href="{{ $paginator2->url($paginator2->lastpage()) }}" aria-label="Last">
                    <span aria-hidden="true">Last</span>
                </a>
            </li>
        @endif

    </ul>

@endif