@if (isset($paginator3) && $paginator3->lastPage() > 1)

    <ul class="pagination">

        <?php
        $interval = isset($interval) ? abs(intval($interval)) : 2 ;
        $from = $paginator3->currentPage() - $interval;
        if($from < 1){
            $from = 1;
        }

        $to = $paginator3->currentPage() + $interval;
        if($to > $paginator3->lastPage()){
            $to = $paginator3->lastPage();
        }
        ?>

        <!-- first/previous -->
        @if($paginator3->currentPage() > 1)
            <li>
                <a href="{{ $paginator3->url(1) }}" aria-label="First">
                    <span aria-hidden="true">First</span>
                </a>
            </li>

            <li>
                <a href="{{ $paginator3->url($paginator3->currentPage() - 1) }}" aria-label="Previous">
                    <span aria-hidden="true">Prev</span>
                </a>
            </li>
        @endif

        <!-- links -->
        @for($i = $from; $i <= $to; $i++)
            <?php 
            $isCurrentPage = $paginator3->currentPage() == $i;
            ?>
            <li class="{{ $isCurrentPage ? 'active' : '' }}">
                <a href="{{ !$isCurrentPage ? $paginator3->url($i) : '#' }}">
                    {{ $i }}
                </a>
            </li>
        @endfor

        <!-- next/last -->
        @if($paginator3->currentPage() < $paginator3->lastPage())
            <li>
                <a href="{{ $paginator3->url($paginator3->currentPage() + 1) }}" aria-label="Next">
                    <span aria-hidden="true">Next</span>
                </a>
            </li>

            <li>
                <a href="{{ $paginator3->url($paginator3->lastpage()) }}" aria-label="Last">
                    <span aria-hidden="true">Last</span>
                </a>
            </li>
        @endif

    </ul>

@endif