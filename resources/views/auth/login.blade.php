@extends('layouts.login')
<style>
	.alert{
		background-color: #fc9d85;
		color: white;
	}
</style>
@section('content')
<div class="content">
@if(session()->has('failed'))
            <div class="alert bg-red alert-dismissible" role="alert">
                 {{ session()->get('failed') }}
            </div>
            @endif
    <div class="header">
        <div class="logo text-left"><h1 style="margin-bottom: 0px;">Wellcome</h1></div>
        <p class="lead">Login to your account</p>
    </div>
    <form class="form-auth-small" method="POST" action="{{url('/login-new2') }}" autocomplete="off">
    {{ csrf_field() }}
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                <input id="email" type="text" class="form-control" name="email" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password">
            </div>
        </div>
		<div class="form-group">
			<div class="row clearfix">
				<div class="col-sm-5">
					<div class="form-group">
						<span class="element-left captcha">{!! captcha_img('default') !!}</span>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="form-group">
						<input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha" style="margin-top: 5px;">
					</div> 
				</div>
			</div>
		</div>
		<div class="form-group" style="margin-top: -100px;">
			<div class="row clearfix">
				<div class="col-sm-6">
					<div class="form-group">
						<i class="element-left btn btn-success btn-lg fa fa-refresh" id="refresh"></i>
					</div>
				</div>
			</div>
		</div>
        <div class="form-group clearfix">
            <label class="fancy-checkbox element-left">
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <span>Remember me</span>
            </label>
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN <i class="fa fa-sign-in"></i></button>
        <div class="bottom">
            <span class="helper-text"><i class="fa fa-lock"></i> <a href="{{ route('password.request') }}">Forgot password?</a></span>
        </div>
    </form>
</div>
<!---div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div-->

 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   

 <script type="text/javascript">
$('#refresh').click(function(){
  $.ajax({
		url: "{{ url('/refreshcaptcha') }}",
		type: 'get',
		  dataType: 'html',        
		  success: function(json) {
			$('.captcha').html(json);
		  },
		  error: function(data) {
			alert('Try Again.');
		  }
	});
}); 

setTimeout(function(){
		   $("div.alert").remove();
		}, 4000 );
</script>



@endsection
