@extends('layouts.login')

@section('content')
<div class="content">
    <div class="header">
        <div class="logo text-left"><h1 style="margin-bottom: 0px;">Forget Password</h1></div>
        <p class="lead" style="letter-spacing: 1px; color: #9c9c9c; line-height: 1;">Please enter your email address.<br>You will receive a new password via email.</p>
    </div>
    <form class="form-auth-small" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                <input id="email" type="text" class="form-control" name="email" placeholder="Email">
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-lg btn-block">Send New Password <i class="fa fa-send-o"></i></button>
        <div class="bottom">
            <span class="helper-text"><i class="fa fa-arrow-left"></i> <a href="{{ route('login') }}">Back to login page</a></span>
        </div>
    </form>
</div>
<!--div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div-->
@endsection
