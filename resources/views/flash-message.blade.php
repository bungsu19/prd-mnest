@if ($message = Session::get('success'))
<div class="alert alert-success alert-block" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	<strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())

@if(count($errors->all()) > 0)
<div class="alert alert-danger alert-block" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>
	@foreach ($errors->all() as $error)
	<strong>{{ $error }}</strong><br>
	@endforeach
</div>
@else
<div class="alert alert-danger" style="position: relative;">
	<button type="button" class="close" data-dismiss="alert">×</button>	
	Please check the form below for errors
</div>
@endif

@endif