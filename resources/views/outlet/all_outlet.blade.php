@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>TYPE OF OUTLET</b></h1>
                            </div><br>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">

                            <input type="fie" name="import" class="form-control">

                             <button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalOutletCategory">Add CATEGORY</button>
                              <!-- <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a> -->
                            
                            </div>
                            
                           <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <form class="form-signin" method="POST" action="{{url('/data-people/filter-data')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All People</option>
                                                        <option value="1"> People Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow"> No</font></td>
                                            <td class="bg-column"><font color="yellow">Outlet Type</font></td>
                                            <td class="bg-column"><font color="yellow">Desciption</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1 ?>
                                         @foreach($data as $da)
                                        <tr>
                                            <td>{{ $no }} </td>
                                            <td><a href="{{url('/all-outlet/category/')}}/{{ $da->id }}">{{ $da->outlet_category }}</a></td>
                                            <td>{{ $da->outlet_description }}</td>
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <form class="form-signin" method="POST" action="{{url('/all-outlet/addCategory')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalOutletCategory" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="largeModalLabel">Add Category Outlet</h2>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Type Category</label>
                                <select name="category_outlet" class="form-control">
                                    <option>-pilih-</option>
                                    <option >Toko Kelontong</option>
                                    <option >Toko Serba Ada</option>
                                    <option>Mini Market</option>
                                    <option>Supermarket</option>
                                    <option>Apotik</option>
                                </select>
                                 
                            </div>
                            <div class="col-sm-12">
                                <label>Description Outlet</label>
                                <textarea name="description" class="form-control" placeholder="input description"></textarea>
                                 <br>
                                 <br>
                            </div>
                             
                            
                        </div>
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE DATA</button>
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
    

@endsection
