@extends('layout-menu.app')

@section('content')

   
<section class="content">
    <!-- Markers -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                MARKERS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="mymap" class="gmap"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Markers -->
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

         
            <script src="http://maps.google.com/maps/api/js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.js"></script>

             <script type="text/javascript">


                var locations = <?php print_r(json_encode($data)) ?>;


                var mymap = new GMaps({
                  el: '#mymap',
                  lat: 21.170240,
                  lng: 72.831061,
                  zoom:6
                });


                $.each( locations, function( index, value ){
                    mymap.addMarker({
                      lat: value.lat,
                      lng: value.lng,
                      title: value.city,
                      click: function(e) {
                        alert('This is '+value.city+', gujarat from India.');
                      }
                    });
               });


              </script>
            

</section>
    

@endsection
