@extends('layout-menu.app')

@section('content')

   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>OUTLET OPEN</b></h1>
                            </div><br>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             
                              <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            </div>
                            
                           <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->
                            <form class="form-signin" method="GET" action="{{url('/filter-outlet-open')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-sm-3">
                                                    <label>Filter By</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option value="2">All</option>
                                                        <option value="0">Create By</option>
                                                        <option value="1">District</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5">
                                                    <label>Value</label>
                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
					</div>
							@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Outlet Name</font></td>
                                            <td class="bg-column"><font color="yellow">Address</font></td>
                                            <td class="bg-column"><font color="yellow">Phone Number</font></td>
                                            <td class="bg-column"><font color="yellow">District</font></td>
                                            <td class="bg-column"><font color="yellow">Created By</font></td>
                                            <td class="bg-column"><font color="yellow">Created Date</font></td>
                                            
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 0;
                                        if($outlet->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($outlet->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($outlet as $out)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $out->outlet_name }}</td>
                                            <td><span class="span-dots-outlet-open" title="{{ $out->outlet_address1 }}">{{ $out->outlet_address1 }}</span></td>
                                            <td>{{ $out->outlet_phone1 }}</td>
                                            <td>{{ $out->outlet_city }}</td>
                                            <td>{{ $out->created_by }}</td>
                                            <td>{{ substr($out->created_at,0.10) }}</td>
                                            <td align="center">
												<div class="row clearfix">
													<div class="col-sm-2">
														<a href="{{url('/all-outlet/detail-outlet/')}}/{{ $out->id }}" data-toggle="tooltip" title="View Data">
															  <i class="material-icons">remove_red_eye</i>
														</a>
													</div>
													<div class="col-sm-2">
														@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
															<a href="{{url('all-outlet/edit-outlet/')}}/{{ $out->id }}" data-toggle="tooltip" title="Edit Data">
																<i class="material-icons">mode_edit</i>
															</a>
														@endif
													</div>
													<div class="col-sm-2">
														@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
															<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete outlet '{{ $out->outlet_name }}' ?" data-href="{{url('/get-delete-outlet-open')}}/{{ $out->id }}" title="Delete Data">
																<i class="material-icons">delete_forever</i>
															</a>
														@endif
													</div>
												</div>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
							@endif

                    </div>
                </div>
            </div>
            
			<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
			
    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
			 
			
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>
    @endsection    

@endsection
