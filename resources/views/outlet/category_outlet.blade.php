@extends('layout-menu.app')

@section('content')

   
<section class="content">
          
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
					@if(session()->has('gagal'))
						<div class="alert bg-red alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 {{ session()->get('gagal') }}
						</div>
					@endif
					  @if(session()->has('failed'))
					<div class="alert bg-secondary alert-dismissible" role="alert" style="height: 200px; overflow-y: auto;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
						  <?php $pesan = session()->get('failed'); ?>
					   <?php for ($i=0; $i < count($pesan) ; $i++) { 
						   echo $pesan[$i].'<br>';
					   }
					   ?>
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                               <h1><b>Outlet</b></h1>
                            </div><br>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                 <div class="row clearfix">
                                    <div class="col-sm-7">
										<form action="{{ url('/importOutlet') }}" method="POST" enctype="multipart/form-data" class="form-inline">
										{{ csrf_field() }}
											<div class="col-xs-12 col-sm-12 col-md-12">
												<div class="form-group">
													<div class="col-md-9">
														<input type="file" name="import" class="form-control" >
													</div>
													<div class="col-md-3">
														<button type="submit" class="btn bg-blue waves-effect m-r-20">Import</button>		
													</div>
												</div>
											</div>
										</form>
                                    </div>
                                    <div class="col-sm-2">
										<a href="{{url('downloadDataOutlet')}}" class="btn bg-blue waves-effect m-r-20">Download</a>
                                    </div>
                                    <div class="col-sm-2">
										@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
											<a href="{{url('all-outlet/add-outlet')}}" class="btn bg-blue waves-effect m-r-20">New Outlet</a>
										@endif
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                            </div>
                            
                           <div class="row clearfix">
								<form class="form-signin" method="GET" action="{{url('/filter-outlet-resmi')}}" enctype="multipart/form-data">
								{{ csrf_field() }}
									 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
										<div class="card" style="margin: 0px;">
												<div class="body">
													<div class="row clearfix">
														<div class="col-sm-3">
															<label>Filter By</label>
															<select name="search" class="form-control show-tick">
																<option value="2">All</option>
																<option value="0">Name</option>
																<option value="1">Address</option>
															</select>
														</div>
														<div class="col-sm-5">
															<label>Value</label>
															<input type="text" name="filter_value" class="form-control" placeholder="value">
														</div>
														<div class="col-sm-4">
														<br>
															<label></label>
															<button type="submit" class="btn bg-blue waves-effect m-r-20">Search</button>
														</div>
													</div>
												</div>
											</div>
										</div>
								</form>
							</div>
						</div>
							@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Outlet Name</font></td>
                                            <td class="bg-column"><font color="yellow">Address</font></td>
                                            <td class="bg-column"><font color="yellow">Phone Number</font></td>
                                            <td class="bg-column"><font color="yellow">Email Address</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                        $no = 0;
                                        if($outlet->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($outlet->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($outlet as $out)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td><span class="span-dots-name-outlet">{{ $out->outlet_name }}</span></td>
                                            <td><span class="span-dots" title="{{ $out->outlet_address1 }}">{{ $out->outlet_address1 }}</span></td>
                                            <td>{{ $out->outlet_phone1 }}</td>
                                            <td>{{ $out->outlet_email_address }}</td>
                                            <td align="center">
												<a href="{{url('/all-outlet/detail-outlet/')}}/{{ $out->id }}" data-toggle="tooltip" title="View Data">
													<i class="material-icons">remove_red_eye</i>
												</a>
												@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2 || Auth::user()->hak_access == 4)
													<a href="{{url('all-outlet/edit-outlet/')}}/{{ $out->id }}" data-toggle="tooltip" title="Edit Data">
														<i class="material-icons">mode_edit</i>
													</a>
												@endif
												@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
													<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete outlet '{{ $out->outlet_name }}' ?" data-href="{{url('/get-delete-outlet')}}/{{ $out->id }}" title="Delete Data">
														<i class="material-icons">delete_forever</i>
													</a>
												@endif
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
							@endif
                </div>
            </div>
        </div>
            

        <form class="form-signin" method="POST" action="{{url('/all-outlet/addCategory')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalOutletCategory" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="largeModalLabel">Add Category Outlet</h2>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Type Category</label>
                                <select name="category_outlet" class="form-control">
                                    <option>-pilih-</option>
                                    <option >Toko Kelontong</option>
                                    <option >Toko Serba Ada</option>
                                    <option>Mini Market</option>
                                    <option>Supermarket</option>
                                    <option>Apotik</option>
                                </select>
                                 
                            </div>
                            <div class="col-sm-12">
                                <label>Description Outlet</label>
                                <textarea name="description" class="form-control" placeholder="input description"></textarea>
                                 <br>
                                 <br>
                            </div>
                             
                            
                        </div>
                        
                        <div class="modal-footer">
                            <br>
                           <button type="submit" class="btn btn-link bg-blue waves-effect"> SAVE DATA</button>
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else { 
                $('#formfilter').hide(999); 
                $statusfilter = 0;
            }
             }); 
			 
			 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
        });
		
	</script>

    @endsection    

@endsection
