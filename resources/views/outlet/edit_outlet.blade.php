@extends('layout-menu.app')

@section('content')
<style type="text/css">
    .text-error
    {
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
            margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow">Edit Outlet</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('all-outlet/edit-outlet')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                             <select name="branch" class="form-control" id="branch">
                                                 @foreach($type as $br)
                                                       <option value="{{ $br->id }}" @if($data->branch_id === $br->id) selected='selected' @endif > {{ $br->branch_name }}</option>
                                                    @endforeach
                                                
                                            </select>
                                             <span class="text-error" id="txt_branch"></span>
                                         </div>
                                        
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Outlet Category <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                            
                                             <select name="outlet_id" class="form-control" id="outlet_id">
                                               @foreach($category as $cate)
                                                   <option value="{{ $cate->id }}" @if($data->outlet_category_id === $cate->id) selected='selected' @endif > {{ $cate->outlet_category }}</option>
                                                @endforeach
                                                
                                            </select>
                                            <span class="text-error" id="txt_outlet_id"></span>
                                         </div>
                                    </div>
                                   
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Outlet Name <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="name_outlet" class="form-control" placeholder="input Outlet Name" value="{{ $data->outlet_name }}" id="name_outlet" maxlength="50">
                                             </div>
                                             <span class="text-error" id="txt_name_outlet"></span>
                                         </div>
                                        </div>
                                    </div>
									
									<div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Address 1 <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                    <textarea type="text" name="address1" class="form-control" placeholder="input address 1" id="address1" style="height: 55px; max-height: 55px; min-height: 55px; max-width: 100%; min-width: 100%;" maxlength="150">{{ $data->outlet_address1 }}</textarea>
                                                </div>
												<span class="text-error" id="txt_address1"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Address 2</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <textarea type="text" name="address2" class="form-control" placeholder="input address 2" id="address2" style="height: 55px; max-height: 55px; min-height: 55px; max-width: 100%; min-width: 100%;" maxlength="150">{{ $data->outlet_address2 }}</textarea>
                                             </div>
											 <span class="text-error" id="txt_address2"></span>
                                         </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                         <div class="col-sm-4">
                                            <label>Province <span class="mandatory">*</span></label>
                                            <select name="province" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province">
                                                <option>{{ $data->outlet_province }}</option>
                                                @foreach($province as $result)
                                                <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                                @endforeach
                                            </select>
                                            <span class="text-error" id="txt_province"></span>
                                             </div>
                                         <div class="col-sm-4">
                                            <label>District <span class="mandatory">*</span></label>
                                            <div class="regency">
                                            <select name="district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="district">
                                                        <option>{{ $data->outlet_city }}</option>
                                                    </select>
                                                    <span class="text-error" id="txt_district"></span>
                                            </div>
                                         </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                         <div class="col-sm-4">
                                            <label>Sub District <span class="mandatory">*</span></label>
                                            <div class="subdistrict">
                                            <select name="sub_district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="sub_district">
                                                    <option>{{ $data->outlet_sub_district }}</option>
                                            </select>
                                            <span class="text-error" id="txt_sub_district"></span>
                                        </div>
                                         </div>
                                         <div class="col-sm-4">
                                            <label>Village <span class="mandatory">*</span></label>
                                            <div class="village">
                                                <select name="village" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="village">
                                                    <option>{{ $data->outlet_village }}</option>
                                                </select>
                                                <span class="text-error" id="txt_village"></span>
                                            </div>
                                         </div>
                                    </div>

                                     
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                        <div class="col-sm-2">
                                             <label>RT <span class="mandatory">*</span></label>
                                              <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="rt_rw" class="form-control" placeholder="Input RW" value="{{ $data->outlet_tr_rw }}" id="rt" min="1" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
                                              <span class="text-error" id="txt_rt"></span>
                                         </div>
                                         </div>
                                         <div class="col-sm-2">
                                             <label>RW <span class="mandatory">*</span></label>
                                              <div class="form-group">
                                               <div class="form-line">
                                                     <input type="text" name="rw" class="form-control" placeholder="Input RW" value="{{ $data->outlet_rw }}" id="rw" min="1" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                 </div>
                                                 <span class="text-error" id="txt_rw"></span>
                                             </div>
                                         </div>
                                         <div class="col-sm-4">
                                            <label>Post Code <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="post_code" class="form-control" placeholder="input Post Code" value="{{ $data->outlet_post_code }}" id="post_code" min="1" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
                                             <span class="text-error" id="txt_post_code"></span>
                                         </div>
                                         </div>
                                    </div>

                                    

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 1 <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="phone1" class="form-control" placeholder="input phone number 1" value="{{ $data->outlet_phone1 }}" id="phone1" min="1" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                 </div>
                                                  <span class="text-error" id="txt_phone1"></span>
                                             </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 2</label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="phone2" class="form-control" placeholder="input phone number 2" value="{{ $data->outlet_phone2 }}" id="phone2" min="1"oninput="this.value = this.value.replace(/[^0-9]/g, '');" >
                                             </div>
                                             <span class="text-error" id="txt_phone2"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Email Address <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="email" class="form-control" placeholder="input email address" value="{{ $data->outlet_email_address }}"  id="email" maxlength="50">
                                             </div>
                                              <span class="text-error" id="txt_email"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contact Person <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="owner_name" class="form-control" placeholder="Input Contact Person" value="{{ $data->owner_name }}" id="owner_name" maxlength="50">
                                             </div>
                                              <span class="text-error" id="txt_owner_name"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>NPWP <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                  <input type="text" name="npwp" class="form-control" placeholder="Input NPWP" value="{{ $data->npwp }}" id="npwp" min="1" oninput="this.value = this.value.replace(/[^0-9]/g, '');">

                                                   <input type="hidden" name="langitude" class="form-control" placeholder="Input Longitude" value="{{ $data->outlet_langitude }}" id="longitude" >

                                                    <input type="hidden" name="latitude" class="form-control" placeholder="Input Lattitude" value="{{ $data->outlet_latitude }}" id="lattitude" >
                                              </div>
                                              <span class="text-error" id="txt_npwp"></span>
                                          </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Image <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
												<div class="form-line">
													<input type="file" name="image1" class="form-control" id="image" onchange="loadFile(event)">
													<img id="output" class="p-image" width="100px" src="{{ URL::to('outlet/'.$data->image1) }}"/>
													<input type="hidden" name="image1_old" value="{{ $data->image1 }}">
												</div>
												<span class="text-error" id="txt_output"></span>
											</div>
                                        </div>
                                    </div>

                                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('all-outlet')}}" class="btn btn-block bg-gray waves-effect" ><font size="4">Cancel</font></a>
                                            </div>
                                            <div class="col-sm-5">
                                                <button href="#" type="button" class="btn bg-blue waves-effect" id="send" ><font size="4">Update</font></button>
                                                <button href="#" type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim"><h4>Update</h4></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   
	<script type="text/javascript">
	$(document).ready(function() {
	  $(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	  });
	}); 
	</script>  
    
	<script>
	  var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
	  };
	</script> 

    <script type="text/javascript">
        $('#province').change(function(){
			
			$('.regency select').find('option').remove().end().append('<option value="">- Select Regency/City -</option>').val('');
			$('.subdistrict select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
			
			$('.subdistrict select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
			
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>

    <script type="text/javascript">
     $(function(){
          $('#branch').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0)
                    e.preventDefault();
          });

		  //outlet name cannot long space on input
		  $('#name_outlet').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name_outlet').val().length > 0){
					$('#name_outlet').val($('#name_outlet').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on outlet name*/
		  $(document).on('paste', '#name_outlet', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
         
		  //address 1 cannot long space on input
		  $('#address1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#address1').val().length > 0){
					$('#address1').val($('#address1').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on address 1*/
		  $(document).on('paste', '#address1', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 150));
		  });
		  
		  //address 2 cannot long space on input
		  $('#address2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#address2').val().length > 0){
					$('#address2').val($('#address2').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on address 2*/
		  $(document).on('paste', '#address2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 150));
		  });
		  
		  //email cannot long space on input
		  $('#email').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
				
		  });
		  
		  /*handle paste with extra space on email*/
		  $(document).on('paste', '#email', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s+/g, '');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  function validateEmail($email) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              return emailReg.test( $email );
            }	

			//rw maximum length 3
		  $('#rt').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#rt').val().length == 3)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on rt*/
			  $(document).on('paste', '#rt', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 3);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		   
		   //rw maximum length 3
		  $('#rw').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#rw').val().length == 3)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on rw*/
			  $(document).on('paste', '#rw', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 3);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		   
		   //post code maximum length 3
		  $('#post_code').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#post_code').val().length == 7)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on post code*/
			  $(document).on('paste', '#post_code', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 7);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		   
		   //phone 1 maximum length 15
		  $('#phone1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#phone1').val().length == 15)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on phone 1*/
			  $(document).on('paste', '#phone1', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 15);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		   
		    //phone 2 maximum length 15
		  $('#phone2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#phone2').val().length == 15)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on phone 2*/
			  $(document).on('paste', '#phone2', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 15);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		   
		   //owner_name cannot long space on input
		  $('#owner_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#owner_name').val().length > 0){
					$('#owner_name').val($('#owner_name').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on owner_name*/
		  $(document).on('paste', '#owner_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  //npwp maximum length 20
		  $('#npwp').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#npwp').val().length == 20)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on npwp*/
			  $(document).on('paste', '#npwp', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  var isi = text.substring(0, 20);
				  $(this).val(isi.replace(/\D/g, ''));
		   });
		  
		  $("#image").change(function(){
				var fileExtension = ['jpeg', 'jpg', 'png'];
				if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
					$("#output").val();
					$('#txt_output').text("Only formats are allowed : jpeg, jpg, png");
				}else{
					$('#txt_output').text('');
					var loadFile = function(event) {
						var output = document.getElementById('output');
						output.src = URL.createObjectURL(event.target.files[0]);
					  };
				}
			});
          
          $("#send").click(function(){
              if ($('#branch').val().length < 1) {
                    $('#txt_branch').text('Branch is empty');
              }else{
                    $('#txt_branch').text('');
              }
              
              if ($('#outlet_id').val().length < 1) {
                    $('#txt_outlet_id').text('Outlet not selected');
              }else{
                    $('#txt_outlet_id').text('');
              }
              
              if ($('#name_outlet').val().length < 1) {
                    $('#txt_name_outlet').text('Outlet name is empty');
              }else{
                    $('#txt_name_outlet').text('');
              }
              
              if ($('#address1').val().length < 1) {
                    $('#txt_address1').text('Address 1 is empty');
              }else{
                    $('#txt_address1').text(''); 
              }
              
              if ($('#province').val().length < 1) {
                    $('#txt_province').text('Province not selected');
              }else{
                    $('#txt_province').text('');
              }
              
              if ($('#district').val().length < 1) {
                    $('#txt_district').text('District not selected');
              }else{
                    $('#txt_district').text('');
              }
              
              if ($('#sub_district').val().length < 1) {
                    $('#txt_sub_district').text('Sub district not selected');
              }else{
                    $('#txt_sub_district').text('');
              }
              
              if ($('#village').val().length < 1) {
                    $('#txt_village').text('Village not selected');
              }else{
                    $('#txt_village').text('');
              }
              
              if ($('#rt').val().length < 1) {
                    $('#txt_rt').text('Rt is empty');
              }else if ($('#rt').val() == 0) {
                    $('#txt_rt').text('Rt not entered');
              }else{
                    $('#txt_rt').text('');
              }
              
              if ($('#rw').val().length < 1) {
                    $('#txt_rw').text('Rw is empty');
              }else if ($('#rw').val() == 0) {
                    $('#txt_rw').text('Rw not entered');
              }else{
                    $('#txt_rw').text('');
              }
              
              if ($('#post_code').val().length < 1) {
                    $('#txt_post_code').text('Postal code is empty');
              }else if ($('#post_code').val() == 0) {
                    $('#txt_post_code').text('Postal code not entered');
              }else{
                    $('#txt_post_code').text('');
              }
              
              if ($('#phone1').val().length < 1) {
                    $('#txt_phone1').text('Phone 1 is empty');
              }else if ($('#phone1').val() == 0) {
                    $('#txt_phone1').text('Phone 1 not entered');
              }else{
                    $('#txt_phone1').text('');
              }
              
              if ($('#email').val().length < 1) {
                    $('#txt_email').text('Email is empty');
              }else if (!validateEmail($('#email').val())) {
                    $('#txt_email').text('Email format is false');
              }else{
                    $('#txt_email').text('');
              }
              
              if ($('#owner_name').val().length < 1) {
                    $('#txt_owner_name').text('Owner name is empty');
              }else{
                    $('#txt_owner_name').text('');
              }
              
              if ($('#npwp').val().length < 1) {
                    $('#txt_npwp').text('Npwp is empty');
              }else if ($('#npwp').val() == 0) {
                    $('#txt_npwp').text('Npwp not entered');
              }else{
                    $('#txt_npwp').text('');
              }
			  
              
			  var fileExtension = ['jpeg', 'jpg', 'png'];
              if($('#branch').val().length > 0 &&
                 $('#outlet_id').val().length > 0 &&
                 $('#name_outlet').val().length > 0 &&
                 $('#address1').val().length > 0 &&
                 $('#province').val().length > 0 &&
                 $('#district').val().length > 0 &&
                 $('#village').val().length > 0 &&
                 ($('#rt').val().length > 0 && $('#rt').val() != '0') &&
                 ($('#rw').val().length > 0 && $('#rw').val() != '0') &&
                 ($('#post_code').val().length > 0 && $('#post_code').val() != '0') &&
                 ($('#phone1').val().length > 0 && $('#phone1').val() != '0') &&
                 ($('#email').val().length > 0 && validateEmail($('#email').val())) &&
                 $('#owner_name').val().length > 0 &&
                 ($('#npwp').val().length > 0 && $('#npwp').val() != '0')){
                      $("#kirim").click();
                  }
              
          });
     });
     </script>

</section>
    

@endsection
