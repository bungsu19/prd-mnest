@extends('layout-menu.app')

@section('content')

   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                
                    @if(session()->has('failed'))
                    <div class="alert bg-red alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                         {{ session()->get('failed') }}
                    </div>
                    @endif

                     @if(session()->has('message'))
                    <div class="alert bg-green alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                         {{ session()->get('message') }}
                    </div>
                    @endif
                
                    <div class="card">
                        <div class="header">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>OUTLET OPEN</b></h1>
                            </div><br>
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             
                              <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            </div>
                            
                           <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->
                            <form class="form-signin" method="GET" action="{{url('/filter-outlet-open')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-sm-3">
                                                    <label>Filter By</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option value="2">All</option>
                                                        <option value="0">Create By</option>
                                                        <option value="1">District</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5">
                                                    <label>Value</label>
                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                            <div class="table-responsive">
                               <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                             <th>No</th>
                                            <th>Provinsi</th>
                                            <th>Kabupaten</th>
                                            <th>Kecamatan</th>
                                            <th>Desa</th>
                                            <th>Kode Pos</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <td align="center">{{ $no }}</td>
                                                <td align="center"> {{ $da->province }}</td>
                                                <td align="center"> {{ $da->district }}</td>
                                                <td align="center"> {{ $da->subdistrict }}</td>
                                                <td align="center"> {{ $da->village }}</td>
                                                <td align="center"> {{ $da->code_pos }}</td>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                    </tbody>
                                </table>
                                 <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
                        

                    </div>
                </div>
            </div>
            
            <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
                    <div class="modal-footer">
                         <a class="btn-ok"> 
                            <button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
                        </a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
            
    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
             
             $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
             
             
            
        });
        
        setTimeout(function(){
           $("div.alert").remove();
        }, 5000 );
</script>
    @endsection    

@endsection
