@extends('layout-menu.app')

@section('content')
<div style="padding-top: 100px;">
<section class="content" style="margin-top: 0px;">
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="header">
                    <h2><b>Update Form {{ $titlepage }}</b></h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="body">
                    @include('flash-message')
                    <!-- form add product -->
                    <form method="POST" autocomplete="off" action="{{ url('/product/edit/') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $id }}" name="id">
                    <div class="form-row clearfix">
                        <div class="form-group row left">
                            <div class="col-md-12" style="margin: 0; padding: 0">
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Name <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="text" name="name" class="form-control" placeholder="Product Name" style="border-bottom: 1px solid #eee;" value="{{ old('name', $product->product_name) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product SKU <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="text" name="sku" class="form-control" placeholder="Product SKU" value="{{ old('sku', $product->product_sku) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Category <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="category" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                            <option value="" selected="selected" disabled="disabled">- Select Category -</option>
                                            <option data-tokens="Category 1" value="1"
                                            @if(old('category', $product->product_category) == 1)
                                            selected="selected"
                                            @endif
                                            >Category 1</option>
                                            <option data-tokens="Category 2" value="2"
                                            @if(old('category', $product->product_category) == 2)
                                            selected="selected"
                                            @endif
                                            >Category 2</option>
                                            <option data-tokens="Category 3" value="3"
                                            @if(old('category', $product->product_category) == 3)
                                            selected="selected"
                                            @endif
                                            >Category 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Type <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="type" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                            <option value="" selected="selected" disabled="disabled">- Select Type -</option>
                                            <option data-tokens="Type 1" value="1"
                                            @if(old('type', $product->product_type) == 1)
                                            selected="selected"
                                            @endif
                                            >Type 1</option>
                                            <option data-tokens="Type 2" value="2"
                                            @if(old('type', $product->product_type) == 2)
                                            selected="selected"
                                            @endif
                                            >Type 2</option>
                                            <option data-tokens="Type 3" value="3"
                                            @if(old('type', $product->product_type) == 3)
                                            selected="selected"
                                            @endif
                                            >Type 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Client <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="client" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                            <option value="" selected="selected" disabled="disabled">- Select Client -</option>
                                            @foreach($client as $result)
                                                <option data-tokens="{{ $result->company_name }}" value="{{ $result->client_id }}"
                                                @if(old('client', $product->client_code) == $result->client_id)
                                                selected="selected"
                                                @endif
                                                >{{ $result->company_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Color</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="text" name="color" class="form-control" placeholder="Product Color"  value="{{ old('color', $product->color) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Weight</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="number" min="0"  name="weight" class="form-control" placeholder="0" value="{{ old('weight', $product->weight) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Height</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="number" min="0" name="height" class="form-control" placeholder="0" value="{{ old('height', $product->height) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Packaging <small style="color: #f00;">*</small></label>
                                    </div>
                                    <div class="col-md-6">
                                        <select name="packaging" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                            <option value="" selected="selected" disabled="disabled">- Select Packaging -</option>
                                            <option data-tokens="Packaging 1" value="1"
                                            @if(old('packaging', $product->packaging) == 1)
                                            selected="selected"
                                            @endif
                                            >Packaging 1</option>
                                            <option data-tokens="Packaging 2" value="2"
                                            @if(old('packaging', $product->packaging) == 2)
                                            selected="selected"
                                            @endif
                                            >Packaging 2</option>
                                            <option data-tokens="Packaging 3" value="3"
                                            @if(old('packaging', $product->packaging) == 3)
                                            selected="selected"
                                            @endif
                                            >Packaging 3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Dimension</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="text" name="dimension" class="form-control" placeholder="Product Dimension" value="{{ old('dimension', $product->dimension) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Capacity</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-line">
                                            <input type="text" name="capacity" class="form-control" placeholder="Product Capacity" value="{{ old('capacity', $product->product_capacity) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Price</label>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            <div class="form-line">
                                                <input id="price" type="number" class="form-control" name="price" min="0" placeholder="0" value="{{ old('price', $product->product_price) }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Date</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="input-group date" id="bs_datepicker_component_container">
                                            <div class="form-line">
                                                <input type="text" name="date" id="datepicker" class="form-control" placeholder="Please choose a date..."  value="{{ old('date', $product->product_date) }}">
                                            </div>
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4">
                                        <label for="exampleInputEmail1">Product Expiry Date</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="input-group date" id="bs_datepicker_component_container">
                                            <div class="form-line">
                                                <input type="text" name="exdate" id="datepickerx" class="form-control" placeholder="Please choose a date..."  value="{{ old('exdate', $product->expiry_date) }}">
                                            </div>
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group right" style="text-align: left;">
                            <div class="col-md-4"></div>
                            <div class="col-md-8" style="margin: 0; padding: 0">
                                <a href="{{url('client/index')}}" class="btn btn-default" style="padding: 5px 20px 10px;"><i class="fa fa-close"></i> Cancel</a>
                                <button class="btn btn-primary" type="submit" style="padding: 5px 20px 10px;"><i class="fa fa-send-o"></i> Update</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- #END# form add prodct -->
                </div>

            </div>
        </div>
    </div>

</section>
</div>
@endsection

@section('script')

<script type="text/javascript">
$(document).ready(function () {
    $('#datepicker').datepicker({
      uiLibrary: 'bootstrap',
      autoclose: true,
      todayHighlight: true,
      pickerPosition: 'top-left',
      dateFormat: 'dd-mm-yyyy',
    });

    $('#datepickerx').datepicker({
      uiLibrary: 'bootstrap',
      autoclose: true,
      todayHighlight: true,
      pickerPosition: 'top-left',
      dateFormat: 'dd-mm-yyyy',
    });
});
</script>

@endsection