@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <!-- <h2>
                   All Product
                </h2> -->
                 
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                All Religion
                            </h2>
                            <br>
                            <div class="col-xs-2">
                             <button type="button" class="btn btn-block bg-red waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">Add Religion </button>

                            </div>

                        <br>
                        <br>
                            <!-- Select -->
                   
            <!-- #END# Select -->
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name Religion</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                             <th>No</th>
                                            <th>Name Reigion</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                         <?php $no = 1 ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <th scope="row">{{ $no }}</th>
                                                <td> {{ $da->name_religion }}</td>
                                                <td align="center">
                                                <ul class="header-dropdown m-r--5">
                                                <li class="dropdown">
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                   <ul class="dropdown-menu pull-right">
                                                   
                                                     <li><a data-toggle="modal" data-target="#modal-edit{{ $da->id }}">Edit </a></li>
                                                    <li><a onclick="return confirm('Are you sure?')" href="/religion/delete/{{ $da->id }}">Delete</a></li>
                                                </ul>
                                            </li>
                                            </ul> 
                                            </td>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>

         <!-- Default Size -->
         <form class="form-signin" method="POST" action="{{url('/religion/add')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel"> Add Data </h4>
                        </div>
                        <div class="modal-body">
                            
                            <div class="col-sm-7">
                                <label>Nama Agama</label>
                               
                            <input type="text" class="form-control" name="name_religion">
                            <br>
                            <br>
                            <button type="submit" class="btn bg-red waves-effect" >ADD DATA</button>
                            <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CANCEL</button>
                               
                            </div>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
        @foreach($data as $dada)
            <form class="form-signin" method="POST" action="{{url('/religion/edit')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="modal-edit{{ $dada->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel"> Add Data </h4>
                        </div>
                        <div class="modal-body">
                            
                            <div class="col-sm-7">
                                <label>Nama Agama</label>
                               
                            <input type="text" class="form-control" name="name_religion" value="{{ $dada->name_religion }}">

                            <input type="hidden" name="id" value="{{ $dada->id }}">
                            <br>
                            <br>
                            <button type="submit" class="btn bg-red waves-effect" >EDIT DATA</button>
                            <button type="button" class="btn bg-red waves-effect" data-dismiss="modal">CANCEL</button>
                               
                            </div>
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
            @endforeach
    </section>
    

@endsection
