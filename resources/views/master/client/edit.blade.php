@extends('layout-menu.app')

@section('content')
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                <div class="header">
                    <h2><b>Create Form {{ $titlepage }}</b></h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="body">
                    @include('flash-message')
                    <!-- form add product -->
                    <form method="POST" autocomplete="off" action="{{ url('/client/edit/') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{ $id }}" name="id">
                    <div class="form-row clearfix">
                        <div class="form-group left">
                            <div class="col-md-12" style="margin: 0; padding: 0">
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Name <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="name" class="form-control" placeholder="Company Name" value="{{ old('name', $client->company_name) }}"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Branch <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><select name="branch" class="form-control"  data-live-search="true">
                                        <option>- Select Branch -</option>
                                        <option data-tokens="Branch 1" value="1"
                                        @if(old('branch', $client->branch_id) == 1)
                                        selected="selected"
                                        @endif
                                        >Branch 1</option>
                                        <option data-tokens="Branch 2" value="2"
                                        @if(old('branch', $client->branch_id) == 2)
                                        selected="selected"
                                        @endif
                                        >Branch 2</option>
                                        <option data-tokens="Branch 3" value="3"
                                        @if(old('branch', $client->branch_id) == 3)
                                        selected="selected"
                                        @endif
                                        >Branch 3</option>
                                    </select></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Address</label></div>
                                    <div class="col-md-6"><div class="form-line"><textarea class="form-control" rows="5" id="address" name="address">{{ old('address', $client->company_address) }}</textarea></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">RT</label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="rt" value="{{ old('rt', $client->company_rt) }}" class="form-control" placeholder="RT"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">RW</label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="rw" value="{{ old('rw', $client->company_rw) }}" class="form-control" placeholder="RW"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Postcode</label></div>
                                    <div class="col-md-6"><div class="postcode autocomplete">
                                        <div class="form-line"><input type="number" id="myInput" name="postcode" value="{{ old('postcode', $client->company_postcode) }}" class="form-control" placeholder="Company Postcode"></div>
                                    </div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Province</label></div>
                                    <div class="col-md-6"><div class="form-line"><select name="province" class="form-control" data-live-search="true" id="province">
                                        <option data-tokens="{{ $client->company_province }}" value="{{ $client->company_province }}">{{ $client->company_province }}</option>
                                        <option>- Select Province -</option>
                                        @foreach($province as $result)
                                        <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                        @endforeach
                                    </select></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Regency/City</label></div>
                                    <div class="col-md-6"><div class="regency form-line">
                                        <select name="regency" class="form-control"  data-live-search="true">
                                            <option data-tokens="{{ $client->company_regency }}" value="{{ $client->company_regency }}">{{ $client->company_regency }}</option>
                                            <option>- Select Regency/City -</option>
                                        </select>
                                    </div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Subdistrict</label></div>
                                    <div class="col-md-6"><div class="subdistrict form-line">
                                        <select name="subdistrict" class="form-control"  data-live-search="true">
                                            <option data-tokens="{{ $client->company_subdistrict }}" value="{{ $client->company_subdistrict }}">{{ $client->company_subdistrict }}</option>
                                            <option>- Select Subdistrict -</option>
                                        </select>
                                    </div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Village</label></div>
                                    <div class="col-md-6"><div class="village form-line">
                                        <select name="village" class="form-control"  data-live-search="true">
                                            <option data-tokens="{{ $client->company_village }}" value="{{ $client->company_village }}">{{ $client->company_village }}</option>
                                            <option>- Select Village -</option>
                                        </select>
                                    </div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Phone <small style="color: #f00;">*</small><br><small style="font-size: 11px; font-style: italic;color: #cccccc;">Note: 0 if don't have</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="number" name="phone" value="{{ old('phone', $client->company_phone1) }}" class="form-control" placeholder="Company Phone"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Handphone <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="number" name="handphone" value="{{ old('handphone', $client->company_phone2) }}" class="form-control" placeholder="Company Handphone"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Fax</label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="number" name="fax" value="{{ old('fax', $client->company_fax) }}" class="form-control" placeholder="Company Fax"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Company Email <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="email" value="{{ old('email', $client->company_email) }}" class="form-control" placeholder="Company Email"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Website URL</label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="weburl" value="{{ old('weburl', $client->website_url) }}" class="form-control" placeholder="Http://"></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Contact Person <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="contperson" value="{{ old('contperson', $client->contact_person) }}" class="form-control" placeholder="Contact Person"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">Contact Person Phone <small style="color: #f00;">*</small></label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="number" name="contpersonphone" value="{{ old('contpersonphone', $client->contact_person_phone) }}" class="form-control" placeholder="Contact Person Phone" style="border-bottom: 1px solid #eee;"></div></div>
                                </div>
                                <div class="col-md-12" style="margin: 0; padding: 0">
                                    <div class="col-md-4"><label for="exampleInputEmail1">NPWP</label></div>
                                    <div class="col-md-6"><div class="form-line"><input type="text" name="npwp" class="form-control" value="{{ old('npwp', $client->npwp) }}" placeholder="NPWP"></div></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group right" style="text-align: left;">
                            <div class="col-md-4"></div>
                            <div class="col-md-8" style="margin: 0; padding: 0">
                                <a href="{{url('client/index')}}" class="btn btn-default" style="padding: 5px 20px 10px;"><i class="fa fa-close"></i> Cancel</a>
                                <button class="btn btn-primary" type="submit" style="padding: 5px 20px 10px;"><i class="fa fa-send-o"></i> Update</button>
                            </div>
                        </div>
                    </div>
                    </form>
                    <!-- #END# form add prodct -->
                </div>

            </div>
        </div>
    </div>

</section>
@endsection

@section('script') 
<script src="{{ asset('js/sweetalert.min.js') }}"></script>        
    <script type="text/javascript">
        $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var subdist = $('.subdistrict select').val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id1: village,data_id2: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode input').val(obj.data);
                        return false;
                    }
                }
            });
        });
    </script>

<script>
function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      if(this.value.length == '5'){
            var postcode = this.value;
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/provregen/') }}",
                type:"POST",
                data:{ data_id: postcode,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('#province option[value="'+obj.prov+'"]').prop('selected', true);
                        $('#province').selectpicker('refresh');

                        $('.regency select').html(obj.data1);
                        $('.regency select').selectpicker('refresh');

                        $('.subdistrict select').html(obj.data2);
                        $('.subdistrict select').selectpicker('refresh');

                        if(obj.data3){
                            $('.village select').html(obj.data3);
                            $('.village select').selectpicker('refresh');
                        }
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
      }
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = [
@foreach($codepost as $result)
@if($result->code_pos != 'BENGKULU')
'{{ $result->code_pos }}',
@endif
@endforeach
];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);
</script>

@endsection