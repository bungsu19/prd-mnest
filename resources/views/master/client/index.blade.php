@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
               <!--  <h2>
                   All People
                </h2>
                 
            </div> -->
            <!-- Basic Examples -->

                @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: left;">
                                <h2><i class="fa fa-child"></i> All Data {{ $titlepage }}</h2>
                                <small style="font-style: italic;color: #b5b5b5;">All Data {{ $titlepage }} - {{ $titlepage }} Page</small>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                <a href="{{url('/client/create/')}}" class="btn bg-blue waves-effect mr-20">Create New {{ $titlepage }}</a>
                                <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            </div>
                            <div class="row clearfix">
                                <!-- filter -->
                            <form class="form-signin" method="POST" action="{{ url('client/index') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-sm-10">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option value="" disabled="disabled" selected="selected">- Select Filter Data -</option>
                                                        <option value="2">View All Client</option>
                                                        <option value="1">View All Client Active</option>
                                                        <option value="0">View All Client Deactive</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-2">
                                                    <label>&nbsp;</label>
                                                    <button type="submit" class="btn bg-blue form-control waves-effect m-r-20">Filter {{ $titlepage }}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- end filter -->

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            @include('flash-message')

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="table">
                                    <thead>
                                        <tr>
                                            <th style="width: 20px;">No</th>
                                            <th>Company Name</th>
                                            <th>Active</th>
                                            <th>Branch</th>
                                            <th>Phone/HP</th>
                                            <th>Email</th>
                                            <td align="center" width="30">Action</td>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th style="width: 20px;">No</th>
                                            <th>Company Name</th>
                                            <th>Active</th>
                                            <th>Branch</th>
                                            <th>Phone/HP</th>
                                            <th>Email</th>
                                            <td align="center" width="30">Action</td>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>


@section('script')
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script type="text/javascript">
var $statusfilter = 0;
var table;
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
    table = $('#table').DataTable({ 
 
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
 
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo url('client/data')?>",
            "type": "POST",
            "data" : { _token : CSRF_TOKEN, csearch : '<?php echo $search; ?>' }
        },
 
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0,2,3,4,5,6 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
 
    });

    $('.filterbtn').click(function(){
        if($statusfilter == 0){
            $('#formfilter').show(999);
            $statusfilter = 1;
        } else {
            $('#formfilter').hide(999);
            $statusfilter = 0;
        }
    });
});

    function btnDisable() {
        var _id = $('.btnDisable').attr('data-id');
        var _active = $('.btnDisable').attr('data-active');
        if(_active == '0'){
            var msg = 'Are you sure want to deactive this Client ?';
        } else {
            var msg = 'Are you sure want to active this Client ?';
        }

        swal({
          title: "Confirmation",
          text: msg,
          icon: "warning",
          buttons: true,
          dangerMode: true,
        }).then((willDelete) => {
          if (willDelete) 
          {
            $.ajax({
                url : '{{ url('/client/hidden') }}',
                method : "POST",
                data : { id : _id,active : _active,_token : CSRF_TOKEN }
            }).success(function(response){
                if("OK" === response.Result)
                {
                    location.href = "{{ url('/client/index') }}";
                }
                else
                {
                    swal("Oooops",'Something went wrong.','error');
                }
            });     
          }
        });
    }

    function btnDelete() {
        var _id = $('.btnDelete').attr('data-id');

        swal({
          title: "Confirmation",
          text: "Are you sure want to delete this Client ?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) 
          {
            $.ajax({
                url : '{{ url('/client/delete') }}',
                method : "POST",
                data : { id : _id,_token : CSRF_TOKEN }
            }).success(function(response){
                if("OK" === response.Result)
                {
                    location.href = "{{ url('/client/index') }}";
                }
                else if ("ERRORS" === response.Result)
                {
                    swal("ERROR",response.Message);
                }
                else
                {
                    swal("Oooops",'Something went wrong.','error');
                }
            });     
          }
        });
    }
</script>
@endsection
