@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
			
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif	
			
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow">New Promo</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('promo-add-post')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    
                                    
                                   
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Title <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="title" class="form-control" placeholder="input Title" id="title" maxlength="50">
                                             </div>
											 <span class="text-error" id="txt_title"></span>
                                         </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Description <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                    <textarea type="text" name="message" class="form-control" placeholder="input Description" id="message" style="height: 55px; max-height: 55px; min-height: 300px; max-width: 100%; min-width: 100%;" maxlength="150"></textarea>
                                                </div>
												<span class="text-error" id="txt_message"></span>
                                            </div>
                                        </div>
                                    </div>


                                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('promo')}}" class="btn btn-block bg-gray waves-effect" ><font size="4">Cancel</font></a>
                                            </div>
                                            <div class="col-sm-5">
												<button href="#" type="button" class="btn btn-block bg-blue waves-effect" id="send"><font size="4">Save</font></button>
                                                <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   
<script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
}); 
</script> 
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script> 
    
	
	<script type="text/javascript">
     $(function(){
		
		  
		  //outlet name cannot long space on input
		  $('#title').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#title').val().length > 0){
					$('#title').val($('#title').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on outlet name*/
		  $(document).on('paste', '#title', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  //address 1 cannot long space on input
		  $('#message').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#message').val().length > 0){
					$('#message').val($('#message').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	      /*handle paste with extra space on address 1*/
		  $(document).on('paste', '#message', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 150));
		  });
		  
		  
		  
		  $("#send").click(function(){
			 
			  
			  
			  if ($('#title').val().length < 1) {
					$('#txt_title').text('title name is empty');
			  }else{
					$('#txt_title').text('');
			  }
			  
			  if ($('#message').val().length < 1) {
					$('#txt_message').text('message 1 is empty');
			  }else{
					$('#txt_message').text('');
			  }
			  
			  if($('#title').val().length > 0 &&
				 $('#message').val().length > 0 ){
					  $("#kirim").click();
				  }
			  
		  });
	 });
	 </script>

</section>
    

@endsection
