@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                           <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>
                                People</b></h1>
                            </div>

                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                             <a href="{{url('data-people/add')}}" class="btn bg-blue waves-effect m-r-20">New People</a>

                             <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            </div>

                          
                            <div class="row clearfix">
                         <!-- filter -->
                            
                        <!-- end filter -->

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <form class="form-signin" method="POST" action="{{url('/data-people/filter-data')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All People</option>
                                                        <option value="1"> People Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow"> No</font></td>
                                            <td class="bg-column"><font color="yellow">Candidate's name</font></td>
                                            <td class="bg-column"><font color="yellow">Email Address</font></td>
                                            <td class="bg-column"><font color="yellow">Working project</font></td>
                                            <td class="bg-column"><font color="yellow">Data Collection</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php $no = 1 ?>
                                         @foreach($data as $da)
                                        <tr>
                                            <td>{{ $no }}</td>
                                           <td>{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}</td>
                                            <td>{{ $da->email_address }}</td>
                                            <td>
                                                @if($da->status_project == 1)
                                                    Aktif
                                                @else
                                                    None
                                                @endif
                                            </td>
                                            <td align="center">
                                                @if($da->count_bank != 0 && $da->count_education != 0 && $da->count_family != 0 && $da->count_image >= 2 && $da->count_doc != 0)
                                                    <a href="#" class=" btn bg-green"> Complete</a>
                                                @else
                                                    <a href="#" class="btn bg-red">Not Complete</a>
                                                @endif
                                            </td>
                                            <td align="center">

                                            <a href="{{url('data-people/view-data/')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                  <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            <a href="{{url('/data-people/add-info')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                           
                                             @if($da->delete_flag == 0)

                                                <a href="{{url('/data-people/active-data')}}/{{ $da->id }}" data-toggle="tooltip" title="Restore Data">
                                                     <i class="material-icons">restore_page</i>
                                                </a>
                                            @else
                                                <a onclick="return confirm('Are you sure?')" href="{{url('/data-people/delete-data')}}/{{ $da->id }}" data-toggle="tooltip" title="Delete Data">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                            @endif

                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>

        <!-- Large Size -->
         <form class="form-signin" method="POST" action="/data-people/add" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="largeModalLabel">Add Data</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-3">
                                <label>Cabang</label>
                                <select name="branch" class="form-control">
                                    <option>-pilih-</option>
                                    <option value="1">testing</option>
                                    
                                </select>
                                 
                            </div>
                             <div class="col-sm-3">
                                <label> Nama Awal</label>
                                <input type="text" name="first_name" class="form-control">

                             </div>
                             <div class="col-sm-3">
                                <label> Nama Tengah</label>
                                <input type="text" name="middle_name" class="form-control">

                             </div>
                             <div class="col-sm-3">
                                <label> Surname</label>
                                <input type="text" name="surname" class="form-control">

                             </div>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-3">
                                <label>Nama</label>
                                <select name="branch_id" class="form-control">
                                    <option>-pilih-</option>
                                    <option value="1">testing</option>
                                    
                                </select>
                                 
                            </div>
                             <div class="col-sm-3">
                                <label>Nama Panggilan</label>
                                <input type="text" name="nick_name" class="form-control">

                             </div>
                             <div class="col-sm-3">
                                <label> D O B</label>
                                <input type="text" name="dob" class="form-control">

                             </div>
                             <div class="col-sm-3">
                                <label> P O B</label>
                                <input type="text" name="pob" class="form-control">

                             </div>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-4">
                                <label>Jenis Kelamin</label>
                                <select name="gender" class="form-control">
                                    <option>-pilih-</option>
                                    <option>Laki-Laki</option>
                                    <option>Perempuan</option>
                                    
                                </select>
                                 
                            </div>
                             <div class="col-sm-4">
                                <label>Agama</label>
                               <select name="religion" class="form-control">
                                    <option>-pilih-</option>
                                    @foreach($religion as $rel)
                                    <option>{{ $rel->name_religion }}</option>
                                    @endforeach
                                    
                                </select>

                             </div>
                             <div class="col-sm-4">
                                <label> Negara</label>
                                 <select name="nationality" class="form-control">
                                    <option>-pilih-</option>
                                    <option>INDONESIA</option>
                                    <option>BELGIA</option>
                                    
                                </select>

                             </div>
                        </div>
                         <div class="modal-body">
                            <div class="col-sm-4">
                                <label>Email </label>
                               <input type="text" name="email" class="form-control">
                                 
                            </div>
                             <div class="col-sm-4">
                                <label>Phone 1</label>
                              <input type="text" name="phone1"  class="form-control">

                             </div>
                             <div class="col-sm-4">
                                <label> Phone 2</label>
                                 <input type="text" name="phone2" class="form-control">

                             </div>
                        </div>
                         <div class="modal-body">
                            <div class="col-sm-6">
                                <label>Facebook</label>
                                <input type="text" name="sosmed1" class="form-control">
                                 
                            </div>
                             <div class="col-sm-6">
                                <label>Twitter</label>
                                <input type="text" name="sosmed2" class="form-control">
                             </div>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-3">
                                <label>Nomor NPWP</label>
                                <input type="text" name="npwp_number" class="form-control">
                                 
                            </div>
                             <div class="col-sm-3">
                                <label>Konfirmasi NPWP</label>
                                <input type="text" name="npwp_validity" class="form-control">
                             </div>
                             <div class="col-sm-3">
                                <label>Tinggi Badan</label>
                                <input type="text" name="body_height" class="form-control">
                             </div>
                             <div class="col-sm-3">
                                <label>Berat Badan</label>
                                <input type="text" name="body_weight" class="form-control">
                             </div>
                        </div>
                        <div class="modal-body">
                             <div class="col-sm-4">
                                <label>NO SIM A</label>
                                <input type="text" name="sim_a_number" class="form-control">
                             </div>
                             <div class="col-sm-4">
                                <label>Konfirmasi SIM</label>
                                <input type="text" name="sim_a_validity" class="form-control">
                             </div>
                             <div class="col-sm-4">
                                <label>NO SIM C</label>
                                <input type="text" name="sim_c_number" class="form-control">
                             </div>
                        </div>
                         <div class="modal-body">
                            <div class="col-sm-6">
                                <label>Golongan Darah</label>
                                <input type="text" name="blood_type" class="form-control">
                                 
                            </div>
                             <div class="col-sm-6">
                                <label>Bank Acc NO</label>
                                <input type="text" name="bank_acc_no" class="form-control">
                             </div>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-4">
                                <label>Interes job 1</label>
                                <select name="interesting_job_1" class="form-control">
                                    <option>-pilih-</option>
                                    @foreach($job as $jb)
                                    <option>{{ $jb->job_name }}</option>
                                    @endforeach
                                </select>
                                 
                            </div>
                             <div class="col-sm-4">
                                <label>Interes Job 2</label>
                               <select name="interesting_job_2" class="form-control">
                                   <option>-pilih-</option>
                                    @foreach($job as $jb)
                                    <option>{{ $jb->job_name }}</option>
                                    @endforeach
                               </select>
                             </div>
                            <div class="col-sm-4">
                                <label>Interes Job 3</label>
                                <select name="interesting_job_3" class="form-control">
                                    <option>-pilih-</option>
                                     @foreach($job as $jb)
                                    <option>{{ $jb->job_name }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <br>
                                 <button type="submit" class="btn btn-link bg-red waves-effect"> SAVE</button>
                                <button type="button" class="btn btn-link bg-red waves-effect" data-dismiss="modal">Cancel</button>
                             </div>

                        </div>
                        <br>
                        <br>
                        <div class="modal-footer">
                            <br>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
   </script>
    @endsection

@endsection
