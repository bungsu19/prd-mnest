@extends('layout-menu.app')

@section('content')

   
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >People Information</h1>
                            <br>
                            <h2 align="center">
                             <img id="myImg" class="p-image" src="{{ URL::to('employe/'.$data->image) }}" alt="" width="200px"> </img></h2>
                            <h2 align="center">
                             
                               {{ $data->first_name }} {{ $data->middle_name }} {{ $data->surname }}
                              
                            </h2>
                        </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Personal Data</a>
                                        </li>
                                        <li role="presentation" >

                                            <a href="#home_animation_2" data-toggle="tab" class="btn bg-blue">Address</a>
                                        </li>
                                         @if(Auth::user()->hak_access != 6)
                                        <li role="presentation">
                                            @if($count_bank != 0)
                                            <a href="#profile_animation_2" class="btn bg-blue" data-toggle="tab">Bank</a>
                                            @else
                                                 <a href="#profile_animation_2" class="btn bg-red" data-toggle="tab">Bank</a>
                                            @endif
                                        </li>
                                         @else

                                          @endif
                                        <li role="presentation">
                                            @if($count_education != 0)
                                                 <a href="#messages_animation_2" class="btn bg-blue" data-toggle="tab">Eduction</a>
                                            @else
                                                 <a href="#messages_animation_2" class="btn bg-red" data-toggle="tab">Eduction</a>
                                            @endif
                                        </li>
                                        <li role="presentation">
                                            @if($count_family != 0)
                                                <a href="#settings_animation_2" class="btn bg-blue" data-toggle="tab">Family Member</a>
                                            @else
                                                <a href="#settings_animation_2" class="btn bg-red" data-toggle="tab">Family Member</a>
                                            @endif
                                        </li>
                                        <li role="presentation">
                                            @if($count_work != 0)
                                                <a href="#settings_work" class="btn bg-blue" data-toggle="tab">Work Exprerience</a>
                                            @else
                                                <a href="#settings_work" class="btn bg-red" data-toggle="tab">Work Exprerience</a>
                                            @endif
                                        </li>
                                        <li role="presentation">
                                            @if($count_image >= 3)
                                                <a href="#settings_image" class="btn bg-blue" data-toggle="tab">Pictures</a>
                                            @else
                                                 <a href="#settings_image" class="btn bg-red" data-toggle="tab">Pictures</a>
                                            @endif
                                        </li>
                                         <li role="presentation">
                                            @if($count_doc != 0 )
                                                 <a href="#settings_documnet" class="btn bg-blue" data-toggle="tab">Document</a>
                                            @else
                                                <a href="#settings_documnet" class="btn bg-red" data-toggle="tab">Document</a>
                                            @endif
                                         </li>
                                         <li role="presentation">
                                            @if(count($interes) != 0 )
                                                 <a href="#settings_interes" class="btn bg-blue" data-toggle="tab">Interes Job</a>
                                            @else
                                                <a href="#settings_interes" class="btn bg-red" data-toggle="tab">Interes Job</a>
                                            @endif
                                         </li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="home_animation_2">
                                           
                                            <table class="table">
                                            <thead>
                                                <tr>
                                                    <th bgcolor="#4682B4"><font color="yellow">No</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">Type</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">Address</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">Province</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">District</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">Sub District</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">Village</font></th>
                                                    <th bgcolor="#4682B4"><font color="yellow">RT/RW</font></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <tr>
                                                    <td>1</td>
                                                    <td>Address KTP </td>
                                                    <td>{{ $data->ktp_address }}</td>
                                                    <td>{{ $data->ktp_province }}</td>
                                                    <td>{{ $data->ktp_district }}</td>
                                                    <td>{{ $data->ktp_subdistrict }}</td>
                                                    <td>{{ $data->ktp_village }}</td>
                                                    <td>{{ $data->ktp_rt }}/{{ $data->ktp_rw }}</td>
                                                </tr>
                                                @if($data->dom_address != NULL)
                                                <tr>
                                                    <td>2</td>
                                                    <td>Home Address</td>
                                                    <td>{{ $data->dom_address }}</td>
                                                    <td>{{ $data->dom_province }}</td>
                                                    <td>{{ $data->dom_district }}</td>
                                                    <td>{{ $data->dom_subdistrict }}</td>
                                                    <td>{{ $data->dom_village }}</td>
                                                    <td>{{ $data->dom_rt }}/{{ $data->dom_rw }}</td>
                                                </tr>
                                                @else

                                                @endif
                                            </tbody>
                                        </table>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="profile_animation_2">
                                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th bgcolor="#4682B4"><font color="yellow"> No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow"> Account No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Account Name</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Bank</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $no = 1 ?>
                                         @foreach($bank as $bnk)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $bnk->bank_acc_no }}</td>
                                            <td>{{ $bnk->bank_acc_name }}</td>
                                            <td>{{ $bnk->bank_name }}</td>
                                            
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                        </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th bgcolor="#4682B4"><font color="yellow">No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Year(From - To)</font> </th>
                                            <th bgcolor="#4682B4"><font color="yellow">Level</font> </th>
                                            <th bgcolor="#4682B4"><font color="yellow">School/Institution/University</font> </th>
                                            <th bgcolor="#4682B4"><font color="yellow">Location</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Major</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($education as $edu)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $edu->periode_from }} - {{ $edu->periode_to }}</td>
                                            <td>@if($edu->education_lvl == 1)
                                                    SD
                                                @elseif($edu->education_lvl == 2)
                                                    SMP
                                                @elseif($edu->education_lvl == 3)
                                                    SMA/SMK
                                                @elseif($edu->education_lvl == 4)
                                                    D3
                                                @elseif($edu->education_lvl == 5)
                                                    S1
                                                @else
                                                    S2
                                                @endif
                                            </td>
                                            <td>{{ $edu->education_institution_name }}</td>
                                            <td>{{ $edu->education_location }}</td>
                                            <td>{{ $edu->education_major }}</td>
                                           
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_animation_2">
                                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th bgcolor="#4682B4"><font color="yellow">No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Family Member</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Name</font></th>

                                            <th bgcolor="#4682B4"><font color="yellow">Phone Number</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Gender</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Home City</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Last Education</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php $no = 1 ?>
                                         @foreach($family as $fml)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>
                                                @if($fml->member_code == 1)
                                                    Father
                                                @elseif($fml->member_code == 2)
                                                    Mother
                                                @elseif($fml->member_code == 3)
                                                    Sister
                                                @elseif($fml->member_code == 4)
                                                    Brother
                                                @else
                                                    Siter
                                                @endif
                                            </td>
                                            <td>{{ $fml->member_name }}</td>
                                            <td>{{ $fml->member_phone1 }}</td>
                                            <td>{{ $fml->member_gender }}</td>
                                            <td>{{ $fml->residence_city }}</td>
                                            <td>@if($fml->member_last_edu == 1)
                                                    SD
                                                @elseif($fml->member_last_edu == 2)
                                                    SMP
                                                @elseif($fml->member_last_edu == 3)
                                                    SMA/SMK
                                                @elseif($fml->member_last_edu == 4)
                                                    D3
                                                @elseif($fml->member_last_edu == 5)
                                                    S1
                                                @else
                                                    S2
                                                @endif

                                            </td> 
                                            
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_work">
                                    <table class="table">
                                    <thead>
                                        <tr>

                                            <th bgcolor="#4682B4"><font color="yellow">No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Company Name</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Year(From - To)</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Job Position</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Salary</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Reason to Quit</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($work as $wk)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $wk->company_name }}</td>
                                            <td>{{ $wk->working_period }} - {{ $wk->working_periode_end }}</td>
                                            <td>{{ $wk->position }}</td>
                                            <td>{{ $wk->salary }}</td>
                                            <td>{{ $wk->reason_resignation }}</td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_image">
                                   
                                    <div class="body">
                                         
                                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                        @foreach($image as $img)
                                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                                         <a>{{ $img->image_type }}</a><br>
                                        <a data-toggle="modal" data-target="#modal-edit{{ $img->id }}"><img id="myImg" class="p-image" src="{{ URL::to('employe_image/'.$img->image) }}" alt="Snow" width="100px"> </img></a>
                                        
                                        </div>
                                        @endforeach
                                        </div>
                                         
                                    </div>
                                   
                                   
                                </div>

                            <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_documnet">
                                   
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <th bgcolor="#4682B4"><font color="yellow">No</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Type Image</font></th>
                                            <th bgcolor="#4682B4"><font color="yellow">Document</font></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($doc as $dc)
                                        <tr>
                                            <td>{{ $no }} </td>
                                            <td>{{ $dc->image_type }}</td>
                                            <td><a href="{{ URL::to('document_people/'.$dc->image_path) }}"download="{{$dc->image_path}}">{{ $dc->image_path }}</a></td>
                                            
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                                   
                                   
                                </div>

                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="settings_interes">
                                   
                                    <table class="table">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" width="40"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Job Name</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                     <?php $no = 1 ?>
                                         @foreach($interes as $int)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $int->job_name }}</td>
                                           
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>    
                                   
                                   
                                </div>


                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                    <div class="body">
                                        <br>

                                        <div class="row clearfix">
                                                <div class="col-sm-2">
                                                    <label>NIK</label>
                                                 
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>: {{ $data->nik_ktp }}  </label>
                                                </div>

                                                 <div class="col-sm-2">
                                                    <label>Nick Name</label>
                                                 
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>: {{ $data->nick_name }}  </label>
                                                </div>
                                        </div>


                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Date Of Brith</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->dob2 }}  </label>
                                            </div>

                                            <div class="col-sm-2">
                                                <label>Place Of Birth</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->pob }}  </label>
                                            </div>
                                        </div>


                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Gender</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->gender }}  </label>
                                            </div>

                                             <div class="col-sm-2">
                                                <label>Religion</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->religion }}  </label>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Nationality</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->nationality }}  </label>
                                            </div>
                                        @if(Auth::user()->hak_access != 6)
                                            <div class="col-sm-2">
                                                <label>Email</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->email_address }}  </label>
                                            </div>
                                        </div>
                                         
                                            <div class="row clearfix">
                                                <div class="col-sm-2">
                                                    <label>Phone Number 1</label>
                                                 
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>: {{ $data->phone1 }}  </label>
                                                </div>

                                                <div class="col-sm-2">
                                                    <label>Phone Number 2</label>
                                                 
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>: {{ $data->phone2 }}  </label>
                                                </div>
                                            </div>
                                        @else

                                        @endif


                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Facebook</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sosmed1 }}  </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Twitter</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sosmed2 }}  </label>
                                            </div>
                                        </div>

                                        <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>NPWP</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->npwp_number }}  </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Last Education</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->last_education }}  </label>
                                            </div>

                                           
                                        </div>


                                         <div class="row clearfix">
                                             <div class="col-sm-2">
                                                <label>SIM A</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sim_a_number }}  </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>SIM A VALIDITY</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sim_a_validity }}  </label>
                                            </div>
                                        </div>

                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>SIM C</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sim_c_number }}  </label>
                                            </div>
                                             <div class="col-sm-2">
                                                <label>SIM C VALIDITY</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->sim_c_validity }}  </label>
                                            </div>
                                        </div>


                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Body weight</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->body_weight }}  </label>
                                            </div>
                                             <div class="col-sm-2">
                                                <label>Body height</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->body_height }}  </label>
                                            </div>
                                        </div>

                                        <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Blood Type</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->blood_type }}  </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Expected Salary</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: Rp.{{ $data->expected_salary }}  </label>
                                            </div>
                                        </div>
                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Allowance</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: Rp. {{ $data->allowance }}  </label>
                                            </div>
                                            <div class="col-sm-2">
                                                <label>Facility</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->facility }}  </label>
                                            </div>
                                        </div>
                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>Insurance</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->insurance }}  </label>
                                            </div>
                                        </div>

                                         <div class="row clearfix">
                                            <div class="col-sm-2">
                                                <label>No Bpjs Kesehatan</label>
                                             
                                            </div>
                                            <div class="col-sm-3">
                                                <label>: {{ $data->no_bpjs_kesehatan }}  </label>
                                            </div>
                                        </div>




                                        
                                    </div>
                                </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        @foreach($image as $dada)
            <form class="form-signin" method="POST" action="{{url('/religion/edit')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="modal-edit{{ $dada->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                           <img id="myImg" class="p-image" src="{{ URL::to('employe_image/'.$dada->image) }}" alt="Snow" width="500px"> </img>
                    </div>
                    
                </div>
            </div>
        </form>
            @endforeach

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
    </section>
    

@endsection
