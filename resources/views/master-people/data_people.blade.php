@extends('layout-menu.app')

@section('content')
<style>
	.nav-tabs li.active a{
		color: #337ab7 !important; 
		background-color: white !important; 
	}
	
	.nav > li > a:hover {
		color: #337ab7 !important; 
		background-color: white !important; 
	}
	
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
<br>
<section class="content">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -50px;">
				
					@if(session()->has('gagal'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('gagal') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif

                    @if(session()->has('failed'))
                        <div class="alert bg-secondary alert-dismissible" role="alert" style="height: 400px; overflow-y: auto;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
                              <?php $pesan = session()->get('failed'); ?>
                           <?php for ($i=0; $i < count($pesan) ; $i++) { 
                               echo $pesan[$i].'<br>';
                           }
                           ?>
                        </div>
                        @endif
				
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >People</h1>
                            <br>
                            
                        </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Register</a>
                                        </li>
                                        <li role="presentation" >

                                            <a href="#Interview" data-toggle="tab" class="btn bg-blue">Interview</a>
                                        </li>
                                        
                                        <li role="presentation">
                                          
                                            <a href="#Candidate" class="btn bg-blue" data-toggle="tab">Candidate</a>
                                           
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                               
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="Interview">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn2"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                       <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/filter-people-interview')}}" enctype="multipart/form-data" autocomplete="off">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter2" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-10">
                                                                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                      													<div class="form-line">
                      														<input type="text" name="from" class="form-control" placeholder="Interview Start.." id="project_start" value="{{ $searchFromValue }}">
                      													</div>
                      													<span class="input-group-addon" style="margin-left:10px;">to</span>
                      													<div class="form-line">
                      														<input type="text" name="to" class="form-control" placeholder="Interview End..." id="project_end" value="{{ $searchToValue }}">
                      													</div>
                      												</div>
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>
                                                                     <button type="submit" class="btn bg-blue waves-effect">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
										<br>

                         
                                     </div>
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
                                                                <td class="bg-column" width="60" align="center"><font color="yellow">No</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Candidate's name</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Age</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Home Address</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Last Education</font></td>
                                                                <!--<td class="bg-column"><font color="yellow">Working on the project</font></td>-->
                                                                <td class="bg-column" align="left"><font color="yellow">Interview Date</font></td>
																<td class="bg-column" align="left"><font color="yellow">Hour</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Description</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($interview->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($interview->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($interview as $da)
                                                            <tr>
                                                                <td align="center">{{ $no }}</td>
                                                                <td align="left"><a href="{{url('data-people/view-data/')}}/{{ $da->id }}" title="View Data">{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}</a></td>
																                                <td align="center">{{ $da->age }} </td>
                                                                <td>
                                                                     @if( $da->dom_district == null)
                                                                       {{ $da->ktp_district }}
                                                                    @else
                                                                         {{ $da->dom_district }}
                                                                    @endif
                                                                 </td>
                                                                <td align="center">{{ $da->last_education }} </td>
                                                                <td align="left">{{ $da->date_confir }}</td>
																                                <td align="left">{{ $da->jam_confirm }}</td>
                                                                <td align="center">
																@if($da->status == 1)
																	<button type="button" class="btn bg-blue waves-effect" data-toggle="modal" data-target="#add_candidate" data-message="are you sure want to add CANDIDATE name '{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}' ?" data-href="{{url('/data-people/add-candidate')}}/{{ $da->id }}">Approve</button>

                                @elseif($da->status == 2)
                                <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $da->id }}" title="Send Interview">
                                    <i class="material-icons">send</i>
                                  </a>
																@else
																	<?php 
																		$todayDate = date("Y-m-d");
																		$now = new DateTime($todayDate);
																		$count1 = $now->diff(new DateTime($da->date1))->format('%R%a');
																		$count2 = $now->diff(new DateTime($da->date2))->format('%R%a');
																		$count3 = $now->diff(new DateTime($da->date3))->format('%R%a');
																		$newDate = '';
																		
																		if($count1 > $count2 && $count1 > $count3){
																			$newDate = $da->date1;
																		}else if($count2 > $count1 && $count2 > $count3){
																			$newDate = $da->date2;
																		}else if($count3 > $count1 && $count3 > $count2){
																			$newDate = $da->date3;
																		}
																		
																		if($todayDate > $newDate){
																			echo '<span class="text-error">Expired</span>';
																		}else{
																			echo '<span class="text-error">Waiting Confirmation</span>';
																		}
																	?>
                                  
																@endif
                                    </td>
                                    <td align="center">
                                      <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete people name '{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}' ?" data-href="{{url('/data-people/delete-data-interview')}}/{{ $da->id_interview }}" title="Delete Data">
                                    <i class="material-icons">delete_forever</i>
                                  </a>
                                                              </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                   <h1><b></b></h1>
                                                    </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;"> 
														{{ $interview->links('paginator.default') }}
                                                    </div>
                                                </div>
                                             </div>
                                    </div>     
                                </div>
								
                                <div role="tabpanel" class="tab-pane animated fadeInRight" id="Candidate">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-7">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                   
                                                </div>
                                                <div class="col-sm-2">
                                                    
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn3"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                       <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/filter-people-candidate')}}" enctype="multipart/form-data" autocomplete="off">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter3" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2" {{ ( $searchCandidate == 2) ? 'selected' : '' }}>All</option>
                                                                        <option value="0" {{ ( $searchCandidate == 0) ? 'selected' : '' }}>Name</option>
                                                                        <option value="1" {{ ( $searchCandidate == 1) ? 'selected' : '' }}>Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value" value="{{ $searchCandidateValue }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>
                                                                     <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div><br>

                         
                                     </div>
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
                                                                <td class="bg-column" width="60" align="center"><font color="yellow">No</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Candidate's name</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Age</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Home Address</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Last Education</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Project Name</font></td>
																<td class="bg-column" align="left"><font color="yellow">Imei</font></td>
																<td class="bg-column" align="left"><font color="yellow"></font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($candidate->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($candidate->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($candidate as $da)
                                                            <tr>
                                                                <td align="center">{{ $no }}</td>
                                                                <td align="left"><a href="{{url('data-people/view-data/')}}/{{ $da->id }}" title="View Data">{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}</a></td>
                                                                <td align="center">{{ $da->age }} </td>
                                                                <td>
                                                                     @if( $da->dom_district == null)
                                                                       {{ $da->ktp_district }}
                                                                    @else
                                                                         {{ $da->dom_district }}
                                                                    @endif
                                                                 </td>
                                                                <td align="center">{{ $da->last_education }} </td>
                                                                <td align="left">
                                                                  @if($da->active_flag == 1)
                                                                    {{ $da->project_name }}
                                                                  @else
                                                                    
                                                                  @endif

                                                                </td>
                                																<td align="left">{{ $da->imei_user }}</td>
                                																<td align="left">
                                																	<?php if($da->imei_user != "" || $da->imei_user != null ){ ?>
                                																		<button type="button" class="btn bg-blue waves-effect">
                                																			<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to reset imei people name '{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}' ?" data-href="{{url('/reset-imei-user')}}/{{ $da->id }}" title="Reset Imei" style="color: #ffffff;">
                                																				Reset
                                																			</a>
                                																		</button>
                                																	<?php } ?>
                                																</td>
                                                                <td align="center">
																	<a href="{{url('/data-people/add-info')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
																		<i class="material-icons">mode_edit</i>
																	</a>
																	
																	<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete people name '{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}' ?" data-href="{{url('/data-people/delete-data')}}/{{ $da->id }}" title="Delete Data">
																		<i class="material-icons">delete_forever</i>
																	</a>
                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                   <h1><b></b></h1>
                                                    </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $candidate->links('paginator.default') }}
                                                    </div>
                                                </div>

                                             </div>
                                    </div>     
                                </div>


                                <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
										<br>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                <div class="col-sm-9">
                                                    <form action="{{ url('/importPeople') }}" method="POST" enctype="multipart/form-data" class="form-inline">
                                                    {{ csrf_field() }}
                                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                            <div class="form-group">
                                                                <div class="col-md-4">
                                                                    <input type="file" name="import" class="form-control" >
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <button class="btn bg-blue waves-effect m-r-20">Import</button>     
                                                                </div>
                          																<div class="col-sm-2">
                          																	<a href="{{url('downloadPeople')}}" class="btn bg-blue waves-effect m-r-20">Download</a>
                          																</div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="col-sm-2">
                                                    <a href="{{url('data-people/add')}}" class="btn bg-blue waves-effect m-r-20">New People</a>
                                                </div>
                                                <div class="col-sm-1">
                                                    <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                                                </div>
                                        </div>

                                        <div class="row clearfix">
                                            <form class="form-signin" method="GET" action="{{url('/filter-people-new')}}" enctype="multipart/form-data" autocomplete="off">
                                            {{ csrf_field() }}
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" id="formfilter" style="margin-top: 10px;display: none;">
                                                    <div class="card" style="margin: 0px;">
                                                        
                                                        <div class="body">
                                                            <div class="row clearfix">
                                                                <div class="col-sm-3">
                                                                    <label>Filter By</label>
                                                                    <select name="search" class="form-control show-tick">
                                                                        <option value="2" {{ ( $searchRegiter == 2) ? 'selected' : '' }}>All</option>
                                                                        <option value="0" {{ ( $searchRegiter == 0) ? 'selected' : '' }}>Name</option>
                                                                        <option value="1" {{ ( $searchRegiter == 1) ? 'selected' : '' }}>Address</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <label>Value</label>
                                                                    <input type="text" name="filter_value" class="form-control" placeholder="value" value="{{ $searchRegiterValue }}">
                                                                </div>
                                                                 <div class="col-sm-2">
                                                                    <label></label>
                                                                     <button type="submit" class="btn bg-blue waves-effect" style="margin-top: 25px;">Search</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
										<br>
                         
                                     </div>
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right;">
                                             <div class="row clearfix">
                                                 <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover ">
                                                        <thead>
                                                            <tr>
                                                                <td class="bg-column" width="60" align="center"><font color="yellow">No</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Candidate's name</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Age</font></td>
                                                                <td class="bg-column" align="left"><font color="yellow">Home Address</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Last Education</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Interview</font></td>
                                                                <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $no = 0;
                                                                if($data->currentPage() == 1){
                                                                    $no = 1;
                                                                }else{
                                                                  $no = ($data->currentPage()-1) * $jumlahData + 1;
                                                                }
                                                            ?>
                                                             @foreach($data as $da)
                                                            <tr>
                                                                <td align="center">{{ $no }}</td>
                                                                <td align="left"><a href="{{url('data-people/view-data/')}}/{{ $da->id }}" title="View Data">{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}</a></td>
                                                                <td align="center">{{ $da->age }} </td>
                                                                <td>
                                                                    @if( $da->dom_district == null)
                                                                       {{ $da->ktp_district }}
                                                                    @else
                                                                         {{ $da->dom_district }}
                                                                    @endif
                                                                 </td>
                                                                <td align="center">{{ $da->last_education }} </td>
                                                                
                                                                <td align="center">
                                                                    <a href="#" data-toggle="modal" data-target="#modalInTerview" data-id="{{ $da->id }}" title="Send Interview">
																		<i class="material-icons">send</i>
																	</a>
                                                                </td>
                                                                <td align="center">

                                                                <a href="{{url('data-people/view-data/')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                                      <i class="material-icons">remove_red_eye</i>
                                                                </a>
                                                                <a href="{{url('/data-people/add-info')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
                                                                    <i class="material-icons">mode_edit</i>
                                                                </a>
                                                                
                                                                <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete people name '{{ $da->first_name }} {{ $da->middle_name }} {{ $da->surname }}' ?" data-href="{{url('/data-people/delete-data')}}/{{ $da->id }}" title="Delete Data">
                                                                    <i class="material-icons">delete_forever</i>
                                                                </a>

                                                                </td>
                                                            </tr>
                                                            <?php $no++; ?>
                                                             @endforeach
                                                            
                                                        </tbody>
                                                    </table>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                   <h1><b></b></h1>
                                                    </div>
                                                   <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                                        {{ $paginator->links('paginator.default') }}
                                                    </div>
                                                </div>

                                             </div>
                                    </div>


                                    
                                </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                     <h3>Delete</h3>
                </div>
                <div class="modal-body">
                    <p>You are about to delete.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                  <a href="#" id="btnYes" class="btn danger">Yes</a>
                  <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
                </div>
            </div>
			
			<form class="form-signin" method="POST" action="{{url('/data-people/send-interview')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="modalInTerview" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Add Date Interview</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Message <span class="mandatory">*</span></label>
                                   <div class="form-group">
                                        <div class="form-line">
                                                <textarea name="message" class="form-control" placeholder=" Input message" id="message" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 150%; min-width: 100%;" maxlength="200">Berdasarkan data registrasi yang Sdr/i telah kirimkan kepada PT Sumber Daya Mandiri, maka kami mengundang Sdr/i untuk wawancara dan melengkapi data pribadi sebagai syarat untuk mengisi lowongan pekerjaan yang ada di PT SDM.
Silahkan pilih salah satu dari 3 waktu dibawah ini yang sesuai:
                                      </textarea>
                                        </div>
                                        <span class="text-error" id="txt_message"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                    <label>Date 1 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="text" name="date1" class="form-control datespicker" placeholder="Date 1 ..." id="date_1">
                                        </div>
                                        <span class="text-error" id="txt_date_1"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 1 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="time" name="jam1" class="form-control " placeholder="Hours 1 ..." id="jam_1">
                                        </div>
                                        <span class="text-error" id="txt_jam_1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
    							           <div class="col-sm-8">
                                    <label>Date 2 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="text" name="date2" class="form-control datespicker2" placeholder="Date 2 ..." id="date_2">
                                        </div>
                                        <span class="text-error" id="txt_date_2"></span>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 2 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="time" name="jam2" class="form-control " placeholder="Hours 2 ..." id="jam_2">
                                        </div>
                                        <span class="text-error" id="txt_jam_2"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
    						          	<div class="col-sm-8">
                                    <label>Date 3 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                            <div class="form-line">
                                                <input type="text" name="date3" class="form-control datespicker3" placeholder="Date 3 ..." id="date_3">
                                            </div>
                                            <span class="text-error" id="txt_date_3"></span>
                                        </div>
    									           <input type="hidden" name="people_id" id="id">
                                </div>
                                <div class="col-sm-4">
                                    <label>Hours 3 <span class="mandatory">*</span></label>
                                    <div class="input-daterange input-group" id="bs_datepicker">
                                        <div class="form-line">
                                                <input type="time" name="jam3" class="form-control " placeholder="Hours 3 ..." id="jam_3">
                                        </div>
                                        <span class="text-error" id="txt_jam_3"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Address <span class="mandatory">*</span></label>
                                   <div class="form-group">
                                        <div class="form-line">
                                                <textarea name="address" class="form-control" placeholder=" Input Address" id="address" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200"></textarea>
                                        </div>
                                        <span class="text-error" id="txt_address"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Name Hrd <span class="mandatory">*</span></label>
                                     <div class="form-line">
                                                <input type="text" name="name_hrd" class="form-control " placeholder="Name Hrd"" id="name_hrd">
                                        </div>
                                        <span class="text-error" id="txt_name_hrd"></span>
                                    
                                </div>

                            </div>
                            <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <label>Location <span class="mandatory">*</span></label>
                                     <div class="form-line">
                                                <input type="text" name="location" class="form-control " placeholder="location"" id="location">
                                        </div>
                                        <span class="text-error" id="txt_location"></span>
                                    
                                </div>

                            </div>

                            

                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimInterview" style="display: none;">Send</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="sendInterview">Send</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        
            <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<b class="message"></b><br>
						</div>
						<div class="modal-footer">
							 <a class="btn-ok"> 
								<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
							</a>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="add_candidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<b class="message"></b><br>
						</div>
						<div class="modal-footer">
							 <a class="btn-ok"> 
								<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
							</a>
							<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
						</div>
					</div>
				</div>
			</div>
			
 @section('script')
      <script src="{{ asset('js/sweetalert.min.js') }}"></script>
	  <script type="text/javascript">
		$(document).ready(function() {
		  activaTab('<?php echo $active; ?>');
			  function activaTab(tab){
				$('.nav-tabs a[href="#' + tab + '"]').tab('show');
			  };
			  
				$('#modalInTerview').on('show.bs.modal', function(e) {
				  
					var id = $(e.relatedTarget).data('id');
					$(e.currentTarget).find('#id').val(id);  
				
					  $(e.currentTarget).find('.datespicker').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate:0,
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
					  
					  $(e.currentTarget).find('.datespicker2').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate:0,
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
					  
					  $(e.currentTarget).find('.datespicker3').datepicker({
						format: "dd-mm-yyyy",
						autoclose: true,
						todayHighlight: true,
						minDate: '+1',
					  }).on('show.bs.modal', function(event) {
						  event.stopPropagation();
					  });
				});
				
				$('#modalInTerview').on('hidden.bs.modal', function(e) {
				  
					$(e.currentTarget).find('#date_1').val(''); 
					$(e.currentTarget).find('#date_2').val(''); 
                    $(e.currentTarget).find('#date_3').val(''); 
                    $(e.currentTarget).find('#jam_1').val(''); 
                    $(e.currentTarget).find('#jam_2').val(''); 
					$(e.currentTarget).find('#jam_3').val(''); 
					
					$(e.currentTarget).find('#txt_date_1').text('');
					$(e.currentTarget).find('#txt_date_2').text('');
                    $(e.currentTarget).find('#txt_date_3').text('');
                    $(e.currentTarget).find('#txt_jam_1').text('');
                    $(e.currentTarget).find('#txt_jam_2').text('');
					$(e.currentTarget).find('#txt_jam_3').text('');
				});
				
		}); 
		</script> 
    <script type="text/javascript">
        var $statusfilter = 0;
    		var $statusfilter2 = 0;
    		var $statusfilter3 = 0;
		
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('.filterbtn2').click(function(){
            if($statusfilter2 == 0){
                $('#formfilter2').show(999);
                $statusfilter2 = 1;
            } else {
                $('#formfilter2').hide(999);
                $statusfilter2 = 0;
            }
             });
			 
			 $('.filterbtn3').click(function(){
            if($statusfilter3 == 0){
                $('#formfilter3').show(999);
                $statusfilter3 = 1;
            } else {
                $('#formfilter3').hide(999);
                $statusfilter3 = 0;
            }
             });
             
            $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
			
			$('#add_candidate').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
			
			$("#sendInterview").click(function(){
                
                if ($('#date_1').val().length < 1) {
                    $('#txt_date_1').text('Date 1 is empty');
                }else{
                    $('#txt_date_1').text('');
                }
                
                if ($('#date_2').val().length < 1) {
                    $('#txt_date_2').text('Date 2 is empty');
                }else{
                    $('#txt_date_2').text('');
                }
				
				if ($('#date_3').val().length < 1) {
                    $('#txt_date_3').text('Date 3 is empty');
                }else{
                    $('#txt_date_3').text('');
                }
                if ($('#jam_1').val().length < 1) {
                    $('#txt_jam_1').text('Hours 1 is empty');
                }else{
                    $('#txt_jam_1').text('');
                }

                if ($('#jam_2').val().length < 1) {
                    $('#txt_jam_2').text('Hours 2 is empty');
                }else{
                    $('#txt_jam_2').text('');
                }

                if ($('#jam_3').val().length < 1) {
                    $('#txt_jam_3').text('Hours 3 is empty');
                }else{
                    $('#txt_jam_3').text('');
                }
                
                if(
                    $('#date_1').val().length > 0 &&
                    $('#date_2').val().length > 0 &&
                    $('#date_3').val().length > 0 &&
                    $('#jam_1').val().length > 0 &&
                    $('#jam_2').val().length > 0 &&
                    $('#jam_3').val().length > 0
                )
                {
                    $("#kirimInterview").click();  
                }
            
            });
			
			$("#bs_datepicker_range_container").datepicker({
				  format: 'dd-mm-yyyy',
				  autoclose: true,
				  todayHighlight: true,
			  });
             
        });
        
        
        setTimeout(function(){
           $("div.alert").remove();
        }, 5000 );
    </script>
    <script type="text/javascript">
        $('#myModal').on('show', function() {
        var id = $(this).data('id'),
            removeBtn = $(this).find('.danger');
        });

        $('.confirm-delete').on('click', function(e) {
            e.preventDefault();

            var id = $(this).data('id');
            $('#myModal').data('id', id).modal('show');
        });

        $('#btnYes').click(function() {
            // handle deletion here
            var id = $('#myModal').data('id');
            $('[data-id='+id+']').remove();
            $('#myModal').modal('hide');
        });

    </script>

      

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
    </section>
   @endsection 

@endsection
