@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
<br>
<section class="content">
            <div class="row clearfix" >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -50px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                People Information
                            </h1>
                            <label>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $data->first_name }} {{ $data->middle_name}} {{ $data->surname }} </i></small> <br>
                            <label>email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </label><small><i> &nbsp;{{ $data->email_address }}</i></small> <br>

                             
                            <div class="row clearfix">
                                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" style="text-align: left;">
                                           
                                        
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-7">
                                             <a href="{{url('/edit-people')}}/{{ $data->id }}" class="btn bg-blue waves-effect m-r-20">Edit People</a>
                                             </div>
                                        </div>
                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>JOB INTEREST</b></h2>
							<h2 align="right">
								<button type="button" class="btn bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#ModalInteres">Add Interest</button>
							</h2>
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" width="20"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Job Name</font></td>
                                            <td align="center" class="bg-column"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                     <?php $no = 1 ?>
                                         @foreach($interes as $int)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $int->job_name }}</td>
                                            <td align="center">
                                            
												<a href="#" style="padding: 5px;" data-toggle="modal" data-target="#ModalInteresEdit{{ $int->id }}" title="Edit Data">
                                                    <i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i>
                                                </a>
												
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete job interest '{{ $int->job_name }}' ?" data-href="{{url('/data-people/add-info/delete')}}/{{ $int->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
												
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
            

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>BANK INFORMATION</b></h2>
                            @if($count_bank == 0)
                            <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalBank">Add BANK Information</button></h2>
                             @else
                                <h2 align="right">
                                <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalBank">Add BANK Information</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Account No</font></td>
                                            <td class="bg-column"><font color="yellow">Account Name</font></td>
                                            <td class="bg-column"><font color="yellow">Bank</font></td>
                                            <td align="center" class="bg-column"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                     <?php $no = 1 ?>
                                         @foreach($bank as $bnk)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $bnk->bank_acc_no }}</td>
                                            <td>{{ $bnk->bank_acc_name }}</td>
                                            <td>{{ $bnk->bank_name }}</td>
                                            <td align="center">
												<a href="#" data-toggle="modal" data-target="#ModalBankEdit" data-id="{{ $bnk->id }}" data-acc-no="{{ $bnk->bank_acc_no }}" data-acc-name="{{ $bnk->bank_acc_name }}" data-bank-name="{{ $bnk->bank_name }}" title="Edit Data">
                                                    <i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i>
                                                </a>
                                            @if($bnk->delete_flag == 1)
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete bank account number '{{ $bnk->bank_acc_no }}' ?" data-href="{{url('/data-people/add-info/delete-bank')}}/{{ $bnk->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore bank account number '{{ $bnk->bank_acc_no }}' ?" data-href="{{url('/data-people/add-info/delete-bank-active')}}/{{ $bnk->id }}" title="Delete Data">
													<i class="material-icons">restore_page</i>
												</a>
												<!--<a onclick="return confirm('Restore This Bank Account {{ $bnk->bank_acc_name }} ?')" href="{{url('/data-people/add-info/delete-bank-active')}}/{{ $bnk->id }}" data-toggle="tooltip" title="Delete Data">
													<i class="material-icons">restore_page</i>
												</a>-->
                                            @endif 

                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>EDUCATION INFORMATION</b></h2>
                            @if($count_education == 0)
                            <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalEducation">Add Education Information</button></h2>
                             @else
                                <h2 align="right">
                                <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalEducation">Add Education Information</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Year(from-to)</font></td>
                                            <td class="bg-column"><font color="yellow">Level</font></td>
                                            <td class="bg-column"><font color="yellow">School/Institution/University</font></td>
                                            <td class="bg-column"><font color="yellow">Major</font></td>
                                            <td class="bg-column"><font color="yellow">Location</font></td>
                                            
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($education as $edu)
                                        <tr> 
                                            <td>{{ $no }}</td>
                                            <td>{{ $edu->periode_from }} - {{ $edu->periode_to }}</td>
                                            <td>@if($edu->education_lvl == 1)
                                                    SD
                                                @elseif($edu->education_lvl == 2)
                                                    SMP
                                                @elseif($edu->education_lvl == 3)
                                                    SMA/SMK
                                                @elseif($edu->education_lvl == 7)
                                                    D1
												@elseif($edu->education_lvl == 8)
                                                    D2
												@elseif($edu->education_lvl == 4)
                                                    D3
                                                @elseif($edu->education_lvl == 5)
                                                    S1
												@elseif($edu->education_lvl == 6)
                                                    S2
                                                @else
                                                    S3
                                                @endif
												
                                            </td>
                                            <td>{{ $edu->education_institution_name }}</td>
                                            <td>{{ $edu->education_major }}</td>
                                            <td>{{ $edu->education_location }}</td>
                                            
                                            <td align="center">
                                            
												<a href="#" style="padding: 5px;" data-toggle="modal" data-target="#ModalEducationEdit" data-id="{{ $edu->id  }}" data-level="{{ $edu->education_lvl }}" data-education_institution_name="{{ $edu->education_institution_name }}" data-periode_start="<?php echo date('d-m-Y', strtotime($edu->periode_from)); ?>" data-periode_to="<?php echo date('d-m-Y', strtotime($edu->periode_to)); ?>" data-education_major="{{ $edu->education_major }}" data-education_location="{{ $edu->education_location }}" title="Edit Data">
														<i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i>
												</a>
                                            @if($edu->delete_flag == 1)
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete education school '{{ $edu->education_institution_name }}' ?" data-href="{{url('/data-people/add-info/delete-education')}}/{{ $edu->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore education school '{{ $edu->education_institution_name }}' ?" data-href="{{url('/data-people/add-info/delete-education-active')}}/{{ $edu->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @endif

                                            </td>
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>FAMILY INFORMATION</b></h2>
                            @if($count_family == 0)
                            <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalFamily">Add Family Information</button></h2>
                             @else
                                <h2 align="right">
                                <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalFamily">Add Family Information</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Family Member</font></td>
                                            <td class="bg-column"><font color="yellow">Name</font></td>
                                            <td class="bg-column"><font color="yellow">Gender</font></td>
                                            <td class="bg-column"><font color="yellow">Last Education</font></td>
                                            <td class="bg-column"><font color="yellow">Home City</font></td>
                                            <td class="bg-column"><font color="yellow">Phone</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                         <?php $no = 1 ?>
                                         @foreach($family as $fml)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>
                                                @if($fml->member_code == 1)
                                                    Father
                                                @elseif($fml->member_code == 2)
                                                    Mother
                                                @elseif($fml->member_code == 3)
                                                    Brother
                                                @elseif($fml->member_code == 4)
                                                    Sister
                                                @elseif($fml->member_code == 5)
                                                    Son
                                                @elseif($fml->member_code == 6)
                                                    Daugther
                                                @else
                                                    Wife/Husband
                                                @endif
                                            </td>
                                            <td>{{ $fml->member_name }}</td>
                                            <td>{{ $fml->member_gender }}</td>
                                             <td>
                                                @if($fml->member_last_edu == 1 )
                                                    SD
                                                @elseif($fml->member_last_edu == 2)
                                                    SMP
                                                @elseif($fml->member_last_edu == 3)
                                                    SMA/SMK
                                                @elseif($fml->member_last_edu == 4)
                                                    D3
                                                @elseif($fml->member_last_edu == 5)
                                                    S1
                                                 @elseif($fml->member_last_edu == 6)
                                                    S2
                                                @else
                                                    
                                                @endif

                                             </td>
                                             <td>{{ $fml->residence_city }}</td>
                                              <td>{{ $fml->member_phone1 }}</td>
                                            <td align="center">
                                            
												<a href="#" style="padding: 5px;" data-toggle="modal" data-target="#ModalFamilyEdit" 
													data-id="{{ $fml->id  }}" 
													data-member_code="{{ $fml->member_code }}" 
													data-member_name="{{ $fml->member_name }}" 
													data-member_gender="{{ $fml->member_gender }}" 
													data-member_education="{{ $fml->member_last_edu }}" 
													data-member_city="{{ $fml->residence_city }}" 
													data-member_phone1="{{ $fml->member_phone1 }}" 
													data-member_phone2="{{ $fml->member_phone1 }}" 
													data-member_dob="<?php echo date('d-m-Y', strtotime($fml->member_dob)) ?>" 
													data-member_pob="{{ $fml->member_pob }}" 
													data-member_company="{{ $fml->member_company }}" 
													data-member_company_address="{{ $fml->member_company_address }}" 
													data-member_job_position="{{ $fml->member_job_position }}"  title="Edit Data">
														<i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i>
												</a>
                                            @if($fml->delete_flag == 1)
                                                <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete family name '{{ $fml->member_name }}' ?" data-href="{{url('/data-people/add-info/delete-family')}}/{{ $fml->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore family name '{{url('/data-people/add-info/delete-family-active')}}/{{ $fml->id }}' ?" data-href="{{url('/data-people/add-info/delete-family')}}/{{ $fml->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @endif

                                            </td>
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>WORK EXPERIENCE INFORMATION</b></h2>
                             @if($count_work == 0)
                             <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalWork">Add Work Information</button></h2>
                             @else
                                <h2 align="right">
                                 <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalWork">Add Work Information</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>

                                            <td class="bg-column"><font color="yellow">No</td>
                                            <td class="bg-column"><font color="yellow">Name Company</font></td>
                                            <td class="bg-column"><font color="yellow">Year(From-To)</font></td>
                                            <td class="bg-column"><font color="yellow">Job Position</font></td>
                                            <td class="bg-column"><font color="yellow">Salary</font></td>
                                            <td class="bg-column"><font color="yellow">Reason To Quit</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($work as $wk)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $wk->company_name }}</td>
                                            <td>{{ $wk->working_period }} - {{ $wk->working_periode_end }}</td>
                                            <td>{{ $wk->position }}</td>
                                            <td>{{ $wk->salary }}</td>
                                            <td>{{ $wk->reason_resignation }}</td>
                                            <td align="center">
                                            
												<a href="#" style="padding: 5px;" data-toggle="modal" data-target="#ModalWorkEdit" 
														data-id="{{ $wk->id  }}" 
														data-work_company_name="{{ $wk->company_name }}" 
														data-work_start="<?php echo date('d-m-Y', strtotime($wk->working_period)) ?>" 
														data-work_end="<?php echo date('d-m-Y', strtotime($wk->working_periode_end)) ?>" 
														data-work_position="{{ $wk->position }}" 
														data-work_reason="{{ $wk->reason_resignation }}"
														data-work_salary="{{ $wk->salary }}" title="Edit Data">
															<i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i>
												</a>
                                            @if($wk->delete_flag == 1)
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete experience on company '{{ $wk->company_name }}' ?" data-href="{{url('/data-people/add-info/delete-work')}}/{{ $wk->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore experience on company '{{ $wk->company_name }}' ?" data-href="{{url('/data-people/add-info/delete-work-active')}}/{{ $wk->id }}" title="Delete Data">
													<i class="material-icons">restore_page</i>
												</a>
                                            @endif

                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>PERSONAL PHOTOGRAPH</b></h2>
                            @if($count_image <= 2)
                            <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalImage">Add Personal Photograph</button></h2>
                             @else
                             <h2 align="right">
                                <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalImage">Add Personal Photograph</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" ><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Type Image</font></td>
                                            <td class="bg-column"><font color="yellow">Image</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($image as $img)
                                        <tr>
                                            <td>{{ $no }} </td>
                                            <td>{{ $img->image_type }}</td>
                                            <td><img id="myImg" class="p-image" src="{{ URL::to('employe_image/'.$img->image) }}" alt="Snow" width="100px"> </img></td>
                                            <td align="center">
                                            @if($img->delete_flag == 1)
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete type image '{{ $img->image_type }}' ?" data-href="{{url('/data-people/add-info/delete-image')}}/{{ $img->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore type image '{{ $img->image_type }}' ?" data-href="{{url('/data-people/add-info/delete-image-active')}}/{{ $img->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @endif

                                            </td>
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2><b>DOCUMENT INFORMATION</b></h2>
                            @if($count_doc == 0)
                            <h2 align="right">
                             <button type="button" class="btn bg-red waves-effect m-r-20" data-toggle="modal" data-target="#ModalDocument">Add Document Information</button></h2>
                             @else
                             <h2 align="right">
                                <button type="button" class="btn bg-green waves-effect m-r-20" data-toggle="modal" data-target="#ModalDocument">Add Document Information</button></h2>
                             @endif
                        </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Type Document</font></td>
                                            <td class="bg-column"><font color="yellow">Document</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                      <?php $no = 1 ?>
                                         @foreach($doc as $dc)
                                        <tr>
                                            <td>{{ $no }} </td>
                                            <td>{{ $dc->image_type }}</td>
                                            <td><a href="{{ URL::to('document_people/'.$dc->image_path) }}"download="{{$dc->image_path}}">{{ $dc->image_path }}</a></td>
                                            <td align="center">
                                            @if($dc->delete_flag == 1)
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete document type '{{ $dc->image_type }}' ?" data-href="{{url('/data-people/add-info/delete-document')}}/{{ $dc->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @else
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to restore document type '{{ $dc->image_type }}' ?" data-href="{{url('/data-people/add-info/delete-document-active')}}/{{ $dc->id }}" title="Delete Data">
													<i class="material-icons">delete_forever</i>
												</a>
                                            @endif

                                            </td>
                                        </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
            </div>

        <!-- Large Size -->

        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addBank')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalBank" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Bank Information</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label> Account No <span class="mandatory">*</span></label>
                                <input type="text" name="no_rek" class="form-control" placeholder="Account No" id="no_rek" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="20">
								<span class="text-error" id="txt_no_rek"></span>
                                 <br>
                            </div>
                             <div class="col-sm-12">
                                <label>Account Name<span class="mandatory">*</span> </label>
                                <input type="text" name="acc_name" class="form-control" placeholder="Account Name" id="acc_name" maxlength="40">
								<span class="text-error" id="txt_acc_name"></span>
								<br>
							 </div>
                             <div class="col-sm-12">
                                <label>Bank<span class="mandatory">*</span></label>
                                <input type="text" name="name_bank" class="form-control" placeholder="Bank Name" id="name_bank" maxlength="30">
								<span class="text-error" id="txt_name_bank"></span>
                                <br>
                                <br>
                                <input type="hidden" name="employe_id" value="{{ $data->id  }}">
                                
                             </div>
                            
                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
								<button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimbank" style="display: none;"> Save</button>
								<button type="button" class="btn btn-link bg-blue waves-effect" id="sendbank"> Save</button>
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" name="formubahbank" method="POST" action="{{url('/data-people/add-info/editBank')}}"  autocomplete="off">
			{{ csrf_field() }}
            <div class="modal fade" id="ModalBankEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Edit Data Bank</font></h2>
                        </div>
                        
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label> Account No <span class="mandatory">*</span> </label>
                                <input type="text" name="no_rek" class="form-control" id="no_rek2" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="20">
								<span class="text-error" id="txt_no_rek2"></span>
                                <br>
                            </div>
                             <div class="col-sm-12">
                                <label>Account Name <span class="mandatory">*</span></label>
                                <input type="text" name="acc_name" class="form-control" id="acc_name2" maxlength="40">
								<span class="text-error" id="txt_acc_name2"></span>
								<br>
                             </div>
                             <div class="col-sm-12">
                                <label>Bank <span class="mandatory">*</span></label>
                               
                                <input type="text" name="name_bank" class="form-control" id="name_bank2" maxlength="30">
								<span class="text-error" id="txt_name_bank2"></span>
                                <br>
                                <br>
                                <input type="hidden" name="id" id="id">
                                
                             </div>
                            
                        </div>
                        
                        <div class="modal-footer">
                            <br>
								<button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimubahbank" style="display:none;"> Update</button>
								<button type="button" class="btn btn-link bg-blue waves-effect" id="sendubahbank"> Update</button>
								<button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" action=" {{url('/data-people/add-info/addEducation')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalEducation" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Education Information</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Level <span class="mandatory">*</span></label>
                                @if($data->last_education == 'SD')
                                    <select name="level" class="form-control" id="levelsd">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                    </select>
                                @elseif($data->last_education == 'SMP')
                                    <select name="level" class="form-control" id="levelsmp">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                    </select>

                                @elseif($data->last_education == 'SMA/SMK')
                                <select name="level" class="form-control" id="levelsmasmk">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                    </select>

                                @elseif($data->last_education == 'D1')
                                    <select name="level" class="form-control" id="leveld1">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                    </select>
                                @elseif($data->last_education == 'D2')
                                    <select name="level" class="form-control" id="leveld2">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                    </select>

                                @elseif($data->last_education == 'D3')
                                    <select name="level" class="form-control" id="leveld3">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                        <option value="4">D3</option>
                                    </select>
                                @elseif($data->last_education == 'D4')
                                    <select name="level" class="form-control" id="leveld4">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                        <option value="4">D3</option>
                                        <option value="9">D4</option>
                                    </select>

                                @elseif($data->last_education == 'S1')
                                    <select name="level" class="form-control" id="levels1">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                        <option value="4">D3</option>
                                        <option value="9">D4</option>
                                        <option value="5">S1</option>
                                    </select>

                                @elseif($data->last_education == 'S2')
                                <select name="level" class="form-control" id="levels2">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                        <option value="4">D3</option>
                                        <option value="9">D4</option>
                                        <option value="5">S1</option>
                                        <option value="6">S2</option>
                                    </select>

                                @elseif($data->last_education == 'S3')
                                    <select name="level" class="form-control" id="levels3">
                                        <option value="">-select-</option>
                                        <option value="1">SD</option>
                                        <option value="2">SMP</option>
                                        <option value="3">SMA/SMK</option>
                                        <option value="7">D1</option>
                                        <option value="8">D2</option>
                                        <option value="4">D3</option>
                                        <option value="9">D4</option>
                                        <option value="5">S1</option>
                                        <option value="6">S2</option>
                                        <option value="10">S3</option>
                                    </select>

                                @else

                                @endif
								<input type="hidden" value="{{ $data->last_education }}" id="last_education">
								<span class="text-error" id="txt_last_education"></span>
                                <br>
                                
                             </div>
                            <div class="col-sm-12">
                                <label>School Name <span class="mandatory">*</span></label>
                                <input type="text" name="education_institution_name" class="form-control" placeholder="School name" id="education_institution_name" maxlength="40">
								<span class="text-error" id="txt_education_institution_name"></span>
                                <br>
                            </div>
							<div class="col-sm-12">
                                <label>Periode <span class="mandatory">*</span></label>
                            </div>
							<div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="input-daterange input-group" id="bs_datepicker_range_container">
                        				<div class="form-line">
                        					<input type="text" name="periode_start" class="form-control" placeholder="From ..." id="periode_start">
                        				</div>
                        				<span class="text-error" id="txt_periode_start"></span>
										
                        				<span class="input-group-addon" style="margin-left:10px;">to</span>
										
                        				<div class="form-line">
                        					<input type="text" name="periode_to" class="form-control" placeholder="To ..." id="periode_to">
                        				</div>
                        				<span class="text-error" id="txt_periode_to"></span>
                        			</div>
                                </div>
                            </div>
                              
                            
                            <div class="col-sm-12">
                                <label>School Major <span class="mandatory">*</span></label>
                                <input type="text" name="education_major" placeholder="Major" class="form-control" id="education_major" maxlength="40">
								<span class="text-error" id="txt_education_major"></span>
								<br>
                            </div>
                             <div class="col-sm-12">
                                <label>Location/City <span class="mandatory">*</span></label>
                              
                                <select name="education_location" class="form-control" id="education_location">
                                    <option value="">-select-</option>
                                    @foreach($district as $dist)
                                        <option>{{ $dist->district }}</option>
                                    @endforeach
                                </select>
								<span class="text-error" id="txt_education_location"></span>
                                 <br>
                                <br><br>
                                <input type="hidden" name="employe_id" value="{{ $data->id  }}">
                                
                             </div>
                            
                            
                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
								<button type="submit" class="btn btn-link bg-blue waves-effect" style="display:none;" id="kirimeducation"> Save</button>
								<button type="button" class="btn btn-link bg-blue waves-effect" id="addeducation"> Save</button>
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" id="formediteducation" action=" {{url('/data-people/add-info/editEducation')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalEducationEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Edit Data Education</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            <div class="col-sm-12">
                                <label>Level <span class="mandatory">*</span></label>
                                <select name="level" class="form-control" id="level_edit">
									<option value="1">SD</option>
									<option value="2">SMP</option>
									<option value="3">SMA/SMK</option>
									<option value="7">D1</option>
									<option value="8">D2</option>
									<option value="4">D3</option>
									<option value="9">D4</option>
									<option value="5">S1</option>
									<option value="6">S2</option>
									<option value="10">S3</option>
								</select>
								<span class="text-error" id="txt_level_edit"></span>
                                <br>
                                
                             </div>
                            <div class="col-sm-12">
                                <label>School Name <span class="mandatory">*</span></label>
                                <input type="text" name="education_institution_name" class="form-control required" id="education_institution_name2" maxlength="40">
								<span class="text-error" id="txt_education_institution_name2"></span>
								<br>
                            </div>
							 <div class="col-sm-12">
                                <label>Periode <span class="mandatory">*</span></label>
                            </div>
							<div class="col-sm-12">
                                    <div class="input-daterange input-group dates" id="bs_datepicker_range_container2">
                        				<div class="form-line">
                        					<input type="text" name="periode_start" class="form-control periode_start_edit" placeholder="From ..." id="periode_start_edit">
                        				</div>
                        				<span class="text-error" id="txt_periode_start_edit"></span>
										
                        				<span class="input-group-addon" style="margin-left:10px;">to</span>
										
                        				<div class="form-line">
                        					<input type="text" name="periode_to" class="form-control periode_to_edit" placeholder="To ..." id="periode_to_edit">
                        				</div>
                        				<span class="text-error" id="txt_periode_to_edit"></span>
                        			</div>
                            </div>
                             <div class="col-sm-12">
                                <label>School Major <span class="mandatory">*</span></label>
                                <input type="text" name="education_major" class="form-control" id="education_major_edit" maxlength="40">
                                <span class="text-error" id="txt_education_major_edit"></span>
								<br>
                            </div>
                             <div class="col-sm-12">
                                <label>Location/City <span class="mandatory">*</span></label>
                                <select name="education_location" class="form-control" id="education_location_edit">
									@foreach($district as $districts)
										<option value="{{ $districts->district }}">{{ $districts->district }}</option>
									@endforeach                                  
                                </select>
								<span class="text-error" id="txt_education_location_edit"></span>
                                 <br>
                                <br>
                                <input type="hidden" name="id" id="id">
                                
                             </div>
                            
                        </div>
                         
                       
                        
                        <div class="modal-footer">
                            <br>
								<button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimediteducation" style="display: none;">Update</button>
								<button type="button" class="btn btn-link bg-blue waves-effect" id="sendediteducation">Update</button>
                                <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addFamily')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalFamily" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> New Family Information</font></h2>
                        </div>
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Family Members <span class="mandatory">*</span> </label>
                                <select name="status" class="form-control" id="status">
                                    <option value="">-select-</option>
                                    <option value="1">Father</option>
                                    <option value="2">Mother</option>
                                    <option value="3">Brother</option>
                                    <option value="4">Sister</option>
                                    <option value="5">Wife/Husband</option>
                                    <option value="6">Son</option>
                                    <option value="7">Daugther</option>
                                </select>
								<span class="text-error" id="txt_status"></span>
                                <br>
                             </div>
                             <div class="col-sm-12">
                                <label> Name <span class="mandatory">*</span></label>
                                <input type="text" name="name" class="form-control" placeholder=" Input Name" id="name" maxlength="40">
								<span class="text-error" id="txt_name"></span>
                            </div>
                            
                            <div class="col-sm-12">
							<br>
                                <label> Gender <span class="mandatory">*</span></label>
                                <select name="gender" class="form-control" id="gender">
                                    <option value="">-select-</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
								<span class="text-error" id="txt_gender"></span>
                            </div>
                             <div class="col-sm-12">
							 <br>
                                <label>Last Education Level <span class="mandatory">*</span></label>
                                <select name="education" class="form-control" id="education">
                                    <option value="">-select-</option>
                                    <option value="1">SD</option>
                                    <option value="2">SMP</option>
                                    <option value="3">SMA/SMK</option>
                                    <option value="7">D1</option>
                                    <option value="8">D2</option>
                                    <option value="4">D3</option>
                                    <option value="9">D4</option>
                                    <option value="5">S1</option>
                                    <option value="6">S2</option>
                                    <option value="10">S3</option>
                                </select>
								<span class="text-error" id="txt_education"></span>
                             </div>
							 
                            <div class="col-sm-12">
							<br>
                                <label> Date of birth <span class="mandatory">*</span></label>
                                <div class="input-group" id="bs_datepicker_component_container">
                                    <div class="form-line">
                                        <input type="text" name="dob" class="form-control datepicker" placeholder="Please choose a date..." id="dob">
                                    </div>
									<span class="text-error" id="txt_dob"></span>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                             <div class="col-sm-12">
                                <label>Place of birth <span class="mandatory">*</span></label>
                                <input type="text" name="pob" class="form-control" placeholder="Place of birth" id="pob" maxlength="40">
								<span class="text-error" id="txt_pob"></span>
								<br>
                             </div>
                            
                            
                            <div class="col-sm-12">
                                <label> Phone 1 <span class="mandatory">*</span></label>
                                <input type="text" name="phone1" class="form-control" placeholder="input phone 1" id="phone1" maxlength="15" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
								<span class="text-error" id="txt_phone1"></span>
								<br>
                            </div>
                             <div class="col-sm-12">
                                <label> Phone 2</label>
                                <input type="text" name="phone2" class="form-control" placeholder="input phone 2" id="phone2" maxlength="15" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
								<span class="text-error" id="txt_phone2"></span>
								<br>
                             </div>
                            
                            <div class="col-sm-12">
                                <label>Company Name <span class="mandatory">*</span></label>
                                <input type="text" name="company" class="form-control" placeholder="input company name" id="company" maxlength="50">
								<span class="text-error" id="txt_company"></span>
								<br>
                            </div>
                             <div class="col-sm-12">
                                <label>Company Address <span class="mandatory">*</span></label>
                                <textarea name="company_address" class="form-control" placeholder=" Input Address" id="company_address" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="90"></textarea>
								<span class="text-error" id="txt_company_address"></span>
								<br>
                             </div>
                            
                            <div class="col-sm-12">
                                <label>Job Position <span class="mandatory">*</span></label>
                                <input type="text" name="position" class="form-control" placeholder="Job Position" id="position" id="position" maxlength="40">
								<span class="text-error" id="txt_position"></span>
								<br>
                            </div>
                             <div class="col-sm-12">
                                <label>Home City <span class="mandatory">*</span></label>
                                <input type="text" name="city" class="form-control" placeholder=" home city" id="city" maxlength="40">
								<span class="text-error" id="txt_city"></span>
                                <br>
                                <br>
                                <input type="hidden" name="employe_id" value="{{ $data->id  }}">
                                 
                            
                             </div>

                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
							<button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimfamily" style="display: none;"> Save</button>
							<button type="button" class="btn btn-link bg-blue waves-effect" id="sendfamily"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

            <form class="form-signin" method="POST" action="{{url('/data-people/add-info/editFamily')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalFamilyEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Edit Data Family</font></h2>
                        </div>
                        
                        <div class="modal-body">
                             <div class="col-sm-12">
                                <label>Family Members <span class="mandatory">*</span></label>
                                <select name="status" class="form-control" id="status2">
                                    <option value="1">Father</option>
                                    <option value="2">Mother</option>
                                    <option value="3">Brother</option>
                                    <option value="4">Sister</option>
                                    <option value="5">Wife/Husband</option>
                                    <option value="6">Son</option>
                                    <option value="7">Daugther</option>
                                </select>
								<span class="text-error" id="txt_status2"></span>
                                
                             </div>
                             <div class="col-sm-12">
								<br>
                                <label> Name <span class="mandatory">*</span></label>
                                <input type="text" name="name" class="form-control" id="name2" maxlength="40">
                                <span class="text-error" id="txt_name2"></span>
                            </div>
                            
                            <div class="col-sm-12">
								<br>
                                <label> Gender <span class="mandatory">*</span></label>
                                <select name="gender" class="form-control" id="gender2">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
								<span class="text-error" id="txt_gender2"></span>
                            </div>
                             <div class="col-sm-12">
								<br>
                                <label>Last Education Level <span class="mandatory">*</span></label>
                                <select name="education" class="form-control" id="education2">
                                    <option value="1">SD</option>
                                    <option value="2">SMP</option>
                                    <option value="3">SMA/SMK</option>
                                    <option value="7">D1</option>
                                    <option value="8">D2</option>
                                    <option value="4">D3</option>
                                    <option value="9">D4</option>
                                    <option value="5">S1</option>
                                    <option value="6">S2</option>
                                    <option value="10">S3</option>
                                </select>
                                <span class="text-error" id="txt_education2"></span>
                             </div>
                            
                            <div class="col-sm-12">
								<br>
                                <label> Date of birth <span class="mandatory">*</span></label>
                                <div class="input-group date" id="bs_datepicker_component_container2">
                                    <div class="form-line">
                                        <input type="text" name="dob" class="form-control datepickerdob2"  placeholder="Please choose a date..." id="dob2">
                                    </div>
									<span class="text-error" id="txt_dob2"></span>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                             <div class="col-sm-12">
                                <label>Place of birth <span class="mandatory">*</span></label>
                                <input type="text" name="pob" class="form-control" id="pob2" maxlength="40">
								<span class="text-error" id="txt_pob2"></span>
                             </div>
                            
                            <div class="col-sm-12">
								<br>
                                <label> Phone 1 <span class="mandatory">*</span></label>
                                <input type="text" name="phone1" class="form-control" id="phone12" maxlength="15" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
								<span class="text-error" id="txt_phone12"></span>
                            </div>
                             <div class="col-sm-12">
								<br>
                                <label> Phone 2</label>
                                <input type="text" name="phone2" class="form-control" id="phone22" maxlength="15" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
								<span class="text-error" id="txt_phone22"></span>
                             </div>
                            
                            <div class="col-sm-12">
								<br>
                                <label>Company Name <span class="mandatory">*</span></label>
                                <input type="text" name="company" class="form-control" id="company2" maxlength="50">
								<span class="text-error" id="txt_company2"></span>
                            </div>
                             <div class="col-sm-12">
								<br>
                                <label>Company Address <span class="mandatory">*</span></label>
								<textarea name="company_address" class="form-control" placeholder=" Input Address" id="company_address2" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="90"></textarea>
								<span class="text-error" id="txt_company_address2"></span>
                             </div>
                            
                            <div class="col-sm-12">
								<br>
                                <label>Job Position <span class="mandatory">*</span></label>
                                <input type="text" name="position" class="form-control" id="job_position" maxlength="40">
								<span class="text-error" id="txt_job_position2"></span>
							</div>
                             <div class="col-sm-12">
								<br>
                                <label>Home City <span class="mandatory">*</span></label>
                                <input type="text" name="city" class="form-control" id="city2" maxlength="40">
								<span class="text-error" id="txt_city2"></span>
                                 <br>
                                <br>
                                <input type="hidden" name="id" id="id">
                                 
                            
                             </div>

                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimeditfamily" style="display: none;">Update</button>
							<button type="button" class="btn btn-link bg-blue waves-effect" id="sendeditfamily">Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addWork')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalWork" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> New Work Experience Information</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Company Name <span class="mandatory">*</span></label>
                                <input type="text" name="name_company" placeholder="Company Name" class="form-control" id="name_company" maxlength="40">
                                <span class="text-error" id="txt_name_company"></span>
                             </div>
                             <div class="col-sm-12">
								<br>
								<div class="input-daterange input-group dateswork" id="bs_datepicker">
                        				<div class="form-line">
                        					<input type="text" name="periode" class="form-control" placeholder="From ..." id="periode_start_work">
                        				</div>
                        				<span class="text-error" id="txt_periode_start_work"></span>
										
                        				<span class="input-group-addon" style="margin-left:10px;">to</span>
										
                        				<div class="form-line">
                        					<input type="text" name="periode_end" class="form-control" placeholder="To ..." id="periode_end_work">
                        				</div>
                        				<span class="text-error" id="txt_periode_end_work"></span>
                        			</div>
                                 
                            </div>
                            <div class="col-sm-12">
                                <label>Job Position <span class="mandatory">*</span></label>
                                <input type="text" name="position" class="form-control" placeholder="Job Position" id="position_work" maxlength="40">
                                <span class="text-error" id="txt_position_work"></span>
                            </div>
                            <div class="col-sm-12">
								<br>
                                <label> Reason to Quit Job <span class="mandatory">*</span></label>
                                <textarea name="reason_resignation" class="form-control" placeholder="Reason to Quit Job" id="reason_resignation" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="90"></textarea>
                                <span class="text-error" id="txt_reason_resignation"></span>
                            </div>
                             <div class="col-sm-12">
								<br>
                                <label>Last Salary <span class="mandatory">*</span></label>
                                <input type="text" name="salary" class="form-control" placeholder="Salary" id="salary" min="1" max="11" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                <span class="text-error" id="txt_salary"></span>
                                 <br>
                                <br>
                                <input type="hidden" name="employe_id" id="id" value="{{ $data->id }}">
                                 
                            
                             </div>

                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimwork" style="display: none;"> Save</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="sendwork"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

         <form class="form-signin" method="POST" action="{{url('/data-people/add-info/editWork')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalWorkEdit" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Edit Work Experience</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Company Name <span class="mandatory">*</span></label>
                                <input type="text" name="name_company" id="name_company_edit" maxlength="40" class="form-control">
                                <span class="text-error" id="txt_name_company_edit"></span>
                             </div>
                             <div class="col-sm-12">
								<br>
                                <label>From <span class="mandatory">*</span></label>
                                <div class="input-daterange input-group datesworkedit" id="bs_datepicker">
                                        <div class="form-line">
                                            <input type="text" name="periode" class="form-control" placeholder="From ..." id="periode_start_work_edit">
                                        </div>
                                        <span class="text-error" id="txt_periode_start_work_edit"></span>
                                        
                                        <span class="input-group-addon" style="margin-left:10px;">to</span>
                                        
                                        <div class="form-line">
                                            <input type="text" name="periode_end" class="form-control" placeholder="To ..." id="periode_end_work_edit">
                                        </div>
                                        <span class="text-error" id="txt_periode_end_work_edit"></span>
                                    </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <label>Job Position <span class="mandatory">*</span></label>
                                <input type="text" name="position" class="form-control" id="position_edit" maxlength="40">
                                <span class="text-error" id="txt_position_edit"></span>
                            </div>
                            <div class="col-sm-12">
								<br>
                                <label>Reason to Quit Job <span class="mandatory">*</span></label>
                                <textarea name="reason_resignation" class="form-control" placeholder="Reason to Quit Job" id="reason_resignation_edit" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="90"></textarea>
                                <span class="text-error" id="txt_reason_resignation_edit"></span>
                            </div>
                             <div class="col-sm-12">
								<br>
                                <label>Last Salary <span class="mandatory">*</span></label>
                                <input type="text" name="salary" class="form-control" placeholder="Salary" id="salary_edit" min="1" max="11" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                <span class="text-error" id="txt_salary_edit"></span>
                                 <br>
                                <br>
                                <input type="hidden" name="id" id="id">
                                 
                            
                             </div>

                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimeditwork" style="display: none;">Update</button>
                            <button type="button" class="btn btn-link bg-blue waves-effect" id="sendeditwork">Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
       
        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addImage')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalImage" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow"> Add Image</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Image Type <span class="mandatory">*</span></label>
                                <select name="image_type" class="form-control" id="image_type">
                                    <option value="">-select-</option>
                                    <option value="Front View">Front View</option>
                                    <option value="Side View">Side View</option>
                                    <option value="Ijazah">Ijazah</option>
                                    <option value="KTP">KTP</option>
                                </select>
                                <span class="text-error" id="txt_image_type"></span>
                             </div>
                             <div class="col-sm-12">
							 <br>
                                <label>Photograph <span class="mandatory">*</span></label>
                                <input type="file" name="image" class="form-control" id="profile-img" onchange="loadFile(event)">
								<span class="text-error" id="txt_profile-img"></span>
                                <br>
                                  <h2 align="center"><img src="" id="profile-img-tag" width="200px" />
                                 <input type="hidden" name="employe_id" value="{{ $data->id  }}"></h2>
                                 
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimimage" style="display: none;"> Save </button>
							<button type="button" class="btn btn-link bg-blue waves-effect" id="sendimage"> Save </button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

         <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addDocument')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalDocument" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Add Document</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Document Type <span class="mandatory">*</span></label>
                                <select name="document_type" class="form-control" id="document_type">
                                    <option value="">-pilih-</option>
                                    <option value="CV">CV</option>
                                    <option value="Sertifikat">Sertifikat</option>
                                </select>
                                <span class="text-error" id="txt_document_type"></span>
                             </div>
                             <div class="col-sm-12">
								<br>
                                <label>Document <span class="mandatory">*</span></label>
                                <input type="file" name="image" class="form-control" id="document">
								<span class="text-error" id="txt_document"></span>
                                <br>
                                  <h2 align="center">
                                 <input type="hidden" name="employe_id" value="{{ $data->id  }}"></h2>
                                 
                            </div>
                            
                        </div>
                        <div class="modal-footer">
                            <br>
                            <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimdocument" style="display: none;"> Save</button>
							<button type="button" class="btn btn-link bg-blue waves-effect" id="senddocument"> Save</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/addInteres')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalInteres" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">New Job Interest</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Job Interes <span class="mandatory">*</span></label>
                                <select name="job_id" class="form-control" id="job_id">
                                    <option value="">-select-</option>  
                                @foreach($job as $jobs)
                                    <option value="{{ $jobs->id }}">{{ $jobs->job_name }}</option>
                                @endforeach                                  
                                </select>
								<span class="text-error" id="txt_job_id"></span>
                                <br>
                                <br>
                                <input type="hidden" name="employe_id" value="{{ $data->id  }}">
                                
                             </div>
                            
                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
                             <button type="submit" class="btn btn-link bg-blue waves-effect" id="kirimnewjobinterest" style="display: none;"> Save</button>
							 <button type="button" class="btn btn-link bg-blue waves-effect" id="sendnewjobinterest"> Save</button>
                             <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form>

        @foreach($interes as $qwer)
        <form class="form-signin" method="POST" action="{{url('/data-people/add-info/editInteres')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="ModalInteresEdit{{ $qwer->id }}" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h2 class="modal-title" id="largeModalLabel"><font color="yellow">Edit Job Interest</font></h2>
                        </div>
                        
                        
                        <div class="modal-body">
                            
                             <div class="col-sm-12">
                                <label>Job Interes <span class="mandatory">*</span></label>
                                <select name="job_id" class="form-control">
                                @foreach($job as $jobs)
                                    <option value="{{ $jobs->id }}" @if($qwer->interes_id === $jobs->id) selected='selected' @endif>{{ $jobs->job_name }}</option>
                                @endforeach                                  
                                </select>
                                <br>
                                <br>
                                <input type="hidden" name="employe_id" value="{{ $data->id  }}">
                                <input type="hidden" name="id" value="{{ $qwer->id  }}">
                                
                             </div>
                            
                        </div>
                       
                        
                        <div class="modal-footer">
                            <br>
							<button type="submit" class="btn btn-link bg-blue waves-effect"> Update</button>
                            <button type="button" class="btn btn-link bg-gray waves-effect" data-dismiss="modal">Cancel</button>
                           
                        </div>
                    </div>
                </div>
            </div>
        </form> 
        @endforeach

		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
	
	<!--bank-->
	<script type="text/javascript"> 
    $(document).ready(function() {
      
	   $('#no_rek2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32)
				e.preventDefault();
      });
	   
		  $(document).on('paste', '#no_rek2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	  $('#acc_name2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#acc_name2').val().length > 0){
				$('#acc_name2').val($('#acc_name2').val().replace(/\s{2,}/g,' '));
			}
      });
	  
		  $(document).on('paste', '#acc_name2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 40));
	   });
	   
	  $('#name_bank2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#name_bank2').val().length > 0){
				$('#name_bank2').val($('#name_bank2').val().replace(/\s{2,}/g,' '));
			}
      });
	  
		  $(document).on('paste', '#name_bank2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 30));
	   }); 
	   
	   $('#sendubahbank').click(function(){
		   
			 if ($('#no_rek2').val().length < 1) {
					$('#txt_no_rek2').text('No account is empty');
			  }else{
					$('#txt_no_rek2').text('');
			  }
			  
			  if ($('#acc_name2').val().length < 1) { 
					$('#txt_acc_name2').text('Acc name is empty'); 
			  }else{
					$('#txt_acc_name2').text('');
			  }
			  
			  if ($('#name_bank2').val().length < 1) {
					$('#txt_name_bank2').text('Name bank is empty');
			  }else{
					$('#txt_name_bank2').text('');
			  }
			  
			    if($('#no_rek2').val().length > 0 && $('#acc_name2').val().length > 0 && $('#name_bank2').val().length > 0){
					$('#kirimubahbank').click();
				}
		});
		
    });

  </script>
	
	<!--konfirmasi hapus-->
	<script type="text/javascript">
    $(document).ready(function() {
        $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
			var message = $(e.relatedTarget).data('message');
			$(e.currentTarget).find('.message').text(message);
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
        });
		
		$('#ModalBankEdit').on('show.bs.modal', function(e) { 
			var id = $(e.relatedTarget).data('id');
			var acc_no = $(e.relatedTarget).data('acc-no');
			var acc_name = $(e.relatedTarget).data('acc-name');
			var bank_name = $(e.relatedTarget).data('bank-name');
			var href = $(e.relatedTarget).data('bank-name');
			$(e.currentTarget).find('#no_rek2').val(acc_no); 
			$(e.currentTarget).find('#acc_name2').val(acc_name);
			$(e.currentTarget).find('#name_bank2').val(bank_name);
			$(e.currentTarget).find('#id').val(id);  
        });
		
		$('#ModalEducationEdit').on('show.bs.modal', function(e) { 
			var id = $(e.relatedTarget).data('id');
			var level = $(e.relatedTarget).data('level');
			var education_institution_name = $(e.relatedTarget).data('education_institution_name');
			var periode_start = $(e.relatedTarget).data('periode_start');
			var periode_to = $(e.relatedTarget).data('periode_to');
			var education_major = $(e.relatedTarget).data('education_major');
			var education_location = $(e.relatedTarget).data('education_location');
			
			$(e.currentTarget).find('#id').val(id);
			$(e.currentTarget).find('#level_edit').val(level).change();
			$(e.currentTarget).find('#education_institution_name2').val(education_institution_name);
			$(e.currentTarget).find('#education_major_edit').val(education_major);	
			$(e.currentTarget).find('#education_location_edit').val(education_location).change();	
			
			$(e.currentTarget).find('.dates').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				todayHighlight: true
			  }).on('show.bs.modal', function(event) {
				  event.stopPropagation();
			  });
			  
			var dates = periode_start.split('-');
			var dates1 = periode_to.split('-');
			$(e.currentTarget).find('#periode_start_edit').datepicker().datepicker('setDate', new Date(parseInt(dates[2]), parseInt(dates[1]) - 1, parseInt(dates[0])));
			$(e.currentTarget).find('#periode_to_edit').datepicker().datepicker('setDate', new Date(parseInt(dates1[2]), parseInt(dates1[1]) - 1, parseInt(dates1[0])));

        });
		
		 $('#ModalFamilyEdit').on('show.bs.modal', function(e) {

			var id = $(e.relatedTarget).data('id');
			var member_code = $(e.relatedTarget).data('member_code');
			var member_name = $(e.relatedTarget).data('member_name');
			var member_gender = $(e.relatedTarget).data('member_gender');
			var member_education = $(e.relatedTarget).data('member_education');
			var member_city = $(e.relatedTarget).data('member_city');
			var member_phone1 = $(e.relatedTarget).data('member_phone1');
			var member_phone2 = $(e.relatedTarget).data('member_phone2');
			var member_dob = $(e.relatedTarget).data('member_dob');
			var member_pob = $(e.relatedTarget).data('member_pob');
			var member_company = $(e.relatedTarget).data('member_company');
			var member_company_address = $(e.relatedTarget).data('member_company_address');
			var member_job_position = $(e.relatedTarget).data('member_job_position');

			$(e.currentTarget).find('#id').val(id);
			$(e.currentTarget).find('#status2').val(member_code); 
			$(e.currentTarget).find('#name2').val(member_name);
			$(e.currentTarget).find('#gender2').val(member_gender);
			$(e.currentTarget).find('#education2').val(member_education);
			
			$(e.currentTarget).find('#pob2').val(member_pob);	
			$(e.currentTarget).find('#phone12').val(member_phone1);	
			$(e.currentTarget).find('#phone22').val(member_phone2);
			$(e.currentTarget).find('#company2').val(member_company);
			$(e.currentTarget).find('#company_address2').val(member_company_address);
			$(e.currentTarget).find('#job_position').val(member_job_position);	
			$(e.currentTarget).find('#city2').val(member_city);	

			$(e.currentTarget).find('#dob2').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				todayHighlight: true
			  }).on('show.bs.modal', function(event) {
				  event.stopPropagation();
			  });
			  
			var dates = member_dob.split('-');
			$(e.currentTarget).find('#dob2').datepicker().datepicker('setDate', new Date(parseInt(dates[2]), parseInt(dates[1]) - 1, parseInt(dates[0])));
        });
		
		
		$('#ModalWorkEdit').on('show.bs.modal', function(e) {

            var id = $(e.relatedTarget).data('id');
            var work_company_name = $(e.relatedTarget).data('work_company_name');
            var work_start = $(e.relatedTarget).data('work_start');
            var work_end = $(e.relatedTarget).data('work_end');
            var work_position = $(e.relatedTarget).data('work_position');
            var work_reason = $(e.relatedTarget).data('work_reason');
            var work_salary = $(e.relatedTarget).data('work_salary');

            $(e.currentTarget).find('#id').val(id);
            $(e.currentTarget).find('#name_company_edit').val(work_company_name); 
            $(e.currentTarget).find('#position_edit').val(work_position);
            $(e.currentTarget).find('#reason_resignation_edit').val(work_reason);   

			var reverse = work_salary.toString().split('').reverse().join(''),
		    ribuan = reverse.match(/\d{1,3}/g);
		    ribuan = ribuan.join('.').split('').reverse().join('');
			$(e.currentTarget).find('#salary_edit').val(ribuan);  
			
			$(e.currentTarget).find('.datesworkedit').datepicker({
				format: "dd-mm-yyyy",
				autoclose: true,
				todayHighlight: true
			  }).on('show.bs.modal', function(event) {
				  event.stopPropagation();
			  });
			  
			var dates = work_start.split('-');
			var dates1 = work_end.split('-');
			$(e.currentTarget).find('#periode_start_work_edit').datepicker().datepicker('setDate', new Date(parseInt(dates[2]), parseInt(dates[1]) - 1, parseInt(dates[0])));
			$(e.currentTarget).find('#periode_end_work_edit').datepicker().datepicker('setDate', new Date(parseInt(dates1[2]), parseInt(dates1[1]) - 1, parseInt(dates1[0])));
			
        });
		
    });

  </script>
     <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>   

    <script type="text/javascript">
        $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>
	
	<!--bank-->
	<script type="text/javascript">
		$(function(){
			
		    $('#no_rek').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		    });
		   
			$(document).on('paste', '#no_rek', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				var isi = text.substring(0, 20);
				$(this).val(isi.replace(/\D/g, ''));
		    });
		   
		    $('#acc_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#acc_name').val().length > 0){
					$('#acc_name').val($('#acc_name').val().replace(/\s{2,}/g,' '));
				}
		    });
		  
		    $(document).on('paste', '#acc_name', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
		    });
		   
		    $('#name_bank').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name_bank').val().length > 0){
					$('#name_bank').val($('#name_bank').val().replace(/\s{2,}/g,' '));
				}
		    });
		  
		    $(document).on('paste', '#name_bank', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 30));
		    });
		   
		    $("#sendbank").click(function(){
			   
				 if ($('#no_rek').val().length < 1) {
						$('#txt_no_rek').text('No account is empty');
				  }else{
						$('#txt_no_rek').text('');
				  }
				  
				  if ($('#acc_name').val().length < 1) { 
						$('#txt_acc_name').text('Acc name is empty'); 
				  }else{
						$('#txt_acc_name').text('');
				  }
				  
				  if ($('#name_bank').val().length < 1) {
						$('#txt_name_bank').text('Name bank is empty');
				  }else{
						$('#txt_name_bank').text('');
				  }
				  
				  if($('#no_rek').val().length > 0 && $('#acc_name').val().length > 0 && $('#name_bank').val().length > 0){
						 $("#kirimbank").click();	 
					 }
			});
		});
	</script>
	
	
	<!--job interest-->
	<script type="text/javascript">
		$(function(){
			$("#sendnewjobinterest").click(function(){
			   if ($('#job_id').val().length < 1) {
					$('#txt_job_id').text('Job not selected');
			    }else{
					$('#txt_job_id').text('');
			    }
			  
			    if($('#job_id').val().length > 0 ){
					$("#kirimnewjobinterest").click();	 
				}
		    });
		});
	</script>
	
	<!--education-->
	<script type="text/javascript">
     $(function(){
		
		$('#education_institution_name').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#education_institution_name').val().length > 0){
				$('#education_institution_name').val($('#education_institution_name').val().replace(/\s{2,}/g,' '));
			}
		});
		  
		$(document).on('paste', '#education_institution_name', function(e) {
			e.preventDefault();
			var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			var text = $(this).val()+withoutSpaces;
			$(this).val(text.substring(0, 40));
		});
		   
		$('#education_major').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#education_major').val().length > 0){
				$('#education_major').val($('#education_major').val().replace(/\s{2,}/g,' '));
			}
		});
		  
		$(document).on('paste', '#education_major', function(e) {
			e.preventDefault();
			var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			var text = $(this).val()+withoutSpaces;
			$(this).val(text.substring(0, 40));
		});
		   
		$("#addeducation").click(function(){
		   
			 if ($('#last_education').val() == 'SD' && $('#levelsd').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'SMP' && $('#levelsmp').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'SMA/SMK' && $('#levelsmasmk').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'D1' && $('#leveld1').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'D2' && $('#leveld2').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'D3' && $('#leveld3').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'D4' && $('#leveld4').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'S1' && $('#levels1').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'S2' && $('#levels2').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else if ($('#last_education').val() == 'S3' && $('#levels3').val() == '') {
					$('#txt_last_education').text('Level education not selected');
			 }else{
					$('#txt_last_education').text('');
			 }
			  
			  if ($('#education_institution_name').val().length < 1) { 
					$('#txt_education_institution_name').text('Education institution name is empty'); 
			  }else{
					$('#txt_education_institution_name').text('');
			  }
			  
			  if ($('#periode_start').val().length < 1) {
					$('#txt_periode_start').text('start is empty');
			  }else{
					$('#txt_periode_start').text('');
			  }
			  
			  if ($('#periode_to').val().length < 1) {
					$('#txt_periode_to').text('to is empty');
			  }else{
					$('#txt_periode_to').text('');
			  }
			  
			  if ($('#education_major').val().length < 1) {
					$('#txt_education_major').text('Education major to is empty');
			  }else{
					$('#txt_education_major').text('');
			  }
			  
			  if ($('#education_location').val().length < 1) {
					$('#txt_education_location').text('Education location to is empty');
			  }else{
					$('#txt_education_location').text('');
			  }
			  
			  if(
					$('#education_institution_name').val().length > 0 && 
					$('#periode_start').val().length > 0 && 
					$('#periode_to').val().length > 0 &&
					$('#education_major').val().length > 0 &&
					$('#education_location').val().length > 0
				){
					if ($('#last_education').val() == 'SD' && $('#levelsd').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'SMP' && $('#levelsmp').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'SMA/SMK' && $('#levelsmasmk').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'D1' && $('#leveld1').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'D2' && $('#leveld2').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'D3' && $('#leveld3').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'D4' && $('#leveld4').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'S1' && $('#levels1').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'S2' && $('#levels2').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else if ($('#last_education').val() == 'S3' && $('#levels3').val() == '') {
							$('#txt_last_education').text('Level education not selected');
					 }else{
							$("#kirimeducation").click();	
					 }
					  
				}
		});		
	   
     });
    </script>
	
	<!--edit education-->
	<script type="text/javascript">
     $(function(){
      
		 $('#education_institution_name2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#education_institution_name2').val().length > 0){
				$('#education_institution_name2').val($('#education_institution_name2').val().replace(/\s{2,}/g,' '));
			}
		});
		  
		$(document).on('paste', '#education_institution_name2', function(e) {
			e.preventDefault();
			var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			var text = $(this).val()+withoutSpaces;
			$(this).val(text.substring(0, 40));
		});
		   
		 $('#education_major_edit').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#education_major_edit').val().length > 0){
				$('#education_major_edit').val($('#education_major_edit').val().replace(/\s{2,}/g,' '));
			}
		});
		  
		$(document).on('paste', '#education_major_edit', function(e) {
			e.preventDefault();
			var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			var text = $(this).val()+withoutSpaces;
			$(this).val(text.substring(0, 40));
		});
		   
		$("#sendediteducation").click(function(){
			if ($('#education_institution_name2').val().length < 1) {
				$('#txt_education_institution_name2').text('School name is empty');
			}else{
				$('#txt_education_institution_name2').text('');
			}
			
			if($('#periode_start_edit').val().length < 1){
				$('#txt_periode_start_edit').text('Start is empty');
			}else{
				$('#txt_periode_start_edit').text('');	
			}
			
			if($('#periode_to_edit').val().length < 1){
				$('#txt_periode_to_edit').text('End is empty');
			}else{
				$('#txt_periode_to_edit').text('');
			}
			
			if($('#education_major_edit').val().length < 1){
				$('#txt_education_major_edit').text('Education major is empty');
			}else{
				$('#txt_education_major_edit').text('');
			}
			
			if($('#level_edit').val().length < 1){
				$('#txt_level_edit').text('Level not selected');
			}else{
				$('#txt_level_edit').text('');
			}
			
			if($('#education_location_edit').val().length < 1){
				$('#txt_education_location_edit').text('Education location is empty');
			}else{
				$('#txt_education_location_edit').text('');
			}
			
			if (
				$('#education_institution_name2').val().length > 0 &&
				$('#periode_start_edit').val().length > 0 &&
				$('#periode_to_edit').val().length > 0 &&
				$('#education_major_edit').val().length > 0 &&
				$('#level_edit').val().length > 0 &&
				$('#education_location_edit').val().length > 0
				)
				{
					$("#kirimediteducation").click();	
				}
			
		});
	  
     });
    </script>
	
	<!--Add family-->
	<script type="text/javascript">
		$(function(){
			
			$('#name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name').val().length > 0){
					$('#name').val($('#name').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#name', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#pob').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#pob').val().length > 0){
					$('#pob').val($('#pob').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#pob', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#phone1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		    });
		   
			$(document).on('paste', '#phone1', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				var isi = text.substring(0, 15);
				$(this).val(isi.replace(/\D/g, ''));
		    });
			
			$('#phone2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		    });
		   
			$(document).on('paste', '#phone2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				var isi = text.substring(0, 15);
				$(this).val(isi.replace(/\D/g, ''));
		    });
			
			$('#company').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#company').val().length > 0){
					$('#company').val($('#company').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#company', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 50));
			});
			
			$('#company_address').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#company_address').val().length > 0){
					$('#company_address').val($('#company_address').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#company_address', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 90));
			});
			
			$('#position').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#position').val().length > 0){
					$('#position').val($('#position').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#position', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#city').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#city').val().length > 0){
					$('#city').val($('#city').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#city', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$("#sendfamily").click(function(){
				
				if ($('#status').val().length < 1) {
					$('#txt_status').text('Status not selected');
				}else{
					$('#txt_status').text('');
				}
				
				if ($('#name').val().length < 1) {
					$('#txt_name').text('Name is empty');
				}else{
					$('#txt_name').text('');
				}
				
				if ($('#gender').val().length < 1) {
					$('#txt_gender').text('Gender is empty');
				}else{
					$('#txt_gender').text('');
				}
				
				if ($('#education').val().length < 1) {
					$('#txt_education').text('Education not selected');
				}else{
					$('#txt_education').text('');
				}
				
				if ($('#dob').val().length < 1) {
					$('#txt_dob').text('Date of birth is empty');
				}else{
					$('#txt_dob').text('');
				}
				
				if ($('#pob').val().length < 1) {
					$('#txt_pob').text('Place of birth is empty');
				}else{
					$('#txt_pob').text('');
				}
				
				if ($('#phone1').val().length < 1) {
					$('#txt_phone1').text('Phone number is empty');
				}else{
					$('#txt_phone1').text('');
				}
				
				if ($('#phone2').val().length < 1) {
					$('#txt_phone2').text('Phone number is empty');
				}else{
					$('#txt_phone2').text('');
				}
				
				if ($('#company').val().length < 1) {
					$('#txt_company').text('Company is empty');
				}else{
					$('#txt_company').text('');
				}
				
				if ($('#company_address').val().length < 1) {
					$('#txt_company_address').text('Company address is empty');
				}else{
					$('#txt_company_address').text('');
				}
				
				if ($('#position').val().length < 1) {
					$('#txt_position').text('Position is empty');
				}else{
					$('#txt_position').text('');
				}
				
				if ($('#city').val().length < 1) {
					$('#txt_city').text('City is empty');
				}else{
					$('#txt_city').text('');
				}
				
				if(
					$('#status').val().length > 0 &&
					$('#name').val().length > 0 &&
					$('#gender').val().length > 0 &&
					$('#education').val().length > 0 &&
					$('#dob').val().length > 0 &&
					$('#pob').val().length > 0 &&
					$('#phone1').val().length > 0 &&
					$('#phone2').val().length > 0 &&
					$('#company').val().length > 0 &&
					$('#company_address').val().length > 0 &&
					$('#position').val().length > 0 &&
					$('#city').val().length > 0 
				)
				{
					$("#kirimfamily").click();	
				}
			
			});
			
		});
	</script>
	
	<!--Edit family-->
	<script type="text/javascript">
		$(function(){
			
			$('#name2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name2').val().length > 0){
					$('#name2').val($('#name2').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#name2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#pob2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#pob2').val().length > 0){
					$('#pob2').val($('#pob2').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#pob2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#phone12').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		    });
		   
			$(document).on('paste', '#phone12', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				var isi = text.substring(0, 15);
				$(this).val(isi.replace(/\D/g, ''));
		    });
			
			$('#phone22').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		    });
		   
			$(document).on('paste', '#phone22', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				var isi = text.substring(0, 15);
				$(this).val(isi.replace(/\D/g, ''));
		    });
			
			$('#company2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#company2').val().length > 0){
					$('#company2').val($('#company2').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#company2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 50));
			});
			
			$('#company_address2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#company_address2').val().length > 0){
					$('#company_address2').val($('#company_address2').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#company_address2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 90));
			});
			
			$('#job_position').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#job_position').val().length > 0){
					$('#job_position').val($('#job_position').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#job_position', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$('#city2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#city2').val().length > 0){
					$('#city2').val($('#city2').val().replace(/\s{2,}/g,' '));
				}
			});
			  
			$(document).on('paste', '#city2', function(e) {
				e.preventDefault();
				var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				var text = $(this).val()+withoutSpaces;
				$(this).val(text.substring(0, 40));
			});
			
			$("#sendeditfamily").click(function(){
				
				if ($('#status2').val().length < 1) {
					$('#txt_status2').text('Status not selected');
				}else{
					$('#txt_status2').text('');
				}
				
				if ($('#name2').val().length < 1) {
					$('#txt_name2').text('Name is empty');
				}else{
					$('#txt_name2').text('');
				}
				
				if ($('#gender2').val().length < 1) {
					$('#txt_gender2').text('Gender is empty');
				}else{
					$('#txt_gender2').text('');
				}
				
				if ($('#education2').val().length < 1) {
					$('#txt_education2').text('Education not selected');
				}else{
					$('#txt_education2').text('');
				}
				
				if ($('#dob2').val().length < 1) {
					$('#txt_dob2').text('Date of birth is empty');
				}else{
					$('#txt_dob2').text('');
				}
				
				if ($('#pob2').val().length < 1) {
					$('#txt_pob2').text('Place of birth is empty');
				}else{
					$('#txt_pob2').text('');
				}
				
				if ($('#phone12').val().length < 1) {
					$('#txt_phone12').text('Phone number is empty');
				}else{
					$('#txt_phone12').text('');
				}
				
				if ($('#phone22').val().length < 1) {
					$('#txt_phone22').text('Phone number is empty');
				}else{
					$('#txt_phone22').text('');
				}
				
				if ($('#company2').val().length < 1) {
					$('#txt_company2').text('Company is empty');
				}else{
					$('#txt_company2').text('');
				}
				
				if ($('#company_address2').val().length < 1) {
					$('#txt_company_address2').text('Company address is empty');
				}else{
					$('#txt_company_address2').text('');
				}
				
				if ($('#job_position').val().length < 1) {
					$('#txt_job_position2').text('Position is empty');
				}else{
					$('#txt_job_position2').text('');
				}
				
				if ($('#city2').val().length < 1) {
					$('#txt_city2').text('City is empty');
				}else{
					$('#txt_city2').text('');
				}
				
				if(
					$('#status2').val().length > 0 &&
					$('#name2').val().length > 0 &&
					$('#gender2').val().length > 0 &&
					$('#education2').val().length > 0 &&
					$('#dob2').val().length > 0 &&
					$('#pob2').val().length > 0 &&
					$('#phone12').val().length > 0 &&
					$('#phone22').val().length > 0 &&
					$('#company2').val().length > 0 &&
					$('#company_address2').val().length > 0 &&
					$('#job_position').val().length > 0 &&
					$('#city2').val().length > 0 
				)
				{
					$("#kirimeditfamily").click();	
				}
			
			});
			
		});
	</script>
	
	<!--add work experience-->
	<script type="text/javascript">
     $(function(){
		 
			$('#name_company').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#name_company').val().length > 0){
                    $('#name_company').val($('#name_company').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#name_company', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 40));
            });

            $('#position').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#position').val().length > 0){
                    $('#position').val($('#position').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#position', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 40));
            });

            $('#reason_resignation').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#reason_resignation').val().length > 0){
                    $('#reason_resignation').val($('#reason_resignation').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#reason_resignation', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 90));
            });

           $('#salary').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if ($('#salary').val().length == 11)
                    e.preventDefault();
          });
          
              $(document).on('paste', '#salary', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 11);
                  $(this).val(isi.replace(/\D/g, ''));
           });

			 $('#salary').keyup(function() {
				$('#salary').val(formatRupiah($('#salary').val(), ''));
		  });
		  
		  function formatRupiah(angka, prefix){
			  var number_string = angka.replace(/[^,\d]/g, '').toString(),
			  split       = number_string.split(','),
			  sisa        = split[0].length % 3,
			  rupiah        = split[0].substr(0, sisa),
			  ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
		 
			  if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			  }
		 
			  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			  return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
			}


        $("#sendwork").click(function(){
                
                if ($('#name_company').val().length < 1) {
                    $('#txt_name_company').text('Name company is empty');
                }else{
                    $('#txt_name_company').text('');
                }
                
                if ($('#periode_start_work').val().length < 1) {
                    $('#txt_periode_start_work').text('Start is empty');
                }else{
                    $('#txt_periode_start_work').text('');
                }
                
                if ($('#periode_end_work').val().length < 1) {
                    $('#txt_periode_end_work').text('End is empty');
                }else{
                    $('#txt_periode_end_work').text('');
                }
                
                if ($('#position_work').val().length < 1) {
                    $('#txt_position_work').text('Position is empty');
                }else{
                    $('#txt_position_work').text('');
                }
                
                if ($('#reason_resignation').val().length < 1) {
                    $('#txt_reason_resignation').text('Reason resignation is empty');
                }else{
                    $('#txt_reason_resignation').text('');
                }
                
                if ($('#salary').val().length < 1) {
                    $('#txt_salary').text('Salary is empty');
                }else if ($('#salary').val().length > 0 && $('#salary').val() == '0') {
                    $('#txt_salary').text('Salaray not entered');
                }else{
                    $('#txt_salary').text('');
                }
                
                if(
                    $('#name_company').val().length > 0 &&
                    $('#periode_start_work').val().length > 0 &&
                    $('#periode_end_work').val().length > 0 &&
                    $('#position_work').val().length > 0 &&
                    $('#reason_resignation').val().length > 0 &&
                    ($('#salary').val().length > 0 && $('#salary').val() != '0')
                )
                {
                    $("#kirimwork").click();  
                }
            
            });
		 
	});
    </script>
	
	<!--edit work experience-->
    <script type="text/javascript">
     $(function(){
         
            $('#name_company_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#name_company_edit').val().length > 0){
                    $('#name_company_edit').val($('#name_company_edit').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#name_company_edit', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 40));
            });

            $('#position_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#position_edit').val().length > 0){
                    $('#position_edit').val($('#position_edit').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#position_edit', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 40));
            });

            $('#reason_resignation_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#reason_resignation_edit').val().length > 0){
                    $('#reason_resignation_edit').val($('#reason_resignation_edit').val().replace(/\s{2,}/g,' '));
                }
            });
              
            $(document).on('paste', '#reason_resignation_edit', function(e) {
                e.preventDefault();
                var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                var text = $(this).val()+withoutSpaces;
                $(this).val(text.substring(0, 90));
            });

           $('#salary_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if ($('#salary_edit').val().length == 11)
                    e.preventDefault();
          });
          
              $(document).on('paste', '#salary_edit', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 11);
                  $(this).val(isi.replace(/\D/g, ''));
           });

             $('#salary_edit').keyup(function() {
                $('#salary_edit').val(formatRupiah($('#salary_edit').val(), ''));
          });
          
          function formatRupiah(angka, prefix){
              var number_string = angka.replace(/[^,\d]/g, '').toString(),
              split       = number_string.split(','),
              sisa        = split[0].length % 3,
              rupiah        = split[0].substr(0, sisa),
              ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
         
              if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
              }
         
              rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
              return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
            }


        $("#sendeditwork").click(function(){
                
                if ($('#name_company_edit').val().length < 1) {
                    $('#txt_name_company_edit').text('Name company is empty');
                }else{
                    $('#txt_name_company_edit').text('');
                }
                
                if ($('#periode_start_work_edit').val().length < 1) {
                    $('#txt_periode_start_work_edit').text('Start is empty');
                }else{
                    $('#txt_periode_start_work_edit').text('');
                }
                
                if ($('#periode_end_work_edit').val().length < 1) {
                    $('#txt_periode_end_work_edit').text('End is empty');
                }else{
                    $('#txt_periode_end_work_edit').text('');
                }
                
                if ($('#position_edit').val().length < 1) {
                    $('#txt_position_edit').text('Position is empty');
                }else{
                    $('#txt_position_edit').text('');
                }
                
                if ($('#reason_resignation_edit').val().length < 1) {
                    $('#txt_reason_resignation_edit').text('Reason resignation is empty');
                }else{
                    $('#txt_reason_resignation_edit').text('');
                }
                
                if ($('#salary_edit').val().length < 1) {
                    $('#txt_salary_edit').text('Salary is empty');
                }else if ($('#salary_edit').val().length > 0 && $('#salary_edit').val() == '0') {
                    $('#txt_salary_edit').text('Salaray not entered');
                }else{
                    $('#txt_salary_edit').text('');
                }
                
                if(
                    $('#name_company_edit').val().length > 0 &&
                    $('#periode_start_work_edit').val().length > 0 &&
                    $('#periode_end_work_edit').val().length > 0 &&
                    $('#position_edit').val().length > 0 &&
                    $('#reason_resignation_edit').val().length > 0 &&
                    ($('#salary_edit').val().length > 0 && $('#salary').val() != '0')
                )
                {
                    $("#kirimeditwork").click();  
                }
            
            });
         
    });
    </script>
	
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
	 
	 setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>
	
	
	<script>
  var loadFile = function(event) {
    var output = document.getElementById('profile-img-tag');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
	
	<!--add image-->
	<script type="text/javascript">
		$(function(){
			$("#sendimage").click(function(){
				if($('#image_type').val().length < 1) {
					$('#txt_image_type').text('Image type not selected');
				}else{
					$('#txt_image_type').text('');
				}
								
				$("#profile-img").change(function(){
					var fileExtension = ['jpeg', 'jpg', 'png'];
					if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
						$("#profile-img-tag").val('');
						$('#txt_profile-img').text("Only formats are allowed : jpeg, jpg, png");
					}else{
						$('#txt_profile-img').text('');
						var loadFile = function(event) {
							var output = document.getElementById('output');
							output.src = URL.createObjectURL(event.target.files[0]);
						};
					}
				});
								
				if ($('#profile-img').val().length < 1) {
					$('#txt_profile-img').text('Image is empty');
				}
								
				var fileExtension = ['jpeg', 'jpg', 'png'];
					if(
						$('#image_type').val().length > 0 &&
						($('#profile-img').val() != '' && $.inArray($('#profile-img').val().split('.').pop().toLowerCase(), fileExtension) != -1 ) 
					){
						$("#kirimimage").click();  
					}
							
			});
		});
	</script>
	
	
	<!--add document-->
	<script type="text/javascript">
		$(function(){
			$("#senddocument").click(function(){
				if($('#document_type').val().length < 1) {
					$('#txt_document_type').text('Document type not selected');
				}else{
					$('#txt_document_type').text('');
				}
								
				$("#document").change(function(){
					var fileExtension = ['pdf', 'doc', 'docx', 'xlx', 'xlsx', 'csv'];
					if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
						$('#txt_document').text("Only formats are allowed : pdf, doc, docx, xlx, xlsx dan csv");
					}else{
						$('#txt_document').text('');
					}
				});
								
				if ($('#document').val().length < 1) {
					$('#txt_document').text('Image is empty');
				}
								
				var fileExtension = ['pdf', 'doc', 'docx', 'xlx', 'xlsx', 'csv'];
					if(
						$('#document_type').val().length > 0 &&
						($('#document').val() != '' && $.inArray($('#document').val().split('.').pop().toLowerCase(), fileExtension) != -1 ) 
					){
						$("#kirimdocument").click();  
					}
							
			});
		});
	</script>
	
	<script type="text/javascript">
     $(function(){
		$("#bs_datepicker_range_container").datepicker({
			format: 'dd-mm-yyyy',
			minDate:0,
			autoclose: true,
			todayHighlight: false,
		});
		
		$(".dateswork").datepicker({
			format: 'dd-mm-yyyy',
			minDate:0,
			autoclose: true,
			todayHighlight: false,
		});
			
	});
    </script>
		
    </section>
    

@endsection
