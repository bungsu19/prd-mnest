@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
<br>
<section class="content">
    <!-- Title Project -->
       <!--  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
            <div class="demo-google-material-icon"> <i class="material-icons">gavel </i> <span class="icon-name">ADD Product</span> </div>
          
        </div> -->
    <!-- End Title Project -->
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -50px;">
			@if(session()->has('message'))
				<div class="alert bg-green alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ session()->get('message') }}
				</div>
			@endif
			
			@if(session()->has('failed'))
				<div class="alert bg-red alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					{{ session()->get('failed') }}
				</div>
			@endif
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow"><b>Edit People</b></font>
                        </div>

                    <!-- form add product -->
                     <form class="form-signin" method="POST" action="{{url('/edit-data-people') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-line">
                                             <select name="branch" class="form-control" id="branch">
                                                @foreach($branch as $br)
                                                    <option value="{{ $br->id }}" @if($people->branch_id === $br->id) selected='selected' @endif> {{ $br->branch_name }}</option>
                                                @endforeach
                                                
                                            </select>
											<span class="text-error" id="txt_branch"></span>
                                        </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Name <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="first_name" class="form-control" placeholder="Input First Name" value="{{ $people->first_name }}" id="first_name" maxlength="20">
                                               </div>
											   <span class="text-error" id="txt_first_name"></span>
                                            </div>
                                           
                                        </div>
                                        <div class="col-sm-3">
                                             <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="middle_name" class="form-control" placeholder=" Input Midle Name" value="{{ $people->middle_name }}" id="middle_name" maxlength="20">
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div class="col-sm-2">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="surname" class="form-control" placeholder="Input Surname" value="{{ $people->surname }}" id="surname" maxlength="20">
                                             </div>
                                         </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Nick Name <span class="mandatory">*</span></label>
                                         
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="nick_name" class="form-control" placeholder="Nick Name" value="{{ $people->nick_name }}" id="nick_name" maxlength="30">
                                            </div>
											<span class="text-error" id="txt_nick_name"></span>
                                        </div>
                                        </div> 
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>NIK <span class="mandatory">*</span></label>
                                         
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                  <input type="text" name="nik" class="form-control" placeholder="Input NIK KTP" value="{{ $people->nik_ktp }}" id="nik" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="20">
                                              </div>
											  <span class="text-error" id="txt_nik"></span>
                                          </div>
                                        </div>

                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Address <span class="mandatory">*</span></label>
                                         
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <textarea name="address1" class="form-control" placeholder="Input Address" id="address" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200">{{ $people->ktp_address }}</textarea>
                                            </div>
											<span class="text-error" id="txt_address"></span>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                         <div class="col-sm-4">
                                            <label>Province <span class="mandatory">*</span></label>
                                            <select name="ktp_province" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province">
                                                <option>{{ $people->ktp_province }}</option>
                                                @foreach($province as $result)
                                                <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                                @endforeach
                                            </select>
											<span class="text-error" id="txt_province"></span>
                                        </div>
                                         <div class="col-sm-4">
                                            <label>District <span class="mandatory">*</span></label>
                                            <div class="regency">
                                            <select name="ktp_district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="district"> 
                                                        <option>{{ $people->ktp_district }}</option>
                                                    </select>
													<span class="text-error" id="txt_district"></span>
                                            </div>

                                         </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                          <div class="col-sm-4">
                                            <label>Sub District <span class="mandatory">*</span></label>
                                            <div class="subdistrict">
                                                <select name="ktp_subdistrict" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="subdistrict">
                                                        <option>{{ $people->ktp_subdistrict }}</option>
                                                </select>
												<span class="text-error" id="txt_subdistrict"></span>
                                            </div>
                                          </div>
                                          <div class="col-sm-4">
                                            <label>Village <span class="mandatory">*</span></label>
                                             <div class="village">
                                                <select name="ktp_village" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="village">
                                                    <option>{{ $people->ktp_village }}</option>
                                                </select>
												<span class="text-error" id="txt_village"></span>
                                            </div>

                                           </div>
                                    </div>

                                   

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                        <div class="col-sm-2">
                                            <label>RT <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                   <input type="text" name="ktp_rt" class="form-control" placeholder="RT" value="{{ $people->ktp_rt }}" id="rt" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                               </div>
											   <span class="text-error" id="txt_rt"></span>
                                           </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>RW <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="ktp_rw" value="{{ $people->ktp_rw }}" class="form-control" placeholder="RW" id="rw" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                             </div>
											 <span class="text-error" id="txt_rw"></span>
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Post Code <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <div class="postcode">
                                                   <input type="text" name="ktp_post_code" class="form-control" value="{{ $people->ktp_post_code }}" id="myInput" data-live-search="true" placeholder="Post Code" id="myInput" data-live-search="true" placeholder="Post Code" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="7">
                                               </div>
                                           </div>
										   <span class="text-error" id="txt_myInput"></span>
                                             </div>
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Home address is different from ID card? </label>
                                            
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
                                                <label class="custom-control-label filterbtn" for="defaultUnchecked"></label>
                                            </div>
                                           
                                        </div>
                                    </div>

                                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="formfilter" style="margin-top: 10px;display: none;">
                                                <div class="card" style="margin: 0px;">
                                                    
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-sm-3">
                                                                <label>Home Address</label>
                                                            </div>
                                                            <div class="col-sm-8">
                                                                <div class="form-group">
                                                                  <div class="form-line">
                                                                     <textarea name="address2" class="form-control" placeholder="Input Home Address" id="address2" style="height: 80px; max-height: 80px; min-height: 80px; max-width: 100%; min-width: 100%;" maxlength="200"> {{ $people->dom_address }}</textarea>
                                                                 </div>
                                                             </div>
                                                            </div>
                                                        </div>
                                                         <div class="row clearfix">
                                                            <div class="col-sm-3">
                                                                <label></label>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <label>Province</label>
                                                                <select name="dom_province" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province2">
                                                                    <option value=" ">{{ $people->dom_province }}</option>
                                                                    @foreach($province as $result)
                                                                    <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="col-sm-4">
                                                            <label>District</label>
                                                                <div class="regency2">
                                                                 <select name="dom_district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                                                            <option value=" ">{{ $people->dom_district }}</option>
                                                                        </select>
                                                                </div>
                                                            </div>
                                                         </div>
                                                    <div class="row clearfix">
                                                        <div class="col-sm-3">
                                                                <label></label>
                                                         </div>
                                                          <div class="col-sm-4">
                                                            <label>Sub District</label>
                                                            <div class="subdistrict2">
                                                                <select name="dom_subdistrict" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                                                        <option value=" ">{{ $people->dom_subdistrict }}</option>
                                                                </select>
                                                            </div>
                                                          </div>
                                                          <div class="col-sm-4">
                                                            <label>Village</label>
                                                             <div class="village2">
                                                                <select name="dom_village" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true">
                                                                    <option value=" ">{{ $people->dom_village }}</option>
                                                                </select>
                                                            </div>

                                                           </div>
                                                    </div>
                                                             
                                                        

                                                        <div class="row clearfix">
                                                             <div class="col-sm-3">
                                                                <label></label>
                                                             
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <label>RT</label>
                                                                <div class="form-group">
                                                                  <div class="form-line">
                                                                   <input type="text" name="dom_rt" class="form-control" value="{{ $people->dom_rt }}" placeholder="RT" id="dom_rt" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                                               </div>
                                                           </div>
                                                            </div>
                                                             <div class="col-sm-2">
                                                                <label>RW</label>
                                                                <div class="form-group">
                                                                 <div class="form-line">
                                                                   <input type="text" name="dom_rw" class="form-control" value="{{ $people->dom_rw }}" placeholder="RW" id="dom_rw" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                                                   </div>
                                                               </div>
                                                            </div>
                                                             <div class="col-sm-4">
                                                                <label>Post Code</label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                   <input type="text" name="dom_post_code" class="form-control" value="{{ $people->dom_post_code }}" placeholder="Post Code" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="7">
                                                                   </div>
                                                               </div>
                                                            </div>
                                                        </div>
                                                        

                                                       
                                                    </div>
                                                </div>
                                            </div>
                                   
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label> Date of birth <span class="mandatory">*</span></label>
                                           
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="input-group date" id="bs_datepicker_component_container">
                                                <div class="form-line">
                                                    <input type="text" name="dob" value="<?php echo date('d-m-Y', strtotime($people->dob2)); ?>" class="form-control datepicker" placeholder="Date Of Brith..." id="dob">
                                                </div>
												<span class="text-error" id="txt_dob"></span>
                                                <span class="input-group-addon">
                                                    <i class="material-icons">date_range</i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> Place of birth <span class="mandatory">*</span></label>
                                            
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                              <div class="form-line">
                                                <input type="text" name="pob" value="{{ $people->pob }}" class="form-control" placeholder="Place of birth" id="pob" maxlength="50">
                                            </div>
											<span class="text-error" id="txt_pob"></span>
                                        </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Gender <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                         <div class="col-sm-8">
                                            <select name="gender" class="form-control" id="gender">
                                                <option>{{ $people->gender }}</option>
                                                <option>Male</option>
                                                <option>Female</option>
                                                
                                            </select>
											<span class="text-error" id="txt_gender"></span>
                                         </div>
                                    </div>
									<br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Religion <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select name="religion" class="form-control" id="religion">
                                                @foreach($religion as $reg)
                                                    <option>{{ $reg->name_religion }}</option>
                                                @endforeach
                                            </select>
											<span class="text-error" id="txt_religion"></span>
                                        </div>
                                    </div>
									<br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Marital Status <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                         <div class="col-sm-8">
                                            <select name="marital" class="form-control" id="marital">
                                                <option value="{{ $people->matrial_status }}">{{ $people->matrial_status }}</option>
                                                <option>Married</option>
                                                <option>Single</option>
                                                <option>Widow</option>
                                                <option>Widower</option>
                                                
                                            </select>
											<span class="text-error" id="txt_marital"></span>
                                         </div>
                                    </div>
									<br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label> Nationality <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="nationality" class="form-control" placeholder="Input Nationality" value="{{ $people->nationality }}" id="nationality" maxlength="50">
                                                </div>
												<span class="text-error" id="txt_nationality"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Email <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                             <input type="text" name="email" class="form-control" value="{{ $people->email_address }}" placeholder="Email Address" id="email" maxlength="50">
                                         </div>
										 <span class="text-error" id="txt_email"></span>
                                     </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Phone Number 1 <span class="mandatory">*</span></label>
                                            
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                  <input type="number" name="phone1"  value="{{ $people->phone1 }}" class="form-control" placeholder="Phone Number" id="phone1" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                              </div>
											  <span class="text-error" id="txt_phone1"></span>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> Phone Number 2</label>
                                             
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="number" name="phone2" value="{{ $people->phone2 }}" class="form-control" placeholder="Phone Number" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                          <label>Facebook <span class="mandatory">*</span></label>
                                           
                                           
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="sosmed1" value="{{ $people->sosmed1 }}" class="form-control" placeholder="Input Facebook Akun" id="sosmed1" maxlength="50">
                                                 </div>
                                             </div>
                                         </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                          <label>Twitter <span class="mandatory">*</span></label>
                                             
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="sosmed2" value="{{ $people->sosmed2 }}" class="form-control" placeholder="Input Twitter Akun" id="sosmed2" maxlength="50">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>NPWP <span class="mandatory">*</span></label>
                                           
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="npwp_number" class="form-control" placeholder=" Inpu Number NPWP" value="{{ $people->npwp_number }}" id="npwp" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="20">
                                                 </div>
												 <span class="text-error" id="txt_npwp"></span>
                                             </div>
                                             <input type="hidden" name="npwp_validity" class="form-control" value="NULL" required>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>SIM A</label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="sim_a_number" class="form-control" placeholder="Input Number SIM A" value="{{ $people->sim_a_number }}" id="sim_a_number" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                                </div>
												<span class="text-error" id="txt_sim_a_number"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Validity SIM A</label>
                                        </div>
                                        <div class="col-sm-8">
                                            
                                            <select name="sim_a_validity" class="form-control" id="sim_a_validity">
                                                    <option value="{{ $people->sim_a_validity }}">{{ $people->sim_a_validity }}</option>
                                                    <?php
                                                    $thn_skr = date('Y');
                                                    for ($x = $thn_skr; $x >= 2010; $x--) {
                                                    ?>
                                                        <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
												<span class="text-error" id="txt_sim_a_validity"></span>

                                           
                                        </div>
                                    </div>
									<br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>SIM C</label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="sim_c_number" value="{{ $people->sim_c_number }}" class="form-control" placeholder="Input Number SIM C" id="sim_c_number" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                                 </div>
												 <span class="text-error" id="txt_sim_c_number"></span>
                                             </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                           <label>Validity SIM C</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select name="sim_c_validity" class="form-control" id="sim_c_validity">
                                                    <option value="{{ $people->sim_c_validity }}">{{ $people->sim_c_validity }}</option> 
                                                    <?php
                                                    $thn_skr = date('Y');
                                                    for ($x = $thn_skr; $x >= 2010; $x--) {
                                                    ?>
                                                        <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
												<span class="text-error" id="txt_sim_c_validity"></span>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>No BPJS Health</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" name="health" class="form-control" placeholder="No BPJS Health" id="health" value="{{ $people->no_bpjs_kesehatan }}" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                                </div>
                                                <span class="text-error" id="txt_health"></span>
                                            </div>
                                        </div>
                                    </div>
									<br>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Last Education <span class="mandatory">*</span></label>
                                             
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                <select name="last_education" class="form-control" id="last_education">
                                                    <option value="{{ $people->last_education }}">{{ $people->last_education }}</option>
                                                    <option>SD</option>
                                                    <option>SMP</option>
                                                    <option>SMA/SMK</option>
                                                    <option>D1</option>
                                                    <option>D2</option>
                                                    <option>D3</option>
                                                    <option>D4</option>
                                                    <option>S1</option>
                                                    <option>S2</option>
                                                    <option>S3</option>
                                                </select>
												<span class="text-error" id="txt_last_education"></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                          <label></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Body Weight <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="body_weight" class="form-control" placeholder="Input Body Weight" value="{{ $people->body_weight }}" id="body_weight" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                                </div>
												<span class="text-error" id="txt_body_weight"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>Body Height <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="body_height" value="{{ $people->body_height }}" class="form-control" placeholder="Input Body Height" id="body_height" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                                 </div>
												 <span class="text-error" id="txt_body_height"></span>
                                             </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>Blood Type <span class="mandatory">*</span></label>
                                            <select name="blood_type" class="form-control" id="blood_type">
                                                <option value="A" {{ ( $people->blood_type == 'A') ? 'selected' : '' }}>A</option>
                                                <option value="B" {{ ( $people->blood_type == 'B') ? 'selected' : '' }}>B</option>
                                                <option value="O" {{ ( $people->blood_type == 'O') ? 'selected' : '' }}>O</option>
                                            </select>
											<span class="text-error" id="txt_blood_type"></span>
                                         </div>

                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                          <label>Facility Info</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Expected Salary <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="expected_salary" value="{{ $people->expected_salary }}" class="form-control" placeholder="Expected Salary" id="expected_salary" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="11">
                                                </div>
												<span class="text-error" id="txt_expected_salary"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Allowance <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="allowance" value="{{ $people->allowance }}" class="form-control" placeholder="Allowance" id="allowance" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="20">
                                                </div>
												<span class="text-error" id="txt_allowance"></span>
                                            </div>
                                        </div>

                                    </div>
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                          <label></label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Facility <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
                                                <input type="text" name="facility" value="{{ $people->facility }}" class="form-control" placeholder="Facility" id="facility" maxlength="50">
                                                </div>
												<span class="text-error" id="txt_facility"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Insurance <span class="mandatory">*</span></label>
                                             <div class="form-group">
                                               <div class="form-line">
													<select name="insurance" class="form-control" id="insurance">
														<option value=" ">{{ $people->insurance }}</option>
														<option>BPJS KESEHATAN</option>
														<option>BPJS KETENAGAKERJAAN</option>
													</select>
												</div>
												<span class="text-error" id="txt_insurance"></span>
											</div>
										</div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Entrant photograph<span class="mandatory">*</span> </label>
                                        </div>
                                        <div class="col-sm-8">
                                             <input type="file" name="image" class="form-control"  id="image" onchange="loadFile(event)" >
											 <span class="text-error" id="txt_output"></span>
                                             <p>
                                             <img src="{{ asset('/employe')}}/{{ $people->image }}" id="output" width="200px" />
                                             <input type="hidden" name="id" value="{{ $people->id }}">
                                        </div>
                                           
                                    </div>
									<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;"></div>
									<div class="row clearfix">
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
												<div class="col-sm-3">
													<a href="{{url('/data-people/add-info')}}/{{ $people->id }}" class="btn bg-gray waves-effect" ><h4>Back</h4></a>
												</div>
												<div class="col-sm-1"></div>
												<div class="col-sm-3">
													<button href="#" type="button" class="btn bg-blue waves-effect" id="send"><h4>Update</h4></button>
                                                <button href="#" type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim"><h4>Update</h4></button>
												</div>	
											</div>
										</div>
                                    
                                </div>
                                    
                            </div>
                                    
                         </div>
                                    
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>    
    <script src="{{ asset('js-master/people.js') }}"></script>     
    <script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>

       @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 1){
                $('#formfilter').show(1200);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>
 <script type="text/javascript">
        $('#province').change(function(){
			
			$('.regency select').find('option').remove().end().append('<option value="">- Select Regency/City -</option>').val('');
			$('.subdistrict select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
			
			$('.subdistrict select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
			
			$('.village select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>

    <script type="text/javascript">
        $('#province2').change(function(){
			
			$('.regency2 select').find('option').remove().end().append('<option value="">- Select Regency/City -</option>').val('');
			$('.subdistrict2 select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village2 select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency2 select').html(obj.data);
                        $('.regency2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency2 select').change(function(){
			
			$('.subdistrict2 select').find('option').remove().end().append('<option value="">- Select Subdistrict -</option>').val('');
			$('.village2 select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict2 select').html(obj.data);
                        $('.subdistrict2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict2 select').change(function(){
			
			$('.village2 select').find('option').remove().end().append('<option value="">- Select Village -</option>').val('');
			
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village2 select').html(obj.data);
                        $('.village2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village2 select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
        autocomplete(document.getElementById("myInput"), countries);
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
     });
    </script>
	
	<script type="text/javascript">
     $(function(){
	  
	   //first name cannot long space on input
	  $('#first_name').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#first_name').val().length > 0){
				$('#first_name').val($('#first_name').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on first name*/
		  $(document).on('paste', '#first_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 20));
		  });
		  
	//middle_name cannot long space on input
	  $('#middle_name').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#middle_name').val().length > 0){
				$('#middle_name').val($('#middle_name').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on first name*/
		  $(document).on('paste', '#middle_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 20));
		  });
	  
	  //surname cannot long space on input
	  $('#surname').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#surname').val().length > 0){
				$('#surname').val($('#surname').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on surname*/
		  $(document).on('paste', '#surname', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 20));
		  });
	  
	  //nick_name cannot long space on input
	  $('#nick_name').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#nick_name').val().length > 0){
				$('#nick_name').val($('#nick_name').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on first name*/
		  $(document).on('paste', '#nick_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 30));
		  });
	  
	  //nik maximum length 20
	  $('#nik').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#nik').val().length == 20)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on nik*/
		  $(document).on('paste', '#nik', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //address cannot long space on input
	  $('#address').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#address').val().length > 0){
				$('#address').val($('#address').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on address 1*/
		  $(document).on('paste', '#address', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 200));
	   });
	   
	   //rt maximum length 3
	   $('#rt').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#rt').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on rt*/
		  $(document).on('paste', '#rt', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //rw maximum length 3
	  $('#rw').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#rw').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on rw*/
		  $(document).on('paste', '#rw', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //post code maximum length 7
	  $('#myInput').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart; 
			if ($('#myInput').val().length == 7)
				e.preventDefault();
      });
	  
	   /*handle paste with extra space on rw*/
		  $(document).on('paste', '#myInput', function(e) {
			  e.preventDefault(); 
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 7);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //address 2 cannot long space on input
	  $('#address2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#address2').val().length > 0){
				$('#address2').val($('#address2').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on address 2*/
		  $(document).on('paste', '#address2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 200));
	   });
	   
	   //dom rt maximum length 3
	  $('#dom_rt').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#dom_rt').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on dom rt*/
		  $(document).on('paste', '#dom_rt', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //dom rw maximum length 3
	  $('#dom_rw').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#dom_rw').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on dom rw*/
		  $(document).on('paste', '#dom_rw', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //dom post code maximum length 7
	  $('#dom_post_code').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#dom_post_code').val().length == 7)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on dom post code*/
		  $(document).on('paste', '#dom_post_code', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 7);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	   //phone 1 maximum length 15
	   $('#phone1').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#phone1').val().length == 15)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on phone 1*/
		  $(document).on('paste', '#phone1', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	   //phone 2 maximum length 15
	   $('#phone2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#phone2').val().length == 15)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on phone 2*/
		  $(document).on('paste', '#phone2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //sosmed1 cannot long space on input
	  $('#sosmed1').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#sosmed1').val().length > 0){
				$('#sosmed1').val($('#sosmed1').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on sosmed1*/
		  $(document).on('paste', '#sosmed1', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
	  
	  //sosmed2 cannot long space on input
	  $('#sosmed2').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#sosmed2').val().length > 0){
				$('#sosmed2').val($('#sosmed2').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on sosmed2*/
		  $(document).on('paste', '#sosmed2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
	  
	  //nationality cannot long space on input
	  $('#nationality').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#nationality').val().length > 0){
				$('#nationality').val($('#nationality').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on nationality*/
		  $(document).on('paste', '#nationality', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
	  
	 //email cannot long space on input
	   $('#email').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on email*/
		  $(document).on('paste', '#email', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
	  
	  function validateEmail($email) {
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  return emailReg.test( $email );
		}
		
	  //place of birthday cannot long space on input
	  $('#pob').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#pob').val().length > 0){
				$('#pob').val($('#pob').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on place of birthday*/
		  $(document).on('paste', '#pob', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
		
	  //npwp maximum length 20
	   $('#npwp').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#npwp').val().length == 20)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on npwp*/
		  $(document).on('paste', '#npwp', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //sim_a_number maximum length 15
	   $('#sim_a_number').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#sim_a_number').val().length == 15)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on sim_a_number*/
		  $(document).on('paste', '#sim_a_number', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //sim_c_number maximum length 15
	   $('#sim_c_number').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#sim_c_number').val().length == 15)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on sim_c_number*/
		  $(document).on('paste', '#sim_c_number', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	   //body_weight maximum length 3
	   $('#body_weight').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#body_weight').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on body_weight*/
		  $(document).on('paste', '#body_weight', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	   //body_height maximum length 3
	   $('#body_height').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#body_height').val().length == 3)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on body_height*/
		  $(document).on('paste', '#body_height', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	   
	   //expected_salary maximum length 3
	   $('#expected_salary').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#expected_salary').val().length == 11)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on expected_salary*/
		  $(document).on('paste', '#expected_salary', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 11);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	  //allowance maximum length 20
	   $('#allowance').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if ($('#allowance').val().length == 20)
				e.preventDefault();
      });
	  
	  /*handle paste with extra space on allowance*/
		  $(document).on('paste', '#allowance', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
	  
	   //facility cannot long space on input
	  $('#facility').keypress(function (e) {
			var startPos = e.currentTarget.selectionStart;
			if (e.which === 32 && startPos==0){
				e.preventDefault();
			}else if($('#facility').val().length > 0){
				$('#facility').val($('#facility').val().replace(/\s{2,}/g,' '));
			}
      });
	  
	  /*handle paste with extra space on facility*/
		  $(document).on('paste', '#facility', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
	   });
	  
	  $('#expected_salary').keyup(function() {
			$('#expected_salary').val(formatRupiah($('#expected_salary').val(), ''));
	  });
	  
	  
	  $("#image").change(function(){
            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$("#output").val();
                $('#txt_output').text("Only formats are allowed : jpeg, jpg, png");
            }else{
                $('#txt_output').text('');
                var loadFile = function(event) {
                    var output = document.getElementById('output');
                    output.src = URL.createObjectURL(event.target.files[0]);
                  };
            }
        });
	  
	  function formatRupiah(angka, prefix){
          var number_string = angka.replace(/[^,\d]/g, '').toString(),
          split       = number_string.split(','),
          sisa        = split[0].length % 3,
          rupiah        = split[0].substr(0, sisa),
          ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
     
          if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
          }
     
          rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
          return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
        }
	  
	  $("#send").click(function(){
		  if ($('#branch').val().length < 1) {
                $('#txt_branch').text('Branch not selected');
          }else{
                $('#txt_branch').text('');
          }
		  
		  if ($('#first_name').val().length < 1) {
                $('#txt_first_name').text('First name is empty');
          }else{
                $('#txt_first_name').text('');
          }
		  
		  if ($('#nick_name').val().length < 1) {
                $('#txt_nick_name').text('Nick name is empty');
          }else{
                $('#txt_nick_name').text('');
          }
		  
		  if ($('#nik').val().length < 1) {
                $('#txt_nik').text('Nik is empty');
          }else if ($('#nik').val() == 0) {
                $('#txt_nik').text('Nik not entered');
          }else{
                $('#txt_nik').text('');
          }
		  
		  if ($('#address').val().length < 1) {
                $('#txt_address').text('Address is empty');
          }else{
                $('#txt_address').text('');
          }
		  
		  if ($('#province').val().length < 1) {
                $('#txt_province').text('Province not selected');
          }else{
                $('#txt_province').text('');
          }
		  
		  if ($('#district').val().length < 1) {
                $('#txt_district').text('District not selected');
          }else{
                $('#txt_district').text('');
          }
		  
		  if ($('#subdistrict').val().length < 1) {
                $('#txt_subdistrict').text('Sub District not selected');
          }else{
                $('#txt_subdistrict').text('');
          }
		  
		  if ($('#village').val().length < 1) {
                $('#txt_village').text('Village not selected');
          }else{
                $('#txt_village').text('');
          }
		  
		  if ($('#rt').val().length < 1) {
                $('#txt_rt').text('Rt is empty');
          }else if ($('#rt').val() == 0) {
                $('#txt_rt').text('Rt not entered');
          }else{
                $('#txt_rt').text('');
          }
		  
		  if ($('#rw').val().length < 1) {
                $('#txt_rw').text('Rw is empty');
          }else if ($('#rw').val() == 0) {
                $('#txt_rw').text('Rw not entered');
          }else{
                $('#txt_rw').text('');
          }
		  
		  if ($('#myInput').val().length < 1) {
                $('#txt_myInput').text('Postal code is empty');
          }else if ($('#myInput').val() == 0) {
                $('#txt_myInput').text('Postal code not entered');
          }else{
                $('#txt_myInput').text('');
          }
		  
		  if ($('#dob').val().length < 1) {
                $('#txt_dob').text('Date of birth is empty');
          }else{
                $('#txt_dob').text('');
          }
		  
		  if ($('#pob').val().length < 1) {
                $('#txt_pob').text('Place of birth is empty');
          }else{
                $('#txt_pob').text('');
          }
		  
		  if ($('#gender').val().length < 1) {
                $('#txt_gender').text('Gender not selected');
          }else{
                $('#txt_gender').text('');
          }
		  
		  if ($('#religion').val().length < 1) {
                $('#txt_religion').text('Religion not selected');
          }else{
                $('#txt_religion').text('');
          }
		  
		  if ($('#marital').val().length < 1) {
                $('#txt_marital').text('Marital status not selected');
          }else{
                $('#txt_marital').text('');
          }
		  
		  if ($('#nationality').val().length < 1) {
                $('#txt_nationality').text('nationality is empty');
          }else{
                $('#txt_nationality').text('');
          }
		  
		  if ($('#email').val().length < 1) {
                $('#txt_email').text('Email is empty');
          }else if (!validateEmail($('#email').val())) {
                $('#txt_email').text('Email format is false');
          }else{
                $('#txt_email').text('');
          }
		  
		  if ($('#phone1').val().length < 1) {
                $('#txt_phone1').text('Phone number is empty');
          }else if ($('#phone1').val() == 0) {
                $('#txt_phone1').text('Phone number not entered');
          }else{
                $('#txt_phone1').text('');
          }
		  
		  if ($('#npwp').val().length < 1) {
                $('#txt_npwp').text('Npwp is empty');
          }else if ($('#npwp').val() == 0) {
                $('#txt_npwp').text('Npwp not entered');
          }else{
                $('#txt_npwp').text('');
          }
		  
		  if ($('#sim_a_number').val().length > 0 && $('#sim_a_number').val() != 0) {
                if ($('#sim_a_validity').val().length < 1) {
						$('#txt_sim_a_validity').text('Sim A validity not selected');
				}else{
						$('#txt_sim_a_validity').text('');
				}
				$('#txt_sim_a_number').text('');
          }else{
			  $('#txt_sim_a_validity').text('');
			  $('#sim_a_number').val('');
			  $('#txt_sim_a_number').text('');
		  }
		  
		  if ($('#sim_a_validity').val() != '' && $('#sim_a_number').val() == '') {
                $('#sim_a_validity').val('').change();
          }
		  
		  if ($('#sim_c_number').val().length > 0 && $('#sim_c_number').val() != 0) {
                if ($('#sim_c_validity').val().length < 1) {
						$('#txt_sim_c_validity').text('Sim C validity not selected');
				}else{
						$('#txt_sim_c_validity').text('');
				}
				$('#txt_sim_c_number').text('');
          }else{
			  $('#txt_sim_c_validity').text('');
			  $('#sim_c_number').val('');
			  $('#txt_sim_c_number').text('');
		  }
		  
		  if ($('#sim_c_validity').val() != '' && $('#sim_c_number').val() == '') {
                $('#sim_c_validity').val('').change();
          }
		  
		  if ($('#last_education').val().length < 1) {
                $('#txt_last_education').text('Last education is empty');
          }else{
                $('#txt_last_education').text('');
          }
		  
		  if ($('#body_weight').val().length < 1) {
                $('#txt_body_weight').text('Body weight is empty');
          }else if ($('#body_weight').val() == 0) {
                $('#txt_body_weight').text('Body weight not entered');
          }else{
                $('#txt_body_weight').text('');
          }
		  
		  if ($('#body_height').val().length < 1) {
                $('#txt_body_height').text('Body height is empty');
          }else if ($('#body_height').val() == 0) {
                $('#txt_body_height').text('Body height not entered');
          }else{
                $('#txt_body_height').text('');
          }
		  
		  if ($('#blood_type').val().length < 1) {
                $('#txt_blood_type').text('Blood type not selected');
          }else{
                $('#txt_blood_type').text('');
          }
		  
		  if ($('#expected_salary').val().length < 1) {
                $('#txt_expected_salary').text('Expected salary is empty');
          }else if ($('#expected_salary').val() == 0) {
                $('#txt_expected_salary').text('Expected salary not entered');
          }else{
                $('#txt_expected_salary').text('');
          }
		  
		  if ($('#allowance').val().length < 1) {
                $('#txt_allowance').text('Allowance is empty');
          }else if ($('#allowance').val() == 0) {
                $('#txt_allowance').text('Allowance not entered');
          }else{
                $('#txt_allowance').text('');
          }
		  
		  if ($('#facility').val().length < 1) {
                $('#txt_facility').text('Facility is empty');
          }else{
                $('#txt_facility').text('');
          }
		  
		  if ($('#insurance').val().length < 1) {
                $('#txt_insurance').text('Insurance not selected');
          }else{
                $('#txt_insurance').text('');
          }
		  
		  //if ($('#image').val().length < 1) {
			//		$('#txt_output').text('Image is empty');
		  //}
		  
		  var fileExtension = ['jpeg', 'jpg', 'png'];
		  if($('#branch').val().length > 0 && 
			 $('#first_name').val().length > 0 && 
			 $('#nick_name').val().length > 0 &&
			 ($('#nik').val().length > 0 && $('#nik').val() != '0') && 
			 $('#address').val().length > 0 && 
			 $('#province').val().length > 0 &&
			 $('#district').val().length > 0 && 
			 $('#subdistrict').val().length > 0 && 
			 $('#village').val().length > 0 && 
			 ($('#rt').val().length > 0 && $('#rt').val() != '0') && 
			 ($('#rw').val().length > 0 && $('#rw').val() != '0')&& 
			 ($('#myInput').val().length > 0 && $('#myInput').val() != '0')&&
			 $('#dob').val().length > 0 && 
			 $('#pob').val().length > 0 && 
			 $('#gender').val().length > 0 &&
			 $('#religion').val().length > 0 && 
			 $('#marital').val().length > 0 && 
			 $('#nationality').val().length > 0 &&
			 $('#email').val().length > 0 && 
			 ($('#phone1').val().length > 0 && $('#phone1').val() != '0') && 
			 ($('#npwp').val().length > 0 && $('#npwp').val() != '0') &&
			 $('#last_education').val().length > 0 && 
			 ($('#body_weight').val().length > 0 && $('#body_weight').val() != '0') && 
			 ($('#body_height').val().length > 0 && $('#body_height').val() != '0') && 
			 $('#blood_type').val().length > 0 &&
			 ($('#expected_salary').val().length > 0 && $('#expected_salary').val() != '0') && 
			 ($('#allowance').val().length > 0 && $('#allowance').val() != '0') && 
			 $('#facility').val().length > 0 &&
			 $('#insurance').val().length > 0 &&
			 ($('#image').val() == '' || ($('#image').val() != '' && $.inArray($('#image').val().split('.').pop().toLowerCase(), fileExtension) != -1))){
				 $("#kirim").click();	 
			 }
			 
	  });
	  
     });
    </script>
     <script type="text/javascript">
    $(document).ready(function() { 
      $(window).keydown(function(event){
        if(event.keyCode == 13) { 
          event.preventDefault();
          return false;
        }
      });
    }); 
    </script>  

</section>
  @endsection  

@endsection
