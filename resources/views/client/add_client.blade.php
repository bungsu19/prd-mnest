@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
			
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif	
			
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow"><b>New Client</b></font>
                        </div>

                    <!-- form add product -->
                     <form class="form-signin" method="POST" action="{{url('/client-data/add-client') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-line">
                                             <select name="branch" class="form-control" id="branch">
                                                <option value="">-select-</option>
                                                @foreach($branch as $br)
                                                    <option value="{{ $br->id }}"> {{ $br->branch_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
										<span class="text-error" id="txt_branch"></span>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Company Name <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_name" class="form-control" placeholder="Company Name" id="client_name" maxlength="50">
                                               </div>
											   <span class="text-error" id="txt_client_name"></span>
                                            </div>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Company Address <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <textarea name="client_addres1" class="form-control" placeholder="Company Address" id="client_addres1" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" maxlength="255"></textarea>
                                                 <input type="hidden" name="client_addres2" class="form-control" value="qwer">
                                               </div>
											   <span class="text-error" id="txt_client_addres1"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                         <div class="col-sm-4">
                                            <label>Province <span class="mandatory">*</span></label>
                                            <select name="client_province" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province">
                                                <option value="">- Province -</option>
                                                @foreach($province as $result)
                                                <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                                @endforeach
                                            </select>
											<span class="text-error" id="txt_province"></span>
                                        </div>
                                         <div class="col-sm-4">
                                            <label>District <span class="mandatory">*</span></label>
                                            <div class="regency">
                                            <select name="client_district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="client_district">
                                                        <option value="">-Regency/City -</option>
                                            </select>
											<span class="text-error" id="txt_client_district"></span>
                                            </div>

                                         </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                          <div class="col-sm-4">
                                            <label>Sub District <span class="mandatory">*</span></label>
                                            <div class="subdistrict">
                                                <select name="client_subdistrict" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="client_subdistrict">
                                                        <option value="">-Subdistrict -</option>
                                                </select>
												<span class="text-error" id="txt_client_subdistrict"></span>
                                            </div>
                                          </div>
                                          <div class="col-sm-4">
                                            <label>Village <span class="mandatory">*</span></label>
                                             <div class="village">
                                                <select name="client_village" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="village">
                                                    <option value="">-Village-</option>
                                                </select>
												<span class="text-error" id="txt_village"></span>
                                            </div>

                                           </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label></label>
                                         
                                        </div>
                                        <div class="col-sm-2">
                                            <label>RT <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">											
                                                   <input type="text" name="client_rt" class="form-control rt" placeholder="RT" id="rt" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                               </div>
											   <span class="text-error" id="txt_rt"></span>
                                           </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>RW <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_rw" class="form-control" placeholder="RW" id="rw" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="3">
                                             </div>
											 <span class="text-error" id="txt_rw"></span>
                                         </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Post Code <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <div class="postcode">
                                                   <input type="text" name="client_post_code" class="form-control" value="{{ old('postcode') }}" id="myInput" data-live-search="true" placeholder="Post Code" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="7">
                                               </div>
                                           </div>
										   <span class="text-error" id="txt_myInput"></span>
                                             </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Email Address <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="Email" name="client_email" class="form-control" placeholder="Email client" id="client_email" maxlength="50">
                                               </div>
											   <span class="text-error" id="txt_client_email"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Website <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_website" class="form-control" placeholder="https://example.com" id="client_website">
                                               </div>
											   <span class="text-error" id="txt_client_website"></span>
                                            </div>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 1 <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_phone1" class="form-control" placeholder="Phone Number" id="client_phone1" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                               </div>
											   <span class="text-error" id="txt_client_phone1"></span>
                                            </div>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 2</label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_phone2" class="form-control" placeholder="Phone Number" id="client_phone2" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                               </div>
											   <span class="text-error" id="txt_client_phone2"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contact Person <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="clinet_person" class="form-control" placeholder=" Contact Person" id="client_person" maxlength="50">
                                               </div>
											   <span class="text-error" id="txt_client_person"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contact Person Phone <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="clinet_person_phone" class="form-control" placeholder=" Contact Person" id="clinet_person_phone" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="15">
                                               </div>
											   <span class="text-error" id="txt_clinet_person_phone"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>NPWP <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_npwp" class="form-control" placeholder="NPWP" id="client_npwp" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="20">
                                               </div>
											   <span class="text-error" id="txt_client_npwp"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>FAX <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="client_fax" class="form-control" placeholder="Number Fax" id="fax" oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="20">
                                               </div>
											   <span class="text-error" id="txt_fax"></span>
                                            </div>
                                           
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Logo <span class="mandatory">*</span></label>
                                           
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="file" name="image" class="form-control" placeholder="Number Fax" id="image">
												 <img src="{{ asset('default.png') }}" id="profile-img-tag" width="200px" />
                                               </div>
											   <span class="text-error" id="txt_image"></span>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    
                                    
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('client-data')}}" class="btn bg-gray waves-effect" ><font size="4">Cancel</font></a>
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn bg-blue waves-effect" id="send"><font size="4">Save</font></button>
												<button href="#" type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                    
                            </div>
                                    
                         </div>
                                    
                            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
   <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
	
	setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>        
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
			
        }
        $("#image").change(function(){
			var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
			if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
				$('#txt_image').text("Only formats are allowed : jpeg, jpg, png, gif, bmp");
			}else{
				$('#txt_image').text('');
				readURL(this);
			}
        });
    </script>

       @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
</script>
 <script type="text/javascript">
        $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>

    <script type="text/javascript">
        $('#province2').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency2 select').html(obj.data);
                        $('.regency2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency2 select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict2 select').html(obj.data);
                        $('.subdistrict2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict2 select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village2 select').html(obj.data);
                        $('.village2 select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village2 select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
        autocomplete(document.getElementById("myInput"), countries);
    </script>
    <script type="text/javascript">
     $(function(){
      $(".datepicker").datepicker({
          format: 'dd-mm-yyyy',
          autoclose: true,
          todayHighlight: true,
      });
	  
		   $('#client_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#client_name').val().length > 0){
					$('#client_name').val($('#client_name').val().replace(/\s{2,}/g,' '));
				}
		  });

             //handel max rt
            $('#rt').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#rt').val().length == 3)
                e.preventDefault();
            });
			
			$(document).on('paste', '#rt', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
			});

            // handle max rw
            $('#rw').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#rw').val().length == 3)
                e.preventDefault();
            });
			
			$(document).on('paste', '#rw', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 3);
			  $(this).val(isi.replace(/\D/g, ''));
	   });

             // handle max post code
            $('#myInput').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#myInput').val().length == 7)
                e.preventDefault();
            });
			
			$(document).on('paste', '#myInput', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 7);
			  $(this).val(isi.replace(/\D/g, ''));
	   });

            // handle max post code
            $('#client_phone1').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#client_phone1').val().length == 15)
                e.preventDefault();
            });
			
			$(document).on('paste', '#client_phone1', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });

             // handle max post code
            $('#client_phone2').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#client_phone2').val().length == 15)
                e.preventDefault();
            });
			
			$(document).on('paste', '#client_phone2', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 15);
			  $(this).val(isi.replace(/\D/g, ''));
	   });

             // handle max post code
            $('#client_person').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#client_person').val().length == 20)
                e.preventDefault();
            });

             // handle max post code
            $('#client_npwp').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#client_npwp').val().length == 20)
                e.preventDefault();
            });
			
			$(document).on('paste', '#client_npwp', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });


            // handle max post code
            $('#clinet_person_phone').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#clinet_person_phone').val().length == 20)
                e.preventDefault();
            });
		  


		  /*handle paste with extra space on company name*/
		  $(document).on('paste', '#client_name', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  $('#client_addres1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#client_addres1').val().length > 0){
					$('#client_addres1').val($('#client_addres1').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space on client address*/
		  $(document).on('paste', '#client_addres1', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 255));
		  });
		  
		  $('#client_email').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  /*handle paste with extra space on client email*/
		  $(document).on('paste', '#client_email', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  $('#client_website').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  $('#client_phone1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  $('#client_phone2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  $('#client_person').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#client_person').val().length > 0){
					$('#client_person').val($('#client_person').val().replace(/\s{2,}/g,' '));
				}
		  });
		  
		  /*handle paste with extra space on client person*/
		  $(document).on('paste', '#client_person', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 50));
		  });
		  
		  $('#clinet_person_phone').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  $('#fax').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  $(document).on('paste', '#fax', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  var isi = text.substring(0, 20);
			  $(this).val(isi.replace(/\D/g, ''));
	   });
		  
		  function validateEmail($email) {
			  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			  return emailReg.test( $email );
			}
		  
		  function isUrlValid($url) {
			  var urlReg = /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
			  return urlReg.test( $url );
			}
	  
		  $("#send").click(function(){
			   if ($('#branch').val().length < 1) {
					$('#txt_branch').text('Branch not selected');
			  }else{
					$('#txt_branch').text('');
			  }
			  
			  if ($('#client_name').val().length < 1) {
					$('#txt_client_name').text('Client name is empty');
			  }else{
					$('#txt_client_name').text('');
			  }
			  
			  if ($('#client_addres1').val().length < 1) {
					$('#txt_client_addres1').text('Client address is empty');
			  }else{
					$('#txt_client_addres1').text('');
			  }
			  
			  if ($('#province').val().length < 1) {
					$('#txt_province').text('Province is empty');
			  }else{
					$('#txt_province').text('');
			  }
			  
			  if ($('#client_district').val().length < 1) {
					$('#txt_client_district').text('Client district not selected');
			  }else{
					$('#txt_client_district').text('');
			  }
			  
			  if ($('#client_subdistrict').val().length < 1) {
					$('#txt_client_subdistrict').text('Client subdistrict not selected');
			  }else{
					$('#txt_client_subdistrict').text('');
			  }
			  
			  if ($('#village').val().length < 1) {
					$('#txt_village').text('Village not selected');
			  }else{
					$('#txt_village').text('');
			  }
			  
			  if ($('#rt').val().length < 1) {
					$('#txt_rt').text('Rt is empty');
			  }else if ($('#rt').val() == 0) {
					$('#txt_rt').text('Rt not entered');
			  }else{
					$('#txt_rt').text('');
			  }
			  
			  if ($('#rw').val().length < 1) {
					$('#txt_rw').text('Rw is empty');
			  }else if ($('#rw').val() == 0) {
					$('#txt_rw').text('Rw not entered');
			  }else{
					$('#txt_rw').text('');
			  }
			  
			  if ($('#myInput').val().length < 1) {
					$('#txt_myInput').text('Postal code is empty');
			  }else if ($('#myInput').val() == 0) {
					$('#txt_myInput').text('Postal code not entered');
			  }else{
					$('#txt_myInput').text('');
			  }
			  
			  if ($('#client_email').val().length < 1) {
					$('#txt_client_email').text('Email is empty');
			  }else if (!validateEmail($('#client_email').val())) {
					$('#txt_client_email').text('Email format is false');
			  }else{
					$('#txt_client_email').text('');
			  }
			  
			  if ($('#client_website').val().length < 1) {
					$('#txt_client_website').text('Website is empty');
			  }else if (!isUrlValid($('#client_website').val())) {
					$('#txt_client_website').text('Website format is false');
			  }else{
					$('#txt_client_website').text('');
			  }
			  
			  if ($('#client_phone1').val().length < 1) {
					$('#txt_client_phone1').text('Phone number is empty');
			  }else if ($('#client_phone1').val() == 0) {
					$('#txt_client_phone1').text('Phone number not entered');
			  }else if ($('#client_phone1').val().length > 13) {
					$('#txt_client_phone1').text('Phone number maximum 13 character');
			  }else{
					$('#txt_client_phone1').text('');
			  }
			  
			  if ($('#client_phone2').val().length > 0 && $('#client_phone2').val() == 0) {
					$('#txt_client_phone2').text('Phone number not entered');
			  }else if ($('#client_phone2').val().length > 13) {
					$('#txt_client_phone2').text('Phone number maximum 13 character');
			  }else{
					$('#txt_client_phone2').text('');
			  }
			  
			  if ($('#client_person').val().length < 1) {
					$('#txt_client_person').text('Contact person is empty');
			  }else{
					$('#txt_client_person').text('');
			  }
			  
			  if ($('#clinet_person_phone').val().length < 1) {
					$('#txt_clinet_person_phone').text('Phone number is empty');
			  }else if ($('#clinet_person_phone').val() == 0) {
					$('#txt_clinet_person_phone').text('Phone number not entered');
			  } if ($('#clinet_person_phone').val().length > 13) {
					$('#txt_clinet_person_phone').text('Phone number maximum 13 character');
			  }else{
					$('#txt_clinet_person_phone').text('');
			  }
			  
			  if ($('#client_npwp').val().length < 1) {
					$('#txt_client_npwp').text('Npwp is empty');
			  }else if ($('#client_npwp').val() == 0) {
					$('#txt_client_npwp').text('Npwp not entered');
			  }else if ($('#client_npwp').val().length > 15) {
					$('#txt_client_npwp').text('Npwp maximum 15 character');
			  }else{
					$('#txt_client_npwp').text('');
			  }
			  
			  if ($('#fax').val().length < 1) {
					$('#txt_fax').text('Fax is empty');
			  }else if ($('#fax').val() == 0) {
					$('#txt_fax').text('Fax not entered');
			  }else if ($('#fax').val().length > 15) {
					$('#txt_fax').text('Fax maximum 15 character');
			  }else{
					$('#txt_fax').text('');
			  }
			  
			  if ($('#image').val().length < 1) {
					$('#txt_image').text('Image is empty');
			  }
			  
			  if($('#branch').val().length > 0 && 
				 $('#client_name').val().length > 0 && 
				 $('#client_addres1').val().length > 0 &&
				 $('#province').val().length > 0 &&
				 $('#client_district').val().length > 0 &&
				 $('#client_subdistrict').val().length > 0 &&
				 $('#village').val().length > 0 &&
				 ($('#rt').val().length > 0 && $('#rt').val() != '0') &&
				 ($('#rw').val().length > 0 && $('#rw').val() != '0') &&
				 ($('#myInput').val().length > 0 && $('#myInput').val() != '0') &&
				 ($('#client_email').val().length > 0 && validateEmail($('#client_email').val())) &&
				 ($('#client_website').val().length > 0 && isUrlValid($('#client_website').val())) &&
				 ($('#client_phone1').val().length > 0 && $('#client_phone1').val() != '0' && $('#client_phone1').val().length < 14) &&
				 $('#client_phone2').val().length < 14 &&
				 $('#client_person').val().length > 0 &&
				 ($('#clinet_person_phone').val().length > 0 && $('#clinet_person_phone').val() != '0' && $('#clinet_person_phone').val().length < 14) &&
				 ($('#client_npwp').val().length > 0 && $('#client_npwp').val() != '0' && $('#client_npwp').val().length < 16) &&
				 ($('#fax').val().length > 0 && $('#fax').val() != '0' && $('#fax').val().length < 16) &&
				 ($('#image').val().length > 0 && $('#txt_image').text() == '')){
					$("#kirim").click();
			  }
			  
		  });
	  
     });
    </script>

</section>
    @endsection
    

@endsection
