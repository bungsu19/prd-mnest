@extends('layout-menu.app')

@section('content')

   
<section class="content">
              @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif

            @if(session()->has('failed'))
                <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('failed') }}
                </div>
                @endif
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                Client Information
                            </h1>
                             
                             <h2 align="center">
                             <img id="myImg" class="p-image" src="{{ URL::to('client/'.$data->image) }}" alt="" width="200px"> </img></h2>
                            
                            
                        </div>
						
                            <div class="row clearfix">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Client Information</a>
                                        </li>
                                        
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                       
                         
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                           <div class="table-responsive">
                                                 <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                           <small>:
                                            @foreach($branch as $br)
                                             @if($data->branch_id == $br->id)
                                                     {{ $br->branch_name}}
                                                    @else

                                                    @endif
                                            @endforeach
                                           </small>
                                        </div>
                                        
                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Company Name</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->company_name }}</small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Company Address</label>
                                           
                                        </div>
                                        <div class="col-sm-7">
                                            <small>: {{ $data->company_address1 }}</small>
                                           
                                        </div>
                                        <div class="col-sm-2">
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label></label>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Province</label><br>
                                            <small>: <i>{{ $data->company_province }}</i></small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                            <label> District</label><br>
                                            <small>: <i>{{ $data->company_regency }}</i></small>
                                           
                                        </div>

                                    </div>
                                    
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label> </label>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                             <label> Sub District</label><br>
                                            <small>: {{ $data->company_subdistrict }}</small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                            <label> Village</label><br>
                                            <small>: <i>{{ $data->company_village }}</i></small>
                                           
                                        </div>

                                    </div>
                                    
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label> </label>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Post Code</label><br>
                                            <small>: <i>{{ $data->company_postcode }}</i></small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                             <label>RT/RW</label><br>
                                            <small>: {{ $data->company_rt }}/{{ $data->company_rw }}</small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Email Address</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->company_email }}</small>
                                           
                                        </div>

                                    </div>
                                     <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Website</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: <a href="{{ $data->webiste_url }}">{{ $data->website_url }}</a></small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Phone Number 1</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->company_phone1 }}</small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Phone Number 2</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->company_phone2 }}</small>
                                           
                                        </div>

                                    </div>
                                    
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Contact Person name</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->contact_person }}</small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Contact Person Phone</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->contact_person_phone }}</small>
                                           
                                        </div>

                                    </div>
                                    <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>NPWP</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->npwp }}</small>
                                           
                                        </div>

                                    </div>
                                   
                                     <div class="row clearfix">
                                         <div class="col-sm-3">
                                            <label>Fax</label>
                                           
                                        </div>
                                        <div class="col-sm-9">
                                            <small>: {{ $data->company_fax }}</small>
                                           
                                        </div>

                                    </div>
                                </div>
                                            </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="maps">
                                                 <div id="map" class="gmap"></div>
                                         </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        
    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>


    </section>
    

@endsection
