@extends('layout-menu.app')

@section('content')

   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
					@if(session()->has('gagal'))
						<div class="alert bg-red alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							 {{ session()->get('gagal') }}
						</div>
					@endif
					  @if(session()->has('failed'))
					<div class="alert bg-secondary alert-dismissible" role="alert" style="height: 200px; overflow-y: auto;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color: #DC143C;"><span aria-hidden="true">&times;</span></button>
						  <?php $pesan = session()->get('failed'); ?>
					   <?php for ($i=0; $i < count($pesan) ; $i++) { 
						   echo $pesan[$i].'<br>';
					   }
					   ?>
					</div>
					@endif
				
					@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
                    <div class="card">
                        <div class="header">
                             <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                               <h1><b>Client</b></h1>
                            </div><br>
                           <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">

                                <div class="row clearfix">
                                    <div class="col-sm-6">
                    										<form class="form-signin" method="POST" action="{{url('importClientData')}}" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                    											<div class="input-group">
                    											  <input type="file" name="import" class="form-control">
                    											  <span class="input-group-btn">
                    												<button class="btn bg-blue waves-effect m-r-20" type="submit">Import</button>
                    											  </span>
                    											</div>
                    										</form>
                                     </div>
                                    <div class="col-sm-2">
                                       <a href="{{url('downloadClientData')}}" class="btn bg-blue waves-effect m-r-20">Download</a>
                                    </div>
                                    <div class="col-sm-4">
                                          <a href="{{url('client-data/add-client')}}" class="btn bg-blue waves-effect m-r-20">New Client</a>
                                         <!--<a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>-->
                                    </div>
                                </div>
                           
                            
                            </div>
                            <div class="row clearfix">
 
                            <form class="form-signin" method="POST" action="{{url('client-data/filter-client')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="seacrh" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="1">All client</option>
                                                        <option value="0">Client Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter People</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover    ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                             <td class="bg-column"><font color="yellow">Company Logo</font></td>
                                            <td class="bg-column"><font color="yellow">Company Name</font></td>
                                            <td class="bg-column"><font color="yellow">Email Address</font></td>
                                            <td class="bg-column"><font color="yellow">Phone</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                         <tr>
                                             <td>{{ $no }}</td>
                                             <td align="center"><img id="myImg" class="avatar" src="{{ URL::to('client/'.$da->image) }}" alt="" > </img></td>
                                             <td><span class="span-dots" title="{{ $da->company_name }}">{{ $da->company_name }} </span> </td>
                                             <td>{{ $da->company_email }}</td>
                                             <td><span class="span-dots" title="{{ $da->company_phone1 }}">{{ $da->company_phone1 }}</span></td>
                                             <td align="center">
                                                  <a href="{{url('/client-data/detail-client')}}/{{ $da->client_id }}" data-toggle="tooltip" title="View Data">
                                                  <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            <a href="{{url('/client-data/edit-client')}}/{{ $da->client_id }}" data-toggle="tooltip" title="Edit Data">
                                                <i class="material-icons">mode_edit</i>
                                            </a>

                                            <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete client name '{{ $da->company_name }}' ?" data-href="{{url('/client-data/delete-client')}}/{{ $da->client_id }}" title="Delete Data">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                             </td>
                                         </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                    </tbody>
                                </table> 
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
                    </div>
					@endif
                </div>
          
            <!-- #END# Basic Examples -->

            <div id="myModal" class="modal hide">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" aria-hidden="true" class="close">×</a>
                     <h3>Delete</h3>
                </div>
                <div class="modal-body">
                    <p>You are about to delete.</p>
                    <p>Do you want to proceed?</p>
                </div>
                <div class="modal-footer">
                  <a href="#" id="btnYes" class="btn danger">Yes</a>
                  <a href="#" data-dismiss="modal" aria-hidden="true" class="btn secondary">No</a>
                </div>
            </div>
        
			<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>

    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
			 
        });
</script>

<script type="text/javascript">
    $('#myModal').on('show', function() {
    var id = $(this).data('id'),
        removeBtn = $(this).find('.danger');
    });

    $('.confirm-delete').on('click', function(e) {
        e.preventDefault();

        var id = $(this).data('id');
        $('#myModal').data('id', id).modal('show');
    });

    $('#btnYes').click(function() {
        // handle deletion here
        var id = $('#myModal').data('id');
        $('[data-id='+id+']').remove();
        $('#myModal').modal('hide');
    });

</script>
   @endsection 

@endsection
