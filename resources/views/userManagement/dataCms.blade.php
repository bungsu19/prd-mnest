@extends('layout-menu.app')

@section('content')

   
<section class="content">
     @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif
                  @if(session()->has('failed'))
                <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('failed') }}
                </div>
                @endif
        <div class="container-fluid">
          
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
						
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                               <h1><b>User CMS</b></h1>
                            </div><br>
                           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                 <div class="row clearfix">
                                    <div class="col-sm-7">
										
                                    </div>
                                    <div class="col-sm-2">
										
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{url('add-user-cms')}}" class="btn bg-blue waves-effect m-r-20">New User</a>
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                             
                            
                            </div>
                            
                           <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->
                             <form class="form-signin" method="GET" action="{{url('/filter-outlet-resmi')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-sm-3">
                                                    <label>Filter By</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option value="2">All</option>
                                                        <option value="0">Name</option>
                                                        <option value="1">Address</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5">
                                                    <label>Value</label>
                                                    <input type="text" name="filter_value" class="form-control" placeholder="value">
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Search</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>


                            <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Name</font></td>
                                            <td class="bg-column"><font color="yellow">Email</font></td>
                                            <td class="bg-column"><font color="yellow">Phone Number</font></td>
                                            <td class="bg-column"><font color="yellow">Hak Access</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                       <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                        <tr>
                                            @foreach($data as $datas)
                                            <td>{{ $no }}</td>
                                            <td>{{ $datas->name }}</td>
                                            <td>{{ $datas->email }}</td>
                                            <td>{{ $datas->phone_number }}</td>
                                            <td>@if($datas->hak_access == 1)
                                                  Super Admin
                                                @elseif($datas->hak_access == 2)
                                                    Admin Mnest
                                                @elseif($datas->hak_access == 3)
                                                    Hrd
                                                @elseif($datas->hak_access == 4 )
                                                    Admin Support
                                                @elseif($datas->hak_access == 5 )
                                                    Team Leader
                                                @else
                                                     
                                                @endif </td>
                                             <td align="center">
                                                <!-- <a href="#" data-toggle="tooltip" title="View Data">
                                                  <i class="material-icons">remove_red_eye</i>
                                            </a> -->
                                            <a href="{{url('get-edit-user-cms')}}/{{ $datas->id }}" data-toggle="tooltip" title="Edit Data">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                              <a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete User name '{{ $datas->name }}' ?" data-href="{{url('/get-delete-user-cms')}}/{{ $datas->id }}" title="Delete Data">
                                                <i class="material-icons">delete_forever</i>
                                            </a>
                                            </td>

                                        </tr>
                                            <?php $no++; ?>
                                        @endforeach
                                    </tbody>
                                </table>
                                  <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
                    <div class="modal-footer">
                         <a class="btn-ok"> 
                            <button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
                        </a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
            
    </section>

    @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });

        $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
                var message = $(e.relatedTarget).data('message');
                $(e.currentTarget).find('.message').text(message);
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
</script>
    @endsection    

@endsection
