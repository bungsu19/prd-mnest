@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow">New User</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('post-add-user-cms')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                  
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Name User <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="name_user" class="form-control" placeholder="input Name User" id="name_user" maxlength="30">
                                             </div>
											 <span class="text-error" id="txt_name_user"></span>
                                         </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Email Address <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="email" class="form-control" placeholder="input email address" id="email" maxlength="30">
                                             </div>
											 <span class="text-error" id="txt_email"></span>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                                 <div class="form-line">
                                                    <input type="text" name="phone" class="form-control" placeholder="input phone number" id="phone" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                 </div>
                                                 <span class="text-error" id="txt_phone"></span>
                                             </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Hak Access <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <select name="group" class="form-control" id="group">
                                                    <option value="">-select-</option> 
                                                    <option value="1"> Super Admin </option>
                                                    <option value="2"> Admin Mnest </option>
                                                    <option value="3"> Hrd</option>
                                                    <option value="4"> Admin Support</option> 
                                                    <option value="5"> Team Leader</option>                                                   
                                                 </select>
                                             </div>
											 <span class="text-error" id="txt_group"></span>
                                         </div>
                                         </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Password <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="password" name="password" class="form-control" placeholder="Password ..." id="password" maxlength="8">
                                             </div>
                                             <span class="text-error" id="txt_password"></span>
                                         </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Confirmation Password <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="password" name="confirmation_password" class="form-control" placeholder="Confirmation Password" id="confirmation_password" maxlength="8">
                                             </div>
                                             <span class="text-error" id="txt_confirmation_password"></span>
                                         </div>
                                        </div>
                                    </div>



                                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('all-outlet')}}" class="btn btn-block bg-gray waves-effect" ><h4>Cancel</h4></a>
                                            </div>
                                            <div class="col-sm-5">
												<button href="#" type="button" class="btn btn-block bg-blue waves-effect" id="send"><h4>Save</h4></button>
                                                <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                            </div>
                                        </div>
                                    </div>
                               
                                </div>
                            </div>
                        </div>
                    </div>
                     </form>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   
<script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
}); 
</script>  
	
	<script type="text/javascript">
     $(function(){
		  
		  
		  $('#name_user').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name_user').val().length > 0){
					$('#name_user').val($('#name_user').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	     
		  $(document).on('paste', '#name_user', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 30));
		  });
		  
		  //email cannot long space on input
		  $('#email').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
				
		  });
		  
		  /*handle paste with extra space on email*/
		  $(document).on('paste', '#email', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s+/g, '');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 30));
		  });
		   
		  $('#phone').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#phone').val().length == 15)
					e.preventDefault();
		  });
		  
			$(document).on('paste', '#phone', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  $(this).val(text.substring(0, 15));
		   });
		   
		  
		  function validateEmail($email) {
			  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			  return emailReg.test( $email );
			}
			
			function validatePassword($password) {
			  var passwordReg = /\d/;
			  return passwordReg.test( $password );
			}
	
		  
		  $("#send").click(function(){

			  if ($('#name_user').val().length < 1) {
					$('#txt_name_user').text('Name user is empty');
			  }else{
					$('#txt_name_user').text('');
			  }
			  
			  if ($('#email').val().length < 1) {
					$('#txt_email').text('Email is empty');
			  }else if (!validateEmail($('#email').val())) {
					$('#txt_email').text('Email format is false');
			  }else{
					$('#txt_email').text('');
			  }
			  
			  if ($('#phone').val().length < 1) {
					$('#txt_phone').text('Phone number is empty');
			  }else if ($('#phone').val() == 0) {
					$('#txt_phone').text('Phone number not entered');
			  }else{
					$('#txt_phone').text('');
			  }
			  
			  if ($('#group').val().length < 1) {
					$('#txt_group').text('Group not selected');
			  }else{
					$('#txt_group').text('');
			  }
			  
			  if ($('#password').val().length < 1) {
					$('#txt_password').text('Password is empty');
			  }else if ($('#password').val().length >= 1 && $('#password').val().length < 6) {
					$('#txt_password').text('Minimum password is 6 character');
			  }else if (!validatePassword($('#password').val())) {
					$('#txt_password').text('Password must contain at least one number');
			  }else{
					$('#txt_password').text('');
			  }
			  
			  if ($('#confirmation_password').val().length < 1) {
					$('#txt_confirmation_password').text('Confirmation password is empty');
			  }else if ($('#confirmation_password').val().length >= 1 && $('#confirmation_password').val().length < 6) {
					$('#txt_confirmation_password').text('Minimum confirmation password is 6 character');
			  }else if (!validatePassword($('#confirmation_password').val())) {
					$('#txt_confirmation_password').text('Password must contain at least one number');
			  }else if ($('#confirmation_password').val() != $('#password').val()) {
					$('#txt_confirmation_password').text('Confirmation password not match');
			  }else{
					$('#txt_confirmation_password').text('');
			  }
			  
			  if($('#name_user').val().length > 0 &&
				 ($('#phone').val().length > 0 && $('#phone').val() != '0') &&
				 ($('#email').val().length > 0 && validateEmail($('#email').val())) &&
				 $('#group').val().length > 0 &&
				 ($('#password').val().length > 5 && validatePassword($('#password').val())) &&
				 ($('#confirmation_password').val().length > 5 && validatePassword($('#confirmation_password').val())) &&
				 ($('#email').val().length > 0 && validateEmail($('#email').val())) &&
				 $('#confirmation_password').val() == $('#password').val()){
					  $("#kirim").click();
				  }
			  
		  });
	 });
	 </script>

</section>
    

@endsection
