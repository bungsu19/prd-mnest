@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             <div class="card">
                 <div class="header bg-header">
                            <font size="5" color="yellow">Edit User</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('/post-update-user-cms')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                  
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Name User <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="name_user" class="form-control" placeholder="input Name User" id="name_user" maxlength="30" value="{{ $data->name }}">
												 <input type="hidden" name="id" value="{{ $data->id }}">
                                             </div>
											 <span class="text-error" id="txt_name_user"></span>
                                         </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                                 <div class="form-line">
                                                    <input type="text" name="phone" class="form-control" placeholder="input phone number" id="phone" min="1" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" value="{{ $data->phone_number }}">
                                                 </div>
                                                 <span class="text-error" id="txt_phone"></span>
                                             </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Hak Access <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-8">
                                             <div class="form-group">
                                               <div class="form-line">
                                                 <select name="group" class="form-control" id="group">
                                                    <option value="1" <?php if($data->hak_access == '1'){ echo "selected='selected'";} ?>> Super Admin </option>
                                                    <option value="2" <?php if($data->hak_access == '2'){ echo "selected='selected'";} ?>> Admin Mnest </option>
                                                    <option value="3" <?php if($data->hak_access == '3'){ echo "selected='selected'";} ?>> Hrd</option>
                                                    <option value="4" <?php if($data->hak_access == '4'){ echo "selected='selected'";} ?>> Admin Support</option> 
                                                    <option value="5" <?php if($data->hak_access == '5'){ echo "selected='selected'";} ?>> Team Leader</option>                                                   
                                                 </select>
                                             </div>
											 <span class="text-error" id="txt_group"></span>
                                         </div>
                                         </div>
                                    </div>

                                     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('all-outlet')}}" class="btn btn-block bg-gray waves-effect" ><h4>Cancel</h4></a>
                                            </div>
                                            <div class="col-sm-5">
												<button href="#" type="button" class="btn btn-block bg-blue waves-effect" id="send"><h4>Update</h4></button>
                                                <button href="#" type="submit" class="btn btn-block bg-blue waves-effect" style="display:none;" id="kirim"><h4>Save</h4></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   
<script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
}); 
</script>  
	
	<script type="text/javascript">
     $(function(){
		  
		  
		  $('#name_user').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0){
					e.preventDefault();
				}else if($('#name_user').val().length > 0){
					$('#name_user').val($('#name_user').val().replace(/\s{2,}/g,' '));
				}
		  });
	  
	     
		  $(document).on('paste', '#name_user', function(e) {
			  e.preventDefault();
			  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
			  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
			  var text = $(this).val()+withoutSpaces;
			  $(this).val(text.substring(0, 30));
		  });
		  
		  $('#phone').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if ($('#phone').val().length == 15)
					e.preventDefault();
		  });
		  
			$(document).on('paste', '#phone', function(e) {
				  e.preventDefault();
				  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
				  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
				  var text = $(this).val()+withoutSpaces;
				  $(this).val(text.substring(0, 15));
		   });
		 
		  $("#send").click(function(){

			  if ($('#name_user').val().length < 1) {
					$('#txt_name_user').text('Name user is empty');
			  }else{
					$('#txt_name_user').text('');
			  }
			  
			  if ($('#phone').val().length < 1) {
					$('#txt_phone').text('Phone number is empty');
			  }else if ($('#phone').val() == 0) {
					$('#txt_phone').text('Phone number not entered');
			  }else{
					$('#txt_phone').text('');
			  }
			  
			  if ($('#group').val().length < 1) {
					$('#txt_group').text('Group not selected');
			  }else{
					$('#txt_group').text('');
			  }
			  
			  if($('#name_user').val().length > 0 &&
				 ($('#phone').val().length > 0 && $('#phone').val() != '0') &&
				 $('#group').val().length > 0){
					  $("#kirim").click();
				  }
			  
		  });
	 });
	 </script>

</section>
    

@endsection
