@extends('layout-menu.app')

@section('content')

   
<section class="content">
              @if(session()->has('message'))
                <div class="alert bg-green alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('message') }}
                </div>
                @endif

            @if(session()->has('failed'))
                <div class="alert bg-red alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     {{ session()->get('failed') }}
                </div>
                @endif
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
                    <div class="card">
                        <div class="header">
                            <h1 align="left" >
                                Branch Information
                            </h1>
                             
                             <h2 align="center">
                             <img id="myImg" class="p-image" src="{{ URL::to('upload/imageBranch/'.$data->image) }}" alt="" width="200px"> </img></h2>
                            
                            
                        </div>
						
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#personal_data" class="btn bg-blue" data-toggle="tab">Branch Information</a>
                                        </li>
                                         <li role="presentation" >
                                            <a href="#maps" class="btn bg-blue" data-toggle="tab">Location Branch</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                       
                         
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="personal_data">
                                           <div class="table-responsive">
                                                 <div class="body">

                                   
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Name Branch </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_name }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Address 1 </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_address1 }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Address 2 </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_address2 }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label> </label>
                                        </div>
                                        <div class="col-sm-4">
                                          <label>Province </label><br>
                                           <small>: {{ $data->branch_province }}</small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                             <label>District/city </label><br>
                                           <small>: {{ $data->branch_city }}</small>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label> </label>
                                        </div>
                                        <div class="col-sm-4">
                                           <label>Sub District </label><br>
                                           <small>: {{ $data->branch_sub_district }}</small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                             <label>Village </label><br>
                                           <small>: {{ $data->branch_village }}</small>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label> </label>
                                        </div>
                                        <div class="col-sm-2">
                                             <label>RT</label><br>
                                           <small>: {{ $data->branch_tr_rw }}</small>
                                           
                                        </div>
                                        <div class="col-sm-2">
                                             <label>RW</label><br>
                                           <small>: {{ $data->branch_rw }}</small>
                                           
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Code Post </label><br>
                                           <small>: {{ $data->branch_post_code }}</small>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Phone Number 1 </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_phone1 }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Phone Number 2 </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_phone2 }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Email Address </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->branch_email }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>Contact Person </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->pic_name }}</label>
                                           
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <label>NPWP </label>
                                        </div>
                                        <div class="col-sm-8">
                                           <label>: {{ $data->npwp }}</label>
                                           
                                        </div>
                                    </div>
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-8">
                                                <a href="{{url('branch')}}" class="btn btn-block bg-blue waves-effect" ><font size="4">Back</font></a>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                    

                                    
                                </div>
                                            </div>
                                        </div>

                                        <div role="tabpanel" class="tab-pane animated fadeInRight " id="maps">
                                                 <div id="map" class="gmap"></div>
                                         </div>


                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        <div id="myModal" class="modal">
          <span class="close">&times;</span>
          <img class="modal-content" id="img01">
          <div id="caption"></div>
        </div>

        
    

        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            img.onclick = function(){
                modal.style.display = "block";
                modalImg.src = this.src;
                captionText.innerHTML = this.alt;
            }

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() { 
                modal.style.display = "none";
            }
        </script>
         <script src="http://maps.googleapis.com/maps/api/js"></script>
       <script>

          function initMap() {
            var locations = [
                    <?php $lang = $data->branch_langitude; ?>
                    <?php $lat = $data->branch_latitude; ?>
                    ['marker', <?php echo $lat; ?>,<?php echo $lang; ?>],
                    
                    
                ];
        
            var myLatLng = {lat: -6.1927204, lng: 106.837188};

            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 10,
              center: myLatLng,
               styles: [ 
                      {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                      {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                      {
                          featureType: 'administrative.locality',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'geometry',
                          stylers: [{color: '#263c3f'}]
                      },
                      {
                          featureType: 'poi.park',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#6b9a76'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry',
                          stylers: [{color: '#38414e'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#212a37'}]
                      },
                      {
                          featureType: 'road',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#9ca5b3'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry',
                          stylers: [{color: '#746855'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'geometry.stroke',
                          stylers: [{color: '#1f2835'}]
                      },
                      {
                          featureType: 'road.highway',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#f3d19c'}]
                      },
                      {
                          featureType: 'transit',
                          elementType: 'geometry',
                          stylers: [{color: '#2f3948'}]
                      },
                      {
                          featureType: 'transit.station',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#d59563'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'geometry',
                          stylers: [{color: '#17263c'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.fill',
                          stylers: [{color: '#515c6d'}]
                      },
                      {
                          featureType: 'water',
                          elementType: 'labels.text.stroke',
                          stylers: [{color: '#17263c'}]
                      }
                  ]
            });

             var markerin, i;
  
                for (i = 0; i < locations.length; i++) {  
                        markerin = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                        map: map
                    });
            
                    google.maps.event.addListener(markerin, 'click', (function(markerin, i) {
                        return function() {
                            infowindow.setContent(locations[i][0]);
                            infowindow.open(map, markerin);
                        }
                    })(markerin, i));
                }

               
          }
        </script>

     <script async defer
                 src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpIkSIwMX6KNvhWzKe66KqutyWhY6zWFI&callback=initMap">
    </script>


    </section>
    

@endsection
