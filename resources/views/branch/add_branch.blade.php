@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
     <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: -30px;">
		
			@if(session()->has('failed'))
            <div class="alert bg-red alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('failed') }}
            </div>
            @endif

             @if(session()->has('message'))
            <div class="alert bg-green alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 {{ session()->get('message') }}
            </div>
            @endif
			
             <div class="card">
                 <div class="header bg-header">
                            <font size="6" color="yellow">New Branch</font>
                        </div>

                    <!-- form add product -->

                     <form class="form-signin" method="POST" action="{{url('branch/add-branch')}}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Branch Name <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="name_branch" class="form-control" placeholder="input branch Name" maxlength="50" id="branch">
                                                </div>
												<span class="text-error" id="txt_branch"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Address 1 <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="address1" class="form-control" placeholder="input address 1" maxlength="150" id="address1">
                                                </div>
												<span class="text-error" id="txt_address1"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Address 2</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                   <input type="text" name="address2" class="form-control" placeholder="input address 2" maxlength="150" id="address2">
                                               </div>
											   <span class="text-error" id="txt_address2"></span>
                                           </div>
                                        </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                         <div class="col-sm-3">
                                            <label>Province <span class="mandatory">*</span></label>
                                             <select name="province" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="province">
                                                <option value="">- Province -</option>
                                                @foreach($province as $result)
                                                <option data-tokens="{{ $result->province }}" value="{{ $result->province }}">{{ $result->province }}</option>
                                                 @endforeach
                                            </select>
											<span class="text-error" id="txt_province"></span>
                                         </div>
                                         <div class="col-sm-3">
                                            <label>District <span class="mandatory">*</span></label>
                                             <div class="regency">
                                                 <select name="district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="district">
                                                            <option value="">-Regency/City -</option>
                                                 </select>
												 <span class="text-error" id="txt_district"></span>
                                             </div>
                                         </div>
                                    </div>

                                    
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                         <div class="col-sm-3">
                                             <label>Sub District <span class="mandatory">*</span></label>
                                             <div class="subdistrict">
                                                    <select name="sub_district" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="sub_district">
                                                            <option value="">-Subdistrict -</option>
                                                    </select>
													<span class="text-error" id="txt_sub_district"></span>
                                                </div>
                                         </div>
                                         <div class="col-sm-3">
                                            <label>Village <span class="mandatory">*</span></label>
                                            <div class="village">
                                                <select name="village" style="border-bottom: 1px solid #eee;" class="form-control"  data-live-search="true" id="village">
                                                    <option value="">- Village -</option>
                                                </select>
												<span class="text-error" id="txt_village"></span>
                                            </div>
                                         </div>
                                    </div>

                                   
                                     <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label> </label>
                                        </div>
                                         <div class="col-sm-2">
                                            <label>RT <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="rt_rw" class="form-control" placeholder="Input RT" id="rt" maxlength="3" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                </div>
												<span class="text-error" id="txt_rt"></span>
                                            </div>
                                         </div>
                                         <div class="col-sm-2">
                                            <label>RW <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="rw" class="form-control" placeholder="Input RW" id="rw" maxlength="3" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                </div>
												<span class="text-error" id="txt_rw"></span>
                                            </div>
                                         </div>
                                         <div class="col-sm-2">
                                             <label>Post Code <span class="mandatory">*</span></label>
                                            <div class="form-group">
                                               <div class="form-line">
                                                    <input type="text" name="post_code" class="form-control" placeholder="input Post Code" maxlength="7" id="post_code" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                                </div>
												<span class="text-error" id="txt_post_code"></span>
                                            </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 1 <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="phone1" class="form-control" placeholder="input phone number 1" id="phone1" maxlength="20" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
											 <span class="text-error" id="txt_phone1"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Phone Number 2</label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="phone2" class="form-control" placeholder="input phone number 2" maxlength="20" id="phone2" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
											 <span class="text-error" id="txt_phone2"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Email Address <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="email" name="email" class="form-control" placeholder="input email address" id="email" maxlength="50">
                                             </div>
											 <span class="text-error" id="txt_email"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Contact Person <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="pic_name" class="form-control" placeholder="Input Owner Name" id="pic_name" maxlength="20">
                                             </div>
											 <span class="text-error" id="txt_pic_name"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>NPWP <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="npwp" class="form-control" placeholder="Input NPWP" id="npwp" maxlength="20" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
											 <span class="text-error" id="txt_npwp"></span>
                                         </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Langitude <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                  <input type="text" name="langitude" class="form-control" placeholder="Input Longitude" id="longitude" maxlength="20" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                              </div>
											  <span class="text-error" id="txt_longitude"></span>
                                          </div>
                                         </div>
                                    </div>

                                    <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Latitude <span class="mandatory">*</span></label>
                                        </div>
                                         <div class="col-sm-6">
                                            <div class="form-group">
                                               <div class="form-line">
                                                 <input type="text" name="latitude" class="form-control" placeholder="Input Lattitude" maxlength="20" id="lattitude" oninput="this.value = this.value.replace(/[^0-9]/g, '');">
                                             </div>
											 <span class="text-error" id="txt_lattitude"></span>
                                         </div>
                                         </div>
                                    </div>

                                      <div class="row clearfix">
                                        <div class="col-sm-3">
                                            <label>Image <span class="mandatory">*</span></label>
                                        </div>
                                        <div class="col-sm-6">
                                             <input type="file" name="image" class="form-control" required id="profile-img" >
											 <span class="text-error" id="txt_profile-img"></span>
                                             <br>
                                             <img src="{{ asset('default.png') }}" id="profile-img-tag" width="200px" />
                                        </div>
                                           
                                    </div>
                                    
                                 
                                     <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                         
                                        
                                        </div>
                                    <div class="row clearfix">
                                         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: right;">
                                            <div class="col-sm-5">
                                                <a href="{{url('branch')}}" class="btn bg-gray waves-effect" ><font size="4"> Cancel</font></a>
                                            </div>
                                            <div class="col-sm-3">
												<button href="#" type="button" class="btn bg-blue waves-effect" id="send"><font size="4"> Save</font></button>
                                                <button href="#" type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim"><font size="4"> Save</font></button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- #END# form add prodct -->
            </div>
        </div>
    </div>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
   <script type="text/javascript">
$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
}); 

setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>          
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag2').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img2").change(function(){
            readURL(this);
        });
    </script>

    <script type="text/javascript">
        $('#province').change(function(){
            var prov = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/regency/') }}",
                type:"POST",
                data:{ data_id: prov,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.regency select').html(obj.data);
                        $('.regency select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.regency select').change(function(){
            var regen = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/subdistrict/') }}",
                type:"POST",
                data:{ data_id: regen,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.subdistrict select').html(obj.data);
                        $('.subdistrict select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.subdistrict select').change(function(){
            var subdist = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/village/') }}",
                type:"POST",
                data:{ data_id: subdist,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.village select').html(obj.data);
                        $('.village select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });

        $('.village select').change(function(){
            var village = $(this).val();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:"{{ url('/postcode/get/postcode/') }}",
                type:"POST",
                data:{ data_id: village,_token: CSRF_TOKEN },
                success:function(response) {
                    var obj = JSON.parse(response);
                    if(obj.hasil == 'OK'){
                        $('.postcode select').html(obj.data);
                        $('.postcode select').selectpicker('refresh');
                        return false;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
        });
    </script>

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#profile-img-tag').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#profile-img").change(function(){
            readURL(this);
        });
    </script>
	
	<script type="text/javascript">
     $(function(){
		  $('#branch').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $('#address1').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $('#address2').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $('#email').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32)
					e.preventDefault();
		  });
		  
		  function validateEmail($email) {
			  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			  return emailReg.test( $email );
			}
		  
		  $('#pic_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $("#send").click(function(){
			  if ($('#branch').val().length < 1) {
					$('#txt_branch').text('Branch is empty');
			  }else{
					$('#txt_branch').text('');
			  }
			  
			  if ($('#address1').val().length < 1) {
					$('#txt_address1').text('Address 1 is empty');
			  }else{
					$('#txt_address1').text('');
			  }
			  
			  if ($('#address2').val().length < 1) {
					$('#txt_address2').text('Address 2 is empty');
			  }else{
					$('#txt_address2').text('');
			  }
			  
			  if ($('#province').val().length < 1) {
					$('#txt_province').text('Province is empty');
			  }else{
					$('#txt_province').text('');
			  }
			  
			  if ($('#district').val().length < 1) {
					$('#txt_district').text('District not selected');
			  }else{
					$('#txt_district').text('');
			  }
			  
			  if ($('#sub_district').val().length < 1) {
					$('#txt_sub_district').text('Subdistrict not selected');
			  }else{
					$('#txt_sub_district').text('');
			  }
			  
			  if ($('#village').val().length < 1) {
					$('#txt_village').text('Village not selected');
			  }else{
					$('#txt_village').text('');
			  }
			  
			  if ($('#rt').val().length < 1) {
					$('#txt_rt').text('Rt is empty');
			  }else if ($('#rt').val() == 0) {
					$('#txt_rt').text('Rt not entered');
			  }else{
					$('#txt_rt').text('');
			  }
			  
			  if ($('#rw').val().length < 1) {
					$('#txt_rw').text('Rw is empty');
			  }else if ($('#rw').val() == 0) {
					$('#txt_rw').text('Rw not entered');
			  }else{
					$('#txt_rw').text('');
			  }
			  
			  if ($('#post_code').val().length < 1) {
					$('#txt_post_code').text('Postal code is empty');
			  }else if ($('#post_code').val() == 0) {
					$('#txt_post_code').text('Postal code not entered');
			  }else{
					$('#txt_post_code').text('');
			  }
			  
			  if ($('#phone1').val().length < 1) {
					$('#txt_phone1').text('Phone 1 is empty');
			  }else if ($('#phone1').val() == 0) {
					$('#txt_phone1').text('Phone 1 not entered');
			  }else{
					$('#txt_phone1').text('');
			  }
			  
			  if ($('#phone2').val().length < 1) {
					$('#txt_phone2').text('Phone 2 is empty');
			  }else if ($('#phone2').val() == 0) {
					$('#txt_phone2').text('Phone 2 not entered');
			  }else{
					$('#txt_phone2').text('');
			  }
			  
			  if ($('#email').val().length < 1) {
					$('#txt_email').text('Email is empty');
			  }else if (!validateEmail($('#email').val())) {
					$('#txt_email').text('Email format is false');
			  }else{
					$('#txt_email').text('');
			  }
			  
			  if ($('#pic_name').val().length < 1) {
					$('#txt_pic_name').text('Pic name is empty');
			  }else{
					$('#txt_pic_name').text('');
			  }
			  
			  if ($('#npwp').val().length < 1) {
					$('#txt_npwp').text('Npwp is empty');
			  }else if ($('#npwp').val() == 0) {
					$('#txt_npwp').text('Npwp not entered');
			  }else{
					$('#txt_npwp').text('');
			  }
			  
			  if ($('#longitude').val().length < 1) {
					$('#txt_longitude').text('Longitude is empty');
			  }else if ($('#longitude').val() == 0) {
					$('#txt_longitude').text('Longitude not entered');
			  }else{
					$('#txt_longitude').text('');
			  }
			  
			  if ($('#lattitude').val().length < 1) {
					$('#txt_lattitude').text('Lattitude is empty');
			  }else if ($('#lattitude').val() == 0) {
					$('#txt_lattitude').text('Lattitude not entered');
			  }else{
					$('#txt_lattitude').text('');
			  }
			  
			  if ($('#profile-img').val().length < 1) {
					$('#txt_profile-img').text('Image is empty');
			  }else{
					$('#txt_profile-img').text('');
			  }
			  
			  if($('#branch').val().length > 0 &&
				 $('#address1').val().length > 0 &&
				 $('#address2').val().length > 0 &&
				 $('#province').val().length > 0 &&
				 $('#district').val().length > 0 && 
				 $('#sub_district').val().length > 0 &&
				 $('#village').val().length > 0 &&
				 ($('#rt').val().length > 0 && $('#rt').val() != '0') &&
				 ($('#rw').val().length > 0 && $('#rw').val() != '0') &&
				 ($('#post_code').val().length > 0 && $('#post_code').val() != '0') &&
				 ($('#phone1').val().length > 0 && $('#phone1').val() != '0') &&
				 ($('#phone2').val().length > 0 && $('#phone2').val() != '0') &&
				 ($('#email').val().length > 0 && validateEmail($('#email').val())) &&
				 $('#pic_name').val().length > 0 &&
				 ($('#npwp').val().length > 0 && $('#npwp').val() != '0') &&
				 ($('#longitude').val().length > 0 && $('#longitude').val() != '0') &&
				 ($('#lattitude').val().length > 0 && $('#lattitude').val() != '0') &&
				 $('#profile-img').val().length > 0){
				  $("#kirim").click();
			  }
			  
		  });
	 });
	 </script>

</section>
    

@endsection
