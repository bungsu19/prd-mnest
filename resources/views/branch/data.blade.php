@extends('layout-menu.app')

@section('content')

   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
                    <div class="card">
                        <div class="header">
						
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                               <h1><b>Branch</b></h1>
                            </div><br>
                           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="text-align: right;">
                                 <div class="row clearfix">
                                    <div class="col-sm-8">
                                    </div>
                                    <div class="col-sm-2">
										<!--<a href="{{url('downloadDataOBranch')}}" class="btn bg-blue waves-effect m-r-20">Download</a>-->
                                    </div>
                                    <div class="col-sm-2">
										@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
											<a href="{{url('branch/add-branch')}}" class="btn bg-blue waves-effect m-r-20">Add Branch</a>
										@endif
									</div>
                                </div>
                             
                            
                            </div>
						

                             <div class="row clearfix">
							 
                            <form class="form-signin" method="POST" action="{{url('/branch/filter')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All Branch</option>
                                                        <option value="1">Branch Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
					</div>	
							@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column"><font color="yellow"> No</font></td>
                                            <td class="bg-column"> <font color="yellow">Branch Name</font></td>
                                            <td class="bg-column"><font color="yellow">Address</font></td>
                                            <td class="bg-column"><font color="yellow">phone</font></td>
                                            <td class="bg-column"><font color="yellow">email address</font></td>
                                            <!--<td class="bg-column"><font color="yellow">Contact Person</font></td>-->
                                            <td class="bg-column"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                        <tr>
                                            <td>{{ $no }}</td>
                                            <td>{{ $da->branch_name }}</td>
                                            <td><span class="span-dots" data-toggle="tooltip" title="{{ $da->branch_address1 }}">{{ $da->branch_address1 }}</span></td>
                                            <td>{{ $da->branch_phone1 }}</td>
                                            <td>{{ $da->branch_email }}</td>
                                            <!--<td>{{ $da->pic_name }}</td>-->
                                            <td>
                                                <a href="{{url('branch/view-branch/')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
                                                    <i class="material-icons">remove_red_eye</i>
												</a>
												<a href="{{url('branch/edit-branch/')}}/{{ $da->id }}" data-toggle="tooltip" title="Edit Data">
													<i class="material-icons">mode_edit</i>
												</a>
												<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete branch '{{ $da->branch_name }}' ?" data-href="{{url('branch/delete-branch/')}}/{{ $da->id }}" data-toggle="tooltip" title="View Data">
													  <i class="material-icons">delete_forever</i>
												</a>
                                            </td>
                                        </tr> 
                                        <?php $no++; ?>
                                         @endforeach
                                      
                                    </tbody>
                                </table>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
							@endif
                    </div>
                </div>
            </div>
			
			 <div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
			 
        </div>
            
    </section>
     @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
			 
			 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
				var message = $(e.relatedTarget).data('message');
				$(e.currentTarget).find('.message').text(message);
				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});
			 
        });
		
		setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
</script>
    @endsection
@endsection
