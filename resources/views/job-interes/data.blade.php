@extends('layout-menu.app')

@section('content')

<style type="text/css">
	.text-error
	{
            font-size: 12px;
            font-style: italic;
            color: red;
            letter-spacing: 0.7px;
			margin-top: 5px;
    }
	
	.mandatory
	{
            font-size: 15px;
            color: red;
    }
</style>
   
<section class="content">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: -30px;">
				
					@if(session()->has('failed'))
					<div class="alert bg-red alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('failed') }}
					</div>
					@endif

					 @if(session()->has('message'))
					<div class="alert bg-green alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						 {{ session()->get('message') }}
					</div>
					@endif
				
					@if(Auth::user()->hak_access == 1 || Auth::user()->hak_access == 2)
                    <div class="card">
                        <div class="header">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <h1><b>
                                    All Job Interest</b>
                                </h1>
                            </div>
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">

                             <div class="row clearfix">
                                    <div class="col-sm-4">
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <a  class="btn btn-block bg-blue waves-effect m-r-20" data-toggle="modal" data-target="#defaultModal">New Job </a></h2>
                                         
                                    </div>
                             </div>

                            
                            
                            </div>

                        <br>
                        <br>
                            <!-- Select -->
                   
            <!-- #END# Select -->
                          
                        </div>
						<div class="row clearfix"></div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover ">
                                    <thead>
                                        <tr>
                                            <td class="bg-column" align="center"><font color="yellow">No</font></td>
                                            <td class="bg-column"><font color="yellow">Name Job</font></td>
                                            <td class="bg-column"><font color="yellow">Description</font></td>
                                            <td class="bg-column" align="center"><font color="yellow">Action</font></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php 
                                        $no = 0;
                                        if($data->currentPage() == 1){
                                            $no = 1;
                                        }else{
                                          $no = ($data->currentPage()-1) * $jumlahData + 1;
                                        }
                                        ?>
                                         @foreach($data as $da)
                                            <tr>
                                                <td align="center" scope="row">{{ $no }}</td>
                                                <td> {{ $da->job_name }}</td>
                                                <td><span class="span-dots" title="{{ $da->description }}s"> {{ $da->description }}</span></td>
                                                <td align="center">
                                       
													<a href="#" data-toggle="modal" data-target="#modal-edit" data-id="{{ $da->id }}" data-job_name="{{ $da->job_name }}" data-description="{{ $da->description }}" title="Edit Data">
														<i class="material-icons">mode_edit</i>
													</a>

													<a href="#" data-toggle="modal" data-target="#konfirmasi_hapus" data-message="are you sure want to delete job interest '{{ $da->job_name }}' ?" data-href="{{url('job-interes/delete')}}/{{ $da->id }}" title="Delete Data">
														<i class="material-icons">delete_forever</i>
													</a>
                                            </tr>
                                         <?php $no++; ?>
                                         @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               <h1><b></b></h1>
                                </div>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                                    {{ $paginator->links('paginator.default') }}
                                </div>
                            </div>
                    </div>
					@endif
                </div>
            </div>
            <!-- #END# Basic Examples -->

         <!-- Default Size -->
         <form class="form-signin" method="POST" action="{{url('/job-interes/add')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"> <font color="yellow">New Job</font></h4>
                        </div>
                        <div class="modal-body">
						
							<div class="col-sm-12">
                                <label>Name Job <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="job_name" id="job_name" placeholder=" Name Job" maxlength="30">
								<span class="text-error" id="txt_job_name"></span>
                                <br>
                               
                               
                            </div>

                            <div class="col-sm-12">
                                <label>Description <span class="mandatory">*</span></label>
                                <textarea name="desc" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" id="desc" placeholder="Description" maxlength="100"></textarea>
                                <span class="text-error" id="txt_desc"></span>
								<br>
                                <br>
                            </div>
							
							<div class="modal-footer">
                            <br>
                            <br>
								<button type="button" class="btn bg-blue waves-effect" id="send">Save</button>
								<button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim">Save</button>
								<button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">Cancel</button>
							</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
        
            <form class="form-signin" method="POST" action="{{url('/job-interes/edit')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
            <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-header">
                            <h4 class="modal-title" id="defaultModalLabel"> <font color="yellow">Edit Data </font></h4>
                        </div>
                        <div class="modal-body">
						
							<div class="col-sm-12">
                                <label>Name Job <span class="mandatory">*</span></label>
                                <input type="text" class="form-control" name="job_name" id="job_name_edit" placeholder="Name job" maxlength="30">
								<span class="text-error" id="txt_job_name_edit" ></span>
                                <br> 
                                <input type="hidden" name="id" id="id">
                               
                            </div>

                            <div class="col-sm-12">
                                <label>Description <span class="mandatory">*</span></label>
                                <textarea name="desc" class="form-control" style="height: 100px; max-height: 100px; min-height: 100px; max-width: 100%; min-width: 100%;" placeholder="Description" id="desc_edit" maxlength="100" ></textarea>
                                <span class="text-error" id="txt_desc_edit"></span>
								<br>
                                <br>
                            </div>
							
        							<div class="modal-footer">
                                    <br>
                                    <br>
        								<button type="button" class="btn bg-blue waves-effect" id="send_edit">Update</button>
        								<button type="submit" class="btn bg-blue waves-effect" style="display:none;" id="kirim_edit">Update</button>
        								<button type="button" class="btn bg-gray waves-effect" data-dismiss="modal">Cancel</button>
        							</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </form>
		
		<div class="modal fade" id="konfirmasi_hapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <b class="message"></b><br>
                    </div>
					<div class="modal-footer">
						 <a class="btn-ok"> 
							<button type="button" class="btn btn-danger btn-ok" ><i class="fa fa-check"></i> Ok</button>
						</a>
                        <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
					</div>
                </div>
            </div>
        </div>
		
    </section>
    
	@section('script')
<script type="text/javascript">
     $(function(){

			  $('#modal-edit').on('show.bs.modal', function(e) { 
					var id = $(e.relatedTarget).data('id');
					var job_name = $(e.relatedTarget).data('job_name');
					var description = $(e.relatedTarget).data('description');
					 
					$(e.currentTarget).find('#job_name_edit').val(job_name);
					$(e.currentTarget).find('#desc_edit').val(description);
					$(e.currentTarget).find('#id').val(id);  
				});
				
				 $('#konfirmasi_hapus').on('show.bs.modal', function(e) { 
					var message = $(e.relatedTarget).data('message');
					$(e.currentTarget).find('.message').text(message);
					$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
				});
            

            $('#desc').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#desc').val().length > 0){
                    $('#desc').val($('#desc').val().replace(/\s{2,}/g,' '));
                }
            });

           /*handle paste with extra space on rt*/
            $(document).on('paste', '#desc', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 3);
                  $(this).val(isi.replace(/\D/g, ''));
           });

             $('#job_name').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#job_name').val().length > 0){
                    $('#job_name').val($('#job_name').val().replace(/\s{2,}/g,' '));
                }
            });

             /*handle paste with extra space on rt*/
              $(document).on('paste', '#job_name', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 3);
                  $(this).val(isi.replace(/\D/g, ''));
           });

		  $('#job_name').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $('#desc').keypress(function (e) {
				var startPos = e.currentTarget.selectionStart;
				if (e.which === 32 && startPos==0)
					e.preventDefault();
		  });
		  
		  $("#send").click(function(){
			  if ($('#job_name').val().length < 1) {
					$('#txt_job_name').text('Job name is empty');
			  }else{
					$('#txt_job_name').text('');
			  }
			  
			  if ($('#desc').val().length < 1) {
					$('#txt_desc').text('Description is empty');
			  }else{
					$('#txt_desc').text('');
			  }
			  
			  if($('#job_name').val().length > 0 && $('#desc').val().length > 0){
				  $("#kirim").click();
			  }
			  
		  });
		  
		 
		  
	 });
	 </script>

     <script type="text/javascript">
     $(function(){
         
          $('#job_name_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#job_name_edit').val().length > 0){
                    $('#job_name_edit').val($('#job_name_edit').val().replace(/\s{2,}/g,' '));
                }
            });
           /*handle paste with extra space on rt*/
            $(document).on('paste', '#job_name_edit', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 3);
                  $(this).val(isi.replace(/\D/g, ''));
           });
          
          $('#desc_edit').keypress(function (e) {
                var startPos = e.currentTarget.selectionStart;
                if (e.which === 32 && startPos==0){
                    e.preventDefault();
                }else if($('#desc_edit').val().length > 0){
                    $('#desc_edit').val($('#desc_edit').val().replace(/\s{2,}/g,' '));
                }
            });
           /*handle paste with extra space on rt*/
            $(document).on('paste', '#desc_edit', function(e) {
                  e.preventDefault();
                  var withoutSpaces = e.originalEvent.clipboardData.getData('Text');
                  withoutSpaces = withoutSpaces.replace(/\s{2,}/g,' ');
                  var text = $(this).val()+withoutSpaces;
                  var isi = text.substring(0, 3);
                  $(this).val(isi.replace(/\D/g, ''));
           });


          $('#desc_edit').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#desc_edit').val().length == 150)
                e.preventDefault();
            });
         $('#job_name_edit').keypress(function (e) {
            var startPos = e.currentTarget.selectionStart;
            if ($('#job_name_edit').val().length == 30)
                e.preventDefault();
            });
          
          $("#send_edit").click(function(){
              if ($('#job_name_edit').val().length < 1) {
                    $('#txt_job_name_edit').text('Job name is empty');
              }else{
                    $('#txt_job_name_edit').text('');
              }
              
              if ($('#desc_edit').val().length < 1) {
                    $('#txt_desc_edit').text('Description is empty');
              }else{
                    $('#txt_desc_edit').text('');
              }
              
              if($('#job_name_edit').val().length > 0 && $('#desc_edit').val().length > 0){
                  $("#kirim_edit").click();
              }
              
          });
         });
     </script>


      <script type="text/javascript">
    $(document).ready(function() {
      $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    }); 
	
	setTimeout(function(){
		   $("div.alert").remove();
		}, 5000 );
    </script>  
    @endsection

@endsection
