@extends('layout-menu.app')

@section('content')

   
<section class="content">
        <div class="container-fluid">
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <h1>OFFICE BRANCH Real</h1>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: right;">
                            <a href="{{url('branch/add-branch')}}" class="btn bg-blue waves-effect m-r-20" >New Branch</a>
                              <a href="javascript:void(0);" style="color: #00f; padding: 5px;position: relative;z-index: 9;" class="filterbtn"><i class="fa fa-search"></i></a>
                            
                            </div>

                             <div class="row clearfix">
                                <!-- filter -->

                            
                        <!-- end filter -->

                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <form class="form-signin" method="POST" action="{{url('/branch/filter')}}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="formfilter" style="margin-top: 10px;display: none;">
                                    <div class="card" style="margin: 0px;">
                                        
                                        <div class="body">
                                            <div class="row clearfix">
                                                
                                                <div class="col-sm-8">
                                                    <label>Filter Data</label>
                                                    <select name="search" class="form-control show-tick">
                                                        <option>-pilih-</option>
                                                        <option value="0">All Branch</option>
                                                        <option value="1">Branch Active</option>
                                                    </select>
                                                </div>
                                                 <div class="col-sm-4">
                                                    <br>
                                                    <label></label>
                                                     <button type="submit" class="btn bg-blue waves-effect m-r-20">Filter Branch</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                            <div class="body">
                            <div class="table-responsive">
                               <table class="table table-bordered" id="datatable-responsive">
                                   <thead>
                                      <tr>
                                         <th>Id</th>
                                         <th>Name</th>
                                         <th>Email</th>
                                      </tr>
                                   </thead>
                                </table>
                            </div>
                        </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
            
    </section>
     @section('script')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <script type="text/javascript">
        var $statusfilter = 0;
        $(document).ready(function(){
            $('.filterbtn').click(function(){
            if($statusfilter == 0){
                $('#formfilter').show(999);
                $statusfilter = 1;
            } else {
                $('#formfilter').hide(999);
                $statusfilter = 0;
            }
             });
        });
</script>

       <script>
           $.ajax({
            type: 'GET', //THIS NEEDS TO BE GET
            url: 'realtime/data',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                container.html('');
                $.each(data, function(index, item) {
                container.html(''); //clears container for new data
                $.each(data, function(i, item) {
                      container.append('<div class="row"><div class="ten columns"><div class="editbuttoncont"><button class="btntimestampnameedit" data-seek="' + item.timestamp_time + '">' +  new Date(item.timestamp_time * 1000).toUTCString().match(/(\d\d:\d\d:\d\d)/)[0] +' - '+ item.timestamp_name +'</button></div></div> <div class="one columns"><form action="'+ item.timestamp_id +'/deletetimestamp" method="POST">' + '{!! csrf_field() !!}' +'<input type="hidden" name="_method" value="DELETE"><button class="btntimestampdelete"><i aria-hidden="true" class="fa fa-trash buttonicon"></i></button></form></div></div>');
              });
                    container.append('<br>');
                });
            },error:function(){ 
                 console.log(data);
            }
        });
    </script>
    @endsection
@endsection
