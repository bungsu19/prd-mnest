<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>SDM - Sumber Daya Mandiri</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('logo-web.png') }}" type="image/x-icon">
     <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

     <!-- Google Fonts -->
    <link href="{{ asset('css/css-menu.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet" type="text/css">
	
	<!-- clock -->
	<link href="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/login/vendor/font-awesome/css/font-awesome.min.css') }}">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="{{ asset('plugins/morrisjs/morris.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- <link href="css/theme.css" rel="stylesheet"> -->

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('css/themes/all-themes.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
	
	<link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet') }}" />
	
    <!-- Bootstrap Select Css -->
<!--     <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" /> -->
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet" />
    
    <link href="{{ asset('style-sdm.css') }}" rel="stylesheet">
	
	
	<link href="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css" rel="stylesheet"/>
	
	<link href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css" rel="stylesheet"/>
	
	<!-- <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet"/> -->

	<style>
		ul{
			list-style-type:none;
			}
	</style>

</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="index.html">Mnest</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   <!--  <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">settings_power</i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header"></li>
                            <li class="body">
                                <ul class="menu">
								<li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">account_box</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>
														@if(Auth::check())
															{{ Auth::user()->name }}
														@endif
												</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">loop</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Change Password</h4>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('logout')}}">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">arrow_forward</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4 >Log Out</h4>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">John Doe</div>
                    <div class="email">john.doe@example.com</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                @if(Auth::user()->hak_access == 1)
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="{{ (request()->segment(1) == 'templete') ? 'active' : '' }}">
                        <a href="{{url('/templete')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    
                    
                    <li class="{{ (request()->segment(1) == 'all-project' || request()->segment(1) == 'task-list') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">business</i>
                            <span>Projects</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (request()->segment(1) == 'all-project') ? 'active' : '' }}">
                                <a href="{{url('all-project')}}">All Projects</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'task-list') ? 'active' : '' }}">
                                <a href="{{url('task-list')}}">Task</a>
                            </li>
                            <li>
                                <a href="#">Report</a>
                            </li>
                            
                        </ul>
                    </li>
                      <li class="{{ (request()->segment(1) == 'data-people') ? 'active' : '' }}">
                        <a href="{{url('data-people')}}">
                            <i class="material-icons">nature_people</i>
                            <span>People</span>
                        </a>
                    </li>

                      <li class="{{ (request()->segment(1) == 'structure-area-data' || 
									 request()->segment(1) == 'branch' ||
									 request()->segment(1) == 'client-data' ||
									 request()->segment(1) == 'job-interes' ||
									 request()->segment(1) == 'all-outlet' ||
									 request()->segment(1) == 'outlet-open' ||
									 request()->segment(1) == 'product-data' ||
									 request()->segment(1) == 'code-pos') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">developer_board</i>
                            <span>Master</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (request()->segment(1) == 'aggree') ? 'active' : '' }}">
                                <a href="{{url('aggree')}}">Aggree</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'structure-area-data') ? 'active' : '' }}">
                                <a href="{{url('structure-area-data')}}">Area Structure</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'branch') ? 'active' : '' }}">
                                <a href="{{url('branch')}}">Branch</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'client-data') ? 'active' : '' }}">
                                <a href="{{url('/client-data')}}">Client</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'job-interes') ? 'active' : '' }}">
                                <a href="{{url('job-interes')}}">Job Interest</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'all-outlet') ? 'active' : '' }}">
                                <a href="{{url('all-outlet')}}">Outlets</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'outlet-open') ? 'active' : '' }}">
                                <a href="{{url('/outlet-open')}}">Outlet Open</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'product-data') ? 'active' : '' }}"> 
                                <a href="{{url('/product-data')}}">Products</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'promo') ? 'active' : '' }}"> 
                                <a href="{{url('/promo')}}">Promo</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'code-pos') ? 'active' : '' }}">
                                <a href="{{url('code-pos')}}">Zip</a>
                            </li>
                            
                             
                    
                            
                        </ul>
                    </li>

                     <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                           <i class="material-icons">layers</i>
                            <span>Report</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="#">Activity</a>
                            </li>
                            <li>
                                <a href="#">Sales Report</a>
                            </li>
                            <li>
                                <a href="#">MD Report</a>
                            </li>
                            
                        </ul>
                    </li>

                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                           <i class="material-icons">face</i>
                            <span>User Management</span>
                        </a>
                        <ul class="ml-menu">
                           
                            <li class="{{ (request()->segment(1) == 'user-cms') ? 'active' : '' }}">
                                <a href="{{url('user-cms')}}">User CMS</a>
                            </li>

                            <li class="{{ (request()->segment(1) == 'user-client') ? 'active' : '' }}">
                                <a href="{{url('user-client')}}">User Client</a>
                            </li>
                            
                            
                        </ul>
                    </li>

                </ul>
                @elseif(Auth::user()->hak_access == 2)
                    <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="{{ (request()->segment(1) == 'templete') ? 'active' : '' }}">
                        <a href="{{url('/templete')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    
                    
                    <li class="{{ (request()->segment(1) == 'all-project' || request()->segment(1) == 'task-list') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">business</i>
                            <span>Projects</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (request()->segment(1) == 'all-project') ? 'active' : '' }}">
                                <a href="{{url('all-project')}}">All Projects</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'task-list') ? 'active' : '' }}">
                                <a href="{{url('task-list')}}">Task</a>
                            </li>
                            <li>
                                <a href="#">Report</a>
                            </li>
                            
                        </ul>
                    </li>
                      <li class="{{ (request()->segment(1) == 'data-people') ? 'active' : '' }}">
                        <a href="{{url('data-people')}}">
                            <i class="material-icons">nature_people</i>
                            <span>People</span>
                        </a>
                    </li>

                      <li class="{{ (request()->segment(1) == 'structure-area-data' || 
                                     request()->segment(1) == 'branch' ||
                                     request()->segment(1) == 'client-data' ||
                                     request()->segment(1) == 'job-interes' ||
                                     request()->segment(1) == 'all-outlet' ||
                                     request()->segment(1) == 'outlet-open' ||
                                     request()->segment(1) == 'product-data' ||
                                     request()->segment(1) == 'code-pos') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">developer_board</i>
                            <span>Master</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (request()->segment(1) == 'structure-area-data') ? 'active' : '' }}">
                                <a href="{{url('structure-area-data')}}">Area Structure</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'branch') ? 'active' : '' }}">
                                <a href="{{url('branch')}}">Branch</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'client-data') ? 'active' : '' }}">
                                <a href="{{url('/client-data')}}">Client</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'job-interes') ? 'active' : '' }}">
                                <a href="{{url('job-interes')}}">Job Interest</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'all-outlet') ? 'active' : '' }}">
                                <a href="{{url('all-outlet')}}">Outlets</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'outlet-open') ? 'active' : '' }}">
                                <a href="{{url('/outlet-open')}}">Outlet Open</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'product-data') ? 'active' : '' }}"> 
                                <a href="{{url('/product-data')}}">Products</a>
                            </li>
                             <li class="{{ (request()->segment(1) == 'code-pos') ? 'active' : '' }}">
                                <a href="{{url('code-pos')}}">Zip</a>
                            </li>
                            
                             
                    
                            
                        </ul>
                    </li>

                  

                    

                    </ul>
                @elseif(Auth::user()->hak_access == 6)
                    <ul class="list">
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="{{ (request()->segment(1) == 'templete') ? 'active' : '' }}">
                            <a href="{{url('/templete')}}">
                                <i class="material-icons">home</i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        
                        
                        <li class="{{ (request()->segment(1) == 'all-project' || request()->segment(1) == 'task-list') ? 'active' : '' }}">
                            <a href="javascript:void(0);" class="menu-toggle">
                                <i class="material-icons">business</i>
                                <span>Projects</span>
                            </a>
                            <ul class="ml-menu">
                                <li class="{{ (request()->segment(1) == 'all-project') ? 'active' : '' }}">
                                    <a href="{{url('all-project')}}">All Projects</a>
                                </li>
                                <li class="{{ (request()->segment(1) == 'task-list') ? 'active' : '' }}">
                                    <a href="{{url('task-list')}}">Task</a>
                                </li>
                                
                            </ul>
                        </li>

                    </ul>

                @else
                    <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="{{ (request()->segment(1) == 'templete') ? 'active' : '' }}">
                        <a href="{{url('/templete')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    
                    
                    <li class="{{ (request()->segment(1) == 'all-project' || request()->segment(1) == 'task-list') ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">business</i>
                            <span>Projects</span>
                        </a>
                        <ul class="ml-menu">
                            <li class="{{ (request()->segment(1) == 'all-project') ? 'active' : '' }}">
                                <a href="{{url('all-project')}}">All Projects</a>
                            </li>
                            <li class="{{ (request()->segment(1) == 'task-list') ? 'active' : '' }}">
                                <a href="{{url('task-list')}}">Task</a>
                            </li>
                            <li>
                                <a href="#">Report</a>
                            </li>
                            
                        </ul>
                    </li>
                      <li class="{{ (request()->segment(1) == 'data-people') ? 'active' : '' }}">
                        <a href="{{url('data-people')}}">
                            <i class="material-icons">nature_people</i>
                            <span>People</span>
                        </a>
                    </li>
                    </ul>
                @endif
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 - 2019 <a href="javascript:void(0);">Mnest</a>.
                </div>
                <div class="version">
                    
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <a href="{{url('logout')}}"></a>
            </ul>
            
        </aside>
        <!-- #END# Right Sidebar -->
    </section>

     @yield('content')

   
    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
	
	<script src="http://cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script>
	
	<script src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>
    
     <script src="https://maps.google.com/maps/api/js?v=3&sensor=false"></script>

     <!-- GMaps PLugin Js -->
    <script src="{{ asset('plugins/gmaps/gmaps.js') }}"></script>


    <!-- Select Plugin Js -->
    <!--<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>->

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="{{ asset('plugins/jquery-countto/jquery.countTo.js') }}"></script>
     <script src="{{ asset('plugins/momentjs/moment.js') }}"></script>

    @yield('script')

    <!-- Morris Plugin Js -->
    <script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('plugins/morrisjs/morris.js') }}"></script>

    <!-- ChartJs -->
    <script src="{{ asset('plugins/chartjs/Chart.bundle.js') }}"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="{{ asset('plugins/flot-charts/jquery.flot.js') }}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('plugins/flot-charts/jquery.flot.time.js') }}"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="{{ asset('plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>
	
	<script src="{{ asset('plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>

    
  <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>
    <script src="{{ asset('js/pages/index.js') }}"></script>
    <script src="{{ asset('js/pages/forms/basic-form-elements.js') }}"></script>
    <script src="{{ asset('js/pages/maps/google.js') }}"></script>
    <script src="{{ asset('js/pages/forms/form-wizard.js') }}"></script>
    <script src="{{ asset('js/pages/forms/basic-form-elements.js') }}"></script>

    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/pages/maps/google.js') }}"></script>
    <script src="{{ asset('js/admin.js') }}"></script>
	
	<!-- clock -->
	<script src="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- CKEditor -->
  <script type="text/javascript" src="{{ asset('ckeditor/ckeditor.js')}}"></script>
  <script type="text/javascript" src="{{ asset('ckeditor/style.js')}}"></script>

    <!-- Demo Js -->
    
    <script src="{{ asset('js/demo.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script><!-- 
    <script src="js/jquery.flot.resize.min.js"></script> -->
  <!--   <script src="js/test.js"></script> -->


<!-- 
  <script src="{{ asset('js-master/people.js') }}"></script> -->
  


</body>

</html>
