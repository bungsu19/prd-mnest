<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('testing', 'Api\TestingController@test');
Route::get('test','Api\LoginController@test');

//app
Route::get('timeszone','Api\TaskListApiController@getTimesZone');
Route::get('app-android','Api\TaskListApiController@getAppAndroid');
//user
Route::post('login','Api\LoginController@getLoginDev');
Route::post('login-dev','Api\LoginController@getLoginDev');
//task
Route::post('task-list','Api\TaskListApiController@getTasklist');
Route::post('absensi-in','Api\TaskListApiController@getAbsensiIn');
Route::post('absensi-out','Api\TaskListApiController@getAbsensiOut');
Route::post('absensi-in-outlet','Api\TaskListApiController@getAbsensiOutletIn');
Route::post('absensi-out-outlet','Api\TaskListApiController@getAbsensiOutletOut');
Route::post('task-log','Api\TaskListApiController@postUpdateTask');
Route::post('task-open','Api\TaskListApiController@postTaskOpen');
Route::post('task-open-update','Api\TaskListApiController@postUpdateTaskOpen');

Route::post('list-outlet-open','Api\TaskListApiController@listOutletOpen');
Route::post('generate-task-list-by-outlet-open','Api\TaskListApiController@generateTasklistByOutlet');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('stmik-amik','Api\TestingController@getData');


// pase 2
//  register
Route::post('register-user', 'Api\RegisterController@postRegisterUser');
Route::post('login-gmail', 'Api\RegisterController@getLoginGmail');

Route::post('confirmation-interview','Api\SendInterviewController@postConfirmationInterview');
Route::post('confirmation-interview-project','Api\SendInterviewController@postConfirmationInterviewProject');
Route::post('list-interview','Api\SendInterviewController@getListInterview');
Route::post('list-interview-project','Api\SendInterviewController@getListInterviewProject');
Route::post('detail-inbox','Api\SendInterviewController@getDetailInterView');



Route::get('send-email', 'Api\TestingController@sendMail');
Route::post('register-mnest', 'Api\RegisterController@registerUser');
Route::post('login-phase2','Api\RegisterController@getLoginPhase2');
Route::post('complete-data-people','Api\RegisterController@postCompleteDataPeople');
Route::post('complete-data-people-file','Api\RegisterController@postCompleteDataPeopleWithFile');

//dropdown select area
Route::post('get-province','Api\SendInterviewController@getProvince');
Route::post('get-district','Api\SendInterviewController@getDistrict');
Route::post('get-sub-district','Api\SendInterviewController@getSubDistrict');
Route::post('get-village','Api\SendInterviewController@getVillage');
Route::post('get-job-interes','Api\SendInterviewController@getJobInteres');


// profile android
Route::post('get-profile-user','Api\ProfileController@getDeteailProfile');
Route::post('update-fisik-user','Api\ProfileController@getUpdateFisik');
Route::post('update-domisili-user','Api\ProfileController@getUpdateDomisili');
Route::post('update-npwp-user','Api\ProfileController@getUpdateNpwp');
Route::post('update-bpjs-user','Api\ProfileController@getUpdateBpjs');
Route::post('update-phone-number','Api\ProfileController@getUpdatePhoneNumber');

//post-education-user
Route::post('add-education-user','Api\ProfileController@postAddEducationUser');
Route::post('update-education-user','Api\ProfileController@postUpdateEducationUser');
Route::post('delete-education-user','Api\ProfileController@postDeleteEducationUser');

Route::post('add-bank-user','Api\ProfileController@postAddBankUser');
Route::post('update-bank-user','Api\ProfileController@postUpdateBankUser');
Route::post('delete-bank-user','Api\ProfileController@postDeleteBankUser');

Route::post('add-work-user','Api\ProfileController@postAddWorkUser');
Route::post('update-work-user','Api\ProfileController@postUpdateWorkUser');
Route::post('delete-work-user','Api\ProfileController@postDeleteWorkUser');

Route::post('add-foto-user','Api\ProfileController@postAddPictureUser');
Route::post('delete-foto-user','Api\ProfileController@postDeleteFotoUser');

Route::post('add-foto-cv-user','Api\ProfileController@postAddPictureCv');
Route::post('delete-foto-cv-user','Api\ProfileController@postDeleteCvUser');


//log-log
Route::post('log-login-android','Api\ProfileController@getLogLoginAndroid');
Route::post('history-absensi-user','Api\ProfileController@getHistoryAbsensi');
Route::post('history-absensi-outlet-user','Api\ProfileController@getHistoryAbsensiOutlet');
Route::post('history-absensi-date-user','Api\ProfileController@getHistoryAbsensiDate');
Route::post('history-absensi-outlet-date-user','Api\ProfileController@getHistoryAbsensiOutletDate');
Route::post('detail-absensi-outlet-user','Api\ProfileController@getDetailAbsensiOutletUser');

Route::post('testing-aaaaa','Api\TestingController@getTasklistBaru');
Route::post('task-active', 'Api\TaskListApiController@getTasklisActive');
Route::post('task-expire', 'Api\TaskListApiController@getTasklisExpire');
Route::post('list-shift-outlet', 'Api\TaskListApiController@getQtyShift');
Route::post('check-absensi-outlet', 'Api\TaskListApiController@CheckAbsensiOutletNews');

Route::post('Add-interes-user', 'Api\ProfileController@getAddInteresJob');
Route::post('check-interes', 'Api\ProfileController@getInteresReady');
Route::post('delete-interes-user', 'Api\ProfileController@getDeleteInteresUser');


Route::post('cekloogin','Api\TestingController@ceklogin');
Route::post('educat','Api\TestingController@getedu');

//forgot

Route::post('cek-email','Api\LoginController@getCekEmail');
Route::post('forgot-password','Api\LoginController@postForgotPassword');
Route::post('change-password','Api\LoginController@postChangePassword');

//aggree

Route::get('aggree','Api\ProfileController@getAggreeApi');


