<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StructureArea extends Model
{
  protected $table ='structure_area';
   protected $fillable =[
       'structure_name',
       'national',
       'title_structure_id',
       'description',
       'created_at	',
       'created_by',
       'updated_by',
       'delete_flag',
       'updated_at'

     ];
}
