<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    protected $table ='promo_umum';
	protected $primaryKey = 'id';
	protected $fillable =[
       'message',
       'date_promo',
       'title'

     ];
}
