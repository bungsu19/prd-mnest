<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobInteres extends Model
{
  protected $table ='job_interesting';
   protected $fillable =[
       'job_name',
       'active_flag',
       'delete_flag',
       'updated_by',
       'created_by',
       'description',
       'created_at	',
       'updated_at'

     ];
}
