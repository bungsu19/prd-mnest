<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogOutletOpen extends Model
{
    protected $table ='log_outlet_open';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'outlet_id',
       'created_at',
       'updated_at'

     ];
}
