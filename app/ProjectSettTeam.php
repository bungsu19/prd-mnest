<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectSettTeam extends Model
{
	protected $table ='project_sett_team';
	protected $fillable =[
       'project_id',
       'people_id',
       'team_id',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'

     ];
}
?>