<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aggree extends Model
{
    protected $table ='tb_aggree';
	protected $primaryKey = 'id';
	protected $fillable =[
       'title_aggree',
       'value_aggree',
       'status',
       'created_at',
       'updated_at'

     ];
}
