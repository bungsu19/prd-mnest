<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTeam extends Model
{
	protected $table ='project_team';
	protected $primaryKey = 'id';
}
?>