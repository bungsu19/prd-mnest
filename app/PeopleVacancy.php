<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleVacancy extends Model
{
	protected $table ='people_vacancy';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'interes_id',
       'active_flag',
       'delete_flag'

     ];
}
?>