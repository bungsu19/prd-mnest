<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendInterview extends Model
{
  protected $table ='send_interview';
   protected $fillable =[
       'people_id',
       'date1',
       'date2',
       'date3',
       'status',
       'date_confir',
       'created_at',
       'updated_at',
       'address',
       'name_hrd',
       'lokasi',
       'event',
       'jam1',
       'jam2',
       'jam3',
       'jam_confirm',
       'status_confirm',
       'message'

     ];
}
