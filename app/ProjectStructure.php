<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectStructure extends Model
{
	protected $table ='project_structure';
	protected $primaryKey = 'id';
}
?>