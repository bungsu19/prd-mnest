<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentPeople extends Model
{
  protected $table ='document_path';
   protected $fillable =[
       'employe_id',
       'image_path',
       'image_type',
       'created_at',
       'updated_at'

     ];
}
