<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StructureAreaStep2 extends Model
{
  protected $table ='structure_area_step2';
   protected $fillable =[
       'stucture_id',
       'title_structure_id',
       'provinsi_id',
       'city_id',
       'kecamatan',
       'desa',
       'created_at	',
       'updated_at'

     ];
}
