<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectMember extends Model
{
    protected $table ='reject_member';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'project_id',
       'reason',
       'active_flag',
       'delete_flag'

     ];
}
