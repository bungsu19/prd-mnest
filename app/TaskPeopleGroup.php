<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskPeopleGroup extends Model
{
    protected $table ='task_people_group';
	protected $primaryKey = 'id';
}
