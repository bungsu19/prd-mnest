<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
   protected $table ='tb_inbox';
   protected $fillable =[
       'message',
       'id_interview',
       'status_code',
       'status_read',
       'people_id',
       'created_at	',
       'updated_at',
       'status_confirm'

     ];
}
