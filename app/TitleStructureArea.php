<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TitleStructureArea extends Model
{
  protected $table ='title_structure_area';
   protected $fillable =[
       'title_structure',
       'national',
       'description',
       'created_by',
       'updated_by',
       'delete_flag',
       'created_at	',
       'updated_at'

     ];
}
