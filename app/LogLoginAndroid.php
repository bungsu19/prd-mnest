<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogLoginAndroid extends Model
{
    protected $table ='log_login_android';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'date_login',
       'jam_login',
       'created_at',
       'updated_at'

     ];
}
