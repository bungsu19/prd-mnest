<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model
{
	protected $table ='project_member';
	protected $primaryKey = 'id';
}
?>