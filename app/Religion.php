<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
  protected $table ='m_religion';
   protected $fillable =[
       'name_religion',
       'created_at	',
       'updated_at'

     ];
}
