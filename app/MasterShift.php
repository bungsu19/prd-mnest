<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterShift extends Model
{
   protected $table ='master_shift';
   protected $fillable =[
       'id',
       'shift',
       'hour_in',
       'hour_out'
     ];
}
