<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendInterviewProject extends Model
{
  protected $table ='send_interview_project';
   protected $fillable =[
       'people_id',
       'project_id',
       'date1',
       'date2',
       'date3',
       'status',
       'date_confirm',
       'created_at',
       'updated_at',
       'jam1',
       'jam2',
       'jam3',
       'jam_confirm',
       'status_confirm',
       'lokasi',
       'event',
       'address',
       'name',
       'message',
       'active_flag'

     ];
}
