<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateLogMoveTeam extends Model
{
	protected $table ='log_change_team';
	protected $primaryKey = 'id';
}
?>