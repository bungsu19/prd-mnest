<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbsensiOutlet extends Model
{
    protected $table ='absensi_outlet_people';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'project_id',
       'task_id',
       'outlet_id',
       'absensi_in',
       'absensi_out',
       'in_langitude',
       'in_latitude',
       'out_langitude',
       'out_latitude',
       'date_absensi',
       'date_absensi_out',
       'schedule_date',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at',
       'date_create_task'

     ];
}
