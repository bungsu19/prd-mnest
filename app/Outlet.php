<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
  protected $table ='outlet';
   protected $fillable =[
       'branch_id',
       'outlet_category_id',
       'outlet_name',
       'outlet_address1',
       'outlet_address2',
       'outlet_tr_rw',
       'outlet_rw',
       'outlet_village',
       'outlet_sub_district',
       'outlet_city',
       'outlet_province',
       'outlet_post_code',
       'outlet_phone1',
       'outlet_phone2',
       'outlet_email_address',
       'owner_name',
       'npwp',
       'outlet_langitude',
       'outlet_latitude',
       'image1',
       'image2',
       'image3',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at',
       'status',
       'people_id'

     ];
}
