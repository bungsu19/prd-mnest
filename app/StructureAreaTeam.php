<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StructureAreaTeam extends Model
{
  protected $table ='structure_area_team';
   protected $fillable =[
       'structure_area_step2_id',
       'title_structure_id',
       'name_team',
       'created_at	',
       'updated_at'

     ];
}
