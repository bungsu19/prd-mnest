<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationPeople extends Model
{
  protected $table ='education';
   protected $fillable =[
       'employe_id',
       'education_lvl',
       'education_institution_name',
       'education_location',
       'education_major',
       'periode_from',
       'periode_to',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'

     ];
}
