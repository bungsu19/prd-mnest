<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingShift extends Model
{
   protected $table ='setting_shift_project';
   protected $fillable =[
       'project_id',
       'outlet_id',
       'code_shift',
       'hour_in',
       'hour_out',
       'active_flag',
       'delete_flag',
       'created_at',
       'updated_at'

     ];
}
