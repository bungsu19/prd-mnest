<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectVacancy extends Model
{
	protected $table ='project_vacancy';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'interes_id',
       'delete_flag',
       'created_at	',
       'updated_at'

     ];
}
?>