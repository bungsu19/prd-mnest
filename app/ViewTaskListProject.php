<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewTaskListProject extends Model
{
	protected $table ='view_task_project_test';
	protected $primaryKey = 'id';
}
?>