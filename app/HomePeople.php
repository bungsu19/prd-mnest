<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePeople extends Model
{
  protected $table ='home_people';
   protected $fillable =[
       'employe_id',
       'address1',
       'address2',
       'rt_rw',
       'village',
       'sub_district',
       'regency_city',
       'province',
       'post_code',
       'home_code',
       'home_sts',
       'delete_flag',
       'created_at',
       'updated_at'

     ];
}
