<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EncryptServiceProvider extends ServiceProvider
{
    public function encrypt()
	{
	   require_once app_path() . '/Helpers/Encrypt.php';
	}
}
