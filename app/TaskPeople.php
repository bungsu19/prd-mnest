<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskPeople extends Model
{
    protected $table ='task_people';
	protected $primaryKey = 'id';
	protected $fillable =[
       'people_id',
       'project_id',
       'outlet_id',
       'task_start',
       'task_end',
       'description',
       'schedule_date',
       'status',
       'status_flag',
       'received_date',
       'task_type',
       'task_number',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at',
       'code_shift'

     ];
}
