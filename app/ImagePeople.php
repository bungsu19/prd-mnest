<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagePeople extends Model
{
  protected $table ='image_people';
   protected $fillable =[
       'employe_id',
       'image_type',
       'image',
       'created_by',
       'updated_by',
       'delete_flag',
       'active_flag',
       'created_at',
       'updated_at'

     ];
}
