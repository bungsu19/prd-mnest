<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacilityPeople extends Model
{
  protected $table ='facility';
   protected $fillable =[
       'employe_id',
       'expected_salary',
       'allowance',
       'facility',
       'facility',
       'active_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'

     ];
}
