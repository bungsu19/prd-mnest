<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FamilyPeople extends Model
{
  protected $table ='family_people';
   protected $fillable =[
       'employe_id',
       'member_code',
       'member_name',
       'member_gender',
       'member_last_edu',
       'member_dob',
       'member_pob',
       'member_phone1',
       'member_phone2',
       'member_company',
       'member_company_address',
       'member_job_position',
       'residence_city',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'
     ];
}
