<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankPeople extends Model
{
  protected $table ='bank_people';
   protected $fillable =[
       'employe_id',
       'bank_acc_no',
       'bank_acc_name',
       'bank_name',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'

     ];
}
