<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
	protected $table ='office_branch';
	protected $primaryKey = 'id';

	function Client(){
		return $this->hasMany('App\Client','id');
	}
}
?>