<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Client;
use Session;
use Redirect;
use Excel;
use Illuminate\Support\Facades\Storage;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use EncryptHelp;

class ClientV2Controller extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jumlahData = 10;
    	$data = DB::table('tb_client')->where('delete_flag',1)->orderBy('client_id','desc')->paginate($jumlahData);
		$paginator = $data;
    	return view('client.data_client',compact('data','jumlahData','paginator')); 
    }

    public function getAddClient()
    {
    	$branch = DB::table('office_branch')->orderBy('id','desc')->get();
    	$province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
    	return view('client.add_client',compact('branch','province'));
    }

    public function postAddClient(Request $request)
    {
    	try{
            $chek = Client::where('company_name', strtoupper($request->client_name))->where('company_email',$request->client_email)->first();
            if($chek){
                return Redirect()->back()->with('failed','Client already exist');
            }else{
                $user = Auth::user();
                $data = new Client();
                $data->branch_id = $request->branch;
                $data->company_name = strtoupper($request->client_name);
                $data->company_address1 = $request->client_addres1;
                $data->company_address2 = $request->client_addres2;
                $data->company_rt = $request->client_rt;
                $data->company_rw = $request->client_rw;
                $data->company_province = $request->client_province;
                $data->company_regency = $request->client_district;
                $data->company_city = $request->client_district;
                $data->company_subdistrict = $request->client_subdistrict;
                $data->company_village = $request->client_village;
                $data->company_postcode = $request->client_post_code;
                $data->company_phone1 = $request->client_phone1;
                $data->company_phone2 = $request->client_phone2;
                $data->company_fax = $request->client_fax;
                $data->company_email = $request->client_email;
                $data->website_url = $request->client_website;
                $data->contact_person = $request->clinet_person;
                $data->contact_person_phone = $request->clinet_person_phone;
                $data->npwp = $request->client_npwp;
                $data->active_flag = '1';
                $data->delete_flag = '1';
                $data->created_by = $user->name;
                $data->updated_by = $user->name;
                $foto = $request->file('image');
            
                if ($request->hasFile('image')) {
					
				    $image       = $request->file('image');
					$ext = $image->getClientOriginalExtension();
					$data->image = date('YmdHis').".".$ext;

					$image_resize = Image::make($image->getRealPath());              
					$image_resize->resize(300, 300);
					$image_resize->save(public_path('client/' .$data->image));
                }

                $data->save();
				
				if($data->client_id != null){
					return Redirect('client-data')->with('message', 'Save data client successfully');
				}else{
					return Redirect('client-data')->with('gagal', 'Save data client failed');
				}

            }
		    	
		} catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('gagal', 'Save data client failed');
        }


    }

    public function getEditClient($id)
    {
    	$data = DB::table('tb_client')->where('client_id', EncryptHelp::decrypt($id))->first();
    	$branch = DB::table('office_branch')->orderBy('id','desc')->get();
    	$province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();

    	return view('client.edit_client',compact('data','province','branch'));

    }

    public function testing()
    {
        return EncryptHelp::encrypt('123')."<==>".EncryptHelp::decrypt('eyJpdiI6Im1RXC96YlJkYUQxbGhvbHcrUHRJYk13PT0iLCJ2YWx1ZSI6IndHcll6cE9lSWFQS21SdmdJRWVQZWc9PSIsIm1hYyI6IjRhYjE3NDU4MzVlYTI0NmM0Njg0ODcxYzgxOWNiMmRiMTA4MTVkMDRiZDE4MzU1YTEyZjUyY2U0NTY0Y2Y2YjYifQ==');

    }

    public function postEditClient(Request $request)
    {
        try{
        	$user = Auth::user();
        	$update = Client::where('client_id',$request->id)->first();
        	$update->branch_id = $request->branch;
        	$update->company_name = strtoupper($request->client_name);
        	$update->company_address1 = $request->client_addres1;
        	$update->company_address2 = $request->client_addres2;
        	$update->company_rt = $request->client_rt;
        	$update->company_rw = $request->client_rw;
        	$update->company_province = $request->client_province;
        	$update->company_regency = $request->client_district;
        	$update->company_city = $request->client_district;
        	$update->company_subdistrict = $request->client_subdistrict;
        	$update->company_village = $request->client_village;
        	$update->company_postcode = $request->client_post_code;
        	$update->company_phone1 = $request->client_phone1;
        	$update->company_phone2 = $request->client_phone2;
        	$update->company_fax = $request->client_fax;
        	$update->website_url = $request->client_website;
        	$update->contact_person = $request->clinet_person;
        	$update->contact_person_phone = $request->clinet_person_phone;
        	$update->npwp = $request->client_npwp;
        	$update->updated_by = $user->name;
			
            if($request->file('image') == "")
                { 
                    $update->image = $update->image;
                } 
                else
                {
					
					$image       = $request->file('image');
					$ext = $image->getClientOriginalExtension();
					$update->image = date('YmdHis').".".$ext;

					$image_resize = Image::make($image->getRealPath());              
					$image_resize->resize(300, 300);
					$image_resize->save(public_path('client/' .$update->image));

					$image_path = 'client/'.$request->fotoold;
					if(file_exists($image_path)) {
						File::delete($image_path);
					}
                }

        	$update->update();
			
			if($update->client_id != null){
				return Redirect('client-data')->with('message', 'Update data client successfully');
			}else{
				return Redirect('client-data')->with('gagal', 'Update data client failed');
			}

         } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data client failed');
        }
    }

    public function getDetailClinet($id)
    {
    	$data = Client::where('client_id',$id)->first();
    	$branch = DB::table('office_branch')->orderBy('id','desc')->get();
    	return view('client.detail_client',compact('data','branch'));
    }

    public function postDeleteClient($id)
    {
       $delete = DB::table('tb_client')->where('client_id', $id)->update(['delete_flag' => 0]); 
       if($delete){
             return redirect()->back()->with('message', 'Delete data client successfully');
       }else{
         return redirect()->back()->with('failed', 'Delete data client failed');
       }
      
    }

    public function getFilterData(Request $request)
    {
    	$search = $request->seacrh;
    	if($search == 1){
    		$data = DB::table('tb_client')->orderBy('client_id','desc')->get();
    	   return view('client.data_client',compact('data')); 
		}else{
			$data = DB::table('tb_client')->where('delete_flag',1)->orderBy('client_id','asc')->get();
    	   return view('client.data_client',compact('data'));
		}   	 

    }

     private function uploadFotoClient(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'client';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

	public function importClient(Request $request)
    {
        if($request->hasFile('import')){
            Excel::load($request->file('import')->getRealPath(), function ($reader) {
				
				$temp = array();
                
                foreach ($reader->toArray() as $key => $row) {
					
					$check = Client::where('company_name', strtoupper($row['company_name']))->where('company_email', $row['company_email'])->first();
					
					if($check == null){
						$client = new Client();
						$client->branch_id            =  $row['branch_id'];
						$client->company_name         =  strtoupper($row['company_name']);
						$client->company_address1     =  $row['company_address1'];
						$client->company_address2     =  $row['company_address2'];
						$client->company_rt           =  $row['company_rt'];
						$client->company_rw           =  $row['company_rw'];
						$client->company_province     =  $row['company_province'];
						$client->company_city         =  $row['company_city'];
						$client->company_subdistrict  =  $row['company_subdistrict'];
						$client->company_village      =  $row['company_village'];
						$client->company_postcode     =  $row['company_postcode'];
						$client->company_phone1       =  $row['company_phone1'];
						$client->company_phone2       =  $row['company_phone2'];
						$client->company_fax          =  $row['company_fax'];
						$client->company_email        =  $row['company_email'];
						$client->website_url          =  $row['webiste_url'];
						$client->contact_person       =  $row['contact_person'];
						$client->contact_person_phone =  $row['contact_person_phone'];
						$client->npwp                 = $row['npwp'];
						$client->active_flag          = '1';
						$client->delete_flag          = '1';
						$client->save();
					}else{
						$temp[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
									  .'<div class="col-sm-4">'
											.$row['company_name']
									  .'</div>'
									  .'<div class="col-sm-4">'
											.$row['company_email']
									  .'</div>'
								  .'</div>';
					}
					
					if(count($temp) > 0){
						Session::flash('failed', $temp);
						Session::flash('gagal', count($temp).' Data Already Exists');	
					}else{
						Session::flash('message', 'Import data client successfully');
					}
                    
                }
            });
            return redirect('client-data');
        }else{
			Session::flash('gagal', 'File not selected');
			return redirect('client-data');
		}  
    }
    
    public function downloadDataClient()
    {
        return response()->download('template_excel/example_client.xlsx'); 
    }

}