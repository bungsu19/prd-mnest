<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Client;
use App\Branch;
use App\Product;
use Session;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fisrtpage');
    }

     public function getViewLogin()
     {
        return view('auth.login');
     }

    public function templete()
    {
        return view('fisrtpage');
    }

    public function realtime()
    {
        $search = '';
        return view('realtime',compact('search'));
    }

    public function getdata()
    {
        $getstamps = DB::table('office_branch')->get();

       return response()->json(array('success' => true, 'getstamps' => $getstamps));
    }

    public function totalProject()
    {
        $projectTotal = DB::table('project')->count();
        $totalEmployee = DB::table('employee_people')->where('status_project',1)->count();
        $totalCandidate = DB::table('employee_people')->where('status_project',0)->count();
        $outlet = DB::table('outlet')->where('status',1)->count();
        $outletOpen = DB::table('outlet')->where('status',0)->count();
        $client = DB::table('tb_client')->count();

        return response()->json(array('success' => true, 'totalProject' => $projectTotal, 'employe' => $totalEmployee, 'candidate' => $totalCandidate,'outlet'=> $outlet,'outletOpen' => $outletOpen,'client' => $client ));
    }
}
