<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Project;
use App\TaskPeople;
use App\TaskPeopleGroup;
use App\ProjectStructure;
use App\Absensi;
use App\AbsensiOutlet;
use App\ProjectSettTeam;
use App\SettingShift;
use App\Outlet;
use Session;
use Excel;
use Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use DateTime;

class TaskController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $dt_user = DB::table('users')->where('id',$user->id)->first();
        if($dt_user->hak_access == 4){
            $jumlahData = 10;
            $data = DB::table('view_project')->where('user_id',$dt_user->id)->orderBy('id','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('TaskList.index_data',compact('data','jumlahData','paginator'));
        }elseif($dt_user->hak_access == 6){
            $jumlahData = 10;
            $data = DB::table('view_project')->where('id',$dt_user->project_id)->orderBy('id','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('TaskList.index_data',compact('data','jumlahData','paginator'));
        }else{
            $jumlahData = 10;
            $data = DB::table('view_project')->orderBy('id','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('TaskList.index_data',compact('data','jumlahData','paginator'));
        }
        
    }

    public function getTaskProject($id)
    {
         $jumlahData = 10;
         $project = Project::where('id',$id)->first();
         $structure = ProjectStructure::where('project_id',$id)->first();
         $job = DB::table('view_project_vacancy')->where('project_id',$id)->get();
         $task_group = DB::table('view_task_group')->where('project_id',$id)->get();
         $outlet = DB::table('outlet')->orderBy('id','desc')->get();

         $structure = ProjectStructure::where('project_id',$id)->first();
          $m_district = array();
         if($structure == NULL){
           $m_district = [];
         }else{
             $m_district = DB::table('structure_area_step2')->where('title_structure_id',$structure->structure_id)->distinct('district')->get();
         }
        
        $member = DB::table('task_people_project')->where('project_id',$id)->where('status',1)->distinct('employee_id')->paginate($jumlahData);
		$paginator = $member;
        
         return view('TaskList.task_project',compact('project','member','job','outlet','task_group','m_district','jumlahData', 'paginator'));
    }

    public function getAddTaskPeople($id)
    {
        $jumlahData = 10;
        $member = DB::table('task_people_project')->where('id',$id)->first();   
        $project = Project::where('id',$member->project_id)->first();
        $m_outlet = DB::table('outlet')->where('outlet_city',$member->distict)->get();

        $data = DB::table('view_task_people2')->where('project_id',$project->id)->where('people_id',$member->employee_id)->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $data;
        
        return view('TaskList.task_people',compact('project','member','m_outlet','data', 'jumlahData', 'paginator'));
    }

    public function postAddTaskPeople(Request $request)
    {
        $project = Project::where('id',$request->project_id)->first();
        $day = date('Y-m-d');
        if($day >= $project->project_start && $day <= $project->project_end ){
            
            $outlet = $request->outlet_id;
            $count = DB::table('task_people')->where('people_id', $request->people_id)->get();
            $next_sequence = (int)$count->count() + 1;
            $user = Auth::user();
            $chekShift = [];
            $message = [];
            $message2 = [];
            for ($i=0; $i < count($outlet); $i++) { 

                $chekOutletShift = SettingShift::where('project_id', $request->project_id)->where('outlet_id',$outlet[$i])->first();


                $chekShift[] = $chekOutletShift;

                if($chekOutletShift != null){

                    $countShiftOutlet = SettingShift::where('project_id', $request->project_id)
                                            ->where('outlet_id',$outlet[$i])
                                            ->where('code_shift',$request->shift)
                                            ->first();
                    if ($countShiftOutlet != null) {
                      
                        $task = array();
                        $task['people_id'] = $request->people_id;
                        $task['project_id'] = $request->project_id;
                        $task['outlet_id'] = $request->outlet_id[$i];
                        $task['schedule_date'] = date('Y-m-d', strtotime($request->schedule));
                        $task['task_start'] = date('Y-m-d', strtotime($request->task_start));
                        $task['task_end'] = date('Y-m-d', strtotime($request->task_end));
                        $task['description'] = $request->desc;
                        $task['task_type'] = $request->task_type;
                        $task['code_shift'] = $request->shift;
                        $task['task_number'] = "TS".date('Ymd').$request->people_id.$next_sequence;
                        $task['active_flag'] = '1';
                        $task['status'] = '1';
                        $task['status_flag'] = '0';
                        $task['delete_flag'] = '1';
                        $task['created_by'] = $user->name;
                        $task['updated_by'] = 'system';
                        $insertTask = TaskPeople::create($task);
                        if($insertTask){
                            $leader = ProjectSettTeam::where('team_id',$request->team_id)->first();
                            if($leader){
                                $taskleeader = array();
                                $taskleeader['people_id'] = $leader->people_id;
                                $taskleeader['project_id'] = $leader->project_id;
                                $taskleeader['outlet_id'] = $request->outlet_id[$i];
                                $taskleeader['schedule_date'] = date('Y-m-d', strtotime($request->schedule));
                                $taskleeader['task_start'] = date('Y-m-d', strtotime($request->task_start));
                                $taskleeader['task_end'] = date('Y-m-d', strtotime($request->task_end));
                                $taskleeader['description'] = $request->desc;
                                $taskleeader['task_type'] = $request->task_type;
                                $taskleeader['code_shift'] = $request->shift;
                                $taskleeader['task_number'] = "TS".date('Ymd').$request->people_id.$next_sequence;
                                $taskleeader['active_flag'] = '1';
                                $taskleeader['status'] = '1';
                                $taskleeader['status_flag'] = '0';
                                $taskleeader['delete_flag'] = '1';
                                $taskleeader['created_by'] = $user->name;
                                $taskleeader['updated_by'] = 'system';
                                $insertTaskleader = TaskPeople::create($taskleeader);  
                            }
                            
                        }
                    }else{

                         $NameOutlet = Outlet::where('id',$outlet[$i])->first();
                         $message2[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
                                      .'<div class="col-sm-12">'
                                            .'shift '.$request->shift.' '.'di'.$NameOutlet->outlet_name.' belum di setting'
                                      .'</div>'
                                  .'</div>';

                    }

                    
                }else{
                    $NameOutlet = Outlet::where('id',$outlet[$i])->first();
                    $message[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
                                      .'<div class="col-sm-12">'
                                            .'outlet name '.$NameOutlet->outlet_name.' belum di setting shift'
                                      .'</div>'
                                  .'</div>';
                }
                 
             }
             if (count($message) > 0) {
                Session::flash('gagal', $message);
             }
             if (count($message2) > 0) {
                Session::flash('gagal2', $message2);
             }
            

             return redirect()->back()->with('message','Save task list project Successfully');
        }else{
           
            return redirect()->back()->with('failed','Check range project');
        }
       


        
    }

    public function postAddTaskGroup(Request $request)
    {
        $user = Auth::user();
        $task_group = new  TaskPeopleGroup();
        $task_group->project_id = $request->project_id;
        $task_group->outlet_id = $request->outlet_id;
        $task_group->interes_id = $request->interes_id;
        $task_group->task_start = date('Y-m-d', strtotime($request->task_start));
        $task_group->task_end = date('Y-m-d', strtotime($request->task_end));
        $task_group->description = $request->desc;
        $task_group->status = '1';
        $task_group->active_flag = '1';
        $task_group->delete_flag = '1';
        $task_group->created_by = $user->name;
        $task_group->updated_by = 'system';

        $task_group->save();

        return redirect()->back();
    }

    public function getViewTaskProject($id)
    {
		$jumlahData = 10;
		$active = 'job_vacancy';
		if(Input::get('data') == 'task_people'){
			$active = 'job_vacancy';
		}elseif(Input::get('data') == 'absensi'){
			$active = 'personal_data';
		}
        
		$people_id = 'all';
		$people_id_tasklist = 'all';
		$value = '';
		$dateFrom = '';
		$dateTo = '';
		$dateFromTasksList = '';
		$dateToTasksList = '';
        $project = Project::where('id',$id)->first();
        $task = DB::table('task_people')->where('project_id',$id)->get();
        foreach ($task as $key) {
            $key->qty_task = DB::table('task_people')->where('project_id',$id)->where('people_id',$key->people_id)->count();

        }
        $task_people = DB::table('view_task_project_ok')
                                        ->where('project_id',$id)
                                        ->orderBy('first_name')
                                        ->orderBy('schedule_date','desc')
                                        ->paginate($jumlahData, ['*'], 'task_people');
        $absensi = DB::table('absensi_people_view')->where('project_id',$id)->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
        $member = DB::table('view_project_member')->where('project_id',$id)->orderBy('first_name')->get();
		
		$paginator = $task_people;
		$paginator2 = $absensi;
		
		$task_people->appends(['data' => 'task_people']);
		$task_people->appends(['absensi' => $absensi->currentPage()]);
		
		$absensi->appends(['data' => 'absensi']);
		$absensi->appends(['task_people' => $task_people->currentPage()]);
		
        return view('TaskList.view_task_project',compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList', 'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData', 'paginator', 'paginator2', 'value'));
    }

    public function getDetailTaskProject($id,$people,$date)
    {

        $project = Project::where('id',$id)->first();
        $detail_task = DB::table('view_detail_task')->where('project_id',$id)->where('people_id',$people)->where('schedule_date',$date)->get();

        return view('TaskList.detail_view_task',compact('project','detail_task'));
    }

    public function getAbsensiProject($people,$schedule,$project)
    {
        
        $project = Project::where('id',$project)->first();
        $absensi = Absensi::where('people_id',$people)->get();
        $absensi_outlet = DB::table('detail_absensi_outlet')->where('task_id',$people)->where('schedule_date',$schedule)->get();
        return view('TaskList.absensi_tasklist',compact('project','absensi','absensi_outlet'));
    }

    

        public function getFilterAbsensi2(Request $request)
    {
        if($request->submit == 'download'){
            $people = $request->people;
            $from = date('Y-m-d', strtotime($request->dateFrom));
            $to = date('Y-m-d', strtotime($request->dateTo));
            $active = 'personal_data';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();
            if($project_id != null){
                if($people == 'all' && $request->dateFrom == null && $request->dateTo == null){
                     $absensi = DB::table('data_report_excel')->where('project_id',$project_id)->orderBy('full_name')->orderBy('date_absensi','desc')->get();
                     
					 return view('TaskList.absensi', compact('absensi'));

                }elseif($people == 'all' && $request->dateFrom != null && $request->dateTo != null){
                    $absensi = DB::table('data_report_excel')->where('project_id',$project_id)->WhereBetween('date_absensi',array($from,$to))->orderBy('full_name')->orderBy('date_absensi','desc')->get();
                    return view('TaskList.absensi', compact('absensi'));
                }elseif($people != 'all' && $request->dateFrom != null && $request->dateTo != null){
                     $absensi = DB::table('data_report_excel')->where('project_id',$project_id)->where('people_id',$people)->WhereBetween('date_absensi',array($from,$to))->orderBy('full_name')->orderBy('date_absensi','desc')->get();
                    return view('TaskList.absensi', compact('absensi'));

                }elseif($people != 'all'){
                 
                     $absensi = DB::table('data_report_excel')->where('project_id',$project_id)->where('people_id',$people)->orderBy('full_name')->orderBy('date_absensi','desc')->get();
                    return view('TaskList.absensi', compact('absensi'));

                }else{
                   
                    $absensi = DB::table('data_report_excel')->where('project_id',$project_id)->where('people_id',$people)->orderBy('full_name')->orderBy('date_absensi','desc')->WhereBetween('date_absensi',array($from,$to))->get();
                    return view('TaskList.absensi', compact('absensi'));
                }
                return view('TaskList.view_task_project',compact('project','task_people','task','absensi','member','active'));
            }
        }else{
			$jumlahData = 10;
            $people = $request->people;
            $from = date('Y-m-d', strtotime($request->dateFrom));
            $to = date('Y-m-d', strtotime($request->dateTo));
            $active = 'personal_data';
			if(Input::get('data') == 'task_people'){
				$active = 'job_vacancy';
			}elseif(Input::get('data') == 'absensi'){
				$active = 'personal_data';
			}
            $people_id = $request->people;
            $people_id_tasklist = 'all';
            $dateFrom = '';
            $dateTo = '';
            $dateFromTasksList = '';
			$value = '';
            $dateToTasksList = '';
            $project_id = $request->project_id;
            
            if($project_id != null){
				
				$project = Project::where('id',$project_id)->first();
				$task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
				$member = DB::table('view_project_member')->where('project_id',$project_id)->get();
				
                if($people == 'all' && $request->dateFrom == null && $request->dateTo == null){
                    
                    $absensi = DB::table('absensi_people_view')->where('project_id',$project_id)->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }elseif($people == 'all' && $request->dateFrom != null && $request->dateTo != null){
                    
                    $dateFrom = date('d-m-Y', strtotime($request->dateFrom));
                    $dateTo = date('d-m-Y', strtotime($request->dateTo));
                    $absensi = DB::table('absensi_people_view')->where('project_id',$project_id)->whereBetween('date_absensi', [$from,$to])->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');                    
                    
                }elseif($people != 'all' && $request->dateFrom != null && $request->dateTo != null){
                    
                    $dateFrom = date('d-m-Y', strtotime($request->dateFrom));
                    $dateTo = date('d-m-Y', strtotime($request->dateTo));
                    $absensi = DB::table('absensi_people_view')->where('project_id',$project_id)->where('people_id',$people)->whereBetween('date_absensi', [$from,$to])->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }elseif($people != 'all'){
                    
                    $absensi = DB::table('absensi_people_view')->where('project_id',$project_id)->where('people_id',$people)->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }else{

                    $absensi = DB::table('absensi_people_view')->where('project_id',$project_id)->where('people_id',$people)->whereBetween('date_absensi',array($from,$to))->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                    
                }
				
        				$task_people->appends(['data' => 'task_people']);
        				$task_people->appends(['absensi' => $absensi->currentPage()]);
        				$task_people->appends($request->only('project_id'));
        				$task_people->appends($request->only('people'));
        				
        				$absensi->appends(['data' => 'absensi']);
        				$absensi->appends(['task_people' => $task_people->currentPage()]);
				
                $absensi->appends($request->only('people'));
                $absensi->appends($request->only('project_id'));
                $absensi->appends($request->only('dateFrom'));
                $absensi->appends($request->only('dateTo'));
				
				$paginator = $task_people;
				$paginator2 = $absensi;
                
                 return view('TaskList.view_task_project',compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList', 'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData','paginator','paginator2', 'value'));
               
            }
        }

    }

		
		public function getFilterTaskList2(Request $request){
			
			$jumlahData = 10;
			$people = $request->people_id_tasklist;
            $from = date('Y-m-d', strtotime($request->dateFromTasksList));
            $to = date('Y-m-d', strtotime($request->dateToTasksList));
            $active = 'job_vacancy';
			if(Input::get('data') == 'task_people'){
				$active = 'job_vacancy';
			}elseif(Input::get('data') == 'absensi'){
				$active = 'personal_data';
			}
			$people_id = 'all';
			$people_id_tasklist = $request->people_id_tasklist;
			$dateFrom = '';
			$dateTo = '';
			$value = $request->value;
			$dateFromTasksList = '';
			$dateToTasksList = '';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();

            if($project_id != null){
				
				$absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->paginate($jumlahData, ['*'], 'absensi');
                $member = DB::table('view_project_member')->where('project_id',$project_id)->get();

                if($people == 'all' && $request->dateFromTasksList == null && $request->dateToTasksList == null){
					
					$task_people = DB::table('view_task_project_ok')->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }elseif($people == 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){
					
    					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
    					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
    					$task_people = DB::table('view_task_project_ok')->whereBetween('schedule_date', [$from,$to])->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }elseif($people == 'name'){
					
                    $task_people = DB::table('view_task_project_ok')->where('project_id',$project_id)->where('first_name', 'like', '%' . $request->value . '%')->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');

                }elseif($people == 'task_list'){
					
                    $task_people = DB::table('view_task_project_ok')->where('project_id',$project_id)->where('task_number', 'like', '%' . $request->value . '%')->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }else{

                    $task_people = DB::table('view_task_project_ok')->where('project_id',$project_id)->orderBy('first_name')->orderBy('schedule_date','desc')->paginate($jumlahData, ['*'], 'task_people');
					
                }
				
				$absensi->appends(['data' => 'absensi']);
				$absensi->appends(['task_people' => $task_people->currentPage()]);
				$absensi->appends($request->only('people_id_tasklist'));
				$absensi->appends($request->only('project_id'));
				
				$task_people->appends(['data' => 'task_people']);
				$task_people->appends(['absensi' => $absensi->currentPage()]);
				
				$task_people->appends($request->only('people_id_tasklist'));
				$task_people->appends($request->only('project_id'));
				$task_people->appends($request->only('dateFromTasksList'));
				$task_people->appends($request->only('dateToTasksList'));
				$task_people->appends($request->only('value'));
				
				$paginator = $task_people;
				$paginator2 = $absensi;
				
				return view('TaskList.view_task_project', compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList' ,'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo', 'jumlahData','paginator','paginator2', 'value'));
               
			}
		}
	
    // Ajax get Outlet from city
    public function getOutlet(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Outlet -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('outlet')->select('id', 'outlet_name')->where('outlet_city',$request->data_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->outlet_name.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
	
	//ajax get team from city
	public function getTeam(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required',
            'project_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Team -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('view_distric_team')->select('id', 'name_team','district')->where('delete_flag', 1)->where('project_id',$request->project_id)->orderBy('name_team')->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->name_team.'('.$value->district.')'.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }

    //ajax get team from city
    public function getShiftOutlet(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required',
            'project_id' => 'required',
            'outlet_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Shift -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('setting_shift_project')->select('id', 'code_shift')->where('project_id',$request->project_id)->where('delete_flag',1)->where('outlet_id',$request->outlet_id)->get();
            foreach ($village as $value) {

                if($value->code_shift == 1){
                    $a = 'shift I';
                }elseif ($value->code_shift == 2) {
                    $a = 'shift II';
                }else{
                    $a = 'shift III';
                }
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->code_shift.'">'.$a.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
	
	public function getMember(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $village = DB::table('view_project_member')->select('employee_id', 'first_name','middle_name','surname')->where('team_id',$request->data_id)->get();
            return json_encode($village);
        }
    }
	
	

    public function postInsertGroupTaskList(Request $request)
    {
       
           $project = Project::where('id',$request->project_id)->first();
           $day = date('Y-m-d');
           if($day >= $project->project_start && $day <= $project->project_end){
                $project_id = $request->project_id;
                $outlet_id = $request->outlet_id;
                $people_id = $request->people_id;
                 
                $tim = $request->team;
                if($people_id[0] == 'ALL' ){

                    $chekShift = SettingShift::where('project_id',$request->project_id)->where('outlet_id',$request->outlet_id)->first();
                    if ($chekShift != null) {
                        $data_member = DB::table('view_project_member')->where('team_id',$tim)->get();
                    $randomm = "TG".date('Ymd').rand(10,1000);
                    foreach($data_member as $mem){
                        $count = DB::table('task_people')->where('project_id', $project_id)->get();
                        $next_sequence = rand(10,1000);
                        $user = Auth::user();
                        $task = array();
                        $task['people_id']      = $mem->employee_id;
                        $task['project_id']     = $project_id;
                        $task['outlet_id']      = $outlet_id;
                        $task['schedule_date']  = date('Y-m-d', strtotime($request->schedule_date));
                        $task['task_start']     = date('Y-m-d', strtotime($request->task_start));
                        $task['task_end']       = date('Y-m-d', strtotime($request->task_end));
                        $task['description']    = $request->desc;
                        $task['task_type']      = $request->task_type;
                        $task['code_shift']      = $request->shift;
                        $task['task_number']    = $randomm;
                        $task['active_flag']    = '1';
                        $task['status']         = '1';
                        $task['status_flag']    = '0';
                        $task['delete_flag']    = '1';
                        $task['created_by']     = $user->name;
                        $task['updated_by']     = 'system group all';

                        $insertTask = TaskPeople::create($task);
                    }

                        $leader = ProjectSettTeam::where('team_id',$tim)->first();
                        if($leader){
                            $leadertask = array();
                            $leadertask['people_id']      = $leader->people_id;
                            $leadertask['project_id']     = $project_id;
                            $leadertask['outlet_id']      = $outlet_id;
                            $leadertask['schedule_date']  = date('Y-m-d', strtotime($request->schedule_date));
                            $leadertask['task_start']     = date('Y-m-d', strtotime($request->task_start));
                            $leadertask['task_end']       = date('Y-m-d', strtotime($request->task_end));
                            $leadertask['description']    = $request->desc;
                            $leadertask['task_type']      = $request->task_type;
                            $task['code_shift']      = $request->shift;
                            $leadertask['task_number']    = $randomm;
                            $leadertask['active_flag']    = '1';
                            $leadertask['status']         = '1';
                            $leadertask['status_flag']    = '0';
                            $leadertask['delete_flag']    = '1';
                            $leadertask['created_by']     = $user->name;
                            $leadertask['updated_by']     = 'system group leader all';
                            TaskPeople::create($leadertask);
                        }
                    }else{
                         return redirect()->back()->with('failed','Outlet belum di setting shift');
                    }
                  
                    

            }else{
                $chekShift = SettingShift::where('project_id',$request->project_id)->where('outlet_id',$request->outlet_id)->first();
                if ($chekShift != null) {
                    $count = DB::table('task_people')->where('project_id', $project_id)->get();
                      $next_sequence = $next_sequence = rand(10,1000);
                      $user = Auth::user();
                      for ($i=0; $i < count($people_id); $i++) { 
                          $task = array();
                          $task['people_id']      = $people_id[$i];
                          $task['project_id']     = $project_id;
                          $task['outlet_id']      = $outlet_id;
                        $task['schedule_date']  = date('Y-m-d', strtotime($request->schedule_date));
                          $task['task_start']     = date('Y-m-d', strtotime($request->task_start));
                          $task['task_end']       = date('Y-m-d', strtotime($request->task_end));
                          $task['description']    = $request->desc;
                          $task['task_type']      = $request->task_type;
                          $task['code_shift']      = $request->shift;
                          $task['task_number']    = "TG".date('Ymd').rand(10,1000).$people_id[$i];
                          $task['active_flag']    = '1';
                          $task['status']         = '1';
                          $task['status_flag']    = '0';
                          $task['delete_flag']    = '1';
                          $task['created_by']     = $user->name;
                          $task['updated_by']     = 'system group';

                          $insertTask = TaskPeople::create($task);

                        $leader = ProjectSettTeam::where('team_id',$tim)->first();
                        if($leader){
                            $leadertask = array();
                            $leadertask['people_id']      = $leader->people_id;
                            $leadertask['project_id']     = $project_id;
                            $leadertask['outlet_id']      = $outlet_id;
                            $leadertask['schedule_date']= date('Y-m-d', strtotime($request->schedule_date));
                            $leadertask['task_start']  = date('Y-m-d', strtotime($request->task_start));
                            $leadertask['task_end']      = date('Y-m-d', strtotime($request->task_end));
                            $leadertask['description']    = $request->desc;
                            $leadertask['task_type']      = $request->task_type;
                            $task['code_shift']      = $request->shift;
                            $leadertask['task_number']   ="TG".date('Ymd').rand(10,1000).$people_id[$i];
                            $leadertask['active_flag']    = '1';
                            $leadertask['status']         = '1';
                            $leadertask['status_flag']    = '0';
                            $leadertask['delete_flag']    = '1';
                            $leadertask['created_by']     = $user->name;
                            $leadertask['updated_by']     = 'system group leader all';
                            TaskPeople::create($leadertask);
                        }
                          
                       }
                }else{
                     return redirect()->back()->with('failed','Outlet belum di setting shift');
                }
              
           }
            return redirect()->back()->with('message','Save task list project group Successfully');

       }else{
        
        return redirect()->back()->with('failed','Check range project');
       }

    }
	
	public function getFilterTaskProject(Request $request)
    {
      if($request->search == 'search-project'){
		    $jumlahData = 10;
			$id = $request->id;
            $filterBy = $request->filterby;
			$value = $request->data;
			$member = array();
			
			$project = Project::where('id',$id)->first();
			$structure = ProjectStructure::where('project_id',$id)->first();
			$job = DB::table('view_project_vacancy')->where('project_id',$id)->get();
			$task_group = DB::table('view_task_group')->where('project_id',$id)->get();
			$outlet = DB::table('outlet')->orderBy('id','desc')->get();
			
			$structure = ProjectStructure::where('project_id',$id)->first();
			$m_district = array();
			 if($structure == NULL){
			   $m_district = [];
			 }else{
				 $m_district = DB::table('structure_area_step2')->where('title_structure_id',$structure->structure_id)->distinct('district')->get();
			 }
			
			if($filterBy == '0'){
				$member = DB::table('task_people_project')->where('project_id',$id)->where('status',1)->distinct('employee_id')->paginate($jumlahData);
			}elseif($filterBy == '1'){
				$member = DB::table('task_people_project')->where('project_id',$id)->where('status',1)->where('first_name', 'LIKE', '%'. ucfirst($value) . '%')->distinct('employee_id')->paginate($jumlahData);
			}elseif($filterBy == '2'){
				$member = DB::table('task_people_project')->where('project_id',$id)->where('status',1)->where('job_name', 'LIKE', '%' . $value . '%')->distinct('employee_id')->paginate($jumlahData);
			}
		
			$member->appends($request->only('filterby'));
            $member->appends($request->only('data'));
			$member->appends($request->only('id'));
				
			$paginator = $member;
        
         return view('TaskList.task_project',compact('project','member','job','outlet','task_group','m_district','jumlahData', 'paginator'));
	  }
    }


    public function getResendTasklist($id)
    {
        $resend = TaskPeople::where('id',$id)->update(['status_flag',0]);
        if($resend){
            return redirect()->back()->with('message','Resend Tasklist Successfully');
        }else{
            return redirect()->back()->with('failed','Resend Tasklist failed');
        }
    }


    public function getDeleteTasklist($id)
    {

        $deleteTask = DB::table('task_people')->where('id',$id)->update(['delete_flag' => 0]);
        if($deleteTask){
             return redirect()->back()->with('message','Delete Tasklist Successfully');
         }else{
            return redirect()->back()->with('failed','Delete Tasklist failed');
         }
    }
 
}
