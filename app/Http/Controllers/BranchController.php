<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Branch;
use Redirect;
use Excel;
use Illuminate\Support\Facades\Storage;
use Session;
use Intervention\Image\ImageManagerStatic as Image;
use File;


class BranchController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jumlahData = 10;
        $data = Branch::where('delete_flag',1)->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $data;
        return view('branch.data',compact('data','jumlahData', 'paginator'));
    }

    public function getAddBranch()
    {
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        return view('branch.add_branch',compact('province'));
    }

    public function postAddBranch(Request $request)
    {
			$check = DB::table('office_branch')->where('branch_name', strtoupper($request->name_branch))->where('branch_email', $request->email)->first();
			
			if($check == null){
				$branch = new Branch();
				$branch->branch_name         = strtoupper($request->name_branch);
				$branch->branch_address1     = $request->address1;
				$branch->branch_address2     = $request->address2;
				$branch->branch_rt_rw        = $request->rt_rw;
				$branch->branch_rw           = $request->rw;
				$branch->branch_village      = $request->village;
				$branch->branch_sub_district = $request->sub_district;
				$branch->branch_district     = $request->district;
				$branch->branch_city         = $request->district;
				$branch->branch_province     = $request->province;
				$branch->branch_post_code    = $request->post_code;
				$branch->branch_phone1       = $request->phone1;
				$branch->branch_phone2       = $request->phone2;
				$branch->branch_email        = $request->email;
				$branch->pic_name            = $request->pic_name;
				$branch->npwp                = $request->npwp;
				$branch->branch_langitude    = $request->langitude;
				$branch->branch_latitude     = $request->latitude;
				$branch->active_flag         = '1';
				$branch->delete_flag         = '1';
				$branch->created_by          = 'system';
				$branch->updated_by          = 'system';
				$foto = $request->file('image');

				if ($request->hasFile('image')) {
                    $image       = $request->file('image');
                    $ext = $image->getClientOriginalExtension();
                    $branch->image = date('YmdHis').".".$ext;

                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(300, 300);
                    $image_resize->save(public_path('upload/imageBranch/' .$branch->image));
                }
				
				$branch->save();
				
				if($branch->id != null){
					return Redirect('branch')->with('message', 'Save data branch successfully');
				}else{
					return redirect()->back()->with('failed', 'Save data branch failed');
				}
				
			}else{
				return redirect()->back()->with('failed', 'Branch already exists
');
			}
    }

    public function getEditBranch($id)
    {
        $data = Branch::where('id',$id)->first();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        return view('branch.edit_branch',compact('data','province'));
    }

    public function postEditBranch(Request $request)
    {
            $update = Branch::where('id',$request->id)->first();
            $update->branch_name         = strtoupper($request->name_branch);
            $update->branch_address1     = $request->address1;
            $update->branch_address2     = $request->address2;
            $update->branch_rt_rw        = $request->rt_rw;
            $update->branch_rw           = $request->rw;
            $update->branch_village      = $request->village;
            $update->branch_sub_district = $request->sub_district;
            $update->branch_district     = $request->district;
            $update->branch_city         = $request->district;
            $update->branch_province     = $request->province;
            $update->branch_post_code    = $request->post_code;
            $update->branch_phone1       = $request->phone1;
            $update->branch_phone2       = $request->phone2;
            $update->branch_email        = $request->email;
            $update->pic_name            = $request->pic_name;
            $update->npwp                = $request->npwp;
            $update->branch_langitude    = $request->langitude;
            $update->branch_latitude     = $request->latitude;

             if($request->file('image') == "")
                { 
                    $update->image = $update->image;
                } 
                else
                {
					
					$image       = $request->file('image');
                    $ext = $image->getClientOriginalExtension();
                    $update->image = date('YmdHis').".".$ext;

                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(300, 300);
                    $image_resize->save(public_path('upload/imageBranch/' .$update->image));
					
					$imageold = 'upload/imageBranch/'.$request->image_old;
					if(file_exists($imageold)) {
						File::delete($imageold);
					}
                }

            $update->update();
			
			if($update->id != null){
				return Redirect('branch')->with('message', 'Update data branch successfully');
			}else{
				return redirect()->back()->with('failed', 'Update data branch failed');
			}

    }

   public function getDeleteFlag($id)
    {
      $delete =  DB::table('office_branch')->where('id', $id)->update(['delete_flag' => 0]); 
      if($delete){
        return Redirect('branch')->with('message','Delete data branch successfully');
      }else{
       return redirect('branch')->with('failed','Delete data branch failed');
      }
    }

    public function getViewBranch($id)
    {
        $data = Branch::where('id',$id)->first();
        return view('branch.view_branch',compact('data'));
    }

    public function getFilterBranch(Request $request)
    {
        $search = $request->seacrh;
        if($search == 1){
            $data = DB::table('office_branch')->where('delete_flag',1)->get();
            return view('branch.data',compact('data'));
        }else{
            $data = DB::table('office_branch')->orderBy('id','desc')->get();
            return view('branch.data',compact('data'));
        }
    }


    private function uploadFotoBranch(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'upload/imageBranch';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

public function importBranch(Request $request)
    {
        if($request->hasFile('import')){
            Excel::load($request->file('import')->getRealPath(), function ($reader) {
				
				$temp = array();
				
                foreach ($reader->toArray() as $key => $row) {
					
					$check = DB::table('office_branch')->where('branch_name', strtoupper($row['branch_name']))->where('branch_email', $row['branch_email'])->first();
					if($check == null){
						$branch = new Branch();
						$branch->branch_name = strtoupper($row['branch_name']);
						$branch->branch_address1 = $row['branch_address1'];
						$branch->branch_address2 = $row['branch_address2'];
						$branch->branch_rt_rw = $row['branch_rt_rw'];
						$branch->branch_rw = $row['branch_rw'];
						$branch->branch_village = $row['branch_village'];
						$branch->branch_sub_district = $row['branch_sub_district'];
						$branch->branch_district = $row['branch_district'];
						$branch->branch_city = $row['branch_city'];
						$branch->branch_province = $row['branch_province'];
						$branch->branch_post_code = $row['branch_post_code'];
						$branch->branch_phone1 = $row['branch_phone1'];
						$branch->branch_phone2 = $row['branch_phone2'];
						$branch->branch_email = $row['branch_email'];
						$branch->pic_name = $row['pic_name'];
						$branch->npwp = $row['npwp'];
						$branch->branch_langitude = $row['branch_langitude'];
						$branch->branch_latitude = $row['branch_latitude'];
						$branch->active_flag = 1;
						$branch->delete_flag = 1;
						$branch->save();
					}else{
						$temp[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
									  .'<div class="col-sm-4">'
											.$row['branch_name']
									  .'</div>'
									  .'<div class="col-sm-4">'
											.$row['branch_email']
									  .'</div>'
								  .'</div>';
					}
					
					if(count($temp) > 0){
						Session::flash('failed', $temp);
						Session::flash('gagal', count($temp).' Data Already Exists');	
					}else{
						Session::flash('message', 'Import data branch successfully');	
					}
					
                }
            });
            return Redirect('branch');
        }else{
			Session::flash('failed', 'File not selected');
			return Redirect('branch');
		}  
    }
	
	public function downloadDataOBranch()
    {
        return response()->download('template_excel/example_branch.xlsx'); 
    }

}
