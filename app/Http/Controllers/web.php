<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('auth.login')
;});

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/get-apk', 'Auth\LoginController@getDownload');

//captcha
Route::get('/refreshcaptcha', 'CaptchaController@refreshCaptcha');
Route::post('/login-new2','UserManagementController@postLoginCms');

Route::get('/home', [
   'middleware' => 'auth',
   'uses' => 'HomeController@index'
]);

Route::get('/templete', [
   'middleware' => 'auth',
   'uses' => 'HomeController@templete' 
])->name('templete');;

Route::get('/realtime', [
   'middleware' => 'auth',
   'uses' => 'HomeController@realtime'
]);

Route::get('/realtime/data', [
   'middleware' => 'auth',
   'uses' => 'HomeController@getdata'
]);

//projects
Route::get('/all-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@index'
]);

Route::get('/add-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@addProject'
]);

Route::post('/add-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postAddProject'
]);

Route::get('/add-vacancy', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@vacancy'
]);

Route::post('/add-vacancy', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postVacancy'
]);

Route::get('/add-structure/{project_id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@structure'
]);

Route::get('/add-criteria/{project_id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getCriteria'
]);

Route::post('/add-criteria', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postCriteria'
]);

Route::get('/all-project/edit-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getVacancy'
]);

Route::get('/all-project/edit-structure/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getStructure'
]);

Route::get('/all-project/edit-criteria/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getCriteriaEdit'
]);

Route::get('/all-project/get-team/{id}/{project_id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getTeam'
]);

Route::post('/all-project/get-team', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postProjectTeam'
]);

Route::get('/all-project/edit-team/{city_id}/{project_id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getEditTeam'
]);

Route::get('/all-project/delete-team/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getDeleteTeam'
]);

Route::get('/all-project/team-criteria/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getTeamSubVacancy'
]);

Route::post('/all-project/team-criteria', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postTeamVacancy'
]);

Route::get('/all-project/edit-team-criteria/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getEditTeamVacancy'
]);

Route::get('/all-project/view-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getViewProject'
]);

Route::get('/all-project/filter-view-project-member', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getFilterViewProjectMember'
]);

Route::get('/all-project/run-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@RuningProject'
]);

Route::get('/all-project/filter-run-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postFilterRunningProject'
]);

Route::get('/all-project/candidate-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getCandidateProject'
]);

Route::post('/project-member', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postPeopleMember'
]);

Route::post('/project-member-update', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postUpdateProjectMember'
]);

Route::get('/edit-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getEditProject'
]);

Route::post('/post-edit-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postEditProject'
]);

Route::post('/post-edit-vacancy', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postEditVacancy'
]);

Route::post('/add-structure', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postStructure'
]);

Route::post('/access-task-open', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postActiveTaskOpenMember'
]);

Route::get('/access-task-/post-delete-project-vacancy/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postDeleteProjectVacancy'
]);

Route::get('/post-delete-team-vacancy/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postDeleteProjectTeamVacancy'
]);

Route::get('/setting-team/{people_id}/{project_id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getSettingTeamLeader'
]);

Route::post('/post-setting-team', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@postSettingTeamLeader'
]);

Route::get('/get-delete-setting-team/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getDeleteSettingTeamLeader'
]);

//filter project
Route::get('/filter-project', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getFilterProject'
]);

Route::get('/delete-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getDeleteProjct'
]);

//get team from district 
Route::post('/postdistrict/get/team', [
   'middleware' => 'auth',
   'uses' => 'ProjectsController@getInviteTeam'
]);

//product
Route::post('/all-product', [
   'middleware' => 'auth',
   'uses' => 'ProductController@index'
]);

Route::get('/add-product', [
   'middleware' => 'auth',
   'uses' => 'ProductController@addProduct'
]);

Route::get('/confirmation-end-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProductController@getConfirmation'
]);

//outlet
Route::get('/all-outlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@index'
]);

Route::get('/outlet-open', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getOutletOpen'
]);

Route::post('/importOutlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@importOutlet'
]);

Route::get('/downloadDataOutlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@downloadDataOutlet'
]);

Route::get('/get-delete-outlet/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getDeleteOutletFisik'
]);

Route::get('/get-delete-outlet-open/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getDeleteOutetOpen'
]);

//import excel client
Route::post('/importClientData', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@importClient'
]);

Route::get('/downloadClientData', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@downloadDataClient'
]);

//struktur area
Route::get('/structure-area', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@index'
]);

Route::post('/structure-area', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postStructure'
]);

Route::post('/structure-area-step2', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postStep2Structure'
]);

Route::get('/structure-area-data', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getDataStructure'
]);

Route::get('/structure-area-data/view/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getDetailData'
]);

Route::get('/structure-area-data/add-team/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getAddTeam'
]);

Route::post('/structure-area-add-team', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postAddTeam'
]);

Route::post('/structure-title', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postTitleStructure'
]);

Route::get('/structure-title/view/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getStructureTitle'
]);

Route::get('/structure-title/delete/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postDeleteTitle'
]);

Route::get('/structure-area/delete-structure/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postDeleteStructure'
]);

Route::get('/structure-area/delete-area/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postDeleteArea'
]);

Route::post('/structure-area-v2', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postStructureV2'
]);

Route::post('/structure-area-v2/update', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postEditStructureV2'
]);

Route::get('/structure-title/view/detail/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getViewAll'
]);

Route::post('/structure-title/update-title', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@postEditTitle'
]);

Route::get('/structure-area/view-sturcture/{id}', [
   'middleware' => 'auth',
   'uses' => 'StructureAreaController@getStructureView'
]);

//master religion
 Route::get('/religion', [
   'middleware' => 'auth',
   'uses' => 'ReligionController@index'
]);

Route::post('/religion/add', [
   'middleware' => 'auth',
   'uses' => 'ReligionController@postAddData'
]);

Route::post('/religion/edit', [
   'middleware' => 'auth',
   'uses' => 'ReligionController@postEditData'
]);

Route::get('/religion/delete/{id}', [
   'middleware' => 'auth',
   'uses' => 'ReligionController@postDeleteData'
]);

// Master Client
Route::get('/client/index', [
   'middleware' => 'auth',
   'uses' => 'ClientController@show'
]);

Route::post('/client/index', [
   'middleware' => 'auth',
   'uses' => 'ClientController@postshow'
]);

Route::get('/client/create', [
   'middleware' => 'auth',
   'uses' => 'ClientController@create'
]);

Route::post('/client/create', [
   'middleware' => 'auth',
   'uses' => 'ClientController@store'
]);

Route::get('/client/edit/{id}', [
   'middleware' => 'auth',
   'uses' => 'ClientController@edit'
]);

Route::post('/client/edit', [
   'middleware' => 'auth',
   'uses' => 'ClientController@update'
]);

Route::post('/client/hidden', [
   'middleware' => 'auth',
   'uses' => 'ClientController@hidden'
]);

Route::post('/client/delete', [
   'middleware' => 'auth',
   'uses' => 'ClientController@delete'
]);

Route::post('/client/data', [
   'middleware' => 'auth',
   'uses' => 'ClientController@clientData'
]);

Route::post('/postcode/get/provregen', [
   'middleware' => 'auth',
   'uses' => 'ClientController@clientData'
]);

Route::post('/postcode/get/regency', [
   'middleware' => 'auth',
   'uses' => 'ClientController@getregency'
]);

Route::post('/postcode/get/subdistrict', [
   'middleware' => 'auth',
   'uses' => 'ClientController@getsubdistrict'
]);

Route::post('/postcode/get/village', [
   'middleware' => 'auth',
   'uses' => 'ClientController@getvillage'
]);

Route::post('/postcode/get/postcode', [
   'middleware' => 'auth',
   'uses' => 'ClientController@getpostcode'
]);

// Master Product
Route::get('/product/index', [
   'middleware' => 'auth',
   'uses' => 'ProductController@show'
]);

Route::post('/product/index', [
   'middleware' => 'auth',
   'uses' => 'ProductController@postshow'
]);

Route::post('/product/data', [
   'middleware' => 'auth',
   'uses' => 'ProductController@productData'
]);

Route::get('/product/create', [
   'middleware' => 'auth',
   'uses' => 'ProductController@create'
]);

Route::post('/product/create', [
   'middleware' => 'auth',
   'uses' => 'ProductController@store'
]);

Route::get('/product/edit/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProductController@edit'
]);

Route::post('/product/edit', [
   'middleware' => 'auth',
   'uses' => 'ProductController@update'
]);

Route::post('/product/hidden', [
   'middleware' => 'auth',
   'uses' => 'ProductController@hidden'
]);

Route::post('/product/delete', [
   'middleware' => 'auth',
   'uses' => 'ProductController@delete'
]);

//master people
Route::get('/data-people', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@index'
]);

Route::post('/data-people/add', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddEmploye'
]);

Route::get('/data-people/add-info/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getAddInfo'
]);

Route::post('/data-people/add-info/add', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddAddress'
]);

Route::post('/data-people/add-info/edit', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditAddress'
]);

Route::get('/data-people/add-info/delete/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteAddress'
]);

Route::post('/data-people/add-info/addBank', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddBank'
]);

Route::post('/data-people/add-info/editBank', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditBank'
]);

Route::get('/data-people/add-info/delete-bank/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteBank'
]); 

Route::get('/data-people/add-info/delete-bank-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteBankActive'
]); 

Route::post('/data-people/add-info/addEducation', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddEducation'
]); 

Route::post('/data-people/add-info/editEducation', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditEducation'
]);

Route::get('/data-people/add-info/delete-education/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteEducation'
]);

Route::get('/data-people/add-info/delete-education-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteEducationActive'
]);

Route::post('/data-people/add-info/addFamily', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddFamily'
]);

Route::post('/data-people/add-info/editFamily', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditFamily'
]);

Route::get('/data-people/add-info/delete-family/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteFamily'
]);

Route::get('/data-people/add-info/delete-family-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteFamilyActive'
]);

Route::post('/data-people/add-info/addWork', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddWork'
]);

Route::post('/data-people/add-info/editWork', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditWork'
]);

Route::get('/data-people/add-info/delete-work/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteWork'
]);

Route::get('/data-people/add-info/delete-work-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteWorkActive'
]);

Route::post('/data-people/add-info/addImage', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddImage'
]);

Route::get('/data-people/add-info/delete-image/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteImage'
]);
 
Route::get('/data-people/add-info/delete-image-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteImageActive'
]);

Route::post('/data-people/add-info/addDocument', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddDocument'
]);

Route::get('/data-people/add-info/delete-document/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteDocument'
]);

Route::get('/data-people/add-info/delete-document-active/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteDocumentActive'
]);

Route::post('/data-people/add-info/addFacility', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postAddFacility'
]);

Route::post('/data-people/add-info/editFacility', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditFacility'
]);

Route::get('/data-people/add-info/delete-facility/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postDeleteFaciiity'
]);

Route::get('/data-people/view-data/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getViewData'
]);

Route::get('/data-people/delete-data/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getDeleteFlag'
]);

Route::get('/data-people/active-data/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getActiveFlag'
]);

Route::post('/data-people/filter-data', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getFilterPeople'
]);

Route::get('/data-people/add', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@AddPeople'
]);

Route::post('/data-people/finish', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postFinish'
]);

Route::post('/data-people/filter-data-delete', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@filterDataDelete'
]);

Route::get('/code-pos', [
   'middleware' => 'auth',
   'uses' => 'KodePosController@index'
]);

Route::post('/data-people/add-info/addInteres', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postVacancyPeople'
]);

Route::post('/data-people/add-info/editInteres', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postVacancyPeopleEdit'
]);

Route::get('/data-people/add-info/delete/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getDeleteVacancyPeople'
]);

Route::get('/edit-people/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getEditPoeple'
]);

Route::post('/edit-data-people', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@postEditPeople'
]);
  
// import excel
Route::post('/importPeople', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@importPeople'
]);

Route::get('/downloadPeople', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@downloadPeople'
]);

//master job interes
Route::get('/job-interes', [
   'middleware' => 'auth',
   'uses' => 'JobInteresController@index'
]);

Route::post('/job-interes/add', [
   'middleware' => 'auth',
   'uses' => 'JobInteresController@postAdd'
]);

Route::post('/job-interes/edit', [
   'middleware' => 'auth',
   'uses' => 'JobInteresController@postEdit'
]);

Route::get('/job-interes/delete/{id}', [
   'middleware' => 'auth',
   'uses' => 'JobInteresController@postDeleteData'
]);

 // master outlet
Route::post('/all-outlet/addCategory', [
   'middleware' => 'auth',
   'uses' => 'OutletController@postAddCategoryOutlet'
]);

Route::get('/all-outlet/category/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getCategory'
]);

Route::get('/all-outlet/add-outlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getAddOutlet'
]);

Route::post('/all-outlet/add-outlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@postOutlet'
]);

Route::get('/all-outlet/detail-outlet/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getDetailOutlet'
]);

Route::get('/all-outlet/maps-outlet/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getMapsOutlet'
]);

Route::get('/all-outlet/edit-outlet/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getEditOutlet'
]);

Route::post('all-outlet/edit-outlet', [
   'middleware' => 'auth',
   'uses' => 'OutletController@postEditOutlet'
]);

Route::get('all-outlet/delete-outlet/{id}', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getDeleteFlag'
]);

//office branch
Route::get('branch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@index'
]);

Route::get('branch/add-branch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@getAddBranch'
]);

Route::post('branch/add-branch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@postAddBranch'
]);

Route::get('branch/edit-branch/{id}', [
   'middleware' => 'auth',
   'uses' => 'BranchController@getEditBranch'
]);

Route::post('branch/edit-branch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@postEditBranch'
]);

Route::get('branch/delete-branch/{id}', [
   'middleware' => 'auth',
   'uses' => 'BranchController@getDeleteFlag'
]);

Route::get('branch/view-branch/{id}', [
   'middleware' => 'auth',
   'uses' => 'BranchController@getViewBranch'
]);

Route::post('/branch/filter', [
   'middleware' => 'auth',
   'uses' => 'BranchController@getFilterBranch'
]);

Route::post('/branch/filter/importBranch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@importBranch'
]);

Route::get('/downloadDataOBranch', [
   'middleware' => 'auth',
   'uses' => 'BranchController@downloadDataOBranch'
]);

  //client versi 2
Route::get('/downloadDataOBranch', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@index'
]);

Route::get('/client-data/add-client', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@getAddClient'
]);

Route::post('/client-data/add-client', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@postAddClient'
]);

Route::get('/client-data/edit-client/{id}', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@getEditClient'
]);

Route::post('/client-data/edit-client', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@postEditClient'
]);

Route::get('/client-data/detail-client/{id}', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@getDetailClinet'
]);

Route::get('/client-data/delete-client/{id}', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@postDeleteClient'
]);

Route::post('/client-data/filter-client', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@getFilterData'
]);
  
// import client from excel
Route::post('/client-data/import-client', [
   'middleware' => 'auth',
   'uses' => 'ClientV2Controller@importClient'
]);
  
//product versi 2
Route::get('/product-data', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@index'
]);

Route::get('/product-data/add-product', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@getAddData'
]);

Route::post('/product-data/add-product', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@postAddData'
]);

Route::get('/product-data/edit-product/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@getEditData'
]);

Route::post('/product-data/edit-product', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@postEditData'
]);

Route::get('/product-data/delete-product/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@postDeleteProduct'
]);

Route::get('/product-data/detail-product/{id}', [
   'middleware' => 'auth',
   'uses' => 'ProductV2Controller@getDetailsProduct'
]);

//task list
Route::get('task-list', [
   'middleware' => 'auth',
   'uses' => 'TaskController@index'
]);

Route::get('task-project/{id}', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getTaskProject'
]);

Route::get('task-people/{id}', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getAddTaskPeople'
]);

Route::post('task_people/add', [
   'middleware' => 'auth',
   'uses' => 'TaskController@postAddTaskPeople'
]);

Route::post('task_people/add-group', [
   'middleware' => 'auth',
   'uses' => 'TaskController@postAddTaskGroup'
]);

Route::get('view-task-people/{id}', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getViewTaskProject'
]);

Route::get('view-task-people-detail/{id}/{people}/{date}', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getDetailTaskProject'
]);

Route::get('absensi-project/{people}/{schedule}/{project}', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getAbsensiProject'
]);

Route::post('post-insert-task-team', [
   'middleware' => 'auth',
   'uses' => 'TaskController@postInsertGroupTaskList'
]);


//get outlet from city 
Route::post('/postcity_id/get/outlet', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getOutlet'
]);

//get team from city 
Route::post('/postcity_id/post/team', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getTeam'
]);

//get member from team 
Route::post('/postcity_id/post/member', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getMember'
]);

Route::get('/filter-absensi', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getFilterAbsensi2'
]);

Route::get('/filter-tasklist', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getFilterTaskList2'
]);

Route::get('/filter-outlet-open', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getFilterOutletOpen'
]);

Route::get('/filter-outlet-resmi', [
   'middleware' => 'auth',
   'uses' => 'OutletController@getFilterOutletResmi'
]);

Route::get('/filter-people-new', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getFilterPeoplenew'
]);

Route::get('/filter-task-project', [
   'middleware' => 'auth',
   'uses' => 'TaskController@getFilterTaskProject'
]);

//user management
Route::get('user-cms', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@index'
]);

Route::get('add-user-cms', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@getAddUser'
]);

Route::post('post-add-user-cms', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@postAddUserCms'
]);

Route::post('post-update-user-cms', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@postUpdateUserCms'
]);

Route::get('get-delete-user-cms/{id}', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@getDeleteUser'
]);

Route::get('get-edit-user-cms/{id}', [
   'middleware' => 'auth',
   'uses' => 'UserManagementController@getEditUserCms'
]);


//dashboard
Route::get('total-project', [
   'middleware' => 'auth',
   'uses' => 'HomeController@totalProject'
]);

Route::get('reset-imei-user/{id}', [
   'middleware' => 'auth',
   'uses' => 'PeopleController@getResetImeiUser'
]);

Route::get('/client-data','ClientV2Controller@index')->name('/client-data');