<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Aggree;
use DB;

class CaptchaController extends Controller
{
    public function refreshCaptcha()
    {
        return captcha_img('default');
    }

    public function getAggree()
    {
    	$data = Aggree::where('id',1)->first();
    	return view('aggree.data',compact('data'));
    }

    public function postUpdateAggree(Request $request)
    {
    	$data = DB::table('tb_aggree')->where('id', $request->id_aggree)
    			->update(['title_aggree' => $request->title_aggree,
    					  'value_aggree' => $request->value_aggree,
    					  'status' => 1]);
    	if ($data) {
    		return redirect('aggree')->with('message', 'update aggree berhasil');
    	}else{
    		return redirect('aggree')->with('failed', 'update gagal');
    	}

    }
}
