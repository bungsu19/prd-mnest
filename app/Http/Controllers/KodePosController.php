<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Redirect;


class KodePosController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$jumlahData = 10;
    	$data = DB::table('tb_kodepos')->orderBy('id','desc')->paginate($jumlahData);
    	$paginator = $data;
        return view('kode-pos.data',compact('data','jumlahData','paginator'));
    }


}
