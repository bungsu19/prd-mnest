<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Client;
use App\Branch;
use App\Product;
use App\Project;
use App\ProjectVacancy;
use App\ProjectStructure;
use App\ProjectCriteria;
use App\ProjectTeam;
use App\ProjectTeamVacancy;
use App\EmployePeople;
use App\ProjectMember;
use App\ViewFilterPeople;
use App\ViewProjectVacancy;
use App\SendInterviewProject;
use App\ProjectSettTeam;
use App\Inbox;
use App\UpdateLogMoveTeam;
use App\RejectMember;
use App\WorkPeople;
use Session;
use Redirect;
use EncryptHelp;

class ProjectsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $dt_user = DB::table('users')->where('id',$user->id)->first();

        if($dt_user->hak_access == 4 ){
            $filterBy = '0';
            $value = '';
            $jumlahData = 10;
            $data = DB::table('view_project')->where('delete_flag',1)->where('user_id',$dt_user->id)->orderBy('created_at','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('projects.allProjects',compact('data','jumlahData', 'filterBy', 'value','paginator'));
        }elseif($dt_user->hak_access == 6){
            $filterBy = '0';
            $value = '';
            $jumlahData = 10;
            $data = DB::table('view_project')->where('id',$dt_user->project_id)->orderBy('created_at','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('projects.allProjects',compact('data','jumlahData', 'filterBy', 'value','paginator'));
        }else{

            $filterBy = '0';
            $value = '';
            $jumlahData = 10;
            $data = DB::table('view_project')->where('delete_flag',1)->orderBy('created_at','desc')->paginate($jumlahData);
            $paginator = $data;
            return view('projects.allProjects',compact('data','jumlahData', 'filterBy', 'value','paginator'));

        }

        return view('projects.allProjects',compact('data','jumlahData', 'filterBy', 'value','paginator'));
    }
	
	public function getFilterProject(Request $request)
    {
      if($request->search == 'search-project'){
            $filterBy = $request->filterby;
			$value = $request->data;
			$data = array();
			$jumlahData = 10;
			if($filterBy == '0'){
				$data = DB::table('view_project')->where('delete_flag',1)->orderBy('created_at','desc')->paginate($jumlahData);
			}elseif($filterBy == '1'){
				$data = DB::table('view_project')->where('delete_flag',1)->where('project_name', 'LIKE', '%'.$value.'%')->orderBy('created_at','desc')->paginate($jumlahData);
			}elseif($filterBy == '2'){
				$data = DB::table('view_project')->where('delete_flag',1)->where('client', 'LIKE', '%'.$value.'%')->orderBy('created_at','desc')->paginate($jumlahData);
			}
			
			$filterBy = $request->filterby;
			$value = $request->data;
		
			$data->appends($request->only('filterby'));
            $data->appends($request->only('data'));
				
			return view('projects.allProjects',compact('data','jumlahData', 'filterBy', 'value'));
	  }
    }

    public function addProject()
    {
        $client = Client::where('delete_flag',1)->get();
        $branch = Branch::where('delete_flag',1)->get();
        return view('projects.add_project',compact('client','branch'));
    }

    public function postAddProject(Request $request)
    {
        $user = Auth::user();
        $check = Project::where('project_name',$request->project_name)->first();
        if($check){
                return Redirect()->back();
        }else{
            $data = new Project();
            $data->branch_id = $request->branch_id;
            $data->client_id = $request->client_id;
            $data->project_name = $request->project_name;
            $data->project_contract_no = $request->contact_number;
            $data->project_start = date('Y-m-d', strtotime($request->project_start));
            $data->project_end = date('Y-m-d', strtotime($request->project_end));
            $data->project_desc = $request->description;
            $data->project_value = str_replace('.', '', $request->project_value);
            $data->project_category = 'aas';
            $data->project_package = 'qwerty';
            $data->active_flag = '1';
            $data->delete_flag = '1';
            $data->updated_by = 'system';
            $data->created_by = $user->name;
            $data->user_id = $user->id;
            $data->save();
            $project = Project::where('id',$data->id)->first();
            $vacancy = ViewProjectVacancy::where('project_id',$data->id)->where('delete_flag',1)->get();
            $job = DB::table('job_interesting')->orderBy('id','desc')->get();
            $tb_struc = ProjectStructure::where('project_id',$request->project_id)->first();
            if($tb_struc){
               $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
             }else{
                $dt_struc = array();
             }
            $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();

            return view('projects.add_vacancy2',compact('project','job','vacancy','tb_struc','dt_struc','structure'));
        }
        

    }

    public function vacancy()
    {
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
        return view('projects.add_vacancy',compact('job'));
    }

    public function postVacancy(Request $request)
    {
        $user = Auth::user();
        $check = ProjectVacancy::where('project_id',$request->project_id)->where('interes_id',$request->job_title)->first();
        if($check){
            $project = Project::where('id',$request->project_id)->first();
            $vacancy = ProjectVacancy::where('project_id',$request->project_id)->where('delete_flag',1)->get();
            $job = DB::table('job_interesting')->orderBy('id','desc')->get();
            $tb_struc = ProjectStructure::where('project_id',$request->project_id)->first();
            if($tb_struc){
               $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
             }else{
                $dt_struc = array();
             }
            $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
            return view('projects.add_vacancy2',compact('project','job','vacancy','tb_struc','dt_struc','structure'))->with('failed','Data Job Title Already');
        }else{
            $dt_vacancy = new ProjectVacancy();
            $dt_vacancy->project_id = $request->project_id;
            $dt_vacancy->interes_id = $request->job_title;
            $dt_vacancy->job_title = 'aaa';
            $dt_vacancy->description = $request->description;
            $dt_vacancy->gender = $request->gender;
            $dt_vacancy->last_education = $request->last_education;
            $dt_vacancy->start_age = $request->start_age;
            $dt_vacancy->end_age = $request->end_age;
            $dt_vacancy->qty_people = $request->qty;
            $dt_vacancy->active_flag = '1';
            $dt_vacancy->delete_flag = '1';
            $dt_vacancy->created_by = $user->name;
            $dt_vacancy->updated_by = 'system';
            $dt_vacancy->save();

            $project = Project::where('id',$request->project_id)->first();
            $vacancy = ViewProjectVacancy::where('project_id',$request->project_id)->where('delete_flag',1)->get();
             $tb_struc = ProjectStructure::where('project_id',$request->project_id)->first();
             if($tb_struc){
               $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
             }else{
                $dt_struc = array();
             }
             
            $job = DB::table('job_interesting')->orderBy('id','desc')->get();
            $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();

            return view('projects.add_vacancy2',compact('project','job','vacancy','dt_struc','tb_struc','structure'));

        }

       
    }

     public function postStructure(Request $request)
    {
        $user = Auth::user();
        $check = ProjectStructure::where('project_id',$request->project_id)->where('structure_id',$request->structure_id)->first();
        if($check){
             return Redirect()->back()->with('failed','structure Already');
        }else{
            $dt_structure = new ProjectStructure();
            $dt_structure->project_id = $request->project_id;
            $dt_structure->structure_id = $request->structure_id;
            $dt_structure->active_flag = '1';
            $dt_structure->delete_flag = '1';
            $dt_structure->created_by = $user->name;
            $dt_structure->updated_by = 'system';
            $dt_structure->save();

            $tb_struc = ProjectStructure::where('project_id',$request->project_id)->first();
            $vacancy = ViewProjectVacancy::where('project_id',$request->project_id)->where('delete_flag',1)->get();
            $project = Project::where('id',$dt_structure->project_id)->first();
            $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
            $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
             $job = DB::table('job_interesting')->orderBy('id','desc')->get();
           
            return view('projects.add_vacancy2',compact('project','structure','dt_struc','vacancy','tb_struc','job'));
        }
        


    }

     public function getStructure($id)
    {
        $project = Project::where('id',$id)->first();
        $tb_struc = ProjectStructure::where('project_id',$project->id)->first();
        $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
        $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
        $vacancy = ViewProjectVacancy::where('project_id',$id)->where('delete_flag',1)->get();
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
       
        return view('projects.add_vacancy2',compact('project','structure','dt_struc','vacancy','job')); 
    }

    public function structure($project_id)
    {
        $project = Project::where('id',$project_id)->first();
        $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
        return view('projects.add_structure',compact('project','structure'));
    }

   

    public function getCriteria($project_id)
    {
       $project = Project::where('id',$project_id)->first();
       $vacancy = ProjectVacancy::where('project_id',$project_id)->get();
       return view('projects.add_criteria',compact('project','vacancy'));
        
    }

    public function postCriteria(Request $request)
    { 
        $user = Auth::user();
        $check = ProjectCriteria::where('project_id', $request->project_id)->where('job_title_name',$request->job_title)->first();
        if($check){
             $project = Project::where('id',$request->project_id)->first();
             $vacancy = ProjectVacancy::where('project_id',$request->project_id)->get();
             $criteria = ProjectCriteria::where('project_id',$request->project_id)->get();
            return view('Projects.add_criteria2',compact('project','criteria','vacancy'))->with('failed', 'Job Title Name  Already');

        }else{

            $dt_criteria = new ProjectCriteria();
            $dt_criteria->project_id = $request->project_id;
            $dt_criteria->job_title_name = $request->job_title;
            $dt_criteria->last_education = $request->last_education;
            $dt_criteria->start_age = $request->start_age;
            $dt_criteria->end_age = $request->end_age;
            $dt_criteria->active_flag = '1';
            $dt_criteria->delete_flag = '1';
            $dt_criteria->updated_by = 'system';
            $dt_criteria->created_by = $user->name;
            $dt_criteria->save();

            $project = Project::where('id',$request->project_id)->first();
            $vacancy = ProjectVacancy::where('project_id',$request->project_id)->get();
            $criteria = ProjectCriteria::where('project_id',$request->project_id)->get();
            return view('Projects.add_criteria2',compact('project','criteria','vacancy'));

        }

    }

    public function getVacancy($id)
    {
        $project = Project::where('id',$id)->first();
        $vacancy = ViewProjectVacancy::where('project_id',$id)->where('delete_flag',1)->get();
        $tb_struc = ProjectStructure::where('project_id',$id)->first();
        if($tb_struc == null){
            $dt_struc = array();
        }else{
           $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
        }
        
        $job = DB::table('job_interesting')->orderBy('job_name')->get();
        $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
        return view('projects.add_vacancy2',compact('project','job','vacancy','tb_struc','dt_struc','structure'));
    }

   

    public function getCriteriaEdit($id)
    {
        $project = Project::where('id',$id)->first();
        $vacancy = ProjectVacancy::where('project_id',$id)->get();
        $criteria = ProjectCriteria::where('project_id',$id)->get();
        return view('Projects.add_criteria2',compact('project','criteria','vacancy'));

    }

    public function getTeam($id,$project_id)
    {
       $project = Project::where('id',$project_id)->first();
       $city = DB::table('structure_area_step2')->where('id',$id)->first();
       $structure = ProjectStructure::where('project_id',$project_id)->first();

        return view('projects.add_team_project',compact('project','city','structure'));
    }

    public function postProjectTeam(Request $request)
    {
        $user = Auth::user();
        $check = ProjectTeam::where('project_id',$request->project_id)->where('name_team',$request->name_team)->where('city_id',$request->city_id)->first();
        if($check){
                $project = Project::where('id',$request->project_id)->first();
                $city = DB::table('structure_area_step2')->where('id',$request->city_id)->first();
                $structure = ProjectStructure::where('project_id',$request->project_id)->first();
                $team = ProjectTeam::where('project_id',$request->project_id)->where('structure_id',$request->structure_id)->where('city_id',$request->city_id)->where('delete_flag',1)->get();

                return view('projects.add_team2',compact('project','city','structure','team'))->with('failed','Team already');
        }else{
            $dt_team = new ProjectTeam();
            $dt_team->project_id = $request->project_id;
            $dt_team->structure_id = $request->structure_id;
            $dt_team->city_id = $request->city_id;
            $dt_team->name_team = $request->name_team;
            $dt_team->description = $request->description;
            $dt_team->active_flag = 1;
            $dt_team->delete_flag = 1;
            $dt_team->created_by = $user->name;
            $dt_team->updated_by = 'system';

            $dt_team->save();

            $project = Project::where('id',$request->project_id)->first();
            $city = DB::table('structure_area_step2')->where('id',$request->city_id)->first();
            $structure = ProjectStructure::where('project_id',$request->project_id)->first();
            $team = ProjectTeam::where('project_id',$request->project_id)->where('structure_id',$request->structure_id)->where('city_id',$request->city_id)->where('delete_flag',1)->get();

            // return view('projects.add_team2',compact('project','city','structure','team'));
            return Redirect()->back()->with('message','Add team successfully');
        }

       
    }

    

    public function getEditTeam($city_id,$project_id)
    {
        $project = Project::where('id',$project_id)->first();
        $city = DB::table('structure_area_step2')->where('id',$city_id)->first();
        $structure = ProjectStructure::where('project_id',$project->id)->first();
        $team = ProjectTeam::where('project_id',$project_id)->where('structure_id',$structure->structure_id)->where('city_id',$city_id)->where('delete_flag',1)->get();

        return view('projects.edit_team',compact('project','city','structure','team'));
    }

    public function getDeleteTeam($id)
    {
         $delete = DB::table('project_team')->where('id', $id)->update(['delete_flag' => 0]);
         if($delete){
            return Redirect()->back()->with('message', 'Delete Team successfully');
         }else{
            return Redirect()->back()->with('failed', 'Delete Team failed');
         }
        
    }

    public function getTeamSubVacancy($id)
    {
        $team = ProjectTeam::where('id',$id)->first();
        $project = Project::where('id',$team->project_id)->first();
        $structure = ProjectStructure::where('project_id',$team->project_id)->first();
        $city = DB::table('structure_area_step2')->where('id',$team->city_id)->first();
        $vacancy = ViewProjectVacancy::where('project_id',$team->project_id)->orderBy('id','desc')->get();
        $team_criteria = array();
        return view('projects.team_vacancy2',compact('team','project','structure','city','vacancy','team_criteria'));
    }

    public function postTeamVacancy(Request $request)
    {

        $user = Auth::user();
        $check = ProjectTeamVacancy::where('project_id',$request->project_id)->where('team_id',$request->team_id)->where('job_title_name',$request->job_name_title)->where('delete_flag',1)->first();
        if($check){
          
            $team = ProjectTeam::where('id',$request->team_id)->first();
            $project = Project::where('id',$request->project_id)->first();
            $structure = ProjectStructure::where('project_id',$request->project_id)->first();
            $city = DB::table('structure_area_step2')->where('id',$request->city_id)->first();
            $vacancy = ViewProjectVacancy::where('project_id',$request->project_id)->orderBy('id','desc')->get();
            
            $team_criteria = ProjectTeamVacancy::where('project_id',$request->project_id)->where('structure_id',$request->structure_id)->where('team_id',$request->team_id)->where('delete_flag',1)->get();
           return Redirect()->back()->with('message','Data use already');

        }else{
            $check_qty = ViewProjectVacancy::where('project_id',$request->project_id)->where('job_name',$request->job_name_title)->first();
            $total_qty = $check_qty->qty_people;
            
            if($request->qty <= $total_qty){
                $sisa_qty = $total_qty - $request->qty;
                DB::table('project_vacancy')->where('id', $check_qty->id)->update(['qty_people' => $sisa_qty ]); 
                $tim_vacancy = new ProjectTeamVacancy();
                $tim_vacancy->project_id = $request->project_id;
                $tim_vacancy->structure_id = $request->structure_id;
                $tim_vacancy->city_id = $request->city_id;
                $tim_vacancy->team_id = $request->team_id;
                $tim_vacancy->job_title_name = $request->job_name_title;
                $tim_vacancy->qty_people = $request->qty;
                $tim_vacancy->active_flag = 1;
                $tim_vacancy->delete_flag = 1;
                $tim_vacancy->created_by = $user->name;
                $tim_vacancy->updated_by = 'system';
                $tim_vacancy->save();

            }else{
                return Redirect()->back()->with('message','Qty already finished');
            }

            $team = ProjectTeam::where('id',$tim_vacancy->team_id)->first();
            $project = Project::where('id',$tim_vacancy->project_id)->first();
            $structure = ProjectStructure::where('project_id',$tim_vacancy->project_id)->first();
            $city = DB::table('structure_area_step2')->where('id',$tim_vacancy->city_id)->first();
            $vacancy = ViewProjectVacancy::where('project_id',$tim_vacancy->project_id)->orderBy('id','desc')->get();
            $team_criteria = ProjectTeamVacancy::where('project_id',$request->project_id)->where('structure_id',$request->structure_id)->where('team_id',$request->team_id)->where('delete_flag',1)->get();
            // return view('projects.team_vacancy2',compact('team','project','structure','city','vacancy','team_criteria'));
            return Redirect()->back()->with('message','Save data team vacancy successfully');

        }

       
    }

    public function getEditTeamVacancy($id)
    {
        $team = ProjectTeam::where('id',$id)->first();
        $project = Project::where('id',$team->project_id)->first();
        $structure = ProjectStructure::where('project_id',$team->project_id)->first();
        $city = DB::table('structure_area_step2')->where('id',$team->city_id)->first();
        $vacancy = ViewProjectVacancy::where('project_id',$team->project_id)->orderBy('id','desc')->get();
        $team_criteria = ProjectTeamVacancy::where('project_id',$team->project_id)->where('structure_id',$team->structure_id)->where('team_id',$id)->where('delete_flag',1)->get();
        return view('projects.team_vacancy2',compact('team','project','structure','city','vacancy','team_criteria'));

    }

    public function getViewProject($id)
    {
        $jumlahData = 10;
        $project = DB::table('view_project')->where('id',$id)->first();
        $vacancy = ViewProjectVacancy::where('project_id',$id)->get();
        $structure  = ProjectStructure::where('project_id',$id)->first();
        if($structure == null){
            $dt_structure = array();
        }else{
          $dt_structure = DB::table('structure_area_v2')->where('id_area',$structure->structure_id)->get(); 
        }
       
        $dt_team = DB::table('view_team_project')->where('project_id',$id)->get();
        $member = DB::table('view_project_member')->where('project_id',$id)->where('delete_flag',1)->orderBy('first_name')->paginate($jumlahData);
     
        return view('projects.view-project.view_project',compact('project','vacancy','dt_structure','dt_team','member','jumlahData'));
    }

    

     public function RuningProject($id)
    {
		 $jumlahData = 10;
         $active = 'personal_data';
         $project = DB::table('project')->where('id',$id)->first();
         $vacancy = ProjectVacancy::where('project_id',$id)->get();
         $team = ProjectTeam::where('project_id',$id)->where('delete_flag',1)->get();
       
         $data = ViewFilterPeople::where('project_id',$id)->distinct('people_id')->where('delete_flag',1)->where('status_project',300)->paginate($jumlahData, ['*'], 'data');
		 $interview = ViewFilterPeople::where('project_id',$id)->distinct('people_id')->where('delete_flag',1)->where('status_project',400)->orwhere('status_project',401)->orwhere('status_project',402)->paginate($jumlahData, ['*'], 'interview');
		 $candidate = ViewFilterPeople::where('project_id',$id)->distinct('people_id')->where('delete_flag',1)->where('status_project',500)->paginate($jumlahData, ['*'], 'candidate');
         $member = ViewFilterPeople::where('project_id',$id)->distinct('people_id')->where('delete_flag',1)->where('status_project', 1)->paginate($jumlahData, ['*'], 'candidate');
		  $messageJob = DB::table('view_project_vacancy')->where('project_id',$id)->get();

		  $paginator = $data;
		  
	      $data->appends(['interview' => $interview->currentPage()]);
		  $data->appends(['candidate' => $candidate->currentPage()]);
          $data->appends(['member' => $candidate->currentPage()]);
		
		  $interview->appends(['data' => $data->currentPage()]);
		  $interview->appends(['candidate' => $candidate->currentPage()]);
          $interview->appends(['member' => $candidate->currentPage()]);

          $member->appends(['data' => $data->currentPage()]);
          $member->appends(['candidate' => $candidate->currentPage()]);
          $member->appends(['interview' => $candidate->currentPage()]);
		
		  $candidate->appends(['candidate' => $candidate->currentPage()]);
          $candidate->appends(['member' => $candidate->currentPage()]);
		  $candidate->appends(['interview' => $interview->currentPage()]);
		  $candidate->appends(['data' => $data->currentPage()]);
		  
          return view('projects.run-project.run_project',compact('project', 'people', 'vacancy', 'data', 'team', 'jumlahData', 'interview', 'candidate', 'active', 'member','messageJob'));
    }

    public function FilterRegisterRuningProject(Request $request)
    {
         $jumlahData = 10;
         $active = $request->tab;
         $project = DB::table('project')->where('id', $request->project_id)->first();
         $vacancy = ProjectVacancy::where('project_id', $request->project_id)->get();
         $team = ProjectTeam::where('project_id', $request->project_id)->where('delete_flag',1)->get();

         $data = ViewFilterPeople::where('project_id', $request->project_id)
                 ->distinct('people_id')
                 ->where('delete_flag',1)
                 ->where('status_project',300)
                 ->paginate($jumlahData, ['*'], 'data');
         $interview = ViewFilterPeople::where('project_id', $request->project_id)
                 ->distinct('people_id')
                 ->where('delete_flag',1)
                 ->where('status_project',400)
                 ->paginate($jumlahData, ['*'], 'interview');
         $candidate = ViewFilterPeople::where('project_id', $request->project_id)
                 ->distinct('people_id')
                 ->where('delete_flag',1)
                 ->where('status_project',500)
                 ->paginate($jumlahData, ['*'], 'candidate');
         $member = ViewFilterPeople::where('project_id', $request->project_id)
                 ->distinct('people_id')
                 ->where('delete_flag',1)
                 ->where('status_project', 1)
                 ->paginate($jumlahData, ['*'], 'candidate');

         if($request->search == '0'){
                if($request->tab == 'personal_data'){
                        $data = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',300)
                             ->where('first_name','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'data');
                 }elseif ($request->tab == 'Interview') {
                        $interview = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',400)
                             ->where('first_name','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'interview');
                 }elseif ($request->tab == 'Candidate') {
                        $candidate = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',500)
                             ->where('first_name','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'candidate');
                 }elseif ($request->tab == 'Member') {
                        $member = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project', 1)
                             ->where('first_name','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'candidate');
                 }
         }elseif ($request->search == '1') {
                if($request->tab == 'personal_data'){
                        $data = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',300)
                             ->where('dom_district','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'data');
                 }elseif ($request->tab == 'Interview') {
                        $interview = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',400)
                             ->where('dom_district','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'interview');
                 }elseif ($request->tab == 'Candidate') {
                        $candidate = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project',500)
                             ->where('dom_district','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'candidate');
                 }elseif ($request->tab == 'Member') {
                        $member = ViewFilterPeople::where('project_id', $request->project_id)
                             ->distinct('people_id')
                             ->where('delete_flag',1)
                             ->where('status_project', 1)
                             ->where('dom_district','LIKE',"%{$request->filter_value}%")
                             ->paginate($jumlahData, ['*'], 'candidate');
                 }
         }         
          
          $data->appends(['interview' => $interview->currentPage()]);
          $data->appends(['candidate' => $candidate->currentPage()]);
          $data->appends(['member' => $candidate->currentPage()]);
          $data->appends(['tab' => $request->tab]);
          $data->appends(['project_id' => $request->project_id]);
          $data->appends(['search' => $request->search]);
          $data->appends(['filter_value' => $request->filter_value]);
        
          $interview->appends(['data' => $data->currentPage()]);
          $interview->appends(['candidate' => $candidate->currentPage()]);
          $interview->appends(['member' => $candidate->currentPage()]);
          $interview->appends(['tab' => $request->tab]);
          $interview->appends(['project_id' => $request->project_id]);
          $interview->appends(['search' => $request->search]);
          $interview->appends(['filter_value' => $request->filter_value]);

          $member->appends(['data' => $data->currentPage()]);
          $member->appends(['candidate' => $candidate->currentPage()]);
          $member->appends(['interview' => $candidate->currentPage()]);
          $member->appends(['tab' => $request->tab]);
          $member->appends(['project_id' => $request->project_id]);
          $member->appends(['search' => $request->search]);
          $member->appends(['filter_value' => $request->filter_value]);
        
          $candidate->appends(['candidate' => $candidate->currentPage()]);
          $candidate->appends(['member' => $candidate->currentPage()]);
          $candidate->appends(['interview' => $interview->currentPage()]);
          $candidate->appends(['data' => $data->currentPage()]);
          $candidate->appends(['tab' => $request->tab]);
          $candidate->appends(['project_id' => $request->project_id]);
          $candidate->appends(['search' => $request->search]);
          $candidate->appends(['filter_value' => $request->filter_value]);
          
          return view('projects.run-project.run_project',compact('project', 'people', 'vacancy', 'data', 'team', 'jumlahData', 'interview', 'candidate', 'active', 'member'));
    }

      public function RuningProjectbackup($id)
    {
         $project = DB::table('project')->where('id',$id)->first();
         $vacancy = ProjectVacancy::where('project_id',$id)->get();
         $Count = count($vacancy);
         $id = Array();
         // $name = Array();
         // $data = Array();
         // $people2 = Array();
         if($vacancy){
             for ($i=0; $i < $Count; $i++) { 
               
                $people = EmployePeople::where('last_education',$vacancy[$i]->last_education)
                            ->where(['interesting_job_1' => $vacancy[$i]->job_title])
                            ->get();
                  foreach ($people as $key2) {
                     $id[] = $key2;
                  }
                

            }

         }
          return view('projects.run-project.run_project',compact('project','people','vacancy','id'));
    }

    public function addInterviewMemberProject(Request $request)
    {
        $SendInterviewProject = new SendInterviewProject();
        $SendInterviewProject->people_id  = $request->people_id;
        $SendInterviewProject->project_id = $request->project_id;
        $SendInterviewProject->date1      = date('Y-m-d', strtotime($request->date1));
        $SendInterviewProject->date2      = date('Y-m-d', strtotime($request->date2));
        $SendInterviewProject->date3      = date('Y-m-d', strtotime($request->date3));
        $SendInterviewProject->jam1       = $request->jam1;
        $SendInterviewProject->jam2       = $request->jam2;
        $SendInterviewProject->jam3       = $request->jam3;
        $SendInterviewProject->lokasi     = $request->lokasi;
        $SendInterviewProject->name       = $request->ketemu;
        $SendInterviewProject->event      = $request->event;
        $SendInterviewProject->address    = $request->address;
        $SendInterviewProject->message    = $request->message;
        $SendInterviewProject->status     = '0';
        $SendInterviewProject->active_flag     = 1;
        $SendInterviewProject->save();
        
        if($SendInterviewProject->id == null){
            return Redirect()->back()->with('gagal', 'Sent interview project date failed');
        }else{

            $update = DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 400]); 
            if($update == 0){
                return Redirect()->back()->with('failed', 'Sent interview project date failed');
            }else{
                $inboxData = array();
                $inboxData['message'] = "Undangan Interview di Project";
                $inboxData['id_interview'] = $SendInterviewProject->id;
                $inboxData['status_code'] = 2;
                $inboxData['status_read'] = 0;
                $inboxData['status_confirm'] = 0;
                $inboxData['people_id'] = $request->people_id;
                Inbox::create($inboxData);
                return Redirect()->back()->with('message', 'Sent interview project date successfully');
            }
        }
    } 
 
     public function RuningProject2112($id)
    {
         $project = DB::table('project')->where('id',$id)->first();
         $vacancy = ProjectVacancy::select('last_education')->where('project_id',$id)->get();
            $Count = count($vacancy);

         $people = array();
           for ($i=0; $i < $Count; $i++) { 

                $people[] = EmployePeople::where('last_education',$vacancy[$i]->last_education)->where('interesting_job_1',$vacancy[$i]->interes_id)->get();
                
             }
             dd($people[4]);

              // $people = array();
         // $stack = array();
         //   for ($i=0; $i < $Count; $i++) { 

         //        $tes[] = EmployePeople::where('last_education',$vacancy[$i]->last_education)->get();
                
         //        array_push($people, $tes);
         //     }
          return view('projects.run-project.run_project',compact('project','people'));
    }


    public function getCandidateProject($id)
    {
        $vacancy = ProjectVacancy::where('id',$id)->first();
        $project = Project::where('id',$vacancy->project_id)->first();

        $people = EmployePeople::orwhere('last_education',$vacancy->last_education)->where('interesting_job_1',$vacancy->job_title)->orwhere('interesting_job_2',$vacancy->job_title)->get();

        return view('projects.run-project.cindidate_project',compact('vacancy', 'project','people'));
    }

    public function postPeopleMember(Request $request)
    {
        $check = ProjectMember::where('project_id',$request->project_id)->where('delete_flag',1)->where('employee_id',$request->people_id)->first();
        if($check == null){
            $user = Auth::user();
            $member = new ProjectMember();
            $member->project_id = $request->project_id;
            $member->employee_id = $request->people_id;
            $member->interes_id = $request->interes_id;
            $member->team_id = $request->team_id;
            $member->pm_start = $request->pm_start;
            $member->pm_end = $request->pm_end;
            $member->active_flag = '1';
            $member->delete_flag  = '1';
            $member->status  = '1';
            $member->created_by = $user->name;
            $member->updated_by = 'system';
            $member->save();
            DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 1]);

            return Redirect()->back();
        }else{
             return Redirect()->back();
        }
    }

    public function postUpdateProjectMember(Request $request)
    {
        $status = $request->status;

        if($status == 2){
            $user = Auth::user();
            $update = ProjectMember::where('id',$request->id)->first();
            $update->status = $request->status;
            $update->reason = $request->reason;
            $update->pm_end = $request->pm_end;
            $update->updated_by = $user->name;
            $update->update();
            DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 300]);
            DB::table('project_member')->where('employee_id',$request->people_id)->update(['project_id' => 0]);
            return Redirect()->back();
        }elseif($status == 3){
            $user = Auth::user();
            $update = ProjectMember::where('id',$request->id)->first();
            $update->status = $request->status;
            $update->reason = $request->reason;
            $update->pm_end = $request->pm_end;
            $update->updated_by = $user->name;
            $update->update();
            DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 9]);
            DB::table('project_member')->where('employee_id',$request->people_id)->update(['project_id' => 0]);
            return Redirect()->back();


        }else{
            return Redirect()->back();
        }

    }

    public function getEditProject($id)
    {
        $project = Project::where('id',$id)->first();
        $client = Client::orderBy('client_id','desc')->get();
        $branch = Branch::orderBy('id','desc')->get();
        return view('projects.editProject',compact('project','client','branch'));
    }

    public function postEditProject(Request $request)
    {
        $user = Auth::user();
        $update = Project::where('id',$request->id)->first();
        $update->branch_id = $request->branch_id;
        $update->client_id = $request->client_id;
        $update->project_name = $request->project_name;
        $update->project_contract_no = $request->contact_number;
        $update->project_start = date('Y-m-d', strtotime($request->project_start));
        $update->project_end = date('Y-m-d', strtotime($request->project_end));
        $update->project_desc = $request->description;
        $update->project_value = str_replace('.', '', $request->project_value);
        $update->updated_by = $user->name;
        $update->update();

        return Redirect()->back()->with('message','Update data successfully');
    }

    public function postEditVacancy(Request $request)
    {
        $user = Auth::user();
        $edit = ProjectVacancy::where('id',$request->id)->first();
        $edit->project_id = $request->project_id;
        $edit->interes_id = $request->job_title;
        $edit->description = $request->description;
        $edit->gender = $request->gender;
        $edit->last_education = $request->last_education;
        $edit->start_age = $request->start_age;
        $edit->end_age = $request->end_age;
        $edit->qty_people = $request->qty;
        $edit->updated_by = $user->name;    

        $edit->update();

        $project = Project::where('id',$request->project_id)->first();
        $vacancy = ViewProjectVacancy::where('project_id',$request->project_id)->where('delete_flag',1)->get();
        $tb_struc = ProjectStructure::where('project_id',$request->project_id)->first();
        if($tb_struc == null){
            $dt_struc = array();
        }else{
           $dt_struc = DB::table('structure_area_v2')->where('id_area',$tb_struc->structure_id)->get();
        }
        
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
        $structure = DB::table('title_structure_area')->orderBy('id','desc')->get();
        return view('projects.add_vacancy2',compact('project','job','vacancy','tb_struc','dt_struc','structure'));
    }

    public function postActiveTaskOpenMember(Request $request)
    {
        $statusUser = $request->stausUser;
        if($statusUser == 0){
            $active = EmployePeople::where('id',$request->people_id)->update(['status_user' => 1]);

            return Redirect()->back()->with('message', 'Access Task Open Active ');
        }elseif($statusUser == 1){
            $non_active = EmployePeople::where('id',$request->people_id)->update(['status_user' => 0]);
            return Redirect()->back()->with('failed', 'Access Task Open Non Active ');
        }else{
            return Redirect()->back();
        }
    }

	// Ajax get Outlet from city
    public function getInviteTeam(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required',
            'project_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Team -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('invite_team_view')->select('id','name_team','district')->where('project_id',$request->project_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->name_team.' ('.$value->district.')'.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
	
	//ajax get for move team people
	public function getMoveTeam(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'people_id' => 'required',
			'team_id' => 'required'
	    ]);

	    if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$html = '<option value="">- Select Team -</option>';
        	$response['hasil'] = 'OK';
        	$village = DB::table('view_distric_team')->select('id','name_team','district')->whereNotIn('id', [$request->team_id])->orderBy('name_team')->get();
        	foreach ($village as $value) {
        		$html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->name_team.' ('.$value->district.')'.'</option>';
        	}
	    	$response['data'] = $html;
	        return json_encode($response);
        }
    }
	
	public function getUpdateMoveTeam(Request $request)
    {
        $updateTeam = ProjectMember::where('employee_id', $request->people_id)->update(['team_id' => $request->team_id]);
        if($updateTeam == 1){
			$MoveTeam = new UpdateLogMoveTeam();
			$MoveTeam->project_id = $request->project_id;
			$MoveTeam->people_id = $request->people_id;
			$MoveTeam->team_id_before = $request->id_team_old;
			$MoveTeam->team_id_after = $request->team_id;
			$MoveTeam->update_change = date('Y-m-d');
			$MoveTeam->save();
			
			if($MoveTeam->id != null){
				return Redirect()->back()->with('message', 'Move team successfully');
			}else{
				return Redirect()->back()->with('failed', 'Move team failed');
			}
        }else{
			return Redirect()->back()->with('failed', 'Move team failed');
		}
    }


    public function getConfirmation($id)
    {
        $project = Project::where('id',$id)->first();
        $member = ProjectMember::where('project_id',$id)->get();
        if($member == NULL){
            
            return Redirect('all-project')->with('failed','Confirmation data  failed');
        }else{
            $projectUpdate = Project::where('id',$id)->update(['active_flag' => 0 ]); 
            $update = ProjectMember::where('project_id',$id)->update(['status' => 0 ]);
            if($update){
               foreach ($member as $mem) {
                   $updatePeople = EmployePeople::where('id',$mem->people_id)->update(['status_project' => 300,'active_flag' => 0 ]);
                    $updateMember = ProjectMember::where('employee_id',$mem->people_id)->update(['delete_flag' => 0 ]);




               }
            }
            return Redirect('all-project')->with('message','Confirmation data  successfully');
        }
        

        
    }

    public function getDeleteProjct($id)
    {
        $delete = Project::where('id',$id)->update(['delete_flag' => 0]);
        if($delete){
            return Redirect()->back()->with('message','Delete data project successfully');
        }else{
            return Redirect()->back()->with('failed','Delete data project failed');
        }
    }


    public function postDeleteProjectVacancy($id)
    {
        $data = ProjectVacancy::where('id',$id)->delete();
        if($data){
            return Redirect()->back()->with('message','Delete data project vacancy successfully');
        }else{
            return Redirect()->back()->with('failed','Delete data project vacancy failed');
        }
    }

    public function postDeleteProjectTeamVacancy($id)
    {
        $deleteTeam = ProjectTeamVacancy::where('id',$id)->update(['delete_flag' => 0]);
        if($deleteTeam){
            return Redirect()->back()->with('message','Delete data successfully');
        }else{
            return Redirect()->back()->with('failed','Delete data failed');
        }

    }
	
	public function postSentInterviewProject(Request $request)
    {
        $SendInterview = new SendInterviewProject();
        $SendInterview->people_id = $request->people_id;
        $SendInterview->project_id = $request->people_id;
        $SendInterview->date1 = date('Y-m-d', strtotime($request->date1));
        $SendInterview->date2 = date('Y-m-d', strtotime($request->date2));
        $SendInterview->date3 = date('Y-m-d', strtotime($request->date3));
		$SendInterview->status = '0';
        $SendInterview->save();
		
		if($SendInterview->id == null){
			return Redirect()->back()->with('gagal', 'Sent interview date failed');
		}else{
			$update = DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 100]); 
			if($update == 0){
				return Redirect()->back()->with('failed', 'Sent interview date failed');
			}else{
				return Redirect()->back()->with('message', 'Sent interview date successfully');
			}
		}

    }

    public function getAddCandidateProject($id)
    {
        $update = DB::table('employee_people')->where('id',$id)->update(['status_project' => 500]);
        if($update == 0){
            return Redirect()->back()->with('failed', 'Confirmation candidate failed');
        }else{
            return Redirect()->back()->with('message', 'Confirmation candidate successfully');
        }
    }

    public function getSettingTeamLeader($people_id,$project_id)
    {
        $jumlahData = 10;
        $project = Project::where('id',$project_id)->first();
        $people = EmployePeople::where('id',$people_id)->first();
        $team = DB::table('view_distric_team')->where('project_id',$project_id)->where('delete_flag',1)->where('active_flag',1)->get();
        $leader = DB::table('setting_team_view')->where('project_id',$project_id)->where('people_id',$people_id)->where('delete_flag',1)->paginate($jumlahData);
       
        $paginator = $leader;

        return view('projects.view-project.settingTeam',compact('project','team','people','leader','paginator','jumlahData'));
    }

    public function postSettingTeamLeader(Request $request)
    {
        if($request->team_id == null){
            return Redirect()->back()->with('failed','Team name not selected');
        }else{
            $team = $request->team_id;
            $user = Auth::user();
            for ($i=0; $i < count($team); $i++) { 
                    $task = array();
                    $task['people_id'] = $request->people_id;
                    $task['project_id'] = $request->project_id;
                    $task['team_id'] = $request->team_id[$i];
                    $task['active_flag'] = '1';
                    $task['delete_flag'] = '1';
                    $task['created_by'] = $user->name;
                    $task['updated_by'] = 'system';

                    $insertTask = ProjectSettTeam::create($task);
                    if($insertTask){
                        DB::table('project_team')->where('id',$insertTask->team_id)->update(['active_flag' => 0]);
                    }
                    
                 }

            return Redirect()->back()->with('message','Setting team successfully');
        }
    }

    public function getFilterViewProjectMember(Request $request)
    {
        $jumlahData = 10;
        $project = DB::table('view_project')->where('id', $request->id)->first();
        $vacancy = ViewProjectVacancy::where('project_id', $request->id)->get();
        $structure  = ProjectStructure::where('project_id', $request->id)->first();
        
        if($structure == null){
            $dt_structure = array();
        }else{
          $dt_structure = DB::table('structure_area_v2')->where('id_area',$structure->structure_id)->get(); 
        }
        
         if($request->search == '2'){ 
             $member = DB::table('view_project_member')->where('project_id', $request->id)->paginate($jumlahData, ['*'], 'page');
         }elseif($request->search == '0'){
             $member = DB::table('view_project_member')->where('project_id', $request->id)->where('first_name', $request->filter_value)->paginate($jumlahData, ['*'], 'page');
         }else{
             $member = DB::table('view_project_member')->where('project_id', $request->id)->where('job_name', $request->filter_value)->paginate($jumlahData, ['*'], 'page');
         }
         
         $member->appends(['page' => $member->currentPage()]);
         
         $member->appends($request->only('id'));
         $member->appends($request->only('search'));
         $member->appends($request->only('filter_value'));
       
        $dt_team = DB::table('view_team_project')->where('project_id', $request->id)->get();
     
        return view('projects.view-project.view_project',compact('project','vacancy','dt_structure','dt_team','member','jumlahData'));
    }

    public function postRejectMember(Request $request)
    {
        $opsi = $request->name_select;

        if($opsi == 1){
            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['status_project' => 500]);
            if($update == 0){
                return Redirect()->back()->with('failed', 'Confirmation candidate failed');
            }else{
                return Redirect()->back()->with('message', 'Confirmation candidate successfully');
            }
        }elseif($opsi == 2){
            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['status_project' => 300]);
            if($update){
                $opsiReject = new RejectMember();
                $opsiReject->project_id = $request->project_id;
                $opsiReject->people_id = $request->people_id;
                $opsiReject->reason = $request->reason;
                $opsiReject->active_flag = 1;
                $opsiReject->delete_flag = 1;
                $opsiReject->save();
                return Redirect()->back()->with('message', 'Confirmation candidate successfully');
            }else{
                return Redirect()->back()->with('failed', 'gagal');
  
            }
            

        }else{
             return Redirect()->back()->with('message', 'Confirmation candidate successfully');

        }
    }



}
