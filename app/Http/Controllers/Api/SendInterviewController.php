<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Branch;
use App\EmployePeople;
use App\ProjectMember;
use App\SendInterview;
use App\SendInterviewProject;
use App\ViewInbox;
use App\Project;
use QrCode;
use File;
use Storage;
use DB;



class SendInterviewController extends Controller
{
   
  
	public $succesStatus = 200;
	public $failed = 500;

    public function test()
    {
    	$data = Branch::where('delete_flag',1)->orderBy('id','desc')->get();
    	$test['message'] = 'ok';
        
        $test['status'] = 200;
    	$test['data'] = $data;
    	return response()->json($test, $this->succesStatus);
    }


    public function postConfirmationInterview(Request $request)
    {
        $param = $request->param;
        $check = SendInterview::where('id',$request->id_interview)->where('people_id',$request->people_id)->first();
        if($check){
            if($param == 1){
                 $interview = SendInterview::where('id',$check->id)->update([ 'status' => 2 ]);
                if($interview){
                    DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_confirm' => 1]);
                    $data['message'] = 'reschedule success';
                    $data['code'] = 200;
                    return response()->json($data, $this->succesStatus);
                }else{
                    $data['message'] = 'reschedule failed';
                    $data['code'] = 503;
                    return response()->json($data, $this->succesStatus); 
                }


            }else{
                 $interview = SendInterview::where('id',$check->id)->update(['date_confir' => date('Y-m-d', strtotime($request->date_confirm)), 'status' => 1, 'status_confirm' => 1, 'jam_confirm' => $request->jam_confirm ]);
                if($interview){
                    DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_confirm' => 1]);
                    $data['message'] = 'Confirmation success';
                    $data['code'] = 200;
                    return response()->json($data, $this->succesStatus);
                }else{
                    $data['message'] = 'Confirmation failed';
                    $data['code'] = 500;
                    return response()->json($data, $this->succesStatus); 
                }

            }
           

        }else{
            $data['message'] = 'Data Not Found';
            $data['code'] = 500;
            return response()->json($data, $this->succesStatus);
        }
    }

     public function postConfirmationInterviewProject(Request $request)
    {

        $param = $request->param;
        $check = SendInterviewProject::where('id',$request->id_interview)->where('people_id',$request->people_id)->first();
        if($check){
            if ($param == 1) {
                $interview = DB::table('employee_people')->where('id',$request->people_id)->update(['status_project' => 402]);
                if($interview){
                    DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_confirm' => 1]);
                    $data['message'] = 'Confirmation success';
                    $data['code'] = 200;
                    return response()->json($data, $this->succesStatus);
                }else{
                    $data['message'] = 'Confirmation failed';
                    $data['code'] = 500;
                    return response()->json($data, $this->succesStatus); 
                }
            }else{
                $interview = SendInterviewProject::where('id',$check->id)->update(['date_confirm' => date('Y-m-d', strtotime($request->date_confirm)), 'status' => 1, 'status_confrim' => 1, 'jam_confirm' => $request->jam_confirm ]);
                if($interview){
                    DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_confirm' => 1]);
                    DB::table('employee_people')->where('id',$request->people_id)->update(['status_project' => 401]);
                    $data['message'] = 'Confirmation success';
                    $data['code'] = 200;
                    return response()->json($data, $this->succesStatus);
                }else{
                    $data['message'] = 'Confirmation failed';
                    $data['code'] = 500;
                    return response()->json($data, $this->succesStatus); 
                }
            }
            

        }else{
            $data['message'] = 'Data Not Found';
            $data['code'] = 500;
            return response()->json($data, $this->succesStatus);
        }
    }

    public function getListInterview(Request $request)
    {
        $list = DB::table('view_inbox')->where('people_id',$request->people_id)->get();
        $umum = DB::table('view_inbox')->where('status_code',3)->get();

        if(count($list) == 0){
            $data['message'] = 'Interview kosong';
            $data['code'] = 201;
            return response()->json($data, $this->succesStatus);
        }else{

             $all = [];

             for ($i=0; $i < count($list); $i++) { 
                    $all[] = (object) ["id_inbox" => $list[$i]->id_inbox,
                                         "message" => $list[$i]->message,
                                         "id_interview" => $list[$i]->id_interview,
                                         "status_code" => $list[$i]->status_code,
                                         "status_read" => $list[$i]->status_read,
                                         "people_id" => $list[$i]->people_id,
                                         "status_confirm" => $list[$i]->status_confirm,
                                         "date_inbox" => $list[$i]->date_inbox
                                     ];
             }

             for ($i=0; $i < count($umum); $i++) { 
                    $all[] = (object) ["id_inbox" => $umum[$i]->id_inbox,
                                         "message" => $umum[$i]->message,
                                         "id_interview" => $umum[$i]->id_interview,
                                         "status_code" => $umum[$i]->status_code,
                                         "status_read" => $umum[$i]->status_read,
                                         "people_id" => $umum[$i]->people_id,
                                         "status_confirm" => $umum[$i]->status_confirm,
                                         "date_inbox" => $umum[$i]->date_inbox
                                     ];
             }

             $data['message'] = 'list interview';
             $data['code'] = 200;
             $data['data'] = $all;

            return response()->json($data, $this->succesStatus);
        }
    }


    public function getListInterviewPublic(Request $request)
    {
        $list = DB::table('view_inbox')->where('status_code',3)->get();

        if(count($list) == 0){
            $data['message'] = 'Interview kosong';
            $data['code'] = 201;
            return response()->json($data, $this->succesStatus);
        }else{
             $data['message'] = 'list interview';
             $data['code'] = 200;
             $data['data'] = $list;
            return response()->json($data, $this->succesStatus);
        }


    }

    public function getListInterviewProject(Request $request)
    {
        $list = DB::table('view_inbox')->where('people_id',$request->people_id)->where('status_code', 2)->get();

        if(count($list) == 0){
            $data['message'] = 'Interview kosong';
            $data['code'] = 201;
            return response()->json($data, $this->succesStatus);
        }else{
             $data['message'] = 'list interview';
             $data['code'] = 200;
             $data['data'] = $list;
            return response()->json($data, $this->succesStatus);
        }
    }

    public function getDetailInterView(Request $request)
    {
       $statusCode = $request->status_code;
       if($statusCode == 1){
            $detail = DB::table('view_list_interview')->where('id_interview',$request->id_interview)->where('people_id', $request->people_id)->first();
            if ($detail) {
                DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_read' => 1]);
                $data['message'] = 'List Detail Inbox';
                $data['code'] = 200;
                $data['data'] = $detail;
                return response()->json($data, $this->succesStatus);
            }else{
                $data['message'] = 'List Detail Not Fuond';
                $data['code'] = 500;
                return response()->json($data, $this->succesStatus);
            }
       }elseif($statusCode == 2){

            $detail = DB::table('view_list_interview_project')->where('id_interview',$request->id_interview)->where('people_id', $request->people_id)->first();

            if ($detail) {
                DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_read' => 1]);
                $data['message'] = 'List Detail Inbox';
                $data['code'] = 200;
                $data['data'] = $detail;
                return response()->json($data, $this->succesStatus);
            }else{
                $data['message'] = 'List Detail Not Fuond';
                $data['code'] = 500;
                return response()->json($data, $this->succesStatus);
            }

        }elseif($statusCode == 3){
            
            $detail = DB::table('promo_umum')->where('id',$request->id_interview)->first();

            if ($detail) {
                DB::table('tb_inbox')->where('id',$request->id_inbox)->update(['status_read' => 1]);
                $data['message'] = 'List Detail Inbox';
                $data['code'] = 200;
                $data['data'] = $detail;
                return response()->json($data, $this->succesStatus);
            }else{
                $data['message'] = 'List Detail Not Fuond';
                $data['code'] = 500;
                return response()->json($data, $this->succesStatus);
            }

       }else{

       }
    }
    // public function getListInterview(Request $request)
    // {
    //     $list = DB::table('view_list_interview')->where('people_id',$request->people_id)->where('status',0)->get();
    //     if(count($list) == 0){
    //         $data['message'] = 'Interview kosong';
    //         $data['code'] = 201;
    //         return response()->json($data, $this->succesStatus);
    //     }else{
    //          $data['message'] = 'list interview';
    //          $data['code'] = 200;
    //          $data['data'] = $list;
    //         return response()->json($data, $this->succesStatus);
    //     }
    // }


    public function getProvince(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
             $province = DB::table('tb_kodepos')->select('province')->distinct('province')->orderBy('province')->get();
            if($province == null){
                 $data['message'] = 'list province null';
                 $data['code'] = 201;
                return response()->json($data, $this->succesStatus);
            }else{
                 $data['message'] = 'list province';
                 $data['code'] = 200;
                 $data['data'] = $province;
                return response()->json($data, $this->succesStatus);
            }

        }else{
             $data['message'] = 'User Not found';
             $data['code'] = 500;
            return response()->json($data, $this->succesStatus);

        }
       
    }

    public function getDistrict(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
             $district = DB::table('tb_kodepos')->select('district')->where('province',$request->province)->distinct('district')->get();
            if($district == null){
                 $data['message'] = 'list District null';
                 $data['code'] = 201;
                return response()->json($data, $this->succesStatus);
            }else{
                 $data['message'] = 'list District';
                 $data['code'] = 200;
                 $data['data'] = $district;
                return response()->json($data, $this->succesStatus);
            }
        }else{
             $data['message'] = 'User Not found';
             $data['code'] = 500;
            return response()->json($data, $this->succesStatus);
        }
       

    }

    public function getSubDistrict(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $subdistrict = DB::table('tb_kodepos')->select('subdistrict')->where('district',$request->district)->distinct('subdistrict')->get();
            if($subdistrict == null){
                 $data['message'] = 'list Sub District null';
                 $data['code'] = 201;
                return response()->json($data, $this->succesStatus);
            }else{
                 $data['message'] = 'list Sub District';
                 $data['code'] = 200;
                 $data['data'] = $subdistrict;
                return response()->json($data, $this->succesStatus);
            }
        }else{
             $data['message'] = 'User Not found';
             $data['code'] = 500;
            return response()->json($data, $this->succesStatus);
        }
        

    }

    public function getVillage(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $village = DB::table('tb_kodepos')->select('village')->where('subdistrict',$request->subdistrict)->distinct('village')->get();
            if($village == null){
                 $data['message'] = 'list Village null';
                 $data['code'] = 201;
                return response()->json($data, $this->succesStatus);
            }else{
                 $data['message'] = 'list Village';
                 $data['code'] = 200;
                 $data['data'] = $village;
                return response()->json($data, $this->succesStatus);
            }
        }else{
             $data['message'] = 'User Not found';
             $data['code'] = 500;
            return response()->json($data, $this->succesStatus);
        }
        

    }

    public function getJobInteres(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $job = DB::table('job_interesting')->orderBy('job_name')->get();
            if($job == null){
                 $data['message'] = 'list job interes null';
                 $data['code'] = 201;
                return response()->json($data, $this->succesStatus);
            }else{
                $data['message'] = 'list Job interes';
                $data['code'] = 200;
                $data['data'] = $job;
                return response()->json($data, $this->succesStatus);
            }
        }else{
             $data['message'] = 'User not found';
             $data['code'] = 201;
            return response()->json($data, $this->succesStatus);
        }
    }





   
}
