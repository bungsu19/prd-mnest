<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Branch;
use App\User;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer;
use App\EmployePeople;
use App\EducationPeople;


class TestingController extends Controller
{
  
	public $succesStatus = 200;
	public $failed = 500;

    public function test()
    {
        
    	phpinfo();
    }

    public function getLogin(Request $request)
    {
        $password = base64_encode($request->password);
        $user = EmployePeople::where('email_address',$request->email)->first();
        dd($user);
         if ($user && Hash::check($password, $user->password)) {
            $respon['code'] = 200;
            $respon['data'] = $user;
            return response()->json($respon, $this->successStatus);
         }


    }

    public function getData()
    {
        
        $tulisan['data'] = "testing";
        return response()->json($tulisan,$this->successStatus);
    }

    public function sendMail(Request $request)
    {
        $email = "aaa";
        $pas = rand(10,1000);
        $mail = new PHPMailer\PHPMailer;

        $mail->isSMTP();                            // Set mailer to use SMTP
        $mail->SMTPAuth = true;                     // Enable SMTP authentication
        $mail->Username = 'kreasibungsu19@gmail.com';          // SMTP username
        $mail->Password = 'bungsu123'; // SMTP password
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;
        //Set the encryption system to use - ssl (deprecated) or tls
        $mail->setFrom('kreasibungsu19@gmail.com', 'pesan baru');
        // $mail->addReplyTo('hamrullah1902@gmail.com', 'lagi');
        $mail->addAddress('hamrullah1902@gmail.com');   // Add a recipient
        $mail->isHTML(true);  // Set email format to HTML
        $message = '
          <p>email testing hamrullah</p>'
          .'<h2>'.'email ='.$email.'<br>'
          .'password =' .$pas.'</h2>';
        $mail->msgHTML($message);
        if(!$mail->send())
        {
            return "gagal terkirim";
             echo  $mail->ErrorInfo;
        } else {
           return "sukses terkirim";
        }
            
        
    }

    public function getTasklistBaru(Request $request)
    {
        $tasklist = DB::table('view_query_task')->where('people_id',$request->people_id)->where('status_flag',0)->orderBy('task_id','desc')->get();
        $aktif = [];
        $expire = [];

        if(count($tasklist) > 0){
            foreach ($tasklist as $key) {
                $start = $key->task_start;
                $end = $key->task_end;
                $today = date('Y-m-d');
                if($today >= $start && $today <= $end ){
                    $aktif[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user   ];
                }else{
                    $expire[] =  [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user   ];
                }
            }

            $task['message'] = 'list Tasklist';
            $task['code'] = 200;
            $task['task_active'] = $aktif;
            return response()->json($task, $this->succesStatus);

        }elseif(count($tasklist) == 0){
            $task['message'] = 'list Tasklist Null';
            $task['code'] = 201;
            return response()->json($task, $this->succesStatus);
        }else{
            $task['message'] = 'error';
            $task['code'] = 500;
            return response()->json($task, $this->succesStatus); 
        }
        
    }


 public function ceklogin(Request $request){
    $checkmail = EmployePeople::where('email_address',$request->email)->first();
    $pass = base64_encode($request->password);
        
    if ($checkmail && $pass == $checkmail->password){
            $respon['code'] = 200;
            $respon['data'] = $checkmail;
            return response()->json($respon);
    }else{
        $respon['code'] = 500;
        return response()->json($respon);
    }

 }   

public function getedu(Request $id){
    $checkid  = EmployePeople::where('id',$id->id)->first();
    if ($checkid) {
       
        $ids = EducationPeople::where('employe_id',$checkid->id)->get();

        if ($ids != null) {
           $respon['code'] = 200;
           $respon['data'] = $ids;
           return response()->json($respon);
        }else{
            $respon['code'] = 201;
            return response()->json($respon);
        }

         // $respon['code'] = 200;
         // $respon['data'] = $id;
            
    }else{

    }

}

}
