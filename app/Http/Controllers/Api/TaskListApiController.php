<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Branch;
use App\EmployePeople;
use App\Absensi;
use App\AbsensiOutlet;
use App\ViewTaskListProject;
use App\TaskLog;
use App\TaskPeople;
use App\LogOutletOpen;
use App\Outlet;
use App\SettingShift;
use QrCode;
use File;
use Storage;



class TaskListApiController extends Controller
{
   
  
    public $succesStatus = 200;
    public $failed = 500;

    public function getTasklist(Request $request)
    {
        $tasklist = DB::table('view_query_task')->where('people_id',$request->people_id)->where('status_flag',0)->where('delete_flag',1)->orderBy('task_id','desc')->get();

        if(count($tasklist) > 0){
            $task['message'] = 'list Tasklist';
            $task['code'] = 200;
            $task['data'] = $tasklist;
            return response()->json($task, $this->succesStatus);

        }elseif(count($tasklist) == 0){
            $task['message'] = 'list Tasklist Null';
            $task['code'] = 201;
            return response()->json($task, $this->succesStatus);
        }else{
            $task['message'] = 'error';
            $task['code'] = 500;
            return response()->json($task, $this->succesStatus); 
        }
        
    }

    public function getTimesZone()
    {

        $data['code'] = 200;
        $data['message'] = 'times zone';
        $data['tanggal'] = date('Y-m-d');
        $data['jam'] = date("H:i:s");
        $data['hari'] = date("l");

        return response()->json($data, $this->succesStatus); 
    }

    public function getAppAndroid()
    { 
        $app = DB::table('app_android')->orderBy('id_app','desc')->get();

        if(count($app) > 0){

            $data['code']  = 200;
            $data['message'] = 'App Android';
            $data['data'] = $app;
            return response()->json($data,$this->succesStatus);
        }elseif(count($app) == 0){
            $data['code']  = 201;
            $data['message'] = 'App Android null';
            return response()->json($data,$this->succesStatus);
        }else{
            $data['code']  = 500;
            $data['message'] = 'error system';
            return response()->json($data,$this->succesStatus);
        }


    }

    public function getAbsensiInShift(Request $request)
    {
        $people = EmployePeople::where('id',$request->people_id)->first();
        if($people){
            $absensi = array();
            $absensi['project_id'] = $request->project_id;
            $absensi['people_id'] = $request->people_id;
            $absensi['absensi_in'] = date("H:i:s");
            $absensi['date_absensi'] = date('Y-m-d');
            $absensi['in_langitude'] = $request->in_langitude;
            $absensi['in_latitude'] = $request->in_latitude;
            $absensi['active_flag'] = '1';
            $absensi['delete_flag'] = '1';
            $absensi['created_by'] = $people->first_name.' '.$people->middle_name.' '.$people->surname;
            $absensi['updated_by'] = 'system';
            $check = Absensi::where('people_id',$request->people_id)->where('date_absensi',date('Y-m-d'))->first();
            if($check){
                    $android = DB::table('view_absensi_android')->where('absensi_id',$check->id)->first();
                    $data['code']  = 201;
                    $data['message'] = 'Absensi In ready ';
                    $data['data'] = $android;
                    return response()->json($data,$this->succesStatus);
            }else{
                $data_absensi =  Absensi::create($absensi); 
                if($data_absensi){
                    $android = DB::table('view_absensi_android')->where('absensi_id',$data_absensi->id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi In People';
                    $data['data'] = $android;
                    return response()->json($data,$this->succesStatus);
                }else{
                     $data['code']  = 500;
                    $data['message'] = 'Erro Insert Data';
                    return response()->json($data,$this->succesStatus);
                }
            }
                 
        }else{
            $data['code']  = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }

    public function getAbsensiOut1111(Request $request)
    {
        $absensi = Absensi::where('people_id',$request->people_id)->where('date_absensi',$request->date_absensi)->first();
       
        $out = $absensi->absensi_out;

        if($absensi){
             $people = EmployePeople::where('id', $absensi->people_id)->first();
            if($out == null){
                    $update = DB::table('absensi_people')->where('id',$absensi->id)->update(['absensi_out' => date("H:i:s"), 'out_langitude' => $request->out_langitude, 'out_latitude' => $request->out_latitude,'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,'date_absensi_out' => date('Y-m-d') ]);
                if($update){
                    $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi Out People';
                    $data['data'] = $absensi_2;
                    return response()->json($data,$this->succesStatus);
                }else{
                    $data['code']  = 500;
                    $data['message'] = 'Error Update Data';
                    return response()->json($data,$this->succesStatus);
                }
            }else{
                $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$request->absensi_id)->first();
                $data['code']  = 201;
                $data['message'] = 'Absensi Out People ready';
                $data['data'] = $absensi_2;
                return response()->json($data,$this->succesStatus);

                
            }
           

        }else{
             $data['code']  = 500;
            $data['message'] = 'Data Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }

    public function getAbsensiOutletIn3(Request $request)
    {
        $people = EmployePeople::where('id',$request->people_id)->first();
        $absensi_outlet = AbsensiOutlet::where('outlet_id',$request->outlet_id)->where('date_create_task',date('Y-m-d'))->first();
        if($absensi_outlet){
            $update = AbsensiOutlet::where('id',$absensi_outlet->id)->update(['absensi_in' => date("H:i:s"), 'in_langitude' => $request->in_langitude, 'in_latitude' => $request->in_latitude ]);
           
            if($update){
                $android = DB::table('absensi_outlet_android')->where('absensi_outlet_id',$absensi_outlet->id)->first();
                $data['code']    = 200;
                $data['message'] = 'Absensi In People';
                $data['data']    = $android;
                return response()->json($data,$this->succesStatus);
            }else{
                $data['code']    = 500;
                $data['message'] = 'Update Insert Data';
                return response()->json($data,$this->succesStatus);
            }
        }
    }

    public function getAbsensiOutletIn(Request $request)
    {
        $people = EmployePeople::where('id',$request->people_id)->first();
        if($people){
            $outlet = array();
            $outlet['task_id']      = $request->task_id;
            $outlet['project_id']   = $request->project_id;
            $outlet['people_id']    = $request->people_id;
            $outlet['outlet_id']    = $request->outlet_id;
            $outlet['absensi_in']   = date("H:i:s");
            $outlet['absensi_out']  = "00:00:00";
            $outlet['date_absensi'] = date('Y-m-d');
            $outlet['schedule_date']= $request->schedule_date;
            $outlet['in_langitude'] = $request->in_langitude;
            $outlet['in_latitude']  = $request->in_latitude;
            $outlet['active_flag']  = '1';
            $outlet['delete_flag']  = '1';
            $outlet['created_by']   = $people->first_name.' '.$people->middle_name.' '.$people->surname;
            $outlet['updated_by']   = 'system';
            $check = DB::table('absensi_outlet_android')->where('task_id',$request->task_id)->where('project_id',$request->project_id)->where('people_id',$request->people_id)->where('outlet_id',$request->outlet_id)->where('date_absensi',date('Y-m-d'))->first();
            if($check){
                    $android = DB::table('absensi_outlet_android')->where('absensi_outlet_id',$check->absensi_outlet_id)->first();
                    $data['code']  = 201;
                    $data['message'] = 'Absensi In Outlet ready ';
                    $data['data'] = $android;
                    return response()->json($data,$this->succesStatus);
            }else{
                    $outlet_absensi =  AbsensiOutlet::create($outlet); 
                    if($outlet_absensi){
                        $android = DB::table('absensi_outlet_android')->where('absensi_outlet_id',$outlet_absensi->id)->first();
                        $data['code']    = 200;
                        $data['message'] = 'Absensi In People';
                        $data['data']    = $android;
                        return response()->json($data,$this->succesStatus);
                    }else{
                        $data['code']    = 500;
                        $data['message'] = 'Erro Insert Data';
                        return response()->json($data,$this->succesStatus);
                    }
            }

        }else{
            $data['code']    = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }

    public function getAbsensiOutletOut(Request $request)
    {
        
            $people = EmployePeople::where('id', $request->people_id)->first();
            if($people){
                 $outletout = DB::table('absensi_outlet_people')->where('task_id',$request->task_id)->where('people_id', $request->people_id)
                            ->where('outlet_id',$request->outlet_id)
                            ->where('date_absensi', $request->date_absensi)
                            ->update(['absensi_out' => date("H:i:s"),
                                                 'out_langitude' => $request->out_langitude,
                                                 'out_latitude' => $request->out_latitude,
                                                 'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,
                                                  'date_absensi_out' => date('Y-m-d') ]);

                if($outletout){
                    $absensi = DB::table('absensi_outlet_android')->where('task_id',$request->task_id)->where('people_id', $request->people_id)
                            ->where('outlet_id',$request->outlet_id)
                            ->where('date_absensi', $request->date_absensi)->first();
                    $data['code']    = 200;
                    $data['message'] = 'Absensi Out Outlet';
                    $data['data']    = $absensi;
                    return response()->json($data,$this->succesStatus);
                }else{
                    $data['code']    = 500;
                    $data['message'] = 'Absensi Out Outlet Failed';
                    return response()->json($data,$this->succesStatus);
                }
            }else{
                 $data['code']    = 500;
                $data['message'] = 'User Not Found';
                return response()->json($data,$this->succesStatus);
            }
        
                
            
    }

    public function postUpdateTask(Request $request)
    {
        $task_update = TaskPeople::where('id',$request->task_id)->first();
        if($task_update){
            $update = TaskPeople::where('id',$request->task_id)->update(['status_flag' => 1, 'received_date' => $request->received_date ]);
                $log = array();
                $log['task_id']       = $request->task_id;
                $log['status_flag']   = 1;
                $log['received_date'] = $request->received_date;
                $log['task_opened']   = $request->task_opened;
                $log['task_closed']   = $request->task_closed;
                $task_log = TaskLog::create($log);
            if($task_log){
                $data['code']          = 200;
                $data['message']       = 'Update Success';
                $data['task_id']       =$request->task_id;
                $data['received_date'] =$request->received_date;
                $data['task_opened']   =$request->task_opened;
                $data['task_closed']   =$request->task_closed;
                return response()->json($data,$this->succesStatus);
            }else{
                $data['code']    = 500;
                $data['message'] = 'Update failed';
                return response()->json($data,$this->succesStatus);
            }
        }else{
            $data['code']    = 500;
            $data['message'] = 'Task Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }

    public function postTaskOpen(Request $request)
    {

        $people = EmployePeople::where('id',$request->people_id)->first();
        if($people){
            $outlet_data = DB::table('view_outlet_open')->where('outlet_name',$request->outletName)->where('outlet_phone1',$request->phoneNumber)->first();
            if($outlet_data){

                $logOutlet = array();
                $logOutlet['people_id'] = $request->people_id;
                $logOutlet['outlet_id'] = $outlet_data->outlet_id;
                LogOutletOpen::create($logOutlet);

                $data['code']    = 201;
                $data['message'] = 'Outlet Already Available';
                $data['data']   = $outlet_data;
                return response()->json($data,$this->succesStatus);

            }else{
                     $address = DB::table('tb_post_code')->where('code_pos',$request->postCode)->first();
                    if($address == NULL){
                            $insert = new Outlet();
                            $insert->outlet_name          = $request->outletName;
                            $insert->outlet_address1      = $request->outletAddress;
                            $insert->outlet_post_code     = $request->postCode;
                            $insert->outlet_langitude     = $request->outlet_langitude;
                            $insert->outlet_latitude      = $request->outlet_latitude;
                            $insert->outlet_phone1        = $request->phoneNumber;
                            $insert->people_id            = $request->people_id;
                            $insert->created_by           = $people->first_name.' '.$people->middle_name.' '.$people->surname;
                            $insert->status          = '0';
                            $insert->delete_flag          = '1';
                            $foto = $request->file('image');
                            
                            if ($request->hasFile('image')) {
                              $insert->image1 = $this->uploadFoto($request);
                            }

                            $insert->save();
                            if($insert){

                                $logOutlet = array();
                                $logOutlet['people_id'] = $request->people_id;
                                $logOutlet['outlet_id'] = $insert->id;
                                LogOutletOpen::create($logOutlet);

                                $count = DB::table('task_people')->where('people_id', $request->people_id)->get();
                                $next_sequence = (int)$count->count() + 1;
                                $dt_task = array();
                                $dt_task['people_id']     = $request->people_id;
                                $dt_task['project_id']    = $request->project_id;
                                $dt_task['outlet_id']     = $insert->id;
                                $dt_task['task_start']    = date('Y-m-d');
                                $dt_task['task_end']      = date('Y-m-d');
                                $dt_task['description']   = 'task open';
                                $dt_task['schedule_date'] = date('Y-m-d');
                                $dt_task['task_type']     = 'task open';
                                $dt_task['task_number']   = "TP".date('Ymd').$request->people_id.$next_sequence;
                                $dt_task['status']        = '1';
                                $dt_task['status_flag']   = 0;
                                $dt_task['received_date'] = date('Y-m-d');
                                $dt_task['created_by']    = $people->first_name;
                                $dt_task['updated_by']    = 'system';
                                $dt_task['active_flag']   = '1';
                                $dt_task['delete_flag']   = '1';
                                $insertTask = TaskPeople::create($dt_task);
                                if($insertTask){
                                        $resp = DB::table('view_query_task')->where('task_id',$insertTask->id)->first();
                                        $data['code']    = 200;
                                        $data['message'] = 'Add Task Open Success';
                                        $data['data'] = $resp;
                                        return response()->json($data,$this->succesStatus);
                                    
                                }else{
                                    
                                    $data['code']    = 500;
                                    $data['message'] = 'Insert task open Failed';
                                    return response()->json($data,$this->succesStatus);
                                }

                            }else{
                                $data['code']    = 500;
                                $data['message'] = 'Insert Outlet Failed';
                                return response()->json($data,$this->succesStatus);
                            }

                    }else{
                         $insert = new Outlet();
                            $insert->outlet_name          = $request->outletName;
                            $insert->outlet_address1      = $request->outletAddress;
                            $insert->outlet_post_code     = $request->postCode;
                            $insert->outlet_langitude     = $request->outlet_langitude;
                            $insert->outlet_latitude      = $request->outlet_latitude;
                            $insert->outlet_phone1        = $request->phoneNumber;
                            $insert->outlet_province      = $address->province;
                            $insert->outlet_city          = $address->district;
                            $insert->people_id            = $request->people_id;
                            $insert->created_by           = $people->first_name.' '.$people->middle_name.' '.$people->surname;
                            $insert->status               = '0';
                            $insert->delete_flag          = '1';
                            $foto = $request->file('image');
                            
                            if ($request->hasFile('image')) {
                              $insert->image1 = $this->uploadFoto($request);
                            }

                            $insert->save();
                            if($insert){

                                $logOutlet = array();
                                $logOutlet['people_id'] = $request->people_id;
                                $logOutlet['outlet_id'] = $insert->id;
                                LogOutletOpen::create($logOutlet);

                                $count = DB::table('task_people')->where('people_id', $request->people_id)->get();
                                $next_sequence = (int)$count->count() + 1;
                                $dt_task = array();
                                $dt_task['people_id']     = $request->people_id;
                                $dt_task['project_id']    = $request->project_id;
                                $dt_task['outlet_id']     = $insert->id;
                                $dt_task['task_start']    = date('Y-m-d');
                                $dt_task['task_end']      = date('Y-m-d');
                                $dt_task['description']   = 'task open';
                                $dt_task['schedule_date'] = date('Y-m-d');
                                $dt_task['task_type']     = 'task open';
                                $dt_task['task_number']   = "TP".date('Ymd').$request->people_id.$next_sequence;
                                $dt_task['status']        = '1';
                                $dt_task['status_flag']   = 0;
                                $dt_task['received_date'] = date('Y-m-d');
                                $dt_task['created_by']    = $people->first_name;
                                $dt_task['updated_by']    = 'system';
                                $dt_task['active_flag']   = '1';
                                $dt_task['delete_flag']   = '1';
                                $insertTask = TaskPeople::create($dt_task);
                                if($insertTask){
                                        $resp = DB::table('view_query_task')->where('task_id',$insertTask->id)->first();
                                        $data['code']    = 200;
                                        $data['message'] = 'Add Task Open Success';
                                        $data['data'] = $resp;
                                        return response()->json($data,$this->succesStatus);
                                }else{
                                    $data['code']    = 500;
                                    $data['message'] = 'Insert task open Failed';
                                    return response()->json($data,$this->succesStatus);
                                }

                            }else{
                                $data['code']    = 500;
                                $data['message'] = 'Insert Outlet Failed';
                                return response()->json($data,$this->succesStatus);
                            }

                    }
            }
            
           

        }else{
            $data['code']    = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }

       
    }

     private function uploadFoto(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
             $rand = rand(10,100);
            $namaFoto = 'TP'.date('Ymd').$rand.".".$ext;
            $upload_path = 'outlet';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

      public function postUpdateTaskOpen(Request $request)
      {
        $taskId = $request->task_id;
        if($taskId){
                $log = array();
                $log['task_id']       = $request->task_id;
                $log['status_flag']   = 1;
                $log['received_date'] = date('Y-m-d h:s:i');
                $log['task_opened']   = date('Y-m-d');
                $log['task_closed']   = date('Y-m-d');
                $task_log = TaskLog::create($log);
                if($task_log){
                    $update = TaskPeople::where('id',$taskId)->update(['status_flag' => 1, 'received_date' => date('Y-m-d h:s:i') ]);
                    if($update){
                        $data['code']          = 200;
                        $data['message']       = 'update Task Open Success';
                        $data['task_id']       =$task_log->task_id;
                        $data['received_date'] =$task_log->received_date;
                        $data['task_opened']   =$task_log->task_opened;
                        $data['task_closed']   =$task_log->task_closed;
                        return response()->json($data,$this->succesStatus);
                    }else{
                        $data['code']    = 500;
                        $data['message'] = 'update Task Open failed';
                        return response()->json($data,$this->succesStatus);
                    }
                }else{
                    $data['code']    = 500;
                    $data['message'] = 'insert Task log failed';
                    return response()->json($data,$this->succesStatus);
                }
            
        }else{
            $data['code']    = 500;
            $data['message'] = 'Data Not Found';
            return response()->json($data,$this->succesStatus);
        }
      }

      public function listOutletOpen(Request $request)
      {
         $listOutlet = DB::table('view_log_outlet_open')->where('people_id',$request->people_id)->distinct('outlet_name')->get();
         if(count($listOutlet) > 0){
            $data['code']    = 200;
            $data['message'] = 'List Outlet Open';
            $data['data']    = $listOutlet;
            return response()->json($data,$this->succesStatus);
        }elseif(count($listOutlet) == 0 ){
            $data['code']    = 201;
            $data['message'] = 'List outlet not found';
            return response()->json($data,$this->succesStatus);
         }else{
            $data['code']    = 500;
            $data['message'] = 'Data Not Found';
            return response()->json($data,$this->succesStatus);
         }
      }

      public function generateTasklistByOutlet(Request $request)
      {
        $people = EmployePeople::where('id',$request->people_id)->first();
        if($people){
            $check_data = TaskPeople::where('outlet_id',$request->outlet_id)->where('schedule_date',date('Y-m-d'))->first();
            if($check_data){
                $data['code']    = 201;
                $data['message'] = 'already done the task at the outlet';
                return response()->json($data,$this->succesStatus);

            }else{
                $count = DB::table('task_people')->where('people_id', $request->people_id)->get();
            $next_sequence = (int)$count->count() + 1;
            $dt_task = array();
            $dt_task['people_id']     = $request->people_id;
            $dt_task['project_id']    = $request->project_id;
            $dt_task['outlet_id']     = $request->outlet_id;
            $dt_task['task_start']    = date('Y-m-d');
            $dt_task['task_end']      = date('Y-m-d');
            $dt_task['description']   = 'task open';
            $dt_task['schedule_date'] = date('Y-m-d');
            $dt_task['task_type']     = 'task open';
            $dt_task['task_number']   = "TP".date('Ymd').$request->people_id.$next_sequence;
            $dt_task['status']        = '0';
            $dt_task['status_flag']   = 0;
            $dt_task['received_date'] = date('Y-m-d');
            $dt_task['created_by']    = $people->first_name;
            $dt_task['updated_by']    = 'system';
            $dt_task['active_flag']   = '1';
            $dt_task['delete_flag']   = '1';
            $insertTask = TaskPeople::create($dt_task);
             if($insertTask){

                    $outlet = array();
                    $outlet['task_id']      = $insertTask->id;
                    $outlet['project_id']   = $request->project_id;
                    $outlet['people_id']    = $request->people_id;
                    $outlet['outlet_id']    = $request->outlet_id;
                    $outlet['absensi_in']   = "00:00:00";
                    $outlet['absensi_out']  = "00:00:00";
                    $outlet['in_langitude'] = "0";
                    $outlet['in_latitude']  = "0";
                    $outlet['date_absensi'] = date('Y-m-d');
                    $outlet['schedule_date']= date('Y-m-d');
                    $outlet['active_flag']  = '1';
                    $outlet['delete_flag']  = '1';
                    $outlet['date_create_task'] = date('Y-m-d');
                    $outlet['created_by']   = $people->first_name.' '.$people->middle_name.' '.$people->surname;
                    $outlet['updated_by']   = 'system';

                    $outlet_absensi =  AbsensiOutlet::create($outlet);
                    if($outlet_absensi){
                        $resp = DB::table('view_query_task')->where('task_id',$insertTask->id)->first();
                        $data['code']    = 200;
                        $data['message'] = 'Add Task Open Success';
                        $data['data'] = $resp;
                        return response()->json($data,$this->succesStatus);
                    }else{
                        $data['code']    = 500;
                        $data['message'] = 'Insert task open Failed';
                        return response()->json($data,$this->succesStatus);
                    }
                }else{
                    $data['code']    = 500;
                    $data['message'] = 'Insert task open Failed';
                    return response()->json($data,$this->succesStatus);
                }
            }

        }else{
            $data['code']    = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }

      }


    public function getTasklisActive(Request $request)
    {
        $tasklist = DB::table('view_query_task')->where('people_id',$request->people_id)->where('delete_flag',1)->orderBy('task_id','desc')->get();
        $aktif = [];
        $expire = [];
        if(count($tasklist) > 0){
            foreach ($tasklist as $key) {
               
                $start = $key->task_start;
                $end = $key->task_end;
                $today = date('Y-m-d');
                
                if($today >= $start && $today <= $end ){
                    $shiftOutlet = DB::table('setting_shift_project')
                                    ->where('project_id', $key->project_id)
                                    ->where('outlet_id',$key->outlet_id)
                                    ->where('code_shift',$key->code_shift)
                                    ->first();
                    if($shiftOutlet == null){
                        $aktif[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => '',
                                 'hour_out'          => ''   ];

                    }else{
                        $aktif[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => $shiftOutlet->hour_in,
                                 'hour_out'          => $shiftOutlet->hour_out   ];

                    }
                     
                    
                }else{
                     
                    $shiftOutlet = DB::table('setting_shift_project')
                                    ->where('project_id', $key->project_id)
                                    ->where('outlet_id',$key->outlet_id)
                                    ->where('code_shift',$key->code_shift)
                                    ->first();
                    if($shiftOutlet == null){
                        $expire[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => '',
                                 'hour_out'          => ''   ];

                    }else{
                        $expire[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => $shiftOutlet->hour_in,
                                 'hour_out'          => $shiftOutlet->hour_out   ];

                    }
                }

            //endforeach
            }
            $task['message'] = 'list Tasklist';
            $task['code'] = 200;
            $task['data'] = $aktif;
            return response()->json($task, $this->succesStatus);

        }elseif(count($tasklist) == 0){
            $task['message'] = 'list Tasklist Null';
            $task['code'] = 201;
            return response()->json($task, $this->succesStatus);
        }else{
            $task['message'] = 'error';
            $task['code'] = 500;
            return response()->json($task, $this->succesStatus); 
        }
        
    }


    public function getTasklisExpire(Request $request)
    {
        $tasklist = DB::table('view_query_task')->where('people_id',$request->people_id)->where('delete_flag',1)->orderBy('task_id','desc')->get();
        $aktif = [];
        $expire = [];

        if(count($tasklist) > 0){
            foreach ($tasklist as $key) {
                $start = $key->task_start;
                $end = $key->task_end;
                $today = date('Y-m-d');
                
                if($today >= $start && $today <= $end ){
                    $shiftOutlet = DB::table('setting_shift_project')
                                    ->where('project_id', $key->project_id)
                                    ->where('outlet_id',$key->outlet_id)
                                    ->where('code_shift',$key->code_shift)
                                    ->first();
                    if($shiftOutlet == null){
                        $aktif[] = [ "task_id"     => $key->task_id,
                                 "outlet_name"     => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city"     => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village"   => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1"    => $key->outlet_phone1,
                                 "outlet_phone2"    => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude"  => $key->outlet_latitude,
                                 "project_name"   => $key->project_name,
                                 "people_id"      => $key->people_id,
                                 "project_id"     => $key->project_id,
                                 "outlet_id"      => $key->outlet_id,
                                 "task_start"     => $key->task_start,
                                 "task_end"       => $key->task_end,
                                 "schedule_date"  => $key->schedule_date,
                                 "task_type"      => $key->task_type,
                                 "task_number"    => $key->task_number,
                                 "description"    => $key->description,
                                 "status"         => $key->status,
                                 "status_flag"    => $key->status_flag,
                                 "owner_name"     => $key->owner_name,
                                 "imei_user"      => $key->imei_user,
                                 'code_shift'     => $key->code_shift,
                                 'hour_in'        => '',
                                 'hour_out'       => ''   ];

                    }else{
                        $aktif[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                 "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => $shiftOutlet->hour_in,
                                 'hour_out'          => $shiftOutlet->hour_out   ];

                    }
                     
                    
                }else{
                     
                    $shiftOutlet = DB::table('setting_shift_project')
                                    ->where('project_id', $key->project_id)
                                    ->where('outlet_id',$key->outlet_id)
                                    ->where('code_shift',$key->code_shift)
                                    ->first();
                    if($shiftOutlet == null){
                        $expire[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                 "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => '',
                                 'hour_out'          => ''   ];

                    }else{
                        $expire[] = [ "task_id" => $key->task_id,
                                 "outlet_name" => $key->outlet_name,
                                 "outlet_address1" => $key->outlet_address1,
                                 "outlet_province" => $key->outlet_province,
                                 "outlet_city" => $key->outlet_city,
                                 "outlet_sub_district" => $key->outlet_sub_district,
                                 "outlet_village" => $key->outlet_village,
                                 "outlet_post_code" => $key->outlet_post_code,
                                 "outlet_phone1" => $key->outlet_phone1,
                                 "outlet_phone2" => $key->outlet_phone2,
                                 "outlet_langitude" => $key->outlet_langitude,
                                 "outlet_latitude" => $key->outlet_latitude,
                                 "project_name" => $key->project_name,
                                 "people_id" => $key->people_id,
                                 "project_id" => $key->project_id,
                                 "outlet_id" => $key->outlet_id,
                                 "task_start" => $key->task_start,
                                 "task_end" => $key->task_end,
                                 "schedule_date" => $key->schedule_date,
                                 "task_type" => $key->task_type,
                                 "task_number" => $key->task_number,
                                 "description" => $key->description,
                                 "status" => $key->status,
                                 "status_flag" => $key->status_flag,
                                 "owner_name" => $key->owner_name,
                                 "imei_user" => $key->imei_user,
                                 'code_shift'          => $key->code_shift,
                                 'hour_in'         => $shiftOutlet->hour_in,
                                 'hour_out'          => $shiftOutlet->hour_out   ];

                    }
                }

            //endforeach
            }
            $task['message'] = 'list Tasklist';
            $task['code'] = 200;
            $task['data'] = $expire;
            return response()->json($task, $this->succesStatus);

        }elseif(count($tasklist) == 0){
            $task['message'] = 'list Tasklist Null';
            $task['code'] = 201;
            return response()->json($task, $this->succesStatus);
        }else{
            $task['message'] = 'error';
            $task['code'] = 500;
            return response()->json($task, $this->succesStatus); 
        }
        
    }

    public function getAbsensiIn(Request $request)
    {
        $people = EmployePeople::where('id',$request->people_id)->first();
        
        $check_shift = SettingShift::where('project_id', $request->project_id)
                                    ->where('outlet_id', $request->outlet_id)
                                    ->where('code_shift', $request->code_shift)
                                    ->first();

        if($check_shift) {
            if($people){
                $absensi = array();
                $absensi['project_id']   = $request->project_id;
                $absensi['people_id']    = $request->people_id;
                $absensi['absensi_in']   = date("H:i:s");
                $absensi['date_absensi'] = date('Y-m-d');
                $absensi['in_langitude'] = $request->in_langitude;
                $absensi['in_latitude']  = $request->in_latitude;
                $absensi['active_flag']  = '1';
                $absensi['delete_flag']  = '1';
                $absensi['id_shift']     =   $check_shift->id;
                if($request->image == null){
                     $absensi['image']        =  " ";
                }else{
                     $absensi['image']        =  $this->uploadFotoImageAbsensi($request);
                }
               
                $absensi['created_by']   = $people->first_name.' '.$people->middle_name.' '.$people->surname;
                $absensi['updated_by']   = 'system';
                $check = Absensi::where('people_id',$request->people_id)->where('date_absensi',date('Y-m-d'))->first();
                if($check){
                        $android = DB::table('view_absensi_android')->where('absensi_id',$check->id)->first();
                        $data['code']    = 201;
                        $data['message'] = 'Absensi In ready ';
                        $data['data']    = $android;
                        return response()->json($data,$this->succesStatus);
                }else{
                    $data_absensi =  Absensi::create($absensi); 
                    if($data_absensi){
                        $android          = DB::table('view_absensi_android')->where('absensi_id',$data_absensi->id)->first();
                        $data['code']     = 200;
                        $data['message']  = 'Absensi In People';
                        $data['data']     = $android;
                        return response()->json($data,$this->succesStatus);
                    }else{
                         $data['code']   = 500;
                        $data['message'] = 'Erro Insert Data';
                        return response()->json($data,$this->succesStatus);
                    }
                }
                     
            }else{
                $data['code']  = 500;
                $data['message'] = 'User Not Found';
                return response()->json($data,$this->succesStatus);
            }
        }else{
            $data['code']  = 501;
            $data['message'] = 'data Not Found';
            return response()->json($data,$this->succesStatus);

        }

        
    }

     private function uploadFotoImageAbsensi(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'absensi_people';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
    }


    public function getAbsensiOut(Request $request)
    {
        $absensi = DB::table('view_absensi_android')->where('people_id',$request->people_id)->orderBy('absensi_id','desc')->first();
        $people = EmployePeople::where('id', $absensi->people_id)->first();
        $shift = $absensi->code_shift;
        $out = $absensi->absensi_out;
        $date = strtotime($request->date_absensi) - 1;
       
        if($shift == 1){
            if($out == null){
                    $update = DB::table('absensi_people')->where('id',$absensi->absensi_id)->update(['absensi_out' => date("H:i:s"),
                         'out_langitude' => $request->out_langitude,
                         'out_latitude' => $request->out_latitude,
                         'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,
                         'date_absensi_out' => date('Y-m-d') ]);
                if($update){
                    $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi Out People';
                    $data['data'] = $absensi_2;
                    return response()->json($data,$this->succesStatus);
              
                }else{
                    $data['code']  = 500;
                    $data['message'] = 'Error Update Data';
                    return response()->json($data,$this->succesStatus);
                }
            }else{
                $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                $data['code']  = 201;
                $data['message'] = 'Absensi Out People ready';
                $data['data'] = $absensi_2;
                return response()->json($data,$this->succesStatus);
            }
        }elseif($shift == 2){
            if($out == null){
                    $update = DB::table('absensi_people')->where('id',$absensi->absensi_id)->update(['absensi_out' => date("H:i:s"), 'out_langitude' => $request->out_langitude, 'out_latitude' => $request->out_latitude,'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,'date_absensi_out' => date('Y-m-d') ]);
                if($update){
                    $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi Out People';
                    $data['data'] = $absensi_2;
                    return response()->json($data,$this->succesStatus);
              
                }else{
                    $data['code']  = 500;
                    $data['message'] = 'Error Update Data';
                    return response()->json($data,$this->succesStatus);
                }
            }else{
                $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                $data['code']  = 201;
                $data['message'] = 'Absensi Out People ready';
                $data['data'] = $absensi_2;
                return response()->json($data,$this->succesStatus);
            }

        }elseif($shift == 3) {
            if($out == null){
                    $update = DB::table('absensi_people')->where('id',$absensi->absensi_id)->update(['absensi_out' => date("H:i:s"), 'out_langitude' => $request->out_langitude, 'out_latitude' => $request->out_latitude,'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,
                        'date_absensi_out' => date('Y-m-d',$date) ]);
                if($update){
                    $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi Out People';
                    $data['data'] = $absensi_2;
                    return response()->json($data,$this->succesStatus);
              
                }else{
                    $data['code']  = 500;
                    $data['message'] = 'Error Update Data';
                    return response()->json($data,$this->succesStatus);
                }
            }else{

                $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                $data['code']  = 201;
                $data['message'] = 'Absensi Out People ready';
                $data['data'] = $absensi_2;
                return response()->json($data,$this->succesStatus);
            }
        }else{
            if($out == null){
                    $update = DB::table('absensi_people')->where('id',$absensi->absensi_id)->update(['absensi_out' => date("H:i:s"),
                         'out_langitude' => $request->out_langitude,
                         'out_latitude' => $request->out_latitude,
                         'updated_by'=>$people->first_name.' '.$people->middle_name.' '.$people->surname,
                         'date_absensi_out' => date('Y-m-d') ]);
                if($update){
                    $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                    $data['code']  = 200;
                    $data['message'] = 'Absensi Out People';
                    $data['data'] = $absensi_2;
                    return response()->json($data,$this->succesStatus);
              
                }else{
                    $data['code']  = 500;
                    $data['message'] = 'Error Update Data';
                    return response()->json($data,$this->succesStatus);
                }
            }else{
                $absensi_2 = DB::table('view_absensi_android')->where('absensi_id',$absensi->absensi_id)->first();
                $data['code']  = 201;
                $data['message'] = 'Absensi Out People ready';
                $data['data'] = $absensi_2;
                return response()->json($data,$this->succesStatus);
            }

        }
        
    }

    public function getQtyShift(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $listShift = DB::table('view_setting_shift_project')->where('project_id',$request->project_id)->where('outlet_id',$request->outlet_id)->where('delete_flag',1)->get();

            if ($listShift != null) {
                $data['code']  = 200;
                $data['message'] = 'list shift outlet';
                $data['data'] = $listShift;
                return response()->json($data,$this->succesStatus);
            }else{
                $data['code']  = 201;
                $data['message'] = 'List Shift Null';
                return response()->json($data,$this->succesStatus);
            }
        }else{
             $data['code']  = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }


    public function CheckAbsensiOutletNews(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $checkAbsensi = DB::table('absensi_outlet_android')
                            ->where('project_id',$request->project_id)
                            ->where('people_id',$request->people_id)
                            ->where('date_absensi', date('Y-m-d'))
                            ->get();
            if (count($checkAbsensi) > 0){
                $data['code']    = 200;
                $data['message'] = 'sudah melakukan absensi di outlet';
                return response()->json($data,$this->succesStatus);
                 
                
            }else{
               $data['code']     = 404;
                $data['message'] = 'belum melakukan absensi di outlet';
                return response()->json($data,$this->succesStatus);
            }
        }else{
            $data['code']  = 500;
            $data['message'] = 'User Not Found';
            return response()->json($data,$this->succesStatus);
        }
    }


   
}
