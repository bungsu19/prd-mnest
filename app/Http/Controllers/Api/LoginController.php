<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
//use Illuminate\Support\Facedesc\DB;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Branch;
use App\EmployePeople;
use App\ProjectMember;
use App\Project;
use QrCode;
use File;
use Storage;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer;



class LoginController extends Controller
{
   
  
	public $succesStatus = 200;
	public $failed = 500;

    public function test()
    {
    	$data = Branch::where('delete_flag',1)->orderBy('id','desc')->get();
    	$test['message'] = 'ok';
        
        $test['status'] = 200;
    	$test['data'] = $data;
    	return response()->json($test, $this->succesStatus);
    }

    public function getLogin(Request $request)
    {
        $length = 200;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = base64_encode($request->password);
        $user = EmployePeople::where('email_address',$request->email)->first();
         if ($user && $password == $user->password) {
            $project = ProjectMember::where('employee_id',$user->id)->first();
            if($project){
                $project_name = Project::where('id',$project->project_id)->first();
                $respon['code']                 = 200;
                $respon['token']                = $randomString;
                $respon['message']              = 'Login Success';
                $respon['people_id']            = $user->id;
                $respon['project_id']           = $project->project_id;
                $respon['project_name']         = $project_name->project_name;
                $respon['first_name']           = $user->first_name;
                $respon['middle_name']          = $user->middle_name;
                $respon['last_name']            = $user->surname;
                $respon['email']                = $user->email_address;
                $respon['address']              = $user->ktp_address;
                $respon['date_of_britday']      = $user->dob2;
                $respon['ktp_province']         = $user->ktp_province;
                $respon['ktp_district']         = $user->ktp_district;
                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                $respon['domisili']             = $user->dom_address;
                $respon['phone_number1']        = $user->phone1;
                $respon['phone_number2']        = $user->phone2;
                $respon['status_user']           = $user->status_user;
                $respon['image']                = $user->image;
                $respon['password']             = " ";
                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['code']                 = 200;
                $respon['token']                = $randomString;
                $respon['message']              = 'Login Success';
                $respon['people_id']            = $user->id;
                $respon['project_id']           = '0';
                $respon['project_name']         = ' ';
                $respon['first_name']           = $user->first_name;
                $respon['middle_name']          = $user->middle_name;
                $respon['last_name']            = $user->surname;
                $respon['email']                = $user->email_address;
                $respon['address']              = $user->ktp_address;
                $respon['date_of_britday']      = $user->dob2;
                $respon['ktp_province']         = $user->ktp_province;
                $respon['ktp_district']         = $user->ktp_district;
                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                $respon['domisili']             = 'a';
                $respon['phone_number1']        = $user->phone1;
                $respon['phone_number2']        = $user->phone2;
                $respon['image']                = $user->image;
                $respon['status_user']           = $user->status_user;
                $respon['password']             = " ";
                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                return response()->json($respon, $this->succesStatus);
            }
                
         }else{
            $respon['code'] = 500;
            $respon['message']              = 'Login Failed';
            return response()->json($respon, $this->succesStatus);
         }


    }


    public function getLoginDev(Request $request)
    {
        $length = 200;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = base64_encode($request->password);
        $imei = $request->imei_user;

        $user = EmployePeople::where('email_address',$request->email)->first();
        if($user){
                if($user->imei_user == $imei && $user->cek_login == 1){
                
                if ($user && $password == $user->password) {
                    $project = ProjectMember::where('employee_id',$user->id)->first();
                    if($project->project_id != 0 ){
                        $project_name = Project::where('id',$project->project_id)->first();
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = $project->project_id;
                        $respon['project_name']         = $project_name->project_name;
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = $user->dom_address;
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['status_user']           = $user->status_user;
                        $respon['image']                = $user->image;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                        $respon['status_project']       = $user->status_project;
                        $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                        return response()->json($respon, $this->succesStatus);
                    }else{
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = '0';
                        $respon['project_name']         = ' ';
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = 'a';
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['image']                = $user->image;
                        $respon['status_user']           = $user->status_user;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                         $respon['status_project']       = $user->status_project;
                        $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                        return response()->json($respon, $this->succesStatus);
                    }
                        
                 }else{
                    $respon['code'] = 500;
                    $respon['message']              = 'Login failed';
                    return response()->json($respon, $this->succesStatus);
                 }

            }elseif($user->imei_user == $imei && $user->cek_login == 0){

                if ($user && $password == $user->password) {
                    $project = ProjectMember::where('employee_id',$user->id)->first();
                    if($project){
                        $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'cek_login' => 1]);
                        $project_name = Project::where('id',$project->project_id)->first();
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = $project->project_id;
                        $respon['project_name']         = $project_name->project_name;
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = $user->dom_address;
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['status_user']           = $user->status_user;
                        $respon['image']                = $user->image;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                         $respon['status_project']       = $user->status_project;
                         
                        return response()->json($respon, $this->succesStatus);
                    }else{
                         $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'cek_login' => 1]);
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = '0';
                        $respon['project_name']         = ' ';
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = 'a';
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['image']                = $user->image;
                        $respon['status_user']           = $user->status_user;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                         $respon['status_project']       = $user->status_project;
                        
                        return response()->json($respon, $this->succesStatus);
                    }
                        
                 }else{
                    $respon['code'] = 500;
                    $respon['message']              = 'Login failed';
                    return response()->json($respon, $this->succesStatus);
                 }

            }elseif($user->imei_user != $imei && $user->cek_login == 0){
                
                if ($user && $password == $user->password) {
                    $project = ProjectMember::where('employee_id',$user->id)->first();
                    if($project){
                        $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'cek_login' => 1]);
                        $project_name = Project::where('id',$project->project_id)->first();
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = $project->project_id;
                        $respon['project_name']         = $project_name->project_name;
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = $user->dom_address;
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['status_user']           = $user->status_user;
                        $respon['image']                = $user->image;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                         $respon['status_project']       = $user->status_project;
                         
                        return response()->json($respon, $this->succesStatus);
                    }else{
                         $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'cek_login' => 1]);
                        $respon['code']                 = 200;
                        $respon['token']                = $randomString;
                        $respon['message']              = 'Login Success';
                        $respon['people_id']            = $user->id;
                        $respon['project_id']           = '0';
                        $respon['project_name']         = ' ';
                        $respon['first_name']           = $user->first_name;
                        $respon['middle_name']          = $user->middle_name;
                        $respon['last_name']            = $user->surname;
                        $respon['email']                = $user->email_address;
                        $respon['address']              = $user->ktp_address;
                        $respon['date_of_britday']      = $user->dob2;
                        $respon['ktp_province']         = $user->ktp_province;
                        $respon['ktp_district']         = $user->ktp_district;
                        $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                        $respon['domisili']             = 'a';
                        $respon['phone_number1']        = $user->phone1;
                        $respon['phone_number2']        = $user->phone2;
                        $respon['image']                = $user->image;
                        $respon['status_user']           = $user->status_user;
                        $respon['password']             = " ";
                        $respon['imei_user']            = $imei;
                        $respon['cek_login']            = 1;
                         $respon['status_project']       = $user->status_project;
                        
                        return response()->json($respon, $this->succesStatus);
                    }
                        
                 }else{
                    $respon['code'] = 500;
                    $respon['message']              = 'Login failed';
                    return response()->json($respon, $this->succesStatus);
                 }
            }elseif($user->imei_user != $imei && $user->cek_login == 1){
                
                $respon['code'] = 500;
                $respon['message']              = 'User Already Used';
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['code'] = 500;
                $respon['message']              = 'IMEI Not Valid';
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['code'] = 501;
            $respon['message']              = 'user not found';
            return response()->json($respon, $this->succesStatus);
        }   

    }

    public function postForgotPassword(Request $request)
    {
        $email = $request->email;
        $dob = $request->dob;
        $randomPassword = rand(1000,1000000);
        $check = DB::table('employee_people')->where('email_address', $email)->where('delete_flag',1)->first();
        if($check){
                if($check->dob2 == $dob){

                   

                    $mail = new PHPMailer\PHPMailer;
                    $mail->isSMTP();                            // Set mailer to use SMTP
                    $mail->SMTPAuth   = true;                     // Enable SMTP authentication
                    $mail->Username   = 'kreasibungsu19@gmail.com';          // SMTP username
                    $mail->Password   = 'bungsu123'; // SMTP password
                    $mail->Host       = 'smtp.gmail.com';
                    $mail->SMTPSecure = 'tls';
                    $mail->Port       = 587;
                    //Set the encryption system to use - ssl (deprecated) or tls
                    $mail->setFrom('kreasibungsu19@gmail.com', 'PT.SDM');
                    // $mail->addReplyTo('hamrullah1902@gmail.com', 'lagi');
                    $mail->addAddress($email);   // Add a recipient
                    $mail->isHTML(true);  // Set email format to HTML
                    $message = '
                      <p>Anda telah mengaktifkan fitur lupa password.</p>'
                      .'<p>Silahkan login menggunakan email dan password Berikut ini untuk login kembali:</p>'
                      .'<h2>'.'email = '.$email.'<br>'
                      .'password = ' .$randomPassword.'</h2>'

                      .'<p>Terima Kasih. </p>';
                    $mail->msgHTML($message);
                    if(!$mail->send())
                    {
                        $data['message'] = 'Register user failed';
                        $data['code']    = 500;
                        return response()->json($data, $this->succesStatus);
                    } else {
                         $forgot = DB::table('employee_people')->where('id',$check->id)->update(['password' => base64_encode($randomPassword)]);
                       $data['message'] = 'Register Success Please Cek Email';
                       $data['code']    = 200;
                       $data['email']   = $email;
                       return response()->json($data, $this->succesStatus);
                    }
                }else{
                    $respon['code'] = 400;
                    $respon['message']      = 'Tanggal lahir tidak sesuai';
                    return response()->json($respon, $this->succesStatus);
                }
        }else{
            $respon['code'] = 501;
            $respon['message']      = 'email tidak di temukan';
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getCekEmail(Request $request)
    {
        $cekEmail = DB::table('employee_people')->where('email_address', $request->email_address)->first();
        dd($cekEmail);
        if ($cekEmail) {
            $respon['code'] = 200;
            $respon['email'] = $cekEmail->email_address;
            $respon['name'] = $cekEmail->first_name.$cekEmail->middle_name.$cekEmail->surname;
            $respon['message']      = 'email di temukan';
            return response()->json($respon, $this->succesStatus);
        }else{
            $respon['code'] = 501;
            $respon['message']      = 'email tidak di temukan';
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function postChangePassword(Request $request)
    {
        $passwordOld = base64_encode($request->password_old);
        $passwordNew = base64_encode($request->password_new);
        $check = DB::table('employee_people')->where('id', $request->people_id)->first();
        if ($check) {
            if($check->password == $passwordOld){
                $updatePassword = DB::table('employee_people')->where('id',$request->people_id)->update(['password' => $passwordNew]);
                if ($updatePassword) {
                    $respon['code'] = 200;
                    $respon['message']      = 'update password Success';
                    return response()->json($respon, $this->succesStatus);
                }else{
                    $respon['code'] = 503;
                    $respon['message']      = 'update password gagal';
                    return response()->json($respon, $this->succesStatus);
                }

            }else{
                $respon['code'] = 504;
                $respon['message']      = 'password lama tidak sesuai';
                return response()->json($respon, $this->succesStatus);
            }
            
        }else{
            $respon['code'] = 505;
            $respon['message']      = 'user not found';
            return response()->json($respon, $this->succesStatus);
        }
    }
}
