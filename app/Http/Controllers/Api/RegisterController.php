<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Branch;
use App\EmployePeople;
use App\ProjectMember;
use App\Project;
use App\PeopleVacancy;
use App\DocumentPeople;
use App\ImagePeople;
use App\LogLoginAndroid;
use QrCode;
use File;
use DB;
use Storage;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer;
// use Illuminate\Support\Facades\Storage;
// use Intervention\Image\ImageManagerStatic as Image;
// use Illuminate\Support\Facades\Input;



class RegisterController extends Controller
{
   
  
	public $succesStatus = 200;
	public $failed = 500;

    public function test()
    {
    	$data = Branch::where('delete_flag',1)->orderBy('id','desc')->get();
    	$test['message'] = 'ok';
        
        $test['status'] = 200;
    	$test['data'] = $data;
    	return response()->json($test, $this->succesStatus);
    }

    public function postRegisterUser(Request $request)
    {
        $check = DB::table('people_view2')->where('email_address',$request->email)->where('status_account',1)->first();
        
        if($check){
            $data['message'] = 'data use already';
            $data['code'] = 201;
            $data['data'] = $check;
            return response()->json($data, $this->succesStatus);
        }else{

            $people = array();
            $people['email_address'] = $request->email;
            $people['phone1'] = $request->phone;
            $people['first_name'] = $request->first_name;
            $people['middle_name'] = $request->middle_name;
            $people['surname'] = $request->surname;
            $people['password'] = base64_encode($request->password);
            $people['imei_user'] = $request->imei_user;
            $people['status_account'] = 1;
            $people['status_project'] = 2;
            $people['data_status'] = 1;
            $people['status_user'] = 0;
            $people['delete_flag'] = 1;
            $people['active_flag'] = 1;
            $people['branch_id'] = 7;
            $people['cek_login'] = 0;
            $save = EmployePeople::create($people);
            if($save){
                $data['message'] = 'data use already';
                $data['code'] = 200;
                $data['data'] = $save;
                return response()->json($data, $this->succesStatus);
            }else{
                $data['message'] = 'save data people failed';
                $data['code'] = 500;
                return response()->json($data, $this->succesStatus);
            }

        }
    }

    public function getLoginGmail(Request $request)
    {
        $length = 200;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = base64_encode($request->password);
        $imei = $request->imei_user;
        $user = DB::table('employee_people')->where('email_address',$request->email)->where('status_account',$request->status_account)->first();
        if($user){
             $project = ProjectMember::where('employee_id',$user->id)->first();

            if($project == null ){
               $respon['code']                 = 200;
                $respon['token']                = $randomString;
                $respon['message']              = 'Login Success';
                $respon['people_id']            = $user->id;
                $respon['project_id']           = '0';
                $respon['project_name']         = ' ';
                $respon['first_name']           = $user->first_name;
                $respon['middle_name']          = $user->middle_name;
                $respon['last_name']            = $user->surname;
                $respon['email']                = $user->email_address;
                $respon['address']              = $user->ktp_address;
                $respon['date_of_britday']      = $user->dob2;
                $respon['ktp_province']         = $user->ktp_province;
                $respon['ktp_district']         = $user->ktp_district;
                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                $respon['domisili']             = 'a';
                $respon['phone_number1']        = $user->phone1;
                $respon['phone_number2']        = $user->phone2;
                $respon['image']                = $user->image;
                $respon['status_user']           = $user->status_user;
                $respon['password']             = " ";
                $respon['imei_user']            = $imei;
                $respon['cek_login']            = 1;
                $respon['status_project']       = $user->status_project;
                $respon['status_account']       = $user->status_account;
                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                return response()->json($respon, $this->succesStatus);
            }else{
                $project_name = Project::where('id',$project->project_id)->first();
                $respon['code']                 = 200;
                $respon['token']                = $randomString;
                $respon['message']              = 'Login Success';
                $respon['people_id']            = $user->id;
                $respon['project_id']           = $project->project_id;
                $respon['project_name']         = $project_name->project_name;
                $respon['first_name']           = $user->first_name;
                $respon['middle_name']          = $user->middle_name;
                $respon['last_name']            = $user->surname;
                $respon['email']                = $user->email_address;
                $respon['address']              = $user->ktp_address;
                $respon['date_of_britday']      = $user->dob2;
                $respon['ktp_province']         = $user->ktp_province;
                $respon['ktp_district']         = $user->ktp_district;
                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                $respon['domisili']             = $user->dom_address;
                $respon['phone_number1']        = $user->phone1;
                $respon['phone_number2']        = $user->phone2;
                $respon['status_user']           = $user->status_user;
                $respon['image']                = $user->image;
                $respon['password']             = " ";
                $respon['imei_user']            = $imei;
                $respon['cek_login']            = 1;
                $respon['status_project']       = $user->status_project;
                $respon['status_account']       = $user->status_account;
                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $gmail = array();
            $gmail['email_address']  = $request->email;
            $gmail['phone1']         = $request->phone;
            $gmail['first_name']     = $request->first_name;
            $gmail['imei_user']      = $request->imei_user;
            $gmail['status_account'] = $request->status_account;
            $gmail['status_project'] = 0;
            $gmail['data_status']    = 1;
            $gmail['status_user']    = 0;
            $gmail['delete_flag']    = 1;
            $gmail['active_flag']    = 1;
            $gmail['cek_login']      = 0;
            $save = EmployePeople::create($gmail);
            if($save){
                $project = ProjectMember::where('employee_id',$save->id)->first();
                if($project == null ){
                   $respon['code']                  = 200;
                    $respon['token']                = $randomString;
                    $respon['message']              = 'Login Success';
                    $respon['people_id']            = $save->id;
                    $respon['project_id']           = '0';
                    $respon['project_name']         = ' ';
                    $respon['first_name']           = $save->first_name;
                    $respon['middle_name']          = $save->middle_name;
                    $respon['last_name']            = $save->surname;
                    $respon['email']                = $save->email_address;
                    $respon['address']              = $save->ktp_address;
                    $respon['date_of_britday']      = $save->dob2;
                    $respon['ktp_province']         = $save->ktp_province;
                    $respon['ktp_district']         = $save->ktp_district;
                    $respon['ktp_subdistrict']      = $save->ktp_subdistrict;
                    $respon['domisili']             = 'a';
                    $respon['phone_number1']        = $save->phone1;
                    $respon['phone_number2']        = $save->phone2;
                    $respon['image']                = $save->image;
                    $respon['status_user']           = $save->status_user;
                    $respon['password']             = " ";
                    $respon['imei_user']            = $imei;
                    $respon['cek_login']            = 1;
                    $respon['status_project']       = $save->status_project;
                    $respon['status_account']       = $save->status_account;
                    $update = EmployePeople::where('id', $save->id)->update(['createToken' => $randomString]); 
                    return response()->json($respon, $this->succesStatus);
                }else{
                    $project_name = Project::where('id',$project->project_id)->first();
                    $respon['code']                 = 200;
                    $respon['token']                = $randomString;
                    $respon['message']              = 'Login Success';
                    $respon['people_id']            = $save->id;
                    $respon['project_id']           = $project->project_id;
                    $respon['project_name']         = $project_name->project_name;
                    $respon['first_name']           = $save->first_name;
                    $respon['middle_name']          = $save->middle_name;
                    $respon['last_name']            = $save->surname;
                    $respon['email']                = $save->email_address;
                    $respon['address']              = $save->ktp_address;
                    $respon['date_of_britday']      = $save->dob2;
                    $respon['ktp_province']         = $save->ktp_province;
                    $respon['ktp_district']         = $save->ktp_district;
                    $respon['ktp_subdistrict']      = $save->ktp_subdistrict;
                    $respon['domisili']             = $save->dom_address;
                    $respon['phone_number1']        = $save->phone1;
                    $respon['phone_number2']        = $save->phone2;
                    $respon['status_user']          = $save->status_user;
                    $respon['image']                = $save->image;
                    $respon['password']             = " ";
                    $respon['imei_user']            = $imei;
                    $respon['cek_login']            = 1;
                    $respon['status_project']       = $save->status_project;
                    $respon['status_account']       = $save->status_account;
                    $update = EmployePeople::where('id', $save->id)->update(['createToken' => $randomString]); 
                    return response()->json($respon, $this->succesStatus);
                }
            }else{
                $data['message'] = 'save data people failed';
                $data['code'] = 500;
                return response()->json($data, $this->succesStatus);
            }
        }
    }

    public function registerUser(Request $request)
    {
        $email          = $request->email;
        $first_name     = $request->first_name;
        $randomPassword = rand(1000,1000000);

        $check = DB::table('employee_people')->where('email_address',$email)->where('delete_flag',1)->first();

        if($check){
            $data['message'] = 'User already';
            $data['code']    = 201;
            $data['email']   = $email;
            return response()->json($data, $this->succesStatus);
        }else{
                $mail = new PHPMailer\PHPMailer;
                $mail->isSMTP();                            // Set mailer to use SMTP
                $mail->SMTPAuth   = true;                     // Enable SMTP authentication
                $mail->Username   = 'kreasibungsu19@gmail.com';          // SMTP username
                $mail->Password   = 'bungsu123'; // SMTP password
                $mail->Host       = 'smtp.gmail.com';
                $mail->SMTPSecure = 'ssl';
                $mail->Port       = 465;
                //Set the encryption system to use - ssl (deprecated) or tls
                $mail->setFrom('kreasibungsu19@gmail.com', 'PT.SDM');
                // $mail->addReplyTo('hamrullah1902@gmail.com', 'lagi');
                $mail->addAddress($email);   // Add a recipient
                $mail->isHTML(true);  // Set email format to HTML
                $message = '
                  <p>Terima kasih anda sudah terdaftar sebagai keanggotaan dri PT. Sumber Daya Mandiri.</p>'
                  .'<p>Silahkan login menggunakan email dan password Berikut ini untuk melengkapi data-data yang di butuhkan :</p>'
                  .'<h2>'.'email = '.$email.'<br>'
                  .'password = ' .$randomPassword.'</h2>'

                  .'<p>Terima Kasih. </p>';
                $mail->msgHTML($message);
                if(!$mail->send())
                {
                    $data['message'] = 'Register user failed';
                    $data['code']    = 500;
                    return response()->json($data, $this->succesStatus);
                    // return "gagal terkirim";
                    //  echo  $mail->ErrorInfo;
                } else {
                    $user = array();
                    $user['email_address']  = $email;
                    $user['first_name']     = $first_name;
                    $user['imei_user']      = $request->imei_user;
                    $user['delete_flag']    = 1;
                    $user['active_flag']    = 1;
                    $user['branch_id']      = 7;
                    $user['status_project'] = 2;
                    $user['status_user']    = 0;
                    $user['cek_login']      = 0;
                    $user['status_login']   = 0;
                    $user['status_account'] = 1;
                    $user['password']       = base64_encode($randomPassword);
                    $savaData = EmployePeople::create($user);

                   $data['message'] = 'Register Success Please Cek Email';
                   $data['code']    = 200;
                   $data['email']   = $email;
                   return response()->json($data, $this->succesStatus);
                }
            
        }
    }


    public function getLoginPhase2qqqqqqqq(Request $request)
    {
        $email =$request->email;

        $length = 200;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = base64_encode($request->password);

        $user = EmployePeople::where('email_address', $email)->first();
        if($user && $user->password == $password) {
             $project = ProjectMember::where('employee_id', $user->id)->first();

            if($project == null ){
                $logLogin = array();
                $logLogin['people_id'] = $user->id;
                $logLogin['date_login'] = date('Y-m-d');
                $logLogin['jam_login'] = date('H:i:s');
                LogLoginAndroid::create($logLogin);

                $respon['code']                 = 200;
                $respon['token']                = $randomString;
                $respon['message']              = 'Login Success';
                $respon['people_id']            = $user->id;
                $respon['project_id']           = 0;
                $respon['project_name']         = ' ';
                $respon['first_name']           = $user->first_name;
                $respon['middle_name']          = $user->middle_name;
                $respon['last_name']            = $user->surname;
                $respon['email']                = $user->email_address;
                $respon['address']              = $user->ktp_address;
                $respon['date_of_britday']      = $user->dob2;
                $respon['ktp_province']         = $user->ktp_province;
                $respon['ktp_district']         = $user->ktp_district;
                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                $respon['domisili']             = 'a';
                $respon['phone_number1']        = $user->phone1;
                $respon['phone_number2']        = $user->phone2;
                $respon['image']                = $user->image;
                $respon['status_user']           = $user->status_user;
                $respon['password']             = " ";
                $respon['cek_login']            = $user->cek_login;
                $respon['status_project']       = $user->status_project;
                $respon['status_account']       = $user->status_account;
                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 

                return response()->json($respon, $this->succesStatus);
            }else{
                $project_name = Project::where('id',$project->project_id)->first();
                if($project_name == null ){
                    $logLogin = array();
                    $logLogin['people_id'] = $user->id;
                    $logLogin['date_login'] = date('Y-m-d');
                    $logLogin['jam_login'] = date('H:i:s');
                    LogLoginAndroid::create($logLogin);

                    $respon['code']                 = 200;
                    $respon['token']                = $randomString;
                    $respon['message']              = 'Login Success';
                    $respon['people_id']            = $user->id;
                    $respon['project_id']           = 0;
                    $respon['project_name']         = ' ';
                    $respon['first_name']           = $user->first_name;
                    $respon['middle_name']          = $user->middle_name;
                    $respon['last_name']            = $user->surname;
                    $respon['email']                = $user->email_address;
                    $respon['address']              = $user->ktp_address;
                    $respon['date_of_britday']      = $user->dob2;
                    $respon['ktp_province']         = $user->ktp_province;
                    $respon['ktp_district']         = $user->ktp_district;
                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                    $respon['domisili']             = $user->dom_address;
                    $respon['phone_number1']        = $user->phone1;
                    $respon['phone_number2']        = $user->phone2;
                    $respon['status_user']           = $user->status_user;
                    $respon['image']                = $user->image;
                    $respon['password']             = " ";
                    $respon['cek_login']            = $user->cek_login;
                    $respon['status_project']       = $user->status_project;
                    $respon['status_account']       = $user->status_account;
                    $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                    return response()->json($respon, $this->succesStatus);

                }else{
                    $logLogin = array();
                    $logLogin['people_id'] = $user->id;
                    $logLogin['date_login'] = date('Y-m-d');
                    $logLogin['jam_login'] = date('H:i:s');
                    LogLoginAndroid::create($logLogin);

                    $respon['code']                 = 200;
                    $respon['token']                = $randomString;
                    $respon['message']              = 'Login Success';
                    $respon['people_id']            = $user->id;
                    $respon['project_id']           = $project->project_id;
                    $respon['project_name']         = $project_name->project_name;
                    $respon['first_name']           = $user->first_name;
                    $respon['middle_name']          = $user->middle_name;
                    $respon['last_name']            = $user->surname;
                    $respon['email']                = $user->email_address;
                    $respon['address']              = $user->ktp_address;
                    $respon['date_of_britday']      = $user->dob2;
                    $respon['ktp_province']         = $user->ktp_province;
                    $respon['ktp_district']         = $user->ktp_district;
                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                    $respon['domisili']             = $user->dom_address;
                    $respon['phone_number1']        = $user->phone1;
                    $respon['phone_number2']        = $user->phone2;
                    $respon['status_user']           = $user->status_user;
                    $respon['image']                = $user->image;
                    $respon['password']             = " ";
                    $respon['cek_login']            = $user->cek_login;
                    $respon['status_project']       = $user->status_project;
                    $respon['status_account']       = $user->status_account;
                    $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                    return response()->json($respon, $this->succesStatus);

                }

                
            }
        }elseif($user && $user->password != $password){
           $data['message'] = 'login failed cek password anda';
           $data['code'] = 500;
           return response()->json($data, $this->succesStatus);
        }else{
           $data['message'] = 'user not found';
           $data['code'] = 501;
           return response()->json($data, $this->succesStatus);

        }
    }

    public function getLoginPhase2(Request $request)
    {
        $length = 200;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $password = base64_encode($request->password);
        $imei = $request->imei_user;

        $user = EmployePeople::where('email_address',$request->email)->where('delete_flag',1)->first();
        if($user){

            $project11 = ProjectMember::where('employee_id',$user->id)->where('delete_flag',1)->first();
            if ($project11 == null ) {


                if ($user && $password == $user->password) {
                    
                    $logLogin = array();
                    $logLogin['people_id'] = $user->id;
                    $logLogin['date_login'] = date('Y-m-d');
                    $logLogin['jam_login'] = date('H:i:s');
                    LogLoginAndroid::create($logLogin);

                    $respon['code']                 = 200;
                    $respon['token']                = $randomString;
                    $respon['message']              = 'Login Success';
                    $respon['people_id']            = $user->id;
                    $respon['project_id']           = 0;
                    $respon['project_name']         = ' ';
                    $respon['first_name']           = $user->first_name;
                    $respon['middle_name']          = $user->middle_name;
                    $respon['last_name']            = $user->surname;
                    $respon['email']                = $user->email_address;
                    $respon['address']              = $user->ktp_address;
                    $respon['date_of_britday']      = $user->dob2;
                    $respon['ktp_province']         = $user->ktp_province;
                    $respon['ktp_district']         = $user->ktp_district;
                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                    $respon['domisili']             = 'a';
                    $respon['phone_number1']        = $user->phone1;
                    $respon['phone_number2']        = $user->phone2;
                    $respon['image']                = $user->image;
                    $respon['status_user']           = $user->status_user;
                    $respon['password']             = " ";
                    $respon['cek_login']            = $user->cek_login;
                    $respon['status_login']         = $user->status_login;
                    $respon['status_project']       = $user->status_project;
                    $respon['status_account']       = $user->status_account;
                    $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 

                    return response()->json($respon, $this->succesStatus);
                }else{
                    $respon['code'] = 500;
                    $respon['message']              = 'Login failed';
                    return response()->json($respon, $this->succesStatus);

                }
                
            }else{
                    if($user->imei_user == $imei && $user->status_login == 1){
                    
                        if ($user && $password == $user->password) {
                            $project = ProjectMember::where('employee_id',$user->id)->where('delete_flag',1)->first();
                            if($project == null ){
                                $logLogin = array();
                                $logLogin['people_id'] = $user->id;
                                $logLogin['date_login'] = date('Y-m-d');
                                $logLogin['jam_login'] = date('H:i:s');
                                LogLoginAndroid::create($logLogin);

                                $respon['code']                 = 200;
                                $respon['token']                = $randomString;
                                $respon['message']              = 'Login Success';
                                $respon['people_id']            = $user->id;
                                $respon['project_id']           = 0;
                                $respon['project_name']         = ' ';
                                $respon['first_name']           = $user->first_name;
                                $respon['middle_name']          = $user->middle_name;
                                $respon['last_name']            = $user->surname;
                                $respon['email']                = $user->email_address;
                                $respon['address']              = $user->ktp_address;
                                $respon['date_of_britday']      = $user->dob2;
                                $respon['ktp_province']         = $user->ktp_province;
                                $respon['ktp_district']         = $user->ktp_district;
                                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                $respon['domisili']             = 'a';
                                $respon['phone_number1']        = $user->phone1;
                                $respon['phone_number2']        = $user->phone2;
                                $respon['image']                = $user->image;
                                $respon['status_user']           = $user->status_user;
                                $respon['password']             = " ";
                                $respon['cek_login']            = $user->cek_login;
                                $respon['status_login']         = 1;
                                $respon['status_project']       = $user->status_project;
                                $respon['status_account']       = $user->status_account;
                                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 

                                return response()->json($respon, $this->succesStatus);
                            }else{
                                $project_name = Project::where('id',$project->project_id)->first();
                                if($project_name == null ){
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = 0;
                                    $respon['project_name']         = ' ';
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account;
                                    $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                                    return response()->json($respon, $this->succesStatus);

                                }else{
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = $project->project_id;
                                    $respon['project_name']         = $project_name->project_name;
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account;
                                    $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 
                                    return response()->json($respon, $this->succesStatus);

                                }

                                
                            }
                                
                         }else{
                            $respon['code'] = 500;
                            $respon['message']              = 'Login failed';
                            return response()->json($respon, $this->succesStatus);
                         }

                }elseif($user->imei_user == $imei && $user->status_login == 0){
                    //update imei dan status login
                    if ($user && $password == $user->password) {
                        $project = ProjectMember::where('employee_id',$user->id)->where('delete_flag',1)->first();

                            if($project == null ){
                                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                $logLogin = array();
                                $logLogin['people_id'] = $user->id;
                                $logLogin['date_login'] = date('Y-m-d');
                                $logLogin['jam_login'] = date('H:i:s');
                                LogLoginAndroid::create($logLogin);

                                $respon['code']                 = 200;
                                $respon['token']                = $randomString;
                                $respon['message']              = 'Login Success';
                                $respon['people_id']            = $user->id;
                                $respon['project_id']           = 0;
                                $respon['project_name']         = ' ';
                                $respon['first_name']           = $user->first_name;
                                $respon['middle_name']          = $user->middle_name;
                                $respon['last_name']            = $user->surname;
                                $respon['email']                = $user->email_address;
                                $respon['address']              = $user->ktp_address;
                                $respon['date_of_britday']      = $user->dob2;
                                $respon['ktp_province']         = $user->ktp_province;
                                $respon['ktp_district']         = $user->ktp_district;
                                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                $respon['domisili']             = 'a';
                                $respon['phone_number1']        = $user->phone1;
                                $respon['phone_number2']        = $user->phone2;
                                $respon['image']                = $user->image;
                                $respon['status_user']           = $user->status_user;
                                $respon['password']             = " ";
                                $respon['cek_login']            = $user->cek_login;
                                $respon['status_login']         = 1;
                                $respon['status_project']       = $user->status_project;
                                $respon['status_account']       = $user->status_account;
                                $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString]); 

                                return response()->json($respon, $this->succesStatus);
                            }else{
                                $project_name = Project::where('id',$project->project_id)->first();
                                if($project_name == null ){
                                     $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = 0;
                                    $respon['project_name']         = ' ';
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account;
                                    return response()->json($respon, $this->succesStatus);

                                }else{
                                     $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = $project->project_id;
                                    $respon['project_name']         = $project_name->project_name;
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account; 
                                    return response()->json($respon, $this->succesStatus);

                                }

                                
                            }
                            
                     }else{
                        $respon['code'] = 500;
                        $respon['message']              = 'Login failed';
                        return response()->json($respon, $this->succesStatus);
                     }

                }elseif($user->imei_user != $imei && $user->status_login == 0){
                    //update imei dan status login
                    if ($user && $password == $user->password) {
                        $project = ProjectMember::where('employee_id',$user->id)->where('delete_flag',1)->first();
                            if($project == null ){
                                 $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                $logLogin = array();
                                $logLogin['people_id'] = $user->id;
                                $logLogin['date_login'] = date('Y-m-d');
                                $logLogin['jam_login'] = date('H:i:s');
                                LogLoginAndroid::create($logLogin);

                                $respon['code']                 = 200;
                                $respon['token']                = $randomString;
                                $respon['message']              = 'Login Success';
                                $respon['people_id']            = $user->id;
                                $respon['project_id']           = 0;
                                $respon['project_name']         = ' ';
                                $respon['first_name']           = $user->first_name;
                                $respon['middle_name']          = $user->middle_name;
                                $respon['last_name']            = $user->surname;
                                $respon['email']                = $user->email_address;
                                $respon['address']              = $user->ktp_address;
                                $respon['date_of_britday']      = $user->dob2;
                                $respon['ktp_province']         = $user->ktp_province;
                                $respon['ktp_district']         = $user->ktp_district;
                                $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                $respon['domisili']             = 'a';
                                $respon['phone_number1']        = $user->phone1;
                                $respon['phone_number2']        = $user->phone2;
                                $respon['image']                = $user->image;
                                $respon['status_user']           = $user->status_user;
                                $respon['password']             = " ";
                                $respon['cek_login']            = $user->cek_login;
                                $respon['status_login']         = 1;
                                $respon['status_project']       = $user->status_project;
                                $respon['status_account']       = $user->status_account;

                                return response()->json($respon, $this->succesStatus);
                            }else{
                                $project_name = Project::where('id',$project->project_id)->first();
                                if($project_name == null ){
                                     $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = 0;
                                    $respon['project_name']         = ' ';
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account;
                                    return response()->json($respon, $this->succesStatus);

                                }else{
                                     $update = EmployePeople::where('id', $user->id)->update(['createToken' => $randomString,'imei_user' => $imei,'status_login' => 1]);
                                    $logLogin = array();
                                    $logLogin['people_id'] = $user->id;
                                    $logLogin['date_login'] = date('Y-m-d');
                                    $logLogin['jam_login'] = date('H:i:s');
                                    LogLoginAndroid::create($logLogin);

                                    $respon['code']                 = 200;
                                    $respon['token']                = $randomString;
                                    $respon['message']              = 'Login Success';
                                    $respon['people_id']            = $user->id;
                                    $respon['project_id']           = $project->project_id;
                                    $respon['project_name']         = $project_name->project_name;
                                    $respon['first_name']           = $user->first_name;
                                    $respon['middle_name']          = $user->middle_name;
                                    $respon['last_name']            = $user->surname;
                                    $respon['email']                = $user->email_address;
                                    $respon['address']              = $user->ktp_address;
                                    $respon['date_of_britday']      = $user->dob2;
                                    $respon['ktp_province']         = $user->ktp_province;
                                    $respon['ktp_district']         = $user->ktp_district;
                                    $respon['ktp_subdistrict']      = $user->ktp_subdistrict;
                                    $respon['domisili']             = $user->dom_address;
                                    $respon['phone_number1']        = $user->phone1;
                                    $respon['phone_number2']        = $user->phone2;
                                    $respon['status_user']           = $user->status_user;
                                    $respon['image']                = $user->image;
                                    $respon['password']             = " ";
                                    $respon['cek_login']            = $user->cek_login;
                                    $respon['status_login']         = 1;
                                    $respon['status_project']       = $user->status_project;
                                    $respon['status_account']       = $user->status_account;
                                    return response()->json($respon, $this->succesStatus);

                                }
                            }
                            
                     }else{
                        $respon['code'] = 500;
                        $respon['message']              = 'Login failed';
                        return response()->json($respon, $this->succesStatus);
                     }
                }elseif($user->imei_user != $imei){
                    $respon['code'] = 505;
                    $respon['message']              = 'IMei tidak sama';
                    return response()->json($respon, $this->succesStatus);
                }else{
                    $respon['code'] = 500;
                    $respon['message']              = 'IMEI Not Valid';
                    return response()->json($respon, $this->succesStatus);
                }

            }
                
        }else{
            $respon['code'] = 501;
            $respon['message']              = 'user not found';
            return response()->json($respon, $this->succesStatus);
        }   

    }

    public function postCompleteDataPeople(Request $request)
    {
        $checkNik = EmployePeople::where('nik_ktp', $request->nik_ktp)->first();
        if($checkNik){
             $data['message'] = 'nik already';
             $data['code']    = 201;
             return response()->json($data, $this->succesStatus);
        }else{
            $dataPeole = EmployePeople::where('id',$request->people_id)->first();
            $jobInteres = $request->interes_id;

            if($dataPeole){
                
                        $dt_doc_cv = new DocumentPeople();
                        $dt_doc_cv->employe_id = $request->people_id;
                        $dt_doc_cv->image_type = 'Foto CV';
                        $dt_doc_cv->delete_flag = '1';
                        $dt_doc_cv->code = '1';
                        $foto_cv = $request->file('image_cv');
                            
                        if ($request->hasFile('image_cv')) {
                          $dt_doc_cv->image_path = $this->uploadDocumentImage($request);
                        }

                        $dt_doc_cv->save();

                  if($dt_doc_cv->id != null){

                        for ($i=0; $i < count($jobInteres)  ; $i++) { 
                            $insertJob = new PeopleVacancy();
                            $insertJob->people_id   = $request->people_id;
                            $insertJob->interes_id  = $jobInteres[$i];
                            $insertJob->delete_flag = 1;
                            $insertJob->active_flag = 1;
                            $insertJob->save();
                        }

                             $dt_imagektp = new ImagePeople();
                              $dt_imagektp->employe_id  = $request->people_id;
                              $dt_imagektp->image_type  = 'Image KTP';
                              $dt_imagektp->created_by  = 'android';
                              $dt_imagektp->updated_by  = 'android';
                              $dt_imagektp->active_flag = '1';
                              $dt_imagektp->delete_flag = '1';
                              $foto = $request->file('image_ktp');
                                
                              if ($request->hasFile('image_ktp')) {
                                  $dt_imagektp->image = $this->uploadFotoImagePeopleKtp($request);
                                }
                              $dt_imagektp->save();
                           $update = DB::table('employee_people')->where('id',$request->people_id)->update(['nik_ktp'          => $request->nik_ktp,
                                    'dob2'            => date('Y-m-d', strtotime($request->dob)),
                                    'gender'          => $request->gender,
                                    'ktp_address'     => $request->ktp_address,
                                    'ktp_province'    => $request->ktp_province,
                                    'ktp_district'    => $request->ktp_district,
                                    'ktp_subdistrict' => $request->ktp_subdistrict,
                                    'ktp_village'     => $request->ktp_village,
                                    'dom_address'     => $request->dom_address,
                                    'dom_province'    => $request->dom_province,
                                    'dom_district'    => $request->dom_district,
                                    'dom_subdistrict' => $request->dom_subdistrict,
                                    'dom_village'     => $request->dom_village,
                                    'last_education'  => $request->last_education,
                                    'phone1'          => $request->phone_number,
                                    'image'           => $this->uploadFotoImagePeople($request),
                                    'cek_login'       => 1
                                    ]);

                                if($update){

                                        $data['message'] = 'update Success';
                                        $data['code']    = 200;
                                       return response()->json($data, $this->succesStatus);
                                }else{
                                   $data['message'] = 'update failed';
                                   $data['code']    = 500;
                                   return response()->json($data, $this->succesStatus);
                                }
                  }else{
                     $data['message'] = 'update failed';
                    $data['code']    = 500;
                    return response()->json($data, $this->succesStatus);
                  }
            }else{
               $data['message'] = 'user not found';
               $data['code'] = 501;
               return response()->json($data, $this->succesStatus);
            }

        }

        
    }

    public function postCompleteDataPeopleWithFile(Request $request)
    {
        $checkNik = EmployePeople::where('nik_ktp', $request->nik_ktp)->first();
        if($checkNik){
             $data['message'] = 'nik already';
             $data['code']    = 201;
             return response()->json($data, $this->succesStatus);
        }else{
            $dataPeole = EmployePeople::where('id',$request->people_id)->first();
            $jobInteres = $request->interes_id;

            if($dataPeole){
                
                        $dt_doc = new DocumentPeople();
                        $dt_doc->employe_id = $request->people_id;
                        $dt_doc->image_type = 'CV';
                        $dt_doc->delete_flag = '1';
                        $dt_doc->code = '1';
                        $foto = $request->file('file');
                            
                        if ($request->hasFile('file')) {
                          $dt_doc->image_path = $this->uploadDocument($request);
                        }

                        $dt_doc->save();
                        

                        $dt_doc_cv = new DocumentPeople();
                        $dt_doc_cv->employe_id = $request->people_id;
                        $dt_doc_cv->image_type = 'Foto CV';
                        $dt_doc_cv->delete_flag = '1';
                        $dt_doc_cv->code = '1';
                        $foto_cv = $request->file('image_cv');
                            
                        if ($request->hasFile('image_cv')) {
                          $dt_doc_cv->image_path = $this->uploadDocumentImage($request);
                        }

                        $dt_doc_cv->save();

                  if($dt_doc->id != null){

                        for ($i=0; $i < count($jobInteres)  ; $i++) { 
                            $insertJob = new PeopleVacancy();
                            $insertJob->people_id   = $request->people_id;
                            $insertJob->interes_id  = $jobInteres[$i];
                            $insertJob->delete_flag = 1;
                            $insertJob->active_flag = 1;
                            $insertJob->save();
                        }

                             $dt_imagektp = new ImagePeople();
                              $dt_imagektp->employe_id  = $request->people_id;
                              $dt_imagektp->image_type  = 'Image KTP';
                              $dt_imagektp->created_by  = 'android';
                              $dt_imagektp->updated_by  = 'android';
                              $dt_imagektp->active_flag = '1';
                              $dt_imagektp->delete_flag = '1';
                              $foto = $request->file('image_ktp');
                                
                              if ($request->hasFile('image_ktp')) {
                                  $dt_imagektp->image = $this->uploadFotoImagePeopleKtp($request);
                                }
                              $dt_imagektp->save();
                           $update = DB::table('employee_people')->where('id',$request->people_id)->update(['nik_ktp'          => $request->nik_ktp,
                                    'dob2'            => date('Y-m-d', strtotime($request->dob)),
                                    'gender'          => $request->gender,
                                    'ktp_address'     => $request->ktp_address,
                                    'ktp_province'    => $request->ktp_province,
                                    'ktp_district'    => $request->ktp_district,
                                    'ktp_subdistrict' => $request->ktp_subdistrict,
                                    'ktp_village'     => $request->ktp_village,
                                    'dom_address'     => $request->dom_address,
                                    'dom_province'    => $request->dom_province,
                                    'dom_district'    => $request->dom_district,
                                    'dom_subdistrict' => $request->dom_subdistrict,
                                    'dom_village'     => $request->dom_village,
                                    'last_education'  => $request->last_education,
                                    'phone1'          => $request->phone_number,
                                    'image'           => $this->uploadFotoImagePeople($request),
                                    'cek_login'       => 1
                                    ]);

                                if($update){

                                        $data['message'] = 'update Success';
                                        $data['code']    = 200;
                                       return response()->json($data, $this->succesStatus);
                                }else{
                                   $data['message'] = 'update failed';
                                   $data['code']    = 500;
                                   return response()->json($data, $this->succesStatus);
                                }
                  }else{
                     $data['message'] = 'update failed';
                    $data['code']    = 500;
                    return response()->json($data, $this->succesStatus);
                  }
            }else{
               $data['message'] = 'user not found';
               $data['code'] = 501;
               return response()->json($data, $this->succesStatus);
            }

        }

        
    }


     public function postCompleteDataPeople12222(Request $request)
    {
        
        
        $checkNik = EmployePeople::where('nik_ktp', $request->nik_ktp)->first();
        if($checkNik){
             $data['message'] = 'nik already';
             $data['code']    = 201;
             return response()->json($data, $this->succesStatus);
        }else{
            $dataPeole = EmployePeople::where('id',$request->people_id)->first();
            $jobInteres = $request->interes_id;

            if($dataPeole){
                        $dt_doc = new DocumentPeople();
                        $dt_doc->employe_id = $request->people_id;
                        $dt_doc->image_type = 'CV';
                        $dt_doc->delete_flag = '1';
                        $foto = $request->file('file');
                            
                        if ($request->hasFile('file')) {
                          $dt_doc->image_path = $this->uploadDocument($request);
                        }

                        $dt_doc->save();

                  if($dt_doc->id != null){

                        for ($i=0; $i < count($jobInteres)  ; $i++) { 
                            $insertJob = new PeopleVacancy();
                            $insertJob->people_id   = $request->people_id;
                            $insertJob->interes_id  = $jobInteres[$i];
                            $insertJob->delete_flag = 1;
                            $insertJob->active_flag = 1;
                            $insertJob->save();
                        }
                        
                          $dt_image = new ImagePeople();
                          $dt_image->employe_id  = $request->people_id;
                          $dt_image->image_type  = 'Front View';
                          $dt_image->created_by  = 'android';
                          $dt_image->updated_by  = 'android';
                          $dt_image->active_flag = '1';
                          $dt_image->delete_flag = '1';
                          $foto = $request->file('image');
                            
                          if ($request->hasFile('image')) {
                              $dt_image->image = $this->uploadFotoImagePeople($request);
                            }
                          $dt_image->save();

                        if($dt_image->id != null){

                             $dt_imagektp = new ImagePeople();
                              $dt_imagektp->employe_id  = $request->people_id;
                              $dt_imagektp->image_type  = 'Image KTP';
                              $dt_imagektp->created_by  = 'android';
                              $dt_imagektp->updated_by  = 'android';
                              $dt_imagektp->active_flag = '1';
                              $dt_imagektp->delete_flag = '1';
                              $foto = $request->file('image_ktp');
                                
                              if ($request->hasFile('image_ktp')) {
                                  $dt_imagektp->image = $this->uploadFotoImagePeopleKtp($request);
                                }
                              $dt_imagektp->save();

                            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['nik_ktp'          => $request->nik_ktp,
                                    'dob2'            => date('Y-m-d', strtotime($request->dob)),
                                    'gender'          => $request->gender,
                                    'ktp_address'     => $request->ktp_address,
                                    'ktp_province'    => $request->ktp_province,
                                    'ktp_district'    => $request->ktp_district,
                                    'ktp_subdistrict' => $request->ktp_subdistrict,
                                    'ktp_village'     => $request->ktp_village,
                                    'last_education'  => $request->last_education,
                                    'phone1'          => $request->phone_number,
                                    'cek_login'       => 1
                                    ]);

                                if($update){

                                        $data['message'] = 'update Success';
                                        $data['code']    = 200;
                                       return response()->json($data, $this->succesStatus);
                                }else{
                                   $data['message'] = 'update failed';
                                   $data['code']    = 500;
                                   return response()->json($data, $this->succesStatus);
                                }
                        }else{
                            $data['message'] = 'update failed';
                            $data['code']    = 500;
                            return response()->json($data, $this->succesStatus);
                        }
                  }else{
                     $data['message'] = 'update failed';
                    $data['code']    = 500;
                    return response()->json($data, $this->succesStatus);
                  }
            }else{
               $data['message'] = 'user not found';
               $data['code'] = 501;
               return response()->json($data, $this->succesStatus);
            }

        }

        
    }

    private function uploadDocument(Request $request){
        $foto = $request->file('file');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('file')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'document_people';
            $request->file('file')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

      private function uploadDocumentImage(Request $request){
        $foto = $request->file('image_cv');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image_cv')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'document_people';
            $request->file('image_cv')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

    private function uploadFotoImagePeople(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'employe';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
    }

    private function uploadFotoImagePeopleKtp(Request $request){
        $foto = $request->file('image_ktp');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image_ktp')->isValid()){
            $namaFoto = date('YmdHis')."ktp".".".$ext;
            $upload_path = 'employe_image';
            $request->file('image_ktp')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
    }




    


    
}
