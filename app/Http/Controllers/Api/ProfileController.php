<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Branch;
use App\User;
use App\Mail\DemoEmail;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer;
use App\EmployePeople;
use App\EducationPeople;
use App\BankPeople;
use App\WorkPeople;
use App\ImagePeople;
use App\PeopleVacancy;
use App\DocumentPeople;
use App\Aggree;


class ProfileController extends Controller
{
  
	public $succesStatus = 200;
	public $failed = 500;

    public function test()
    {
        
    	phpinfo();
    }

    public function getDeteailProfile(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $interes = DB::table('view_interes')->where('people_id',$request->people_id)->where('delete_flag',1)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$request->people_id)->where('delete_flag',1)->get();
            $education = DB::table('education')->where('employe_id',$request->people_id)->where('delete_flag',1)->get();
            $bank = DB::table('bank_people')->where('employe_id',$request->people_id)->where('delete_flag',1)->get();
            $image = DB::table('image_people')->where('employe_id',$request->people_id)->where('delete_flag',1)->get();
            $doc = DB::table('document_path')->where('employe_id',$request->people_id)->where('delete_flag',1)->where('code',1)->get();
            $image_cv = DB::table('document_path')->where('employe_id',$request->people_id)->where('delete_flag',1)->where('code',2)->get();
            if($interes != null){
                $respon['code'] = 200;
                $respon['first_name'] = $user->first_name;
                $respon['middle_name'] = $user->middle_name;
                $respon['surname'] = $user->surname;
                $respon['dob'] = $user->dob2;
                $respon['pob'] = $user->pob;
                $respon['ktp_address'] = $user->ktp_address;
                $respon['ktp_province'] = $user->ktp_province;
                $respon['ktp_district'] = $user->ktp_district;
                $respon['ktp_subdistrict'] = $user->ktp_subdistrict;
                $respon['ktp_village'] = $user->ktp_village;
                $respon['dom_address'] = $user->dom_address;
                $respon['dom_province'] = $user->dom_province;
                $respon['dom_district'] = $user->dom_district;
                $respon['dom_subdistrict'] = $user->dom_subdistrict;
                $respon['dom_village'] = $user->dom_village;
                $respon['nik_ktp'] = $user->nik_ktp;
                $respon['gender'] = $user->gender;
                $respon['body_height'] = $user->body_height;
                $respon['body_weight'] = $user->body_weight;
                $respon['blood_type'] = $user->blood_type;
                $respon['npwp_number'] = $user->npwp_number;
                $respon['last_education'] = $user->last_education;
                $respon['email_address'] = $user->email_address;
                $respon['no_bpjs_kesehatan'] = $user->no_bpjs_kesehatan;
                $respon['image_profile'] = $user->image;
                $respon['phone1'] = $user->phone1;
                $respon['interes'] = $interes;
                $respon['work_experience'] = $work;
                $respon['education'] = $education;
                $respon['bank'] = $bank;
                $respon['image'] = $image;
                $respon['doc'] = $doc;
                $respon['image_cv'] = $image_cv;

                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'interes null';
                $respon['code'] = 201;
                return response()->json($respon, $this->succesStatus);
            }
            
        }
    }

    public function getUpdateFisik(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['body_weight' => $request->body_weight,
                'body_height' => $request->body_height,
                'blood_type' => $request->blood_type]);
            if($update){
                $respon['message'] = 'update successfully';
                $respon['code'] = 200;
                $respon['body_height'] = $request->body_height;
                $respon['body_weight'] = $request->body_weight;
                $respon['blood_type'] = $request->blood_type;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'user not found';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function getUpdateDomisili(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['dom_address' => $request->address,
                'dom_province' => $request->province,
                'dom_district' => $request->district,
                'dom_subdistrict' => $request->subdistrict,
                'dom_village' => $request->village]);
            if($update){
                $respon['message'] = 'update successfully';
                $respon['code'] = 200;
                $respon['dom_address'] = $request->address;
                $respon['dom_province'] = $request->province;
                $respon['dom_district'] = $request->district;
                $respon['dom_subdistrict'] = $request->subdistrict;
                $respon['dom_village'] = $request->village;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'user not found';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function getUpdateNpwp(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $update = DB::table('employee_people')->where('id',$request->people_id)->update([
                'npwp_number' => $request->npwp_number]);
            if($update){
                $respon['message'] = 'update successfully';
                $respon['code'] = 200;
                $respon['npwp_number'] = $request->npwp_number;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'user not found';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function getUpdateBpjs(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $update = DB::table('employee_people')->where('id',$request->people_id)->update([
                'no_bpjs_kesehatan' => $request->no_bpjs_kesehatan]);
            if($update){
                $respon['message'] = 'update successfully';
                $respon['code'] = 200;
                $respon['no_bpjs_kesehatan'] = $request->no_bpjs_kesehatan;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'user not found';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function postAddEducationUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $dt_edu = new EducationPeople();
            $dt_edu->employe_id                 = $request->people_id;
            $dt_edu->education_lvl              = $request->level;
            $dt_edu->education_institution_name = $request->education_institution_name;
            $dt_edu->education_location         = $request->education_location;
            $dt_edu->education_major            = $request->education_major;
            $dt_edu->periode_from               = date('Y-m-d', strtotime($request->periode_start));
            $dt_edu->periode_to                 = date('Y-m-d', strtotime($request->periode_to));
            $dt_edu->active_flag                = '1';
            $dt_edu->delete_flag                = '1';
            $dt_edu->save();
            if($dt_edu->id != null){
                $dataEdu = EducationPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully insert data';
                $respon['code'] = 200;
                $respon['data'] = $dataEdu;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal insert data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function postDeleteEducationUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user) {
            $update = DB::table('education')->where('id',$request->id_education)->update(['delete_flag' => 0]);
            if ($update) {
                $dataEdu = EducationPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully delete data';
                $respon['code']    = 200;
                $respon['data']    = $dataEdu;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal detete data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }


    public function postUpdateBankUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
              $dt_bank = BankPeople::where('id',$request->id_bank)->first();
              $dt_bank->employe_id    = $request->people_id;
              $dt_bank->bank_acc_no   = $request->no_rek;
              $dt_bank->bank_acc_name = strtoupper($request->acc_name);
              $dt_bank->bank_name     = strtoupper($request->name_bank);
              $dt_bank->active_flag   = '1';
              $dt_bank->delete_flag   = '1';
              $dt_bank->update();
            if($dt_bank->id != null){
                $dataBank = BankPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully insert data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal insert data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function postAddBankUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
              $dt_bank = new BankPeople();
              $dt_bank->employe_id    = $request->people_id;
              $dt_bank->bank_acc_no   = $request->no_rek;
              $dt_bank->bank_acc_name = strtoupper($request->acc_name);
              $dt_bank->bank_name     = strtoupper($request->name_bank);
              $dt_bank->active_flag   = '1';
              $dt_bank->delete_flag   = '1';
              $dt_bank->save();
            if($dt_bank->id != null){
                $dataBank = BankPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully insert data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal insert data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function postDeleteBankUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user) {
            $update = DB::table('bank_people')->where('id',$request->id_bank)->update(['delete_flag' => 0]);
            if ($update) {
                $dataBank = BankPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully delete data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal detete data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }


    public function getLogLoginAndroid(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $logLogin = DB::table('view_log_login_android')->where('people_id',$request->people_id)->get();
            if(count($logLogin) != 0 ){
                $respon['message'] = 'list log login';
                $respon['code'] = 200;
                $respon['data'] = $logLogin;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'log login null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus); 
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }


    public function getHistoryAbsensi(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if($user){
             $historyAbsensi = DB::table('view_absensi_android')->where('people_id', $request->people_id)->get();
             if (count($historyAbsensi) != 0) {
                $respon['message'] = ' list history absensi';
                $respon['code'] = 200;
                $respon['data'] = $historyAbsensi;
                return response()->json($respon, $this->succesStatus);
             }else{
                $respon['message'] = 'history absensi null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus); 
             }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }


    public function getHistoryAbsensiOutlet(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $historyAbsensiOutlet = DB::table('view_absensi_outlet')->where('people_id',$request->people_id)->get();
            if (count($historyAbsensiOutlet) != 0) {
                $respon['message'] = ' list history absensi';
                $respon['code'] = 200;
                $respon['data'] = $historyAbsensiOutlet;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'history absensi outlet null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }


    public function getHistoryAbsensiDate(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if($user){
             $historyAbsensi = DB::table('view_absensi_android')->where('people_id', $request->people_id)->where('date_absensi', date('Y-m-d'))->first();
             if($historyAbsensi == null) {
               $respon['message'] = 'Belum melakukan Absensi Harian';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }elseif($historyAbsensi->absensi_out == null){
                $respon['message'] = 'belum melakukan absen pulang';
                $respon['code'] = 200;
                $respon['data'] = $historyAbsensi;
                return response()->json($respon, $this->succesStatus);
               
            }elseif($historyAbsensi->absensi_out != null) {
                $respon['message'] = 'list absensi hari ini';
                $respon['code'] = 300;
                $respon['data'] = $historyAbsensi;
                return response()->json($respon, $this->succesStatus);
             }else{
                $respon['message'] = 'Belum melakukan Absensi Harian';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus); 
             }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function postAddWorkUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
                $dt_work = new WorkPeople();
                $dt_work->employe_id          = $request->people_id;
                $dt_work->company_name        = strtoupper($request->name_company);
                $dt_work->working_period      = date('Y-m-d', strtotime($request->periode));
                $dt_work->working_periode_end = date('Y-m-d', strtotime($request->periode_end));
                $dt_work->position            = $request->position;
                $dt_work->salary              = str_replace('.', '', $request->salary);
                $dt_work->reason_resignation  = $request->reason_resignation;
                $dt_work->active_flag         = '1';
                $dt_work->delete_flag         = '1';
                $dt_work->created_by          = $user->name;
                $dt_work->updated_by          = $user->name;
                $dt_work->save();
            if($dt_work->id != null){
                $dataWork = WorkPeople::where('employe_id',$request->people_id)->get();
                $respon['message'] = 'successfully insert data';
                $respon['code'] = 200;
                $respon['data'] = $dataWork;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal insert data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function postUpdateWorkUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
                $dt_work = WorkPeople::where('id',$request->id_work)->first();
                $dt_work->employe_id          = $request->people_id;
                $dt_work->company_name        = strtoupper($request->name_company);
                $dt_work->working_period      = date('Y-m-d', strtotime($request->periode));
                $dt_work->working_periode_end = date('Y-m-d', strtotime($request->periode_end));
                $dt_work->position            = $request->position;
                $dt_work->salary              = str_replace('.', '', $request->salary);
                $dt_work->reason_resignation  = $request->reason_resignation;
                $dt_work->active_flag         = '1';
                $dt_work->delete_flag         = '1';
                $dt_work->created_by          = $user->name;
                $dt_work->updated_by          = $user->name;
                $dt_work->update();
            if($dt_work->id != null){
                $dataWork = WorkPeople::where('employe_id',$request->people_id)->get();
                $respon['message'] = 'successfully insert data';
                $respon['code'] = 200;
                $respon['data'] = $dataWork;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal insert data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function postDeleteWorkUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user) {
            $update = DB::table('work_experience_people')->where('id',$request->id_work)->update(['delete_flag' => 0]);
            if ($update) {
                $dataBank = WorkPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully delete data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal detete data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getHistoryAbsensiOutletDate(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $historyAbsensiOutlet = DB::table('view_absensi_outlet')->where('people_id',$request->people_id)->where('outlet_id', $request->outlet_id)->where('task_id',$request->task_id)->where('date_absensi', date('Y-m-d'))->first();
            if ($historyAbsensiOutlet != null) {
                $respon['message'] = ' list history absensi outlet';
                $respon['code'] = 200;
                $respon['data'] = $historyAbsensiOutlet;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'history absensi outlet null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }


    public function postAddPictureUser(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
              $dt_imagektp = new ImagePeople();
              $dt_imagektp->employe_id  = $request->people_id;
              $dt_imagektp->image_type  = $request->image_type;
              $dt_imagektp->created_by  = 'android';
              $dt_imagektp->updated_by  = 'android';
              $dt_imagektp->active_flag = '1';
              $dt_imagektp->delete_flag = '1';
              $foto = $request->file('image');
                
              if ($request->hasFile('image')) {
                  $dt_imagektp->image = $this->uploadFotoImagePeopleKtp($request);
                }
              $dt_imagektp->save();
               if($dt_imagektp->id != null){
                    $dataFoto = ImagePeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                    $respon['message'] = 'successfully insert data';
                    $respon['code'] = 200;
                    $respon['data'] = $dataFoto;
                    return response()->json($respon, $this->succesStatus);
                }else{
                    $respon['message'] = 'gagal insert data';
                    $respon['code'] = 404;
                    return response()->json($respon, $this->succesStatus);
                }

        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function postDeleteFotoUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user) {
            $update = DB::table('image_people')->where('id',$request->id_picture)->update(['delete_flag' => 0]);
            if ($update) {
                $dataBank = ImagePeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully delete data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal detete data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

     private function uploadFotoImagePeopleKtp(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $rand = rand(10,10000);
            $namaFoto = $rand."ktp".".".$ext;
            $upload_path = 'employe_image';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
    }

    public function postUpdateEducationUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user){
            $update = EducationPeople::where('id',$request->id_education)->update([
                'education_lvl' => $request->education_lvl,
                'education_institution_name' => $request->education_institution_name,
                'education_location' => $request->education_location,
                'education_major' => $request->education_major,
                'periode_from' => date('Y-m-d', strtotime($request->periode_start)),
                'periode_to' => date('Y-m-d', strtotime($request->periode_to)) ]);
            if($update != null){
                $dataEdu = EducationPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->get();
                $respon['message'] = 'successfully update data';
                $respon['code'] = 200;
                $respon['data'] = $dataEdu;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal update data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }

    }

    public function getDetailAbsensiOutletUser(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $historyAbsensiOutlet = DB::table('view_absensi_outlet')->where('people_id',$request->people_id)->where('outlet_id', $request->outlet_id)->where('task_id',$request->task_id)->get();
            if ($historyAbsensiOutlet != null) {
                $respon['message'] = ' list detail absensi outlet';
                $respon['code'] = 200;
                $respon['data'] = $historyAbsensiOutlet;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'detail absensi outlet null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function getAddInteresJob(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
                $insInteres = $request->interes_id;
                for ($i=0; $i < count($insInteres) ; $i++) { 
                    $inte = array();
                    $inte['people_id'] = $request->people_id;
                    $inte['interes_id'] = $insInteres[$i];
                    $inte['active_flag'] = 1;
                    $inte['delete_flag'] = 1;
                    PeopleVacancy::create($inte);
                }
                $dataInt = DB::table('view_interes')->where('people_id',$request->people_id)->where('delete_flag',1)->get();
                    $respon['message'] = 'list data';
                    $respon['code'] = 200;
                    $respon['data'] = $dataInt;
                    return response()->json($respon, $this->succesStatus);
               

        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 501;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getInteresReady(Request $request){
        $user = EmployePeople::where('id',$request->people_id)->first();
        if ($user) {
           $data = PeopleVacancy::where('people_id',$request->people_id)->get();
           $id_int = [];
           foreach ($data as $key) {
               $id_int[] = $key->interes_id;
           }

           $job = DB::table('job_interesting')->whereNotIn('id',$id_int)->get();
           if($job != null){
                $respon['message'] = 'list data job interes';
                $respon['code'] = 200;
                $respon['data'] = $job;
                return response()->json($respon, $this->succesStatus);
           }else{
                $respon['message'] = 'job interes null';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
           }

        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 501;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getDeleteInteresUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if ($user) {
            $delInte = DB::table('people_vacancy')->where('id',$request->id_job)->update(['delete_flag' => 0 ]);
            if ($delInte) {
                 $dataInt = DB::table('view_interes')->where('people_id',$request->people_id)->where('delete_flag',1)->get();
                    $respon['message'] = 'delete data successfully';
                    $respon['code'] = 200;
                    $respon['data'] = $dataInt;
                    return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'delete data failed';
                $respon['code'] = 500;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 501;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getUpdatePhoneNumber(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
            $update = DB::table('employee_people')->where('id',$request->people_id)->update(['phone1' => $request->phone1]);
            if($update){
                $respon['message'] = 'update successfully';
                $respon['code'] = 200;
                $respon['phone1'] = $request->phone1;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'user not found';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus); 
        }
    }

    public function postAddPictureCv(Request $request)
    {
        $user = EmployePeople::where('id', $request->people_id)->first();
        if ($user) {
              
                $dt_doc_cv = new DocumentPeople();
                $dt_doc_cv->employe_id = $request->people_id;
                $dt_doc_cv->image_type = 'Foto CV';
                $dt_doc_cv->delete_flag = '1';
                $dt_doc_cv->code = '2';
                $foto_cv = $request->file('image_cv');
                    
                if ($request->hasFile('image_cv')) {
                  $dt_doc_cv->image_path = $this->uploadDocumentImage($request);
                }

                $dt_doc_cv->save();
               if($dt_doc_cv->id != null){
                    $dataFoto = DocumentPeople::where('employe_id',$request->people_id)->where('delete_flag',1)->where('code',2)->get();
                    $respon['message'] = 'successfully insert data';
                    $respon['code'] = 200;
                    $respon['data'] = $dataFoto;
                    return response()->json($respon, $this->succesStatus);
                }else{
                    $respon['message'] = 'gagal insert data';
                    $respon['code'] = 404;
                    return response()->json($respon, $this->succesStatus);
                }

        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

    private function uploadDocumentImage(Request $request)
    {
        $foto = $request->file('image_cv');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image_cv')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'document_people';
            $request->file('image_cv')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
    }

    public function postDeleteCvUser(Request $request)
    {
        $user = EmployePeople::where('id',$request->people_id)->first();
        if($user) {
            $update = DB::table('document_path')->where('id',$request->id_cv)->update(['delete_flag' => 0]);
            if ($update) {
                $dataBank = DB::table('document_path')->where('employe_id',$request->people_id)->where('delete_flag',1)->where('code',2)->get();
                $respon['message'] = 'successfully delete data';
                $respon['code'] = 200;
                $respon['data'] = $dataBank;
                return response()->json($respon, $this->succesStatus);
            }else{
                $respon['message'] = 'gagal detete data';
                $respon['code'] = 404;
                return response()->json($respon, $this->succesStatus);
            }
        }else{
            $respon['message'] = 'user not found';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

    public function getAggreeApi()
    {
        $data = Aggree::where('id',1)->first();
        if ($data) {
            $respon['message'] = 'data aggree';
            $respon['code'] = 200;
            $respon['data'] = $data;
            return response()->json($respon, $this->succesStatus);
        }else{
            $respon['message'] = 'data kosong';
            $respon['code'] = 201;
            return response()->json($respon, $this->succesStatus);
        }
    }

    

    
}
