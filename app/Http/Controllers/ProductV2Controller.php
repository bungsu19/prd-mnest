<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Client;
use Session;
use Redirect;
use App\Product;


class ProductV2Controller extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $jumlahData = 10;
    	$data = DB::table('view_product')->where('delete_flag',1)->orderBy('client_id','asc')->paginate($jumlahData);


    	return view('products.data_product',compact('data','jumlahData')); 
		
    }

    public function getAddData()
    {
        $branch = DB::table('office_branch')->orderBy('id','desc')->get();
        $client = DB::table('tb_client')->where('delete_flag',1)->get();
        $pakcaging = DB::table('product_packaging')->orderBy('id','desc')->get();
        return view('products.add_product',compact('branch','client','pakcaging'));
    }

    public function postAddData(Request $request)
    {
      
            $user = Auth::user();
            $data = new Product();
            $data->branch_id = $request->branch_id;
            $data->client_id = $request->client_id;
            $data->product_type = $request->product_type;
            $data->product_sku = $request->product_sku;
            $data->product_name = $request->product_name;
            $data->color = $request->color;
            $data->weight = (int)$request->weight;
            $data->height = (int)$request->height;
            $data->packaging_id = $request->packaging_id;
            $data->dimension = $request->dimension;
            $data->product_capacity = $request->product_capacity;
            $data->product_price = str_replace('.', '', $request->price);
            $data->production_date = $request->production_date;
            $data->expiry_date = $request->expired_date;
            $data->active_flag = '1';
            $data->delete_flag = '1';
            $data->created_by = $user->name;
            $data->updated_by = 'system';
             $foto = $request->file('image');
                
                    if ($request->hasFile('image')) {
                      $data->image = $this->uploadFotoProduct($request);
                    }
            $data->save();

            return Redirect('product-data')->with('message', 'Insert Data Success');
         


    }

    public function getEditData($id)
    {
        $data = DB::table('tb_product')->where('id',$id)->first();
        $branch = DB::table('office_branch')->orderBy('id','desc')->get();
        $client = DB::table('tb_client')->orderBy('client_id','id')->get();
        $pakcaging = DB::table('product_packaging')->orderBy('id','desc')->get();
        return view('products.edit_product',compact('data','branch','client','pakcaging'));
    }

    public function postEditData(Request $request)
    {
        try{
                $user = Auth::user();
                $update = Product::where('id',$request->id)->first();
                $update->branch_id = $request->branch_id;
                $update->client_id = $request->client_id;
                $update->product_type = $request->product_type;
                $update->product_sku = $request->product_sku;
                $update->product_name = $request->product_name;
                $update->color = $request->color;
                $update->weight = $request->weight;
                $update->height = $request->height;
                $update->packaging_id = $request->packaging_id;
                $update->dimension = $request->dimension;
                $update->product_capacity = $request->product_capacity;
                $update->product_price = str_replace('.', '', $request->price);
                $update->production_date = $request->production_date;
                $update->expiry_date = $request->expired_date;
                $update->updated_by = $user->name;
                 if($request->file('image') == "")
                { 
                    $update->image = $update->image;
                } 
                else
                {
                   
                    $foto = $request->file('image');
                    $ext = $foto->getClientOriginalExtension();
                    $namaFoto = date('YmdHis').".".$ext;
                    $upload_path = 'upload/imageProduct';
                    $request->file('image')->move($upload_path,$namaFoto);
                    $update->image = $namaFoto;
                }
                $update->update();

                return Redirect()->back()->with('message', 'Update Data Success');
             } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update Data Failed');
        }

    }

    public function postDeleteProduct($id)
    {
        try{

            DB::table('tb_product')->where('id', $id)->update(['delete_flag' => 0]); 
           return redirect()->back()->with('message', 'Delete Data Success');
        
        } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Delete Data Failed');
        }
    }

    public function getDetailsProduct($id)
    {
        $data = DB::table('tb_product')->where('id',$id)->first();
        $branch = DB::table('office_branch')->orderBy('id','desc')->get();
        $client = DB::table('tb_client')->orderBy('client_id','id')->get();
        $pakcaging = DB::table('product_packaging')->orderBy('id','desc')->get();
        return view('products.detail_product',compact('data','branch','client','pakcaging'));
    }

   private function uploadFotoProduct(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'upload/imageProduct';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }






}