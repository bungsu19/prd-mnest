<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Project;
use App\TaskPeople;
use App\TaskPeopleGroup;
use App\ProjectStructure;
use App\Absensi;
use Excel;
use Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class DownloadController extends Controller
{
    
	public function getFilterTaskList(Request $request){
		
			$people = $request->people_id_tasklist;
            $from = date('Y-m-d', strtotime($request->dateFromTasksList));
            $to = date('Y-m-d', strtotime($request->dateToTasksList));
            $active = 'job_vacancy';
			$people_id = 'all';
			$people_id_tasklist = $request->people_id_tasklist;
			$dateFrom = '';
			$dateTo = '';
			$dateFromTasksList = '';
			$dateToTasksList = '';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();

            if($project_id != null){
				
				$absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->paginate(10);
                $member = DB::table('view_project_member')->where('project_id',$project_id)->get();
				
                if($people == 'all' && $request->dateFromTasksList == null && $request->dateToTasksList == null){
					
					$task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate(10);
					
                }elseif($people == 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){
					
					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
					$task_people = DB::table('view_task_project_test')->whereBetween('schedule_date', [$from,$to])->where('project_id',$project_id)->paginate(10);
					
                }elseif($people != 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){

					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->whereBetween('schedule_date',array($from,$to))->orderBy('schedule_date','desc')->paginate(10);
	
					
                }elseif($people != 'all'){
					
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->orderBy('schedule_date','desc')->paginate(10);

					
                }else{

                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate(10);
					
                }
				
                 return view('TaskList.view_task_project',compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList' ,'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo'));
               
			}
		}

        public function getFilterAbsensi2(Request $request)
		{
			$absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->get();
			return Excel::create('laravelcode', function($excel) use ($absensi) {
				$excel->sheet('Sheet 1', function ($sheet) use ($absensi) {
					   $sheet->loadView('TaskList.absensi', array(
							  'absensi' => $absensi
					   ));
				 });
			 })->export('xls');
		}

		
		public function getFilterTaskList2(Request $request){
			
			$people = $request->people_id_tasklist;
            $from = date('Y-m-d', strtotime($request->dateFromTasksList));
            $to = date('Y-m-d', strtotime($request->dateToTasksList));
            $active = 'job_vacancy';
			$people_id = 'all';
			$people_id_tasklist = $request->people_id_tasklist;
			$dateFrom = '';
			$dateTo = '';
			$dateFromTasksList = '';
			$dateToTasksList = '';
            $project_id = $request->project_id;
            $project = Project::where('id',$project_id)->first();

            if($project_id != null){
				
				$absensi = DB::table('absensi_people_view')->orderBy('first_name')->orderBy('date_absensi','desc')->paginate(10);
                $member = DB::table('view_project_member')->where('project_id',$project_id)->get();
				
                if($people == 'all' && $request->dateFromTasksList == null && $request->dateToTasksList == null){
					
					$task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate(10);
					
                }elseif($people == 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){
					
					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
					$task_people = DB::table('view_task_project_test')->whereBetween('schedule_date', [$from,$to])->where('project_id',$project_id)->paginate(10);
					
                }elseif($people != 'all' && $request->dateFromTasksList != null && $request->dateToTasksList != null){

					$dateFromTasksList = date('d-m-Y', strtotime($request->dateFromTasksList));
					$dateToTasksList = date('d-m-Y', strtotime($request->dateToTasksList)); 
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->whereBetween('schedule_date',array($from,$to))->orderBy('schedule_date','desc')->paginate(10);
	
					
                }elseif($people != 'all'){
					
                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->where('people_id',$people)->orderBy('schedule_date','desc')->paginate(10);

					
                }else{

                    $task_people = DB::table('view_task_project_test')->where('project_id',$project_id)->orderBy('schedule_date','desc')->paginate(10);
					
                }
				
				$task_people->appends($request->only('people_id_tasklist'));
				$task_people->appends($request->only('project_id'));
				$task_people->appends($request->only('dateFromTasksList'));
				$task_people->appends($request->only('dateToTasksList'));
				
				return view('TaskList.view_task_project', compact('project', 'task_people', 'task', 'absensi', 'member', 'active', 'people_id', 'dateFromTasksList' ,'dateToTasksList', 'people_id_tasklist', 'dateFrom', 'dateTo'));
               
			}
		}
	
    // Ajax get Outlet from city
    public function getOutlet(Request $request){
        $response = array();

        $validator = Validator::make($request->all(), [
            'data_id' => 'required'
        ]);

        if ($validator->fails()) {
            $response['hasil'] = 'GAGAL';
            $response['data'] = '0';
            return json_encode($response);
        } else {
            $html = '<option value="">- Select Outlet -</option>';
            $response['hasil'] = 'OK';
            $village = DB::table('outlet')->select('id', 'outlet_name')->where('outlet_city',$request->data_id)->get();
            foreach ($village as $value) {
                $html .= '<option data-tokens="'.$value->id.'" value="'.$value->id.'">'.$value->outlet_name.'</option>';
            }
            $response['data'] = $html;
            return json_encode($response);
        }
    }
 
    
}
