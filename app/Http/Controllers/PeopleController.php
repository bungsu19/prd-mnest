<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\HomePeople;
use App\EmployePeople;
use App\BankPeople;
use App\EducationPeople;
use App\FamilyPeople;
use App\WorkPeople;
use App\ImagePeople;
use App\DocumentPeople;
use App\FacilityPeople;
use App\ViewInteres;
use App\PeopleVacancy;
use App\Branch;
use App\SendInterview;
use App\Inbox;
use Session;
use Validator;
use Redirect;
use Illuminate\Validation\Rule; 
use Illuminate\Support\Facades\Auth;
use Excel;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Input;


class PeopleController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
	    $active = '';
		if(Input::get('tab') == ''){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'personal_data'){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'Interview'){
			$active = 'Interview';
		}elseif(Input::get('tab') == 'Candidate'){
			$active = 'Candidate';
		}
		
	  $searchRegiter = 2;
	  $searchRegiterValue = '';
	  $searchFromValue = '';
	  $searchToValue = '';
	  $searchCandidate = 2;
	  $searchCandidateValue = '';
		
      $religion = DB::table('m_religion')->orderBy('id','asc')->get();
      $job = DB::table('job_interesting')->orderBy('id','desc')->get();
      $jumlahData = 10;

      $data = DB::table('people_view2')
				  ->where('delete_flag',1)
				  ->where('status_project', 2)
				  ->orderBy('first_name')
				  ->paginate($jumlahData, ['*'], 'data');

	  $interview = DB::table('view_send_interview')
                  ->where('delete_flag',1)
				  ->where('active_flag',0)
				  ->where('status_project', 100)
				  ->orderBy('date_confir')
				  ->paginate($jumlahData, ['*'], 'interview');
				  
	  $candidate = DB::table('people_view2')
					->where('delete_flag',1)
					->wherein('status_project', [200, 300, 0, 1,9])
					->orderBy('first_name')->paginate($jumlahData, ['*'], 'candidate');
	  
      
      foreach($data as $key) {
        $key->count_employe = DB::table('home_people')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_bank = DB::table('bank_people')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_education = DB::table('education')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_family = DB::table('family_people')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_work = DB::table('work_experience_people')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_facility = DB::table('facility')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_image = DB::table('image_people')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
        $key->count_doc = DB::table('document_path')
				->where('employe_id',$key->id)
				->where('delete_flag','1')
				->count();
      }
	  
	  $paginator = $data;
	  
		$data->appends(['tab' => 'personal_data']);
	    $data->appends(['interview' => $interview->currentPage()]);
		$data->appends(['candidate' => $candidate->currentPage()]);
		
		$interview->appends(['tab' => 'Interview']);
		$interview->appends(['data' => $data->currentPage()]);
		$interview->appends(['candidate' => $candidate->currentPage()]);
		
		$candidate->appends(['tab' => 'Candidate']);
		$candidate->appends(['candidate' => $candidate->currentPage()]);
		$candidate->appends(['interview' => $interview->currentPage()]);
		$candidate->appends(['data' => $data->currentPage()]);

      return view('master-people.data_people',compact('job', 'religion', 'data', 'jumlahData', 'paginator', 
													  'interview', 'paginator2', 'candidate' ,'paginator3', 
													  'active', 'searchRegiter', 'searchRegiterValue', 'searchFromValue',
													  'searchToValue', 'searchCandidate', 'searchCandidateValue'));
    }

    public function postAddEmploye(Request $request){
        try{
            $password_default = 123456;

            $check = EmployePeople::where('email_address',$request->email)->where('nik_ktp',$request->nik)->first();
            if($check){
                return redirect()->back()->with('failed', 'This Data Already Exists ');
            }else{

                    $dt_data = new EmployePeople();
                    $dt_data->branch_id      = $request->branch_id;
                    $dt_data->first_name     = $request->first_name;
                    $dt_data->middle_name    = $request->middle_name;
                    $dt_data->surname        = $request->surname;
                    $dt_data->nick_name      = $request->nick_name;
                    $dt_data->dob            = 'aa';
                    $dt_data->dob2            = date('Y-m-d', strtotime($request->dob));
                    $dt_data->pob            = $request->pob;
                    $dt_data->gender         = $request->gender;
                    $dt_data->religion       = $request->religion;
                    $dt_data->nationality    = $request->nationality;
                    $dt_data->phone1         = $request->phone1;
                    $dt_data->phone2         = $request->phone2;
                    $dt_data->email_address  = $request->email;
                    $dt_data->sosmed1        = $request->sosmed1;
                    $dt_data->sosmed2        = $request->sosmed2;
                    $dt_data->npwp_number    = $request->npwp_number;
                    $dt_data->npwp_validity  = $request->npwp_validity;
                    $dt_data->matrial_status = $request->marital;
                    $dt_data->body_height    = $request->body_height;
                    $dt_data->body_weight    = $request->body_weight;
                    $dt_data->blood_type     = $request->blood_type;
                    $dt_data->sim_a_number   = $request->sim_a_number;
                    $dt_data->sim_a_validity = $request->sim_a_validity;
                    $dt_data->sim_c_number   = $request->sim_c_number;
                    $dt_data->sim_c_validity = $request->sim_c_validity;
                    $dt_data->bank_acc_no    = '1 ';
                    $dt_data->active_flag       = '1';
                    $dt_data->delete_flag       = '1';
                    $dt_data->created_by        = 'system';
                    $dt_data->updated_by        = 'system';
                    $dt_data->data_status       = '0';
                    $dt_data->status_project    = '2';
                    $dt_data->status_user       = '0';
                    $dt_data->cek_login         = '0';
                    $dt_data->ktp_province      = $request->ktp_province;
                    $dt_data->ktp_district      = $request->ktp_district;
                    $dt_data->ktp_subdistrict    = $request->ktp_subdistrict;
                    $dt_data->ktp_village       = $request->ktp_village;
                    $dt_data->ktp_rt            = $request->ktp_rt;
                    $dt_data->ktp_rw            = $request->ktp_rw;
                    $dt_data->ktp_post_code     = $request->ktp_post_code;
                    $dt_data->ktp_address       = $request->address1;
                    $dt_data->dom_province      = $request->dom_province;
                    $dt_data->dom_district      = $request->dom_district;
                    $dt_data->dom_subdistrict   = $request->dom_subdistrict;
                    $dt_data->dom_village       = $request->dom_village;
                    $dt_data->dom_rt            = $request->dom_rt;
                    $dt_data->dom_rw            = $request->dom_rw;
                    $dt_data->dom_post_code     = $request->dom_post_code;
                    $dt_data->dom_address       = $request->address2;
                    $dt_data->nik_ktp           = $request->nik;
                    $dt_data->expected_salary   = str_replace('.', '', $request->expected_salary);
                    $dt_data->allowance         = $request->allowance;
                    $dt_data->facility          = $request->facility;
                    $dt_data->insurance         = $request->insurance;
                    $dt_data->last_education    = $request->last_education;
                    $dt_data->no_bpjs_kesehatan = $request->health;
                    $dt_data->password    = base64_encode($password_default);

                    $foto = $request->file('image');
                    if ($request->hasFile('image')) {
                    
                            $image       = $request->file('image');
                            $ext = $image->getClientOriginalExtension();
                            $dt_data->image = date('YmdHis').".".$ext;

                            $image_resize = Image::make($image->getRealPath());              
                            $image_resize->resize(300, 300);
                            $image_resize->save(public_path('employe/' .$dt_data->image));
                    }
                    $dt_data->save();

                    $data = DB::table('employee_people')->where('id',$dt_data->id)->first();
                    $employe = DB::table('home_people')->where('employe_id', $dt_data->id)->get();
                    $bank = DB::table('bank_people')->where('employe_id',$dt_data->id)->get();
                    $education = DB::table('education')->where('employe_id',$dt_data->id)->get();
                    $family = DB::table('family_people')->where('employe_id',$dt_data->id)->get();
                    $work = DB::table('work_experience_people')->where('employe_id',$dt_data->id)->get();
                    $doc = DB::table('document_path')->where('employe_id',$dt_data->id)->get();
                    $image = DB::table('image_people')->where('employe_id',$dt_data->id)->get();
                    $facility = DB::table('facility')->where('employe_id',$dt_data->id)->get();
                    $interes = ViewInteres::where('people_id',$dt_data->id)->where('delete_flag','1')->get();
                    $job = DB::table('job_interesting')->orderBy('id','desc')->get();
                    $prov = DB::table('provinsi')->orderBy('id','desc')->get();
                    $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
                    $kec = DB::table('kecamatan')->orderBy('id','desc')->get();

                    $count_employe = DB::table('home_people')->where('employe_id', $dt_data->id)->where('delete_flag','1')->count();
                    $count_bank = DB::table('bank_people')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_education = DB::table('education')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_family = DB::table('family_people')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_work = DB::table('work_experience_people')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_image = DB::table('image_people')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_doc = DB::table('document_path')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $count_facility = DB::table('facility')->where('employe_id',$dt_data->id)->where('delete_flag','1')->count();
                    $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
                    $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

                    return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district','interes','job')); 
                }
           
        } catch(\Illuminate\Database\QueryException $ex){
              return redirect('data-people')->with('gagal', 'Add People Gagal');
        }

    }

    public function getAddInfo($id)
    {
        $prov = DB::table('provinsi')->orderBy('id','desc')->get();
        $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
        $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
        $data = DB::table('employee_people')->where('id',$id)->first();
        $employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->get();
        $bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->get();
        $education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->get();
        $family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->get();
        $work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->get();
        $image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->get();
        $doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->get();
        $facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->get();
        $interes = ViewInteres::where('people_id',$id)->where('delete_flag','1')->get();
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
        

        $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
        $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
        $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

        return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district','interes','job'));
    }

    public function postAddAddress(Request $request)
    {
        $address = new HomePeople();
        $address->employe_id = $request->employe_id;
        $address->address1 = $request->address1;
        $address->address2 = 'qw';
        $address->rt_tw = $request->rt_rw;
        $address->village = $request->village;
        $address->sub_district = $request->kecamatan;
        $address->regency_city = $request->kabupaten;
        $address->province = $request->provinsi;
        $address->post_code = $request->kode_pos;
        $address->home_code = $request->home_code;
        $address->home_sts = $request->home_sts;
        $address->delete_flag = '1';
        $address->save();

       return redirect()->back();
    }

    public function postEditAddress(Request $request)
    {
        $update = HomePeople::where('id',$request->id)->first();
        $update->address1 = $request->address1;
        $update->rt_tw = $request->rt_rw;
        $update->village = $request->village;
        $update->sub_district = $request->kecamatan;
        $update->regency_city = $request->kabupaten;
        $update->province = $request->provinsi;
        $update->post_code = $request->kode_pos;
        $update->home_code = $request->home_code;
        $update->home_sts = $request->home_sts;
        $update->update();

        return redirect()->back();
    }

    public function postDeleteAddress($id)
    {
        DB::table('home_people')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back();

    }

    public function postAddBank(Request $request)
    {
        try{
          $user = Auth::user();

          $dt_bank = new BankPeople();
          $dt_bank->employe_id = $request->employe_id;
          $dt_bank->bank_acc_no = $request->no_rek;
          $dt_bank->bank_acc_name = $request->acc_name;
          $dt_bank->bank_name = $request->name_bank;
          $dt_bank->active_flag = '1';
          $dt_bank->delete_flag = '1';
          $dt_bank->created_by = $user->name;
          $dt_bank->updated_by = $user->name;
          $dt_bank->save();

          return redirect()->back()->with('message', 'Insert data bank successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data bank failed');
        }
    }

    public function postEditBank(Request $request)
    {
        try{
          $user = Auth::user();
          $dt_update = BankPeople::where('id',$request->id)->first();
          $dt_update->bank_acc_no = $request->no_rek;
          $dt_update->bank_acc_name = $request->acc_name;
          $dt_update->bank_name = $request->name_bank;
          $dt_update->updated_by = $user->name;
          $dt_update->update();

         return redirect()->back()->with('message', 'Update data bank successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data bank failed');
        }
    }

    public function postDeleteBank($id)
    {
		try{
				DB::table('bank_people')->where('id', $id)->update(['delete_flag' => 0]); 
				return redirect()->back()->with('message', 'Delete data bank successfully');
				
		} catch(\Illuminate\Database\QueryException $ex){
				return redirect()->back()->with('failed', 'Delete data bank failed');
        }
    }

    public function postDeleteBankActive($id)
    {
        DB::table('bank_people')->where('id', $id)->update(['delete_flag' => 1]);
        $active = DB::table('bank_people')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
        $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
       } 
       
    }

    public function postAddEducation(Request $request)
    {
        try{

        $user = Auth::user();

        $dt_edu = new EducationPeople();
        $dt_edu->employe_id = $request->employe_id;
        $dt_edu->education_lvl = $request->level;
        $dt_edu->education_institution_name = $request->education_institution_name;
        $dt_edu->education_location = $request->education_location;
        $dt_edu->education_major = $request->education_major;
        $dt_edu->periode_from = $request->periode_start;
        $dt_edu->periode_to = $request->periode_to;
        $dt_edu->active_flag = '1';
        $dt_edu->delete_flag = '1';
        $dt_edu->created_by = $user->name;
        $dt_edu->updated_by = $user->name;
        $dt_edu->save();

         return redirect()->back()->with('message', 'Insert data education successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data education failed');
        }
    }

     public function postEditEducation(Request $request)
    {
        try{
        $dt_edu = EducationPeople::where('id',$request->id)->first();
        $dt_edu->education_lvl = $request->level;
        $dt_edu->education_institution_name = $request->education_institution_name;
        $dt_edu->education_location = $request->education_location;
        $dt_edu->education_major = $request->education_major;
        $dt_edu->periode_from = $request->periode_start;
        $dt_edu->periode_to = $request->periode_to;
        $dt_edu->update();
         return redirect()->back()->with('message', 'Update data education successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data education failed');
        }
    }

    public function postDeleteEducation($id)
    {
        DB::table('education')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message','Delete data education successfully');
    }
    public function postDeleteEducationActive($id)
    {
        DB::table('education')->where('id', $id)->update(['delete_flag' => 1]); 
        $active = DB::table('education')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
           $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
       } 
    }

    public function postAddFamily(Request $request)
    {
        try{
            $user = Auth::user();

            $dt_fml = new FamilyPeople();
            $dt_fml->employe_id = $request->employe_id;
            $dt_fml->member_code = $request->status;
            $dt_fml->member_name = $request->name;
            $dt_fml->member_gender = $request->gender;
            $dt_fml->member_last_edu = $request->education;
            $dt_fml->member_dob = $request->dob;
            $dt_fml->member_pob = $request->pob;
            $dt_fml->member_phone1 = $request->phone1;
            $dt_fml->member_phone2 = $request->phone2;
            $dt_fml->member_company_address = $request->company_address;
            $dt_fml->member_company = $request->company;
            $dt_fml->member_job_position = $request->position;
            $dt_fml->residence_city = $request->city;
            $dt_fml->active_flag = '1';
            $dt_fml->delete_flag = '1';
            $dt_fml->created_by = $user->name;
            $dt_fml->updated_by = $user->name;
            $dt_fml->save();

          return redirect()->back()->with('message', 'Insert data family successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data family failed');
        }

    }

     public function postEditFamily(Request $request)
    {
        try{
            $user = Auth::user();
            $dt_fml = FamilyPeople::where('id',$request->id)->first();
            $dt_fml->member_code = $request->status;
            $dt_fml->member_name = $request->name;
            $dt_fml->member_gender = $request->gender;
            $dt_fml->member_last_edu = $request->education;
            $dt_fml->member_dob = date('Y-m-d', strtotime($request->dob));
            $dt_fml->member_pob = $request->pob;
            $dt_fml->member_phone1 = $request->phone1;
            $dt_fml->member_phone2 = $request->phone2;
            $dt_fml->member_company_address = $request->company_address;
            $dt_fml->member_company = $request->company;
            $dt_fml->member_job_position = $request->position;
            $dt_fml->residence_city = $request->city;
            $dt_fml->updated_by = $user->name;
            $dt_fml->update();

             return redirect()->back()->with('message', 'Update data family successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data family failed');
        }

    }

     public function postDeleteFamily($id)
    {
        DB::table('family_people')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message','Delete data family successfully');
    }

    public function postDeleteFamilyActive($id)
    {
        DB::table('family_people')->where('id', $id)->update(['delete_flag' => 1]); 
        $active = DB::table('family_people')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
           $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
        }
    }

    public function postAddWork(Request $request)
    {
        try{
            $user = Auth::user();
            $dt_work = new WorkPeople();
            $dt_work->employe_id          = $request->employe_id;
            $dt_work->company_name        = $request->name_company;
            $dt_work->working_period      = $request->periode;
            $dt_work->working_periode_end = $request->periode_end;
            $dt_work->position            = $request->position;
            $dt_work->salary              = str_replace('.', '', $request->salary);
            $dt_work->reason_resignation  = $request->reason_resignation;
            $dt_work->active_flag         = '1';
            $dt_work->delete_flag         = '1';
            $dt_work->created_by          = $user->name;
            $dt_work->updated_by          = $user->name;
            $dt_work->save();

        return redirect()->back()->with('message', 'Insert data work experience successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data work experience failed');
        }
    }

    public function postEditWork(Request $request)
    {
        try{
            $user = Auth::user();
            $dt_work = WorkPeople::where('id',$request->id)->first();
            $dt_work->company_name         = $request->name_company;
            $dt_work->working_period       = $request->periode;
            $dt_work->working_periode_end  = $request->periode_end;
            $dt_work->position             = $request->position;
            $dt_work->salary               = str_replace('.', '', $request->salary);
            $dt_work->reason_resignation   = $request->reason_resignation;
            $dt_work->updated_by           = $user->name;
            
            $dt_work->update();

        return redirect()->back()->with('message', 'Update data work experience successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data work experience failed');
        }
    }

    public function postDeleteWork($id)
    {
        DB::table('work_experience_people')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message','Delete data work experience successfully');
    }
    public function postDeleteWorkActive($id)
    {
        DB::table('work_experience_people')->where('id', $id)->update(['delete_flag' => 1]); 
        $active = DB::table('work_experience_people')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
           $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
         }
     }

    public function getViewData($id)
    {
         $data = DB::table('employee_people')->where('id',$id)->first();
         $employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->get();
         $bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->get();
         $education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->get();
         $family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->get();
         $work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->get();
         $image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->get();
         $doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->get();
         $facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->get();
         $interes = ViewInteres::where('people_id',$id)->where('delete_flag','1')->get();
          $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
         return view('master-people.view_data',compact('data','employe','bank','education','family','work','image','doc','facility','count_bank','count_education','count_family','count_work','count_doc','count_image','count_facility','interes'));   
    }

    public function postAddImage(Request $request)
    {
        try{
            $check = DB::table('image_people')->where('employe_id',$request->employe_id)->where('image_type','=',$request->image_type)->where('delete_flag',1)->first();
            if($check){
                return redirect()->back()->with('failed', 'Image'.' '.$request->image_type.' '.'Ready');
            }else{
              $user = Auth::user();
              $dt_image = new ImagePeople();
              $dt_image->employe_id = $request->employe_id;
              $dt_image->image_type = $request->image_type;
              $dt_image->created_by = $user->name;
              $dt_image->updated_by = $user->name;
              $dt_image->active_flag = '1';
              $dt_image->delete_flag = '1';
              $foto = $request->file('image');
               if ($request->hasFile('image')) {
                    
                    $image       = $request->file('image');
                    $ext = $image->getClientOriginalExtension();
                    $dt_image->image = date('YmdHis').".".$ext;

                    $image_resize = Image::make($image->getRealPath());              
                    $image_resize->resize(300, 300);
                    $image_resize->save(public_path('employe_image/' .$dt_image->image));
            }
              $dt_image->save();

            return redirect()->back()->with('message', 'Insert data image successfully');
            }
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data image failed');
        }
    }

    public function postAddDocument(Request $request)
    {
        try{
          $dt_doc = new DocumentPeople();
          $dt_doc->employe_id = $request->employe_id;
          $dt_doc->image_type = $request->document_type;
          $dt_doc->delete_flag = '1';
          $dt_doc->code = '1';
          $foto = $request->file('image');
            
            if ($request->hasFile('image')) {
              $dt_doc->image_path = $this->uploadDocument($request);
            }

          $dt_doc->save();

       return redirect()->back()->with('message', 'Insert data dokumen successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data dokumen failed');
        }

    }

    public function postAddFacility(Request $request)
    {
      try{
          $dt_facility = new FacilityPeople();
          $dt_facility->employe_id = $request->employe_id;
          $dt_facility->expected_salary = $request->expected_salary;
          $dt_facility->allowance = $request->allowance;
          $dt_facility->facility = $request->facility;
          $dt_facility->Insurance = $request->isurance;
          $dt_facility->active_flag = '1';
          $dt_facility->delete_flag = '1';
          $dt_facility->created_by = 'system';
          $dt_facility->updated_by = 'system';
          $dt_facility->save();

       return redirect()->back()->with('message', 'Insert data facility successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Insert data facility failed');
        }

    }

    public function postEditFacility(Request $request)
    {
        try{
          $dt_facility = FacilityPeople::where('id',$request->id)->first();
          $dt_facility->expected_salary = $request->expected_salary;
          $dt_facility->allowance = $request->allowance;
          $dt_facility->facility = $request->facility;
          $dt_facility->Insurance = $request->isurance;
          $dt_facility->updated_by = 'system';
          $dt_facility->update();

       return redirect()->back()->with('message', 'Update data facility successfully');
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data facility failed');
        }

    }

    public function postDeleteFaciiity($id)
    {
        DB::table('facility')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message', 'Delete data facility successfully');
    }

    public function postDeleteImage($id)
    {
        DB::table('image_people')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message', 'Delete data image successfully');
    }

    public function postDeleteImageActive($id)
    {
        DB::table('image_people')->where('id', $id)->update(['delete_flag' => 1]); 
        $active = DB::table('image_people')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
           $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
         }
     }

    public function postDeleteDocument($id)
    {
        DB::table('document_path')->where('id', $id)->update(['delete_flag' => 0]); 
        return redirect()->back()->with('message', 'Delete data dokumen successfully');
    }

    public function postDeleteDocumentActive($id)
    {
        DB::table('document_path')->where('id', $id)->update(['delete_flag' => 1]); 
        $active = DB::table('document_path')->where('id',$id)->first();
       $id = $active->employe_id;

       if($active){
           $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));
        }
    }


    public function getDeleteFlag($id)
    {
        $delete = DB::table('employee_people')->where('id', $id)->update(['delete_flag' => 0]); 
		
		if($delete){
			return redirect('data-people')->with('message','Delete data people successfully');
		}else{
			return redirect('data-people')->with('failed','Delete data people failed');
		}
    }
    public function getDeleteInterview($id)
    {
        $delete = DB::table('send_interview')->where('id', $id)->update(['active_flag' => 1]); 
        
        if($delete){
            return redirect('data-people')->with('message','Delete data people successfully');
        }else{
            return redirect('data-people')->with('failed','Delete data people failed');
        }
    }

    public function getActiveFlag($id)
    {
       DB::table('employee_people')->where('id', $id)->update(['delete_flag' => 1]); 
       return redirect('data-people');
    }

    public function getFilterPeople(Request $request)
    {
      $search = $request->search;
      if($search == 1){
        $data = DB::table('employee_people')->where('delete_flag',$search)->get();
         foreach($data as $key) {
        $key->count_employe = DB::table('home_people')->where('employe_id',$key->id)->count();
        $key->count_bank = DB::table('bank_people')->where('employe_id',$key->id)->count();
        $key->count_education = DB::table('education')->where('employe_id',$key->id)->count();
        $key->count_family = DB::table('family_people')->where('employe_id',$key->id)->count();
        $key->count_work = DB::table('work_experience_people')->where('employe_id',$key->id)->count();
        $key->count_facility = DB::table('facility')->where('employe_id',$key->id)->count();
        $key->count_image = DB::table('image_people')->where('employe_id',$key->id)->count();
        $key->count_doc = DB::table('document_path')->where('employe_id',$key->id)->count();
      }
        $religion = DB::table('m_religion')->orderBy('id','asc')->get();
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
         return view('master-people.data_people',compact('data','job','religion'));
      }else{
         $data = DB::table('employee_people')->orderBy('id','desc')->get();
          foreach($data as $key) {
        $key->count_employe = DB::table('home_people')->where('employe_id',$key->id)->count();
        $key->count_bank = DB::table('bank_people')->where('employe_id',$key->id)->count();
        $key->count_education = DB::table('education')->where('employe_id',$key->id)->count();
        $key->count_family = DB::table('family_people')->where('employe_id',$key->id)->count();
        $key->count_work = DB::table('work_experience_people')->where('employe_id',$key->id)->count();
        $key->count_facility = DB::table('facility')->where('employe_id',$key->id)->count();
        $key->count_image = DB::table('image_people')->where('employe_id',$key->id)->count();
        $key->count_doc = DB::table('document_path')->where('employe_id',$key->id)->count();
      }
         $religion = DB::table('m_religion')->orderBy('id','asc')->get();
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
         return view('master-people.data_filter',compact('data','job','religion'));
      }
    }

    private function uploadFotoPromo(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'employe';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

      private function uploadFotoImagePeople(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'employe_image';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

      private function uploadDocument(Request $request){
        $foto = $request->file('image');
        $ext = $foto->getClientOriginalExtension();
 
        if($request->file('image')->isValid()){
            $namaFoto = date('YmdHis').".".$ext;
            $upload_path = 'document_people';
            $request->file('image')->move($upload_path,$namaFoto);
            return $namaFoto;
        }
        return false;
      }

      public function AddPeople()
      {
        $branch = DB::table('office_branch')->where('delete_flag',1)->orderBy('id','desc')->get();
        $religion = DB::table('m_religion')->orderBy('id','asc')->get();
        $job = DB::table('job_interesting')->orderBy('id','desc')->get();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        return view('master-people.add_people',compact('job','religion','branch','province'));
      }

      public function postFinish(Request $request)
      {

        $employe = DB::table('home_people')->where('employe_id', $request->employe_id)->count();
        $bank = DB::table('bank_people')->where('employe_id',$request->employe_id)->count();
        $education = DB::table('education')->where('employe_id',$request->employe_id)->count();
        $family = DB::table('family_people')->where('employe_id',$request->employe_id)->count();
        $work = DB::table('work_experience_people')->where('employe_id',$request->employe_id)->count();
        $doc = DB::table('document_path')->where('employe_id',$request->employe_id)->count();
        $image = DB::table('image_people')->where('employe_id',$request->employe_id)->count();
        $facility = DB::table('facility')->where('employe_id',$request->employe_id)->count();
        
        if($employe != 0 && $bank != 0 && $education != 0 && $family != 0 && $work != 0 && $doc != 0 && $image != 0 && $facility != 0 ){
             DB::table('employee_people')->where('id', $request->employe_id)->update(['data_status' => 1]); 
             return redirect('data-people')->with('message', 'Data People Complete');
        }else{
          return redirect()->back()->with('failed', 'Data People Not Complete !!!');
        }

      }


      public function filterDataDelete(Request $request)
      {
        $param = $request->search;
        $id = $request->employe_id;
        if($param == 1){
            $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->get();
            $education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->get();
            $family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->get();
            $image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->get();
            $facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));

        }else{
            $prov = DB::table('provinsi')->orderBy('id','desc')->get();
            $kab = DB::table('kabupaten')->orderBy('id','desc')->get();
            $kec = DB::table('kecamatan')->orderBy('id','desc')->get();
            $data = DB::table('employee_people')->where('id',$id)->first();
            $employe = DB::table('home_people')->where('employe_id', $id)->get();
            $bank = DB::table('bank_people')->where('employe_id',$id)->get();
            $education = DB::table('education')->where('employe_id',$id)->get();
            $family = DB::table('family_people')->where('employe_id',$id)->get();
            $work = DB::table('work_experience_people')->where('employe_id',$id)->get();
            $image = DB::table('image_people')->where('employe_id',$id)->get();
            $doc = DB::table('document_path')->where('employe_id',$id)->get();
            $facility = DB::table('facility')->where('employe_id',$id)->get();

            $count_employe = DB::table('home_people')->where('employe_id', $id)->where('delete_flag','1')->count();
            $count_bank = DB::table('bank_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_education = DB::table('education')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_family = DB::table('family_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_work = DB::table('work_experience_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_image = DB::table('image_people')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_doc = DB::table('document_path')->where('employe_id',$id)->where('delete_flag','1')->count();
            $count_facility = DB::table('facility')->where('employe_id',$id)->where('delete_flag','1')->count();
            $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
            $district = DB::table('tb_kodepos')->select('district')->orderBy('district','asc')->groupBy('district')->get();

            return view('master-people.add_info',compact('data','prov','kab','kec','employe','bank','education','family','work','image','doc','facility','count_employe','count_bank','count_education','count_family','count_work','count_image','count_doc','count_facility','province','district'));

        }

      }

      public function postVacancyPeople(Request $request)
      {
        try{
            $check = PeopleVacancy::where('people_id',$request->employe_id)->where('interes_id',$request->job_id)->first();
			
            if($check){
                return redirect()->back()->with('failed', 'Save data interest failed');
            }else{
                $inte = new PeopleVacancy();
                $inte->people_id = $request->employe_id;
                $inte->interes_id = $request->job_id;
                $inte->active_flag = 1;
                $inte->delete_flag = 1;
                $inte->save();
				
				if($inte->id == null){
					return Redirect()->back()->with('failed', 'Save data interest failed');
				}else{
					return Redirect()->back()->with('message', 'Save data interest successfully');
				}
            }
           
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Save data interest failed');
        }


      }

      public function postVacancyPeopleEdit(Request $request)
      {
        $update = PeopleVacancy::where('id',$request->id)->first();
        $update->interes_id = $request->job_id;
        $update->update();
		
		if($update->id == null){
			return Redirect()->back()->with('failed', 'Update data interest failed');
		}else{
			return Redirect()->back()->with('message', 'Update data interest successfully');
		}

      }
 
      public function getDeleteVacancyPeople($id)
      {
        $delete = PeopleVacancy::where('id',$id)->delete();
        if($delete){
            return redirect()->back()->with('message', 'Delete data interest successfully');
        }else{
            return redirect()->back()->with('message', 'Delete data interest failed');
        }
      }

      public function getEditPoeple($id)
      {
        $people   = EmployePeople::where('id',$id)->first();
        $branch   = DB::table('office_branch')->orderBy('id','desc')->get();
        $religion = DB::table('m_religion')->orderBy('id','asc')->get();
        $job      = DB::table('job_interesting')->orderBy('id','desc')->get();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();

        return view('master-people.edit_people',compact('people','branch','religion','job','province'));

      }

      public function postEditPeople(Request $request)
      {
            $update = EmployePeople::where('id',$request->id)->first();
            $update->branch_id         = $request->branch_id;
            $update->first_name        = $request->first_name;
            $update->middle_name       = $request->middle_name;
            $update->surname           = $request->surname;
            $update->nick_name         = $request->nick_name;
            $update->dob2              = date('Y-m-d', strtotime($request->dob));
            $update->pob               = $request->pob;
            $update->gender            = $request->gender;
            $update->religion          = $request->religion;
            $update->nationality       = $request->nationality;
            $update->phone1            = $request->phone1;
            $update->phone2            = $request->phone2;
            $update->email_address     = $request->email;
            $update->sosmed1           = $request->sosmed1;
            $update->sosmed2           = $request->sosmed2;
            $update->npwp_number       = $request->npwp_number;
            $update->npwp_validity     = $request->npwp_validity;
            $update->matrial_status    = $request->marital;
            $update->body_height       = $request->body_height;
            $update->body_weight       = $request->body_weight;
            $update->blood_type        = $request->blood_type;
            $update->sim_a_number      = $request->sim_a_number;
            $update->sim_a_validity    = $request->sim_a_validity;
            $update->sim_c_number      = $request->sim_c_number;
            $update->sim_c_validity    = $request->sim_c_validity;
            $update->updated_by        = 'system';
            $update->ktp_province      = $request->ktp_province;
            $update->ktp_district      = $request->ktp_district;
            $update->ktp_subdistrict   = $request->ktp_subdistrict;
            $update->ktp_village       = $request->ktp_village;
            $update->ktp_rt            = $request->ktp_rt;
            $update->ktp_rw            = $request->ktp_rw;
            $update->ktp_post_code     = $request->ktp_post_code;
            $update->ktp_address       = $request->address1;
            $update->dom_province      = $request->dom_province;
            $update->dom_district      = $request->dom_district;
            $update->dom_subdistrict   = $request->dom_subdistrict;
            $update->dom_village       = $request->dom_village;
            $update->dom_rt            = $request->dom_rt;
            $update->dom_rw            = $request->dom_rw;
            $update->dom_post_code     = $request->dom_post_code;
            $update->dom_address       = $request->address2;
            $update->nik_ktp           = $request->nik;
            $update->expected_salary   = str_replace('.', '', $request->expected_salary);
            $update->allowance         = $request->allowance;
            $update->facility          = $request->facility;
            $update->insurance         = $request->insurance;
            $update->last_education    = $request->last_education;
            $update->no_bpjs_kesehatan    = $request->health;
			
            if($request->file('image') == null)
            { 
				$update->update();
				if($update->id == null){
					return Redirect()->back()->with('failed', 'Data people failed to update');
				}else{
					return Redirect()->back()->with('message', 'Data people successfully update');
				}
            }else{
               
                $foto = $request->file('image');
                $ext = $foto->getClientOriginalExtension();
                $namaFoto = date('YmdHis').".".$ext;
                $upload_path = 'employe';
                $request->file('image')->move($upload_path,$namaFoto);
                $update->image = $namaFoto;
				$update->update();
			
				if($update->id == null){
					return Redirect()->back()->with('failed', 'Data people failed to update');
				}else{
					return Redirect()->back()->with('message', 'Data people successfully update');
				}
            }
      }
	  
	public function importPeople(Request $request)
    {
		
        if($request->hasFile('import')){
            Excel::load($request->file('import')->getRealPath(), function ($reader) {
				$password = '123456';
                $no = 1;
				$temp = array();
                foreach ($reader->toArray() as $key => $row) {
					
					$check = EmployePeople::where('email_address', $row['email'])->where('nik_ktp', $row['id_card_number'])->first();
					
					if($check == null){
                        $people = new EmployePeople();
                        $people->branch_id = $row['branch_code'];
                        $people->first_name = $row['first_name'];
                        $people->middle_name = $row['middle_name'];
                        $people->surname = $row['surname'];
                        $people->nick_name = $row['nick_name'];
                        $people->pob = $row['pob'];
                        $people->gender = $row['gender'];
                        $people->religion = $row['religion'];
                        $people->nationality = $row['nationality'];
                        $people->phone1 = $row['phone_number1'];
                        $people->phone2 = $row['phone_number2'];
                        $people->email_address = $row['email'];
                        $people->sosmed1 = $row['facebook'];
                        $people->sosmed2 = $row['twitter'];
                        $people->npwp_number = $row['npwp'];
                        $people->matrial_status = $row['marital_status'];
                        $people->body_weight = $row['body_weight'];
                        $people->blood_type = $row['blood_type'];
                        $people->sim_a_number = $row['sim_a'];
                        $people->sim_a_validity = $row['valid_until_sim_a'];
                        $people->sim_c_number = $row['sim_c'];
                        $people->sim_c_validity = $row['valid_until_sim_c'];
                        $people->active_flag = '1';
                        $people->delete_flag = '1';
                        $people->created_by = 'Import Excel';
                        $people->data_status = '1';
                        $people->status_project = '2';
                        $people->status_user = '0';
                        $people->cek_login = '0';
                        $people->status_login = '0';
                        $people->ktp_province = $row['province'];
                        $people->ktp_district = $row['district'];
                        $people->ktp_subdistrict = $row['sub_district'];
                        $people->ktp_village = $row['village'];
                        $people->ktp_rt= $row['rt'];
                        $people->ktp_rw = $row['rw'];
                        $people->ktp_post_code = $row['post_code'];
                        $people->dom_province = $row['dom_province'];
                        $people->dom_district = $row['dom_district'];
                        $people->dom_subdistrict = $row['dom_sub_district'];
                        $people->dom_village = $row['dom_village'];
                        $people->dom_rt = $row['dom_rt'];
                        $people->dom_rw = $row['dom_rw'];
                        $people->dom_post_code= $row['dom_post_code'];
                        $people->ktp_address = $row['ktp_address'];
                        $people->dom_address= $row['home_address'];
                        $people->nik_ktp = $row['id_card_number'];
                        $people->expected_salary = $row['expected_salary'];
                        $people->allowance = $row['allowance'];
                        $people->facility = $row['facility'];
                        $people->insurance = $row['insurance'];
                        $people->last_education = $row['last_education'];
                        $people->dob2 = $row['dob'];
                        $people->password = base64_encode($password);
                        $people->save();

                   

                       
                        $bank = new BankPeople();
                        $bank->employe_id = $people->id;
                        $bank->bank_acc_no = $row['bank_acc_no'];
                        $bank->bank_acc_name = $row['bank_acc_name'];
                        $bank->bank_name = $row['bank_name'];
                        $bank->active_flag = 1;
                        $bank->delete_flag = 1;
                        $bank->created_by = 'Import Excel';
                        $bank->save(); 
                       


                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_sd'];
                        $education->education_institution_name = $row['education_institution_name_sd'];
                        $education->education_location         = $row['education_location_sd'];
                        $education->education_major            = $row['education_major_sd'];
                        $education->periode_from               = $row['periode_from_sd'];
                        $education->periode_to                 = $row['periode_to_sd'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_smp'];
                        $education->education_institution_name = $row['education_institution_name_smp'];
                        $education->education_location         = $row['education_location_smp'];
                        $education->education_major            = $row['education_major_smp'];
                        $education->periode_from               = $row['periode_from_smp'];
                        $education->periode_to                 = $row['periode_to_smp'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_sma'];
                        $education->education_institution_name = $row['education_institution_name_sma'];
                        $education->education_location         = $row['education_location_sma'];
                        $education->education_major            = $row['education_major_sma'];
                        $education->periode_from               = $row['periode_from_sma'];
                        $education->periode_to                 = $row['periode_to_sma'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_d1'];
                        $education->education_institution_name = $row['education_institution_name_d1'];
                        $education->education_location         = $row['education_location_d1'];
                        $education->education_major            = $row['education_major_d1'];
                        $education->periode_from               = $row['periode_from_d1'];
                        $education->periode_to                 = $row['periode_to_d1'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_d2'];
                        $education->education_institution_name = $row['education_institution_name_d2'];
                        $education->education_location         = $row['education_location_d2'];
                        $education->education_major            = $row['education_major_d2'];
                        $education->periode_from               = $row['periode_from_d2'];
                        $education->periode_to                 = $row['periode_to_d2'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_d3'];
                        $education->education_institution_name = $row['education_institution_name_d3'];
                        $education->education_location         = $row['education_location_d3'];
                        $education->education_major            = $row['education_major_d3'];
                        $education->periode_from               = $row['periode_from_d3'];
                        $education->periode_to                 = $row['periode_to_d3'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_s1'];
                        $education->education_institution_name = $row['education_institution_name_s1'];
                        $education->education_location         = $row['education_location_s1'];
                        $education->education_major            = $row['education_major_s1'];
                        $education->periode_from               = $row['periode_from_s1'];
                        $education->periode_to                 = $row['periode_to_s1'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $education = new EducationPeople();
                        $education->employe_id                 = $people->id;
                        $education->education_lvl              = $row['education_lvl_s2'];
                        $education->education_institution_name = $row['education_institution_name_s2'];
                        $education->education_location         = $row['education_location_s2'];
                        $education->education_major            = $row['education_major_s2'];
                        $education->periode_from               = $row['periode_from_s2'];
                        $education->periode_to                 = $row['periode_to_s2'];
                        $education->active_flag                = 1;
                        $education->delete_flag                = 1;
                        $education->created_by                 = 'Import Excel';
                        $education->save();

                        $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_father'];
                        $family->member_name = $row['member_name_father'];
                        $family->member_gender = $row['member_gender_father'];
                        $family->member_last_edu = $row['member_last_edu_father'];
                        $family->member_dob = $row['member_dob_father'];
                        $family->member_pob = $row['membeer_pob_father'];
                        $family->member_phone1 = $row['member_phone1_father'];
                        $family->member_company = $row['member_company_father'];
                        $family->member_company_address = $row['member_company_address_father'];
                        $family->member_job_position = $row['member_job_position_father'];
                        $family->residence_city = $row['residence_city_father'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();


                        $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_mother'];
                        $family->member_name = $row['member_name_mother'];
                        $family->member_gender = $row['member_gender_mother'];
                        $family->member_last_edu = $row['member_last_edu_mother'];
                        $family->member_dob = $row['member_dob_mother'];
                        $family->member_pob = $row['membeer_pob_mother'];
                        $family->member_phone1 = $row['member_phone1_mother'];
                        $family->member_company = $row['member_company_mother'];
                        $family->member_company_address = $row['member_company_address_mother'];
                        $family->member_job_position = $row['member_job_position_mother'];
                        $family->residence_city = $row['residence_city_mother'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                         $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_brother'];
                        $family->member_name = $row['member_name_brother'];
                        $family->member_gender = $row['member_gender_brother'];
                        $family->member_last_edu = $row['member_last_edu_brother'];
                        $family->member_dob = $row['member_dob_brother'];
                        $family->member_pob = $row['membeer_pob_brother'];
                        $family->member_phone1 = $row['member_phone1_brother'];
                        $family->member_company = $row['member_company_brother'];
                        $family->member_company_address = $row['member_company_address_brother'];
                        $family->member_job_position = $row['member_job_position_brother'];
                        $family->residence_city = $row['residence_city_brother'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                         $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_sister'];
                        $family->member_name = $row['member_name_sister'];
                        $family->member_gender = $row['member_gender_sister'];
                        $family->member_last_edu = $row['member_last_edu_sister'];
                        $family->member_dob = $row['member_dob_sister'];
                        $family->member_pob = $row['membeer_pob_sister'];
                        $family->member_phone1 = $row['member_phone1_sister'];
                        $family->member_company = $row['member_company_sister'];
                        $family->member_company_address = $row['member_company_address_sister'];
                        $family->member_job_position = $row['member_job_position_sister'];
                        $family->residence_city = $row['residence_city_sister'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                        $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_son'];
                        $family->member_name = $row['member_name_son'];
                        $family->member_gender = $row['member_gender_son'];
                        $family->member_last_edu = $row['member_last_edu_son'];
                        $family->member_dob = $row['member_dob_son'];
                        $family->member_pob = $row['membeer_pob_son'];
                        $family->member_phone1 = $row['member_phone1_son'];
                        $family->member_company = $row['member_company_son'];
                        $family->member_company_address = $row['member_company_address_son'];
                        $family->member_job_position = $row['member_job_position_son'];
                        $family->residence_city = $row['residence_city_son'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                        $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_daugther'];
                        $family->member_name = $row['member_name_daugther'];
                        $family->member_gender = $row['member_gender_daugther'];
                        $family->member_last_edu = $row['member_last_edu_daugther'];
                        $family->member_dob = $row['member_dob_daugther'];
                        $family->member_pob = $row['membeer_pob_daugther'];
                        $family->member_phone1 = $row['member_phone1_daugther'];
                        $family->member_company = $row['member_company_daugther'];
                        $family->member_company_address = $row['member_company_address_daugther'];
                        $family->member_job_position = $row['member_job_position_daugther'];
                        $family->residence_city = $row['residence_city_daugther'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                        $family = new FamilyPeople();
                        $family->employe_id = $people->id;
                        $family->member_code = $row['member_code_wife'];
                        $family->member_name = $row['member_name_wife'];
                        $family->member_gender = $row['member_gender_wife'];
                        $family->member_last_edu = $row['member_last_edu_wife'];
                        $family->member_dob = $row['member_dob_wife'];
                        $family->member_pob = $row['membeer_pob_wife'];
                        $family->member_phone1 = $row['member_phone1_wife'];
                        $family->member_company = $row['member_company_wife'];
                        $family->member_company_address = $row['member_company_address_wife'];
                        $family->member_job_position = $row['member_job_position_wife'];
                        $family->residence_city = $row['residence_city_wife'];
                        $family->active_flag = 1;
                        $family->delete_flag = 1;
                        $family->created_by = 'Import Excel';
                        $family->save();

                        $work = new WorkPeople();
                        $work->employe_id = $people->id;
                        $work->company_name = $row['company_name'];
                        $work->working_period = $row['working_period'];
                        $work->position = $row['position'];
                        $work->salary = $row['salary'];
                        $work->reason_resignation = $row['reason_resignation'];
                        $work->working_periode_end = $row['working_periode_end'];
                        $work->active_flag = 1;
                        $work->delete_flag = 1;
                        $work->created_by = 'Import Excel';
                        $work->save();


                        $interes = new PeopleVacancy();
                        $interes->people_id = $people->id;
                        $interes->interes_id = $row['interes_id'];
                        $interes->active_flag = 1;
                        $interes->delete_flag = 1;
                        $interes->save();
						
					}else{
						$temp[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
									  .'<div class="col-sm-2">'
											.$row['first_name']
									  .'</div>'
									  .'<div class="col-sm-2">'
											.$row['id_card_number']
									  .'</div>'
									  .'<div class="col-sm-7">'
											.$row['email']
									  .'</div>'
								  .'</div>';

					}
					
					if(count($temp) > 0){
						Session::flash('failed', $temp);
						Session::flash('gagal', count($temp).' Data Already Exists');	
					}

                }
                 
				
            });
			
			return Redirect('data-people');
			
        }else{
			Session::flash('gagal', 'File not selected');
			return Redirect('data-people');
		}
		
    }
	
	public function insertPeople(){
		
	}
	
	  public function downloadPeople()
    {
        return response()->download('template_excel/example_people.xlsx');
    }

    public function getFilterPeoplenew(Request $request)
    {
		$active = '';
		if(Input::get('tab') == ''){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'personal_data'){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'Interview'){
			$active = 'Interview';
		}elseif(Input::get('tab') == 'Candidate'){
			$active = 'Candidate';
		}
		
        $filterBy = $request->search;
        $value = $request->filter_value;
		
		$searchRegiter = $request->search;
	    $searchRegiterValue = $request->filter_value;
	    $searchFromValue = '';
	    $searchToValue = '';
	    $searchCandidate = 2;
	    $searchCandidateValue = '';
		
        $jumlahData = 10;
        $data = array();
        if($filterBy == 1){
			
            $data = DB::table('people_view2')
						->where('delete_flag',1)
						->where('status_project', 0)
						->Where('ktp_district', 'LIKE', '%'.$value.'%')
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'data');
						
        }elseif($filterBy == 0){
			
            $data = DB::table('people_view2')
						->where('delete_flag',1)
						->where('status_project', 0)
						->Where('first_name', 'LIKE', '%'.$value.'%')
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'data');
        
		}else{
			
            $data =DB::table('people_view2')
						->where('delete_flag',1)
						->where('status_project', 0)
						->paginate($jumlahData, ['*'], 'data');
						
        }
		
         foreach($data as $key) {
            $key->count_employe = DB::table('home_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_bank = DB::table('bank_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_education = DB::table('education')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_family = DB::table('family_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_work = DB::table('work_experience_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_facility = DB::table('facility')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_image = DB::table('image_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
								
            $key->count_doc = DB::table('document_path')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
          }
        
		$interview = DB::table('view_send_interview')
							->where('delete_flag',1)
							->where('status_project', 100)
							->orderBy('first_name')
							->paginate($jumlahData, ['*'], 'interview');
	    $candidate = DB::table('people_view2')
							->where('delete_flag',1)
							->wherein('status_project', [200, 300, 0, 1])
							->orderBy('first_name')
							->paginate($jumlahData, ['*'], 'candidate');
		
        $data->appends($request->only('filter_value'));
        $data->appends($request->only('search'));
        $paginator = $data;
		
		$data->appends(['tab' => 'personal_data']);
	    $data->appends(['interview' => $interview->currentPage()]);
		$data->appends(['candidate' => $candidate->currentPage()]);
		
		$interview->appends(['tab' => 'Interview']);
		$interview->appends(['data' => $data->currentPage()]);
		$interview->appends(['candidate' => $candidate->currentPage()]);
		
		$candidate->appends(['tab' => 'Candidate']);
		$candidate->appends(['candidate' => $candidate->currentPage()]);
		$candidate->appends(['interview' => $interview->currentPage()]);
		$candidate->appends(['data' => $data->currentPage()]);
		
        return view('master-people.data_people',compact('data', 'jumlahData', 'paginator', 'active', 'interview', 
														'candidate', 'searchRegiter', 'searchRegiterValue', 'searchFromValue', 
														'searchToValue', 'searchCandidate', 'searchCandidateValue'));
    }
	
	public function getFilterPeopleInterview(Request $request)
    {
		$active = '';
		if(Input::get('tab') == ''){
			$active = 'Interview';
		}elseif(Input::get('tab') == 'personal_data'){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'Interview'){
			$active = 'Interview';
		}elseif(Input::get('tab') == 'Candidate'){
			$active = 'Candidate';
		}
        
		$from = date('Y-m-d', strtotime($request->from));
        $to = date('Y-m-d', strtotime($request->to));
		
		$searchRegiter = 2;
	    $searchRegiterValue = '';
	    $searchFromValue = $request->from;
	    $searchToValue = $request->to;
	    $searchCandidate = 2;
	    $searchCandidateValue = '';
		
		$jumlahData = 10;
        $interview = array();
        if($from != "" && $to != ""){
			
            $interview = DB::table('view_send_interview')
								->where('delete_flag',1)
								->where('status_project', 100)
								->whereBetween('date_confir', [$from,$to])
								->orderBy('first_name')
								->paginate($jumlahData, ['*'], 'interview');
        }else{
			
            $interview =DB::table('view_send_interview')
								->where('delete_flag',1)
								->where('status_project', 100)
								->paginate($jumlahData, ['*'], 'interview');
								
        }
        
		$data = DB::table('people_view2')
					->where('delete_flag',1)
					->where('status_project', 0)
					->orderBy('first_name')
					->paginate($jumlahData, ['*'], 'data');
					
	    $candidate = DB::table('people_view2')
					->where('delete_flag',1)
					->wherein('status_project', [200, 300, 0, 1])
					->orderBy('first_name')
					->paginate($jumlahData, ['*'], 'candidate');

		foreach($data as $key) {
            $key->count_employe = DB::table('home_people')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_bank = DB::table('bank_people')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_education = DB::table('education')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_family = DB::table('family_people')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_work = DB::table('work_experience_people')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_facility = DB::table('facility')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_image = DB::table('image_people')->where('employe_id',$key->id)->where('delete_flag','1')->count();
            $key->count_doc = DB::table('document_path')->where('employe_id',$key->id)->where('delete_flag','1')->count();
          }
		
        $interview->appends($request->only('from'));
        $interview->appends($request->only('to'));
		
		$data->appends(['tab' => 'personal_data']);
	    $data->appends(['interview' => $interview->currentPage()]);
		$data->appends(['candidate' => $candidate->currentPage()]);
		$paginator = $data;
		
		$interview->appends(['tab' => 'Interview']);
		$interview->appends(['data' => $data->currentPage()]);
		$interview->appends(['candidate' => $candidate->currentPage()]);
		
		$candidate->appends(['tab' => 'Candidate']);
		$candidate->appends(['candidate' => $candidate->currentPage()]);
		$candidate->appends(['interview' => $interview->currentPage()]);
		
        return view('master-people.data_people',compact('data','jumlahData','paginator', 'active', 'interview', 
														'candidate', 'searchRegiter', 'searchRegiterValue',
														'searchFromValue', 'searchToValue', 'searchCandidate', 
														'searchCandidateValue'));
    }
	
	public function getFilterPeopleCandidate(Request $request)
    {
		$active = '';
		if(Input::get('tab') == ''){
			$active = 'Candidate';
		}elseif(Input::get('tab') == 'personal_data'){
			$active = 'personal_data';
		}elseif(Input::get('tab') == 'Interview'){
			$active = 'Interview';
		}elseif(Input::get('tab') == 'Candidate'){
			$active = 'Candidate';
		}
		
        $filterBy = $request->search;
        $value = $request->filter_value;
		
		$searchRegiter = 2;
	    $searchRegiterValue = '';
	    $searchFromValue = '';
	    $searchToValue = '';
	    $searchCandidate = $request->search;
	    $searchCandidateValue = $request->filter_value;
		
        $jumlahData = 10;
        $candidate = array();
        if($filterBy == 1){
			
            $candidate = DB::table('people_view2')
						->where('delete_flag',1)
						->where('ktp_district', 'LIKE', '%'.$value.'%')
						->wherein('status_project', [200, 300, 0, 1])
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'candidate');
			
        }elseif($filterBy == 0){
			
            $candidate = DB::table('people_view2')
						->where('delete_flag',1)
						->where('first_name', 'LIKE', '%'.$value.'%')
						->wherein('status_project', [200, 300, 0, 1])
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'candidate'); 
	  
						
        }else{
			
            $candidate =DB::table('people_view2')
						->where('delete_flag',1)
						->wherein('status_project', [200, 300, 0, 1])
						->paginate($jumlahData, ['*'], 'candidate');
						
        }
        
		$data = DB::table('people_view2')
						->where('delete_flag',1)
						->where('status_project', 0)
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'data');
	    $interview = DB::table('view_send_interview')
						->where('delete_flag',1)
						->where('status_project', 100)
						->orderBy('first_name')
						->paginate($jumlahData, ['*'], 'interview');

		foreach($data as $key) {
			
            $key->count_employe = DB::table('home_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')->count();
            $key->count_bank = DB::table('bank_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_education = DB::table('education')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_family = DB::table('family_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_work = DB::table('work_experience_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_facility = DB::table('facility')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_image = DB::table('image_people')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
            $key->count_doc = DB::table('document_path')
								->where('employe_id',$key->id)
								->where('delete_flag','1')
								->count();
          }
		
        $candidate->appends($request->only('filter_value'));
        $candidate->appends($request->only('search'));
		
		$data->appends(['tab' => 'personal_data']);
	    $data->appends(['interview' => $interview->currentPage()]);
		$data->appends(['candidate' => $candidate->currentPage()]);
		$paginator = $data;
		
		$interview->appends(['tab' => 'Interview']);
		$interview->appends(['data' => $data->currentPage()]);
		$interview->appends(['candidate' => $candidate->currentPage()]);
		
		$candidate->appends(['tab' => 'Candidate']);
		$candidate->appends(['candidate' => $candidate->currentPage()]);
		$candidate->appends(['interview' => $interview->currentPage()]);
		
        return view('master-people.data_people',compact('data','jumlahData','paginator', 'active', 'interview', 
														'candidate', 'searchRegiter', 'searchRegiterValue',
														'searchFromValue', 'searchToValue', 'searchCandidate', 
														'searchCandidateValue'));
    }
	
	public function postSentInterview(Request $request)
    {
        $SendInterview = new SendInterview();
        $SendInterview->people_id = $request->people_id;
        $SendInterview->date1 =  date('Y-m-d', strtotime($request->date1));
        $SendInterview->date2 = date('Y-m-d', strtotime($request->date2));
        $SendInterview->date3 = date('Y-m-d', strtotime($request->date3));
        $SendInterview->jam1 = $request->jam1;
        $SendInterview->jam2 = $request->jam2;
        $SendInterview->jam3 = $request->jam3;
        $SendInterview->name_hrd = $request->name_hrd;
        $SendInterview->lokasi = $request->location;
        $SendInterview->address = $request->address;
        $SendInterview->message = $request->message;
        $SendInterview->event = 'Wawancara dan Interview';
        $SendInterview->status = '0';
        $SendInterview->active_flag = '0';
		$SendInterview->status_confirm = '0';
        $SendInterview->save();
		
		if($SendInterview->id == null){
			return Redirect()->back()->with('gagal', 'Sent interview date failed');
		}else{
			$update = DB::table('employee_people')->where('id', $request->people_id)->update(['status_project' => 100]); 

			if($update == 0){
				return Redirect()->back()->with('gagal', 'Sent interview date failed');
			}else{
                $inboxData = array();
                $inboxData['message']        = "Undangan Interview di PT.Sumber Data Mandiri";
                $inboxData['id_interview']   = $SendInterview->id;
                $inboxData['status_code']    = 1;
                $inboxData['status_read']    = 0;
                $inboxData['status_confirm'] = 0;
                $inboxData['people_id']      = $request->people_id;
                Inbox::create($inboxData);
				return Redirect()->back()->with('message', 'Sent interview date successfully');
			}
		}

    }
	
	public function getAddCandidate($id)
    {
        $add_candidate = DB::table('employee_people')->where('id', $id)->update(['status_project' => 300]); 
		
		if($add_candidate == 0){
			return Redirect()->back()->with('gagal','Add candidate failed');
		}else{
			return Redirect()->back()->with('message','Add candidate successfully');
		}
    }


    public function getResetImeiUser($id)
    {
        $reset = EmployePeople::where('id',$id)->update(['imei_user' => '', 'status_login' => 0]);
        if($reset){
            return Redirect()->back()->with('message','Reset Imei successfully');
        }else{
             return Redirect()->back()->with('failed','Reset Imei failed');
        }
    }

}
