<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\JobInteres;
use Redirect;


class JobInteresController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $jumlahData = 10;
    	$data = DB::table('job_interesting')->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $data;
        return view('job-interes.data',compact('data','jumlahData', 'paginator'));
    }

    public function postAdd(Request $request)
    {
        $check = JobInteres::where('job_name',strtoupper($request->job_name))->first();
        if($check){
            return Redirect()->back()->with('failed','Data Job'.'('.' '.$request->job_name.' '.')'.'Already');
        }else{
            $data = new JobInteres();
            $data->job_name = strtoupper($request->job_name);
            $data->description = $request->desc;
            $data->active_flag = '1';
            $data->delete_flag = '1';
            $data->updated_by = 'system';
            $data->created_by = 'system';
            $data->save();

            return redirect('job-interes')->with('message','Save data job interest successfully');
        }

    	
    }

    public function postEdit(Request $request)
    {
    	$data = JobInteres::where('id',$request->id)->first();
    	$data->job_name = $request->job_name;
    	$data->description = $request->desc;
    	$data->update();

    	return redirect('job-interes')->with('message','Update data job interest successfully');
    }

    public function postDeleteData($id)
    {
    	$data = JobInteres::where('id',$id)->delete();
        if($data){
            return redirect('job-interes')->with('message','Delete data job interest successfully');
        }else{
             return redirect('job-interes')->with('failed','Delete data job interest failed');
        }
        
    }


}
