<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\OutletCategory;
use App\Outlet;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Excel;
use Illuminate\Support\Facades\Storage;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class OutletController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
	    $jumlahData = 10;
        $type = OutletCategory::orderBy('id','desc')->get();
        $outlet = DB::table('view_outlet')->where('status',1)->where('delete_flag',1)->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $outlet;
        return view('outlet.category_outlet',compact('outlet','type','jumlahData','paginator'));
    }
	
    public function getOutletOpen()
    {
        $jumlahData = 10;
        $outlet = DB::table('outlet')->where('status',0)->where('delete_flag',1)->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $outlet;
        return view('outlet.outletOpen',compact('outlet','jumlahData','paginator'));
    }
    public function postAddCategoryOutlet(Request $request)
    {
    	$data = new OutletCategory();
    	$data->outlet_category = $request->category_outlet;
    	$data->outlet_description = $request->description;
    	$data->active_flag = '1';
    	$data->delete_flag = '1';
    	$data->created_by = 'system';
    	$data->updated_by = 'system';
    	$data->save();

    	return redirect('all-outlet');
    }

    public function getCategory($id)
    {
    	$data = OutletCategory::where('id',$id)->first();
        $type = OutletCategory::orderBy('id','desc')->get();
    	$outlet = DB::table('outlet')->orderBy('id','desc')->get();
    	return view('outlet.category_outlet',compact('data','outlet','type'));
    }

    public function getAddOutlet()
    {
        // $data = OutletCategory::where('id',$id)->first();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        $type = DB::table('office_branch')->orderBy('id','desc')->where('delete_flag',1)->get();
        $category = OutletCategory::orderBy('id','desc')->get();
    	return view('outlet.add_outlet',compact('province','type','category'));
    }

    public function postOutlet(Request $request)
    {
		
		$chek = Outlet::where('outlet_name', strtoupper($request->name_outlet))->where('outlet_phone1',$request->phone1)->first();
		
		if($chek){
            return Redirect()->back()->with('failed', 'Outlet already exist');
        }else{
			$user = Auth::user();
			$dt_outlet = new Outlet();
			$dt_outlet->branch_id            = $request->branch;
			$dt_outlet->outlet_category_id   = $request->outlet_id;
			$dt_outlet->outlet_name          = strtoupper($request->name_outlet);
			$dt_outlet->outlet_address1      = $request->address1;
			$dt_outlet->outlet_address2      = $request->address2;
			$dt_outlet->outlet_tr_rw         = $request->rt_rw;
			$dt_outlet->outlet_rw            = $request->rw;
			$dt_outlet->outlet_village       = $request->village;
			$dt_outlet->outlet_sub_district  = $request->sub_district;
			$dt_outlet->outlet_city          = $request->district;
			$dt_outlet->outlet_province      = $request->province;
			$dt_outlet->outlet_post_code     = $request->post_code;
			$dt_outlet->outlet_phone1        = $request->phone1;
			$dt_outlet->outlet_phone2        = $request->phone2;
			$dt_outlet->outlet_email_address = $request->email;
			$dt_outlet->owner_name           = $request->owner_name;
			$dt_outlet->npwp                 = $request->npwp;
			$dt_outlet->outlet_langitude     = $request->langitude;
			$dt_outlet->outlet_latitude      = $request->latitude;
			$dt_outlet->status               = '1';
			$dt_outlet->active_flag          = '1';
			$dt_outlet->delete_flag          = '1';
			$dt_outlet->created_by           = $user->name;
			$dt_outlet->updated_by           = $user->name;
			$foto = $request->file('image1');
			 if ($request->hasFile('image1')) {
						
				$image       = $request->file('image1');
				$ext = $image->getClientOriginalExtension();
				$dt_outlet->image1 = date('YmdHis').".".$ext;

				$image_resize = Image::make($image->getRealPath());              
				$image_resize->resize(300, 300);
				$image_resize->save(public_path('outlet/' .$dt_outlet->image1));
			}

			$dt_outlet->save();
			
			if($dt_outlet->branch_id != null){
				return Redirect('all-outlet')->with('message', 'Save data outlet successfully');
			}else{
				return Redirect('all-outlet')->with('gagal', 'Save data outlet failed');
			}
			
		}

    }


    private function uploadFoto(Request $request){
        $foto = $request->file('image1');
        $ext = $foto->getClientOriginalExtension();
		$namaFile = '';
 
        if($request->file('image1')->isValid()){
            $rand = rand(10,100);
            $namaFoto = date('YmdHis').$rand.".".$ext;
            $upload_path = 'outlet';
            $request->file('image1')->move($upload_path,$namaFoto);
			$namaFile = $namaFoto;
        }
        return $namaFile;


      }

      public function getDetailOutlet($id)
      {
        $data = Outlet::where('id',$id)->first();
        return view('outlet.detail_outlet',compact('data'));
      }

      public function getMapsOutlet($id)
      {
        $data = Outlet::where('id',$id)->first();
        return view('outlet.maps_outlet',compact('data'));
      }

      public function getEditOutlet($id)
      {
        $type = DB::table('office_branch')->orderBy('id','desc')->where('delete_flag',1)->get();
        $category = OutletCategory::orderBy('id','desc')->get();
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
        $data = Outlet::where('id',$id)->first();

        return view('outlet.edit_outlet',compact('data','type','category','province'));
      }

      public function postEditOutlet(Request $request)
      {
        $user = Auth::user();
        $update = Outlet::where('id', $request->id)->first();
        $update->branch_id            = $request->branch;
        $update->outlet_category_id   = $request->outlet_id;
        $update->outlet_name          = strtoupper($request->name_outlet);
        $update->outlet_address1      = $request->address1;
        $update->outlet_address2      = $request->address2;
        $update->outlet_tr_rw         = $request->rt_rw;
        $update->outlet_rw            = $request->rw;
        $update->outlet_village       = $request->village;
        $update->outlet_sub_district  = $request->sub_district;
        $update->outlet_city          = $request->district;
        $update->outlet_province      = $request->province;
        $update->outlet_post_code     = $request->post_code;
        $update->outlet_phone1        = $request->phone1;
        $update->outlet_phone2        = $request->phone2;
        $update->outlet_email_address = $request->email;
        $update->owner_name           = $request->owner_name;
        $update->npwp                 = $request->npwp;
        $update->outlet_langitude     = $request->langitude;
        $update->outlet_latitude      = $request->latitude;
        $update->updated_by           = $user->name;
        $update->status              = '1';
       
        if($request->file('image1') == null)
        { 
            $update->image1 = $update->image1;
			$update->update();
			
			if($update->id == null){
				return redirect('all-outlet')->with('gagal', 'Update data outlet failed');
			}else{
				return redirect('all-outlet')->with('message', 'Update data outlet successfully');
			}
        }else{
			
			$resultImage = $this->uploadFoto($request);
			
			if($resultImage == ''){
				
				return redirect('all-outlet')->with('gagal', 'Update data outlet failed');
				
			}elseif($resultImage != ''){
				
				$update->image1 = $resultImage;
				$update->update();
				
				if($update->id == null){
					$image_path = 'outlet/'.$resultImage;
					
					if(file_exists($image_path)) {
						File::delete($image_path);
					}
					
					return redirect('all-outlet')->with('gagal', 'Update data outlet failed');
				}else{
					
					if(file_exists('outlet/'.$request->image1_old)) {
						File::delete('outlet/'.$request->image1_old);
					}
					
					return redirect('all-outlet')->with('message', 'Update data outlet successfully');
				}
			}

        }

      }
	  
	public function importOutlet(Request $request)
    {
        if($request->hasFile('import')){
            Excel::load($request->file('import')->getRealPath(), function ($reader) {
				
				$temp = array();
				
                foreach ($reader->toArray() as $key => $row) {
					
					$chek = Outlet::where('outlet_name', strtoupper($row['outlet_name']))->where('outlet_phone1', $row['outlet_phone1'])->first();
					
					if($chek == null){
						$outlet = new Outlet();
						$outlet->branch_id = intval($row['branch_id']);
						$outlet->outlet_category_id = intval($row['outlet_category_id']);
						$outlet->outlet_name = strtoupper($row['outlet_name']);
						$outlet->outlet_address1 = $row['outlet_address1'];
						$outlet->outlet_address2 = $row['outlet_address2'];
						$outlet->outlet_tr_rw = $row['outlet_tr_rw'];
						$outlet->outlet_rw = $row['outlet_rw'];
						$outlet->outlet_village = $row['outlet_village'];
						$outlet->outlet_sub_district = $row['outlet_sub_district'];
						$outlet->outlet_city = $row['outlet_city'];
						$outlet->outlet_province = $row['outlet_province'];
						$outlet->outlet_post_code = intval($row['outlet_post_code']);
						$outlet->outlet_phone1 = $row['outlet_phone1'];
						$outlet->outlet_phone2 = $row['outlet_phone2'];
						$outlet->outlet_email_address = $row['outlet_email_address'];
						$outlet->owner_name = $row['owner_name'];
						$outlet->npwp = $row['npwp'];
						$outlet->outlet_langitude = $row['outlet_langitude'];
						$outlet->outlet_latitude = $row['outlet_latitude'];
						$outlet->status = 1;
						$outlet->delete_flag = 1;
						$outlet->save();
					}else{
						$temp[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
									  .'<div class="col-sm-4">'
											.strtoupper($row['outlet_name'])
									  .'</div>'
									  .'<div class="col-sm-4">'
											.$row['outlet_phone1']
									  .'</div>'
								  .'</div>';
					}
					
					if(count($temp) > 0){
						Session::flash('failed', $temp);
						Session::flash('gagal', count($temp).' Data Already Exists');	
					}else{
						Session::flash('message', 'Import data oulet successfully');
					}
					
					
                }
            });
            return redirect('all-outlet');
        }else{
			Session::flash('gagal', 'File not selected');
			return redirect('all-outlet');
		}  
    }
	
	public function downloadDataOutlet()
    {
        return response()->download('template_excel/example_outlet.xlsx'); 
    }

    public function getFilterOutletOpen(Request $request)
    {
        $filterBy = $request->search;
        $value = $request->filter_value;
        $jumlahData = 10;
		$outlet = array();
        if($filterBy == 1){
            $outlet = Outlet::where('status',0)->Where('outlet_city', 'LIKE', '%'.$value.'%')->orderBy('outlet_name')->paginate($jumlahData);
        }elseif($filterBy == 0){
            $outlet = Outlet::where('status',0)->Where('created_by', 'LIKE', '%'.$value.'%')->orderBy('outlet_name')->paginate($jumlahData);
        }else{
            $outlet = Outlet::where('status',0)->paginate($jumlahData);
        }
		
		$paginator = $outlet;
		
		$outlet->appends($request->only('filter_value'));
        $outlet->appends($request->only('search'));
		return view('outlet.outletOpen',compact('outlet','jumlahData','paginator'));
    }

    public function getDeleteOutletFisik($id)
    {
        $delete = Outlet::where('id',$id)->update(['delete_flag' => 0 ]);
        if($delete){
            return redirect('all-outlet')->with('message','Delete data outlet successfully');
        }else{
            return redirect('all-outlet')->with('gagal','Delete data outlet failed');
        }
    }


    public function getDeleteOutetOpen($id)
    {
        $delete = Outlet::where('id',$id)->update(['delete_flag' => 0 ]);
        if($delete){
             return redirect('outlet-open')->with('message','Delete data outlet open successfully');
        }else{
             return redirect('outlet-open')->with('message','Delete data outlet open failed');
        }
    }

     public function getFilterOutletResmi(Request $request)
    {
        $filterBy = $request->search;
        $value = $request->filter_value;
        $jumlahData = 10;
        $outlet = array();
        if($filterBy == 1){
            $outlet = DB::table('view_outlet')->where('status',1)->Where('outlet_address1', 'LIKE', '%'.$value.'%')->orderBy('outlet_name')->paginate($jumlahData);
        }elseif($filterBy == 0){
            $outlet = DB::table('view_outlet')->where('status',1)->Where('outlet_name', 'LIKE', '%'.$value.'%')->orderBy('outlet_name')->paginate($jumlahData);
        }else{
            $outlet =DB::table('view_outlet')->where('status',1)->paginate($jumlahData);
        }
        
		$paginator = $outlet;
        $outlet->appends($request->only('filter_value'));
        $outlet->appends($request->only('search'));
        return view('outlet.category_outlet',compact('outlet','jumlahData','paginator'));
    }




}
