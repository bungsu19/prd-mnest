<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Promo;
use App\Inbox;


class PromoController extends Controller
{
    public function index()
    {
    	    $filterBy = '0';
            $value = '';
            $jumlahData = 10;
            $data = DB::table('promo_umum')->orderBy('id','desc')->paginate($jumlahData);

            $paginator = $data;
    	   return view('promo.data_promo',compact('data','jumlahData', 'filterBy', 'value','paginator'));
    }

    public function getAddPromo()
    {
        return view('promo.add_promo');
    }

    public function postAddPromo(Request $request)
    {
        $promo = array();
        $promo['title'] = $request->title;
        $promo['message'] = $request->message;
        $promo['date_promo'] = date('Y-m-d');

          $savePromo = Promo::create($promo);
        $id = $savePromo->id;
          if ($savePromo->id != null) {
                $inbox = array();
                $inbox['message'] = $request->title;
                $inbox['id_interview'] = (int)$id;
                $inbox['status_code'] = 3;
                $inbox['status_read'] = 0;
                $inbox['people_id'] = 0;
                $inbox['status_confirm'] = 0;
                Inbox::create($inbox);

                return redirect('promo')->with('message', 'insert data Promo succesfully');
          }else{
            return redirect()->back()->with('failed', 'insert promo failed');
          }

    }




    


}
