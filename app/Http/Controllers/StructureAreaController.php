<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\StructureArea;
use App\StructureAreaStep2;
use App\StructureAreaTeam;
use App\TitleStructureArea;
use Redirect;


class StructureAreaController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('struktur-area.struktur_area');
    }

    public function postStructure(Request $request)
    {
        $data = New StructureArea();
        $data->structure_name = $request->Structure;
        $data->national = "IND";
        $data->save();

        $step2 = StructureAreaStep2::where('stucture_id',$data->id)->get();
        return view('struktur-area.step2',compact('data','step2'));
    }

    public function postStep2Structure(Request $request)
    {
        try{
            $check = StructureAreaStep2::where('stucture_id',$request->stucture_id)->where('provinsi_id',$request->provinsi_id)->where('city_id',$request->city_id)->where('delete_flag',1)->first();
            if($check){
                return redirect()->back()->with('failed', 'City ​​already exists');
            }else{
                $user = Auth::user();
                $step2 = new StructureAreaStep2();
                $step2->stucture_id = $request->stucture_id;
                $step2->provinsi_id = $request->provinsi_id;
                $step2->title_structure_id = $request->title_id;
                $step2->city_id = $request->city_id;
                $step2->kecamatan = $request->kecamatan;
                $step2->desa = $request->village;
                $step2->created_by = $user->name;
                $step2->updated_by = $user->name;
                $step2->delete_flag = '1';
                $step2->save();

                return redirect()->back()->with('message', 'Save data structure successfully');

            }

                
          } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Save data structure failed');
        }

    }

     public function postDeleteArea($id)
    {
        $data = DB::table('structure_area_step2')->where('id', $id)->update(['delete_flag' => 0]); 
        if($data){
             return redirect()->back()->with('message', 'Delete data structure successfully');
        }else{
             return redirect()->back()->with('failed', 'Delete data structure failed');
        }
       

    }

    public function getDataStructure()
    {
        $jumlahData = 10;
        $data = TitleStructureArea::where('delete_flag','1')->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $data;
		
        return view('struktur-area.data',compact('data', 'jumlahData', 'paginator'));
    }

    public function getDetailData($id)
    {
        $jumlahData = 10;
        $data = StructureArea::findOrFail($id);
        $data2 = StructureArea::where('id',$id)->first();
        $prov = DB::table('provinsi')->orderBy('id','desc')->get();
        $dt_step2 = StructureAreaStep2::where('stucture_id',$data->id)->where('delete_flag','1')->paginate($jumlahData);
        $province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
		
		$paginator = $dt_step2;
		
        return view('struktur-area.step2-v2',compact('data','dt_step2','data2','prov','province','jumlahData', 'paginator'));
    }

    public function getAddTeam($id)
    {
        $data = StructureAreaStep2::findOrFail($id);
        $area = StructureArea::where('id',$data->stucture_id)->first();
        $dt_team = StructureAreaTeam::where('structure_area_step2_id',$data->id)->get();
        return view('struktur-area.add_team',compact('data','area','dt_team'));
    }

    public function postAddTeam(Request $request)
    {
        $team = new StructureAreaTeam();
        $team->structure_area_step2_id = $request->step2_id;
        $team->name_team = $request->name_team;
        $team->title_structure_id = $request->title_id;
        $team->save();
        $data = StructureAreaStep2::where('id',$request->step2_id)->first();
        $area = StructureArea::where('id',$data->stucture_id)->first();
        $dt_team = StructureAreaTeam::where('structure_area_step2_id',$request->step2_id)->get();

        return view('struktur-area.add_team',compact('dt_team','data','area'));
    }

    public function postTitleStructure(Request $request)
    {
        $user = Auth::user();
        $check = TitleStructureArea::where('title_structure', strtoupper($request->tite_structure))->first();
        if($check){
            return redirect()->back()->with('failed', 'Save data area structure failed');
        }else{
            $title = new TitleStructureArea();
            $title->title_structure = strtoupper($request->tite_structure);
            $title->national = $request->national;
            $title->description = $request->description;
            $title->delete_flag = '1';
            $title->created_by =  $user->name;
            $title->updated_by =  $user->name;
            $title->save();

            return redirect()->back()->with('message', 'Save data area structure successfully');
        }

       
    }

    public function postEditTitle(Request $request)
    {
        try{
            $user = Auth::user();
            $update = TitleStructureArea::where('id',$request->id)->first();
            $update->title_structure = $request->tite_structure;
            $update->national = $request->national;
            $update->description = $request->description;
            $update->updated_by =  $user->name;
            $update->update();

            return redirect()->back()->with('message', 'Update data stucture successfully');
        } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'Update data structure Failed');
        }

    }

    public function postDeleteTitle($id)
    {
        $delete = DB::table('title_structure_area')->where('id', $id)->update(['delete_flag' => 0]);
		if($delete){
			return redirect()->back()->with('message', 'Delete data area structure successfully');
		}else{
			return redirect()->back('failed', 'Data data area structure failed');
		}	

    }

    public function getStructureTitle($id)
    {
        $jumlahData = 10;
        $title = TitleStructureArea::findOrFail($id);
        $data = StructureArea::where('title_structure_id',$title->id)->where('delete_flag','1')->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $data;
        return view('struktur-area.data_v2',compact('title','data','jumlahData', 'paginator'));
    }

    public function postStructureV2(Request $request)
    {
        $user = Auth::user();
        $check = StructureArea::where('structure_name', strtoupper($request->title_structure))->first();
        if($check){
			return redirect()->back()->with('failed','Save data stucture failed');
        }else{
            $structure = New StructureArea();
            $structure->structure_name = strtoupper($request->title_structure);
            $structure->national = $request->national;
            $structure->title_structure_id = $request->title_id;
            $structure->description = $request->description;
            $structure->created_by = $user->name;
            $structure->updated_by = $user->name;
            $structure->delete_flag = '1';

            $structure->save();
            return redirect()->back()->with('message','Save data stucture successfully');
        }

      
    }

    public function postEditStructureV2(Request $request)
    {
        try{
            $user = Auth::user();
            $update = StructureArea::where('id',$request->id)->first();
            $update->structure_name = $request->title_structure;
            $update->national = $request->national;
            $update->title_structure_id = $request->title_id;
            $update->description = $request->description;
            $update->updated_by = $user->name;
            $update->update();

            return Redirect()->back()->with('message', 'update Data Success');
            } catch(\Illuminate\Database\QueryException $ex){
              return redirect()->back()->with('failed', 'update Data Failed');
        }
    }

    public function postDeleteStructure($id)
    {
        $delete = DB::table('structure_area')->where('id', $id)->update(['delete_flag' => 0]); 
        if($delete){
            return redirect()->back()->with('message','Delete data stucture successfully');
        }else{
             return redirect()->back()->with('failed','Delete data stucture failed');
        }
       

    }

    public function getViewAll($id)
    {
        $data = TitleStructureArea::findOrFail($id);
        $name_struk = StructureArea::where('title_structure_id',$data->id)->where('delete_flag','1')->get();
        $step2 = DB::table('view_area')->where('title_structure_id',$id)->where('delete_flag','1')->get();
       
        $team = DB::table('view_team')->where('title_structure_id',$id)->get();
        return view('struktur-area.view_all_data',compact('data','name_struk','step2','team'));
    }

    public function getStructureView($id)
    {
        $data = TitleStructureArea::where('id',$id)->first();
        $det_data = DB::table('structure_view')->where('id_area','{'.$id.'}')->get();

        return view('struktur-area.view_structure',compact('data','det_data'));
    }

   


}
