<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Product;
use App\Client;
use Session;
use Redirect;

class ProductController extends Controller
{
    private $column_search = array('product_name');
    private $column_order = array(null, 'product_name');
    private $order = array('product_code' => 'asc');

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function productData(Request $request)
    {
        $cque = $request->input('csearch');
        $sval = $request->input('search.value');
        $iord = $request->input('order');
        $cord = $request->input('order.0.column');
        $dord = $request->input('order.0.dir');
        $length = $request->input('length');
        $start = $request->input('start');

        if($length != -1){
            $list = $this->dtQuery($sval,$iord,$cord,$dord,$cque)->skip($start)->take($length)->get();
        } else {
            $list = $this->dtQuery($sval,$iord,$cord,$dord,$cque)->get();
        }
        
        $data = array();
        $no = $request->start;
        foreach ($list as $product) {
            if($product->active_flag == 1){
                $acttxt = '<span class="label label-info">Active</span>';
                $actxtxt = '<a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" onclick="btnDisable()" title="View Data" data-id="'.$product->product_code.'" data-active="0"><i class="fa fa-lock" style="font-size: 20px;"></i></a>';
            } else {
                $acttxt = '<span class="label label-warning">Deactive</span>';
                $actxtxt = '<a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" onclick="btnDisable()" title="View Data" data-id="'.$product->product_code.'" data-active="1"><i class="fa fa-unlock" style="font-size: 20px;"></i></a>';
            }

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $product->product_name;
            $row[] = $product->Client->company_name;
            $row[] = $product->product_category;
            $row[] = $product->product_type;
            $row[] = $acttxt;
            $row[] = '<div>'.$actxtxt.' <a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" title="View Data" data-id="'.$product->product_code.'" data-active="1"><i class="fa fa-eye" style="font-size: 20px;"></i></a></div><div style="padding-top: 10px;"><a href="'.url('/product/edit').'/'.$product->product_code.'" style="padding: 5px;" data-toggle="tooltip" title="Edit Data"><i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i></a> <a data-id="{{ $result->client_id }}" style="padding: 5px;" class="btnDelete" onclick="btnDelete()" href="javascript:void(0)" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash-o" style="color: #dc0000;font-size: 20px;"></i></a></div>';
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $request->draw,
            "recordsTotal" => Product::where('delete_flag','>','0')->count(),
            "recordsFiltered" => $this->dtQuery($sval,$iord,$cord,$dord,$cque)->count(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function dtQuery($searchVal, $orderIsset, $orderColumn, $orderDir, $checkQuery)
    {
        if($checkQuery == '1' || $checkQuery == '0'){
            $query = Product::where('delete_flag','>','0')->where('active_flag', '=', $checkQuery);
        } else {
            $query = Product::where('delete_flag','>','0');
        }
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($searchVal) // if datatable send POST for search
            {
                if($i==0) // first loop
                {
                    $query->where($item, 'LIKE', '%'.$searchVal.'%');
                }
                else
                {
                    $query->orWhere($item, 'LIKE', '%'.$searchVal.'%');
                }
            }
            $i++;
        }
         
        if(isset($orderIsset)) // here order processing
        {
            $query->orderBy($this->column_order[$orderColumn], $orderDir);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $query->orderBy(key($order), $order[key($order)]);
        }
        return $query;
    }

    public function show()
    {
        $search = '';
        $titlepage = 'Product';
        return view('master.product.index',compact('titlepage', 'search'));
    }

    public function postshow(Request $request)
    {
        $messages = [
            'search.required' => 'Please select Filter Data.',
        ];

        $validator = Validator::make($request->all(), [
            'search' => 'required'
        ],$messages);

        if ($validator->fails()) {
            return redirect('product/index')->withErrors($validator)->withInput();
        } else {
            $search = $request->search;

            $titlepage = 'Product';
            return view('master.product.index',compact('titlepage', 'search'));
        }
    }

    public function create()
    {
        $client = Client::where('delete_flag','>','0')->get();
        $titlepage = 'Product';
        return view('master.product.add',compact('client', 'titlepage'));
    }

    public function store(Request $request)
    {
        $messages = [
            'name.required' => 'The Product Name is required.',
            'name.min' => 'The Product Name must be at least 3 characters.',
            'sku.required' => 'The Product SKU is required.',
            'category.required' => 'Please select Product Category.',
            'type.required' => 'Please select Product Type.',
            'client.required' => 'Please select Client.',
            'packaging.required' => 'Please select Product Packaging.',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'sku' => 'required',
            'category' => 'required',
            'type' => 'required',
            'client' => 'required',
            'packaging' => 'required',
        ],$messages);

        if ($validator->fails()) {
            return redirect('product/create')->withErrors($validator)->withInput();
        } else {
            $cquery = Product::where('delete_flag','=','1')->where('product_name', '=', $request->name)->count();
            if($cquery > 0){
                return Redirect::back()->with('error', 'The Product Name has already been taken.')->withInput();
            }

            $user = Auth::user();

            $product = new Product();
            $product->product_name = $request->name;
            $product->product_category = $request->category;
            $product->product_type = $request->type;
            $product->client_code = $request->client;
            $product->product_sku = $request->sku;
            $product->color = $request->color;
            $product->weight = $request->weight;
            $product->height = $request->height;
            $product->packaging = $request->packaging;
            $product->dimension = $request->dimension;
            $product->product_capacity = $request->capacity;
            $product->product_price = $request->price;
            $product->product_date = $request->date;
            $product->expiry_date = $request->exdate;
            $product->active_flag = '1';
            $product->delete_flag = '1';
            $product->created_by = $user->name;
            $product->save();

            return redirect('product/index')->with('success','Data product successfully added.');
        }
    }

    public function edit($id = null)
    {
        $product = Product::find($id);
        if(!$product){
            abort(404);
        }

        $client = Client::where('delete_flag','>','0')->get();
        $titlepage = 'Product';
        return view('master.product.edit',compact('titlepage', 'client', 'id', 'product'));
    }

    public function update(Request $request)
    {
        $messages = [
            'name.required' => 'The Product Name is required.',
            'name.min' => 'The Product Name must be at least 3 characters.',
            'sku.required' => 'The Product SKU is required.',
            'category.required' => 'Please select Product Category.',
            'type.required' => 'Please select Product Type.',
            'client.required' => 'Please select Client.',
            'packaging.required' => 'Please select Product Packaging.',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'sku' => 'required',
            'category' => 'required',
            'type' => 'required',
            'client' => 'required',
            'packaging' => 'required',
        ],$messages);

        if ($validator->fails()) 
        {
            return redirect('product/edit/'.$request->id)->withErrors($validator)->withInput();
        }         

        $product = Product::find($request->id);

        if(!$product){
            abort(404);
        }

        $user = Auth::user();

        $product->product_name = $request->name;
        $product->product_category = $request->category;
        $product->product_type = $request->type;
        $product->client_code = $request->client;
        $product->product_sku = $request->sku;
        $product->color = $request->color;
        $product->weight = $request->weight;
        $product->height = $request->height;
        $product->packaging = $request->packaging;
        $product->dimension = $request->dimension;
        $product->product_capacity = $request->capacity;
        $product->product_price = $request->price;
        $product->product_date = $request->date;
        $product->expiry_date = $request->exdate;
        $product->updated_by = $user->name;
        $product->save();

        return redirect('product/index')->with('success','Data product successfully updated.');
    }

    public function delete(Request $request)
    {
        $product = Product::find($request->input('id'));
        if($product)
        {
            $product->delete_flag = 0;
            $product->save();
            return [
                'Result'    =>  "OK",
                'Message'   =>  "Success"
            ];
        }
        else
        {
            return [
                'Result'    =>  "ERROR",
                'Message'   =>  'User with id '.$request->input('id').' not found.'
            ];
        }
    }

    public function hidden(Request $request)
    {
        $product = Product::find($request->input('id'));
        if($product)
        {
            if($request->input('active') == 0){
                $product->active_flag = 0;
            } else {
                $product->active_flag = 1;
            }
            $product->save();
            return [
                'Result'    =>  "OK",
                'Message'   =>  "Success"
            ];
        }
        else
        {
            return [
                'Result'    =>  "ERROR",
                'Message'   =>  'User with id '.$request->input('id').' not found.'
            ];
        }
    }
}
