<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\OutletCategory;
use App\Outlet;
use App\Project;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\SettingShift;
use App\MasterShift;
use Excel;
use Redirect;
use Illuminate\Support\Facades\Storage;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class SettingShiftController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
	    $jumlahData = 10;
        $type = OutletCategory::orderBy('id','desc')->get();
        $outlet = DB::table('view_outlet')->where('status',1)->where('delete_flag',1)->orderBy('id','desc')->paginate($jumlahData);
		$paginator = $outlet;
		
		
        return view('outlet.category_outlet',compact('outlet','type','jumlahData','paginator'));
    }

    public function getOutletByCity($area,$projectId)
    {
        $project = Project::where('id',$projectId)->first();
        $outlet = Outlet::where('outlet_city', $area)->get();
		
		$data = array();
		for($i = 0; $i < count($outlet); $i++){
			$settingShift = SettingShift::where('outlet_id', $outlet[$i]['id'])->where('project_id', $project->id)->where('delete_flag', 1)->get();
			if(count($settingShift) > 0){
				$data[] = ['id' => $outlet[$i]['id'], 'outlet_name' => $outlet[$i]['outlet_name'], 'outlet_address1' => $outlet[$i]['outlet_address1'], 'outlet_phone1' => $outlet[$i]['outlet_phone1'], 'outlet_city' => $outlet[$i]['outlet_city'], 'jumlah' => count($settingShift)];
			}else{
				$data[] = ['id' => $outlet[$i]['id'], 'outlet_name' => $outlet[$i]['outlet_name'], 'outlet_address1' => $outlet[$i]['outlet_address1'], 'outlet_phone1' => $outlet[$i]['outlet_phone1'], 'outlet_city' => $outlet[$i]['outlet_city'], 'jumlah' => 0];
			}
		}
		
		$outlet = $data;
        return view('projects.setting_shift',compact('project','outlet'));
    }

    public function getDetailSettingShift($outletId,$projectId)
    {
        $project = Project::where('id',$projectId)->first();
        $outlet = Outlet::where('id', $outletId)->first();
        $shift = SettingShift::where('project_id', $projectId)->where('outlet_id',$outletId)->where('delete_flag', 1)->orderBy('code_shift', 'asc')->get();
        return view('projects.detail_setting_shift', compact('project','outlet','shift'));
    }

    public function postSettingShift(Request $request)
    {
		
		$dataShift = $request->shift;
		$already = array();
		if($dataShift != null ){
			for($i = 0; $i < count($dataShift); $i++){
				$check = SettingShift::where('project_id',$request->project_id)
								  ->where('outlet_id', $request->outlet_id)
								  ->where('delete_flag', 1)
								  ->where('code_shift', $dataShift[$i])
								  ->get();
								  
				if(count($check) > 0 ) {
					$already[] = '<div class="row" style="margin-bottom: -15px; color:#DC143C;">'
									  .'<div class="col-sm-4">'
											.'Data shift '.$dataShift[$i].' already'
									  .'</div>'
								  .'</div>';
				}else{
					$checkShift = MasterShift::where('shift', $dataShift[$i])->first();
					if ($checkShift) {
						$shift = new SettingShift();
						$shift->project_id = $request->project_id;
						$shift->outlet_id  = $request->outlet_id;
						$shift->code_shift = $checkShift->shift;
						$shift->hour_in    = $checkShift->hour_in;
						$shift->hour_out    = $checkShift->hour_out;
						$shift->active_flag = 1;
						$shift->delete_flag = 1;
						$shift->save();
					}
				}
			}
		}else{
			return redirect()->back()->with('failed', 'Shift not selected');
		}
		
			if(count($already) > 0){
				Session::flash('gagal', $already);
				return redirect()->back();
			}else{
				return redirect()->back()->with('message', 'Save setting shift successfully');
			}

    }
    
	
	public function updateSettingShift(Request $request)
    {
		$update = DB::table('setting_shift_project')->where('id', $request->id)->update(['hour_in' => $request->hour_in, 'hour_out' => $request->hour_out]); 
		if ($update == 0) {
			return redirect()->back()->with('failed', 'Update setting shift failed');
		}else{
			return redirect()->back()->with('message', 'Update setting shift successfully');
		}
    }
	
	public function deleteSettingShift($id)
    {
		$delete = DB::table('setting_shift_project')->where('id', $id)->update(['delete_flag' => 0]); 
		if ($delete == 0) {
			return redirect()->back()->with('failed', 'Delete setting shift failed');
		}else{
			return redirect()->back()->with('message', 'Delete setting shift successfully');
		}
    }
	
}
