<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Client;
use App\Branch;
use App\Product;
use Session;
use Redirect;

class ClientController extends Controller
{
	private $column_search = array('company_name');
	private $column_order = array(null, 'company_name');
	private $order = array('client_id' => 'asc');

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function clientData(Request $request)
    {
    	$cque = $request->input('csearch');
    	$sval = $request->input('search.value');
        $iord = $request->input('order');
        $cord = $request->input('order.0.column');
        $dord = $request->input('order.0.dir');
        $length = $request->input('length');
        $start = $request->input('start');

        if($length != -1){
        	$list = $this->dtQuery($sval,$iord,$cord,$dord,$cque)->skip($start)->take($length)->get();
        } else {
        	$list = $this->dtQuery($sval,$iord,$cord,$dord,$cque)->get();
        }
        
        $data = array();
        $no = $request->start;
        foreach ($list as $client) {
        	if($client->active_flag == 1){
        		$acttxt = '<span class="label label-info">Active</span>';
        		$actxtxt = '<a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" onclick="btnDisable()" title="View Data" data-id="'.$client->client_id.'" data-active="0"><i class="fa fa-lock" style="font-size: 20px;"></i></a>';
        	} else {
        		$acttxt = '<span class="label label-warning">Deactive</span>';
        		$actxtxt = '<a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" onclick="btnDisable()" title="View Data" data-id="'.$client->client_id.'" data-active="1"><i class="fa fa-unlock" style="font-size: 20px;"></i></a>';
        	}

        	if($client->Phone != 0){
        		$pontxt = '<b>Phone: </b><br>'.$client->company_phone1.'<br><b>Handphone: </b><br>'.$client->company_phone2;
        	} else {
        		$pontxt = '<b>Handphone: </b><br>'.$client->company_phone2;
        	}

            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $client->company_name;
            $row[] = $acttxt;
            $row[] = $client->branch_id;
            $row[] = $pontxt;
            $row[] = $client->company_email;
            $row[] = '<div>'.$actxtxt.' <a href="javascript:void(0)" style="padding: 5px;" data-toggle="tooltip" class="btnDisable" title="View Data" data-id="'.$client->client_id.'" data-active="1"><i class="fa fa-eye" style="font-size: 20px;"></i></a></div><div style="padding-top: 10px;"><a href="'.url('/client/edit').'/'.$client->client_id.'" style="padding: 5px;" data-toggle="tooltip" title="Edit Data"><i class="fa fa-pencil-square-o" style="color: #00a900;font-size: 20px;"></i></a> <a data-id="'.$client->client_id.'" style="padding: 5px;" class="btnDelete" onclick="btnDelete()" href="javascript:void(0)" data-toggle="tooltip" title="Delete Data"><i class="fa fa-trash-o" style="color: #dc0000;font-size: 20px;"></i></a></div>';
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $request->draw,
                        "recordsTotal" => Client::where('delete_flag','>','0')->count(),
                        "recordsFiltered" => $this->dtQuery($sval,$iord,$cord,$dord,$cque)->count(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function dtQuery($searchVal, $orderIsset, $orderColumn, $orderDir, $checkQuery)
    {
    	if($checkQuery == '1' || $checkQuery == '0'){
	    	$query = Client::where('delete_flag','>','0')->where('active_flag', '=', $checkQuery);
		} else {
			$query = Client::where('delete_flag','>','0');
		}
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($searchVal) // if datatable send POST for search
            {
                if($i==0) // first loop
                {
                    $query->where($item, 'LIKE', '%'.$searchVal.'%');
                }
                else
                {
                	$query->orWhere($item, 'LIKE', '%'.$searchVal.'%');
                }
            }
            $i++;
        }
         
        if(isset($orderIsset)) // here order processing
        {
            $query->orderBy($this->column_order[$orderColumn], $orderDir);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $query->orderBy(key($order), $order[key($order)]);
        }
        return $query;
    }

    public function show()
    {
    	$search = '';
    	$titlepage = 'Client';
		return view('master.client.index',compact('titlepage', 'search'));
    }

    public function postshow(Request $request)
    {
    	$messages = [
		    'search.required' => 'Please select Filter Data.',
		];

    	$validator = Validator::make($request->all(), [
	        'search' => 'required'
	    ],$messages);

    	if ($validator->fails()) {
	    	return redirect('client/index')->withErrors($validator)->withInput();
	    } else {
	    	$search = $request->search;

	    	$titlepage = 'Client';
			return view('master.client.index',compact('titlepage', 'search'));
		}
    }

    public function create()
    {
    	$branch = Branch::where('delete_flag','>','0')->get();
    	$codepost = DB::table('tb_kodepos')->select('code_pos')->orderBy('code_pos','asc')->groupBy('code_pos')->get();
    	$province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
    	$titlepage = 'Client';
    	return view('master.client.add',compact('branch', 'titlepage', 'province', 'codepost'));
    }

    public function store(Request $request)
    {
    	$messages = [
		    'name.required' => 'The Company Name is required.',
		    'name.min' => 'The Company Name must be at least 3 characters.',
		    'phone.required' => 'The Company Phone is required.',
		    'phone.min' => 'The Company Phone must be at least 5 characters.',
		    'handphone.required' => 'The Company Handphone is required.',
		    'handphone.min' => 'The Company Phone must be at least 8 characters.',
		    'email.required' => 'The Company Email is required.',
		    'email.email' => 'The Company Email must be a valid email address.',
		    'contperson.required' => 'The Contact Person is required.',
		    'contperson.min' => 'The Contact Person must be at least 3 characters.',
		    'contpersonphone.required' => 'The Contact Person Phone is required.',
		    'contpersonphone.min' => 'The Contact Person Phone must be at least 5 characters.',
		];

    	$validator = Validator::make($request->all(), [
	        'name' => 'required|min:3',
	        'branch' => 'required',
	        'phone' => 'required',
	        'handphone' => 'required|min:8',
	        'email' => 'sometimes|required|email',
	        'contperson' => 'required|min:3',
	        'contpersonphone' => 'required|min:5',
	    ],$messages);

	    if ($validator->fails()) {
	    	return redirect('client/create')->withErrors($validator)->withInput();
	    } else {
	    	$cquery = Client::where('delete_flag','=','1')->where('company_name', '=', $request->name)->count();
	    	if($cquery > 0){
	    		return Redirect::back()->with('error', 'The Client Name has already been taken.')->withInput();
	    	}

	    	$user = Auth::user();

	    	$client = new Client();
	    	$client->branch_id = $request->branch;
	    	$client->company_name = $request->name;
	    	$client->company_address = $request->address;
	    	$client->company_rt = $request->rt;
	    	$client->company_rw = $request->rw;
	    	$client->company_province = $request->province;
	    	$client->company_regency = $request->regency;
	    	$client->company_city = '';
	    	$client->company_subdistrict = $request->subdistrict;
	    	$client->company_village = $request->village;
	    	$client->company_postcode = $request->postcode;
	    	$client->company_phone1 = $request->phone;
	    	$client->company_phone2 = $request->handphone;
	    	$client->company_fax = $request->fax;
	    	$client->company_email = $request->email;
	    	$client->website_url = $request->weburl;
	    	$client->contact_person = $request->contperson;
	    	$client->contact_person_phone = $request->contpersonphone;
	    	$client->npwp = $request->npwp;
	    	$client->active_flag = '1';
	    	$client->delete_flag = '1';
	    	$client->created_by = $user->name;
	    	$client->updated_by = '';
	    	$client->save();

	    	return redirect('client/index')->with('success','Data client successfully added.');
	    }
    }

    public function edit($id = null)
	{
		$client = Client::find($id);
		if(!$client){
			abort(404);
		}

		$codepost = DB::table('tb_kodepos')->select('code_pos')->orderBy('code_pos','asc')->groupBy('code_pos')->get();
		$branch = Branch::all();
    	$province = DB::table('tb_kodepos')->select('province')->orderBy('province','asc')->groupBy('province')->get();
		$titlepage = 'Client';
    	return view('master.client.edit',compact('branch', 'titlepage', 'province', 'client', 'id', 'codepost'));
	}

	public function update(Request $request)
	{
		$messages = [
		    'name.required' => 'The Company Name is required.',
		    'name.min' => 'The Company Name must be at least 3 characters.',
		    'phone.required' => 'The Company Phone is required.',
		    'phone.min' => 'The Company Phone must be at least 5 characters.',
		    'handphone.required' => 'The Company Handphone is required.',
		    'handphone.min' => 'The Company Phone must be at least 8 characters.',
		    'email.required' => 'The Company Email is required.',
		    'email.email' => 'The Company Email must be a valid email address.',
		    'contperson.required' => 'The Contact Person is required.',
		    'contperson.min' => 'The Contact Person must be at least 3 characters.',
		    'contpersonphone.required' => 'The Contact Person Phone is required.',
		    'contpersonphone.min' => 'The Contact Person Phone must be at least 5 characters.',
		];

    	$validator = Validator::make($request->all(), [
	        'name' => 'required|min:3',
	        'branch' => 'required',
	        'phone' => 'required',
	        'handphone' => 'required|min:8',
	        'email' => 'required|email',
	        'contperson' => 'required|min:3',
	        'contpersonphone' => 'required|min:5',
	    ],$messages);

        if ($validator->fails()) 
        {
            return redirect('client/edit/'.$request->id)->withErrors($validator)->withInput();
        }         

		$client = Client::find($request->id);

		if(!$client){
			abort(404);
		}

		$user = Auth::user();

		$client->branch_id = $request->branch;
    	$client->company_name = $request->name;
    	$client->company_address = $request->address;
    	$client->company_rt = $request->rt;
    	$client->company_rw = $request->rw;
    	$client->company_province = $request->province;
    	$client->company_regency = $request->regency;
    	$client->company_subdistrict = $request->subdistrict;
    	$client->company_village = $request->village;
    	$client->company_postcode = $request->postcode;
    	$client->company_phone1 = $request->phone;
    	$client->company_phone2 = $request->handphone;
    	$client->company_fax = $request->fax;
    	$client->company_email = $request->email;
    	$client->website_url = $request->weburl;
    	$client->contact_person = $request->contperson;
    	$client->contact_person_phone = $request->contpersonphone;
    	$client->npwp = $request->npwp;
    	$client->updated_by = $user->name;
    	$client->save();

		return redirect('client/index')->with('success','Data client successfully updated.');
	}

	public function delete(Request $request)
	{
		$client = Client::find($request->input('id'));
		$cprod = Product::where('delete_flag','=','1')->where('client_code', '=', $client->client_id)->count();
		if($cprod > 0){
			return [
				'Result'	=>	"ERRORS",
				'Message'	=>	'Data tidak dapat di hapus karena terhubung dengan product.'
			];
		} else {
			if($client)
			{
				$client->delete_flag = 0;
				$client->save();
				return [
					'Result'	=>	"OK",
					'Message'	=>	"Success"
				];
			}
			else
			{
				return [
					'Result'	=>	"ERROR",
					'Message'	=>	'User with id '.$request->input('id').' not found.'
				];
			}
		}
	}

	public function hidden(Request $request)
	{
		$client = Client::find($request->input('id'));
		if($client)
		{
			if($request->input('active') == 0){
				$client->active_flag = 0;
			} else {
				$client->active_flag = 1;
			}
			$client->save();
			return [
				'Result'	=>	"OK",
				'Message'	=>	"Success"
			];
		}
		else
		{
			return [
				'Result'	=>	"ERROR",
				'Message'	=>	'User with id '.$request->input('id').' not found.'
			];
		}
	}

    // Ajax Kodepos
    public function getregency(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'data_id' => 'required'
	    ]);

	    if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$html = '<option value="">- Select Regency/City -</option>';
        	$response['hasil'] = 'OK';
        	$village = DB::table('tb_kodepos')->select('district')->where('province',$request->data_id)->orderBy('district')->groupBy('district')->get();
        	foreach ($village as $value) {
        		$html .= '<option data-tokens="'.$value->district.'" value="'.$value->district.'">'.$value->district.'</option>';
        	}
	    	$response['data'] = $html;
	        return json_encode($response);
        }
    }

    public function provregen(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'data_id' => 'required'
	    ]);

    	if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$response['hasil'] = 'OK';
        	$cprov = DB::table('tb_kodepos')->select('province','district','subdistrict','village')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district','subdistrict','village')->get();
        	if(count($cprov) == 1){
        		$prov = DB::table('tb_kodepos')->select('province','district','subdistrict','village')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district','subdistrict','village')->first();
        		$response['prov'] = $prov->province;

		    	$html1 = '<option>- Select Regency/City -</option>';
	        	$village = DB::table('tb_kodepos')->select('district')->where('province',$prov->province)->orderBy('district')->groupBy('district')->get();
	        	foreach ($village as $value) {
	        		if($prov->district == $value->district){
	        			$selected = ' selected="selected"';
	        		} else {
	        			$selected = '';
	        		}
	        		$html1 .= '<option data-tokens="'.$value->district.'" '.$selected.' value="'.$value->district.'">'.$value->district.'</option>';
	        	}
		    	$response['data1'] = $html1;

		    	$html2 = '<option>- Select Subdistrict -</option>';
	        	$subdistrict = DB::table('tb_kodepos')->select('subdistrict')->where('district',$prov->district)->orderBy('subdistrict')->groupBy('subdistrict')->get();
	        	foreach ($subdistrict as $value) {
	        		if($prov->subdistrict == $value->subdistrict){
	        			$selected = ' selected="selected"';
	        		} else {
	        			$selected = '';
	        		}
	        		$html2 .= '<option data-tokens="'.$value->subdistrict.'" '.$selected.' value="'.$value->subdistrict.'">'.$value->subdistrict.'</option>';
	        	}
		    	$response['data2'] = $html2;

		    	$html3 = '<option>- Select Village -</option>';
	        	$village = DB::table('tb_kodepos')->select('village')->where('subdistrict',$prov->subdistrict)->orderBy('village')->groupBy('village')->get();
	        	foreach ($village as $value) {
	        		if($prov->village == $value->village){
	        			$selected = ' selected="selected"';
	        		} else {
	        			$selected = '';
	        		}
	        		$html3 .= '<option data-tokens="'.$value->village.'" '.$selected.' value="'.$value->village.'">'.$value->village.'</option>';
	        	}
		    	$response['data3'] = $html3;
        	} else {
	        	$cprov = DB::table('tb_kodepos')->select('province','district','subdistrict')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district','subdistrict')->get();
	        	if(count($cprov) == 1){
	        		$prov = DB::table('tb_kodepos')->select('province','district','subdistrict')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district','subdistrict')->first();
	        		$response['prov'] = $prov->province;

			    	$html1 = '<option>- Select Regency/City -</option>';
		        	$village = DB::table('tb_kodepos')->select('district')->where('province',$prov->province)->orderBy('district')->groupBy('district')->get();
		        	foreach ($village as $value) {
		        		if($prov->district == $value->district){
		        			$selected = ' selected="selected"';
		        		} else {
		        			$selected = '';
		        		}
		        		$html1 .= '<option data-tokens="'.$value->district.'" '.$selected.' value="'.$value->district.'">'.$value->district.'</option>';
		        	}
			    	$response['data1'] = $html1;

			    	$html2 = '<option>- Select Subdistrict -</option>';
		        	$subdistrict = DB::table('tb_kodepos')->select('subdistrict')->where('district',$prov->district)->orderBy('subdistrict')->groupBy('subdistrict')->get();
		        	foreach ($subdistrict as $value) {
		        		if($prov->subdistrict == $value->subdistrict){
		        			$selected = ' selected="selected"';
		        		} else {
		        			$selected = '';
		        		}
		        		$html2 .= '<option data-tokens="'.$value->subdistrict.'" '.$selected.' value="'.$value->subdistrict.'">'.$value->subdistrict.'</option>';
		        	}
			    	$response['data2'] = $html2;

			    	$html3 = '<option>- Select Village -</option>';
		        	$village = DB::table('tb_kodepos')->select('village')->where('subdistrict',$prov->subdistrict)->where('district',$prov->district)->where('province',$prov->province)->where('code_pos',$request->data_id)->orderBy('village')->groupBy('village')->get();
		        	foreach ($village as $value) {
		        		$html3 .= '<option data-tokens="'.$value->village.'" '.$selected.' value="'.$value->village.'">'.$value->village.'</option>';
		        	}
			    	$response['data3'] = $html3;
	        	} else {
	        		$cprov = DB::table('tb_kodepos')->select('province','district')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district')->get();
	   				if(count($cprov) == 1){
		        		$prov = DB::table('tb_kodepos')->select('province','district')->where('code_pos',$request->data_id)->orderBy('province')->groupBy('province','district')->first();
		        		$response['prov'] = $prov->province;

				    	$html1 = '<option>- Select Regency/City -</option>';
			        	$village = DB::table('tb_kodepos')->select('district')->where('province',$prov->province)->orderBy('district')->groupBy('district')->get();
			        	foreach ($village as $value) {
			        		if($prov->district == $value->district){
			        			$selected = ' selected="selected"';
			        		} else {
			        			$selected = '';
			        		}
			        		$html1 .= '<option data-tokens="'.$value->district.'" '.$selected.' value="'.$value->district.'">'.$value->district.'</option>';
			        	}
				    	$response['data1'] = $html1;

				    	$html2 = '<option>- Select Subdistrict -</option>';
			        	$subdistrict = DB::table('tb_kodepos')->select('subdistrict')->where('district',$prov->district)->orderBy('subdistrict')->where('province',$prov->province)->where('code_pos',$request->data_id)->groupBy('subdistrict')->get();
			        	foreach ($subdistrict as $value) {
			        		$html2 .= '<option data-tokens="'.$value->subdistrict.'" value="'.$value->subdistrict.'">'.$value->subdistrict.'</option>';
			        	}
				    	$response['data2'] = $html2;
		        	}
	        	}
	        }
	        return json_encode($response);
        }
    }

    public function getsubdistrict(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'data_id' => 'required'
	    ]);

	    if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$html = '<option value="">- Select Subdistrict -</option>';
        	$response['hasil'] = 'OK';
        	$subdistrict = DB::table('tb_kodepos')->select('subdistrict')->where('district',$request->data_id)->orderBy('subdistrict')->groupBy('subdistrict')->get();
        	foreach ($subdistrict as $value) {
        		$html .= '<option data-tokens="'.$value->subdistrict.'" value="'.$value->subdistrict.'">'.$value->subdistrict.'</option>';
        	}
	    	$response['data'] = $html;
	        return json_encode($response);
        }
    }

    public function getvillage(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'data_id' => 'required'
	    ]);

	    if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$html = '<option value="">- Select Village -</option>';
        	$response['hasil'] = 'OK';
        	$village = DB::table('tb_kodepos')->select('village')->where('subdistrict',$request->data_id)->orderBy('village')->groupBy('village')->get();
        	foreach ($village as $value) {
        		$html .= '<option data-tokens="'.$value->village.'" value="'.$value->village.'">'.$value->village.'</option>';
        	}
	    	$response['data'] = $html;
	        return json_encode($response);
        }
    }

    public function getpostcode(Request $request){
    	$response = array();

    	$validator = Validator::make($request->all(), [
	        'data_id1' => 'required',
	        'data_id2' => 'required'
	    ]);

	    if ($validator->fails()) {
	    	$response['hasil'] = 'GAGAL';
	    	$response['data'] = '0';
            return json_encode($response);
        } else {
        	$response['hasil'] = 'OK';
        	$postcode = DB::table('tb_kodepos')->select('code_pos')->where('village',$request->data_id1)->where('subdistrict',$request->data_id2)->orderBy('code_pos')->groupBy('code_pos')->first();
	    	$response['data'] = $postcode->code_pos;
	        return json_encode($response);
        }
    }
}