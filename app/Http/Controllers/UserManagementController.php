<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Foundation\Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use DB;


class UserManagementController extends Controller
{
    public function index()
    {
    	$jumlahData = 10;
    	$data = User::orderBy('id','desc')->paginate($jumlahData);
    	return view('userManagement.dataCms',compact('data','jumlahData'));
    }

    public function getAddUser()
    {
    	return view('userManagement.addUserCms');
    } 

    public function postAddUserCms(Request $request)
    {
	    $check = User::where('email',$request->email)->first();
	    if($check){
	    	return redirect('user-cms')->with('failed','User Already');
	    }else{
	    	$user = new User();
	    	$user->name         = $request->name_user;
	    	$user->email        = $request->email;
	    	$user->phone_number = $request->phone;
	    	$user->hak_access   = $request->group;
	    	$user->password     = bcrypt($request->password);

	    	$user->save();
	    	 return redirect('user-cms')->with('message','Insert Data User Success');
	    }
    }
	
	public function postUpdateUserCms(Request $request)
    {
	    $user = User::where('id', $request->id)->update([
            'name' => $request->name_user,
            'phone_number' => $request->phone,
            'hak_access' => $request->group ]);
    	 return redirect('user-cms')->with('message','Update Data User Success');
    }

    public function postLoginCms(Request $request)
    {
		$rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()){
           return Redirect('/login')->with('failed', 'Captcha Anda Salah');
        }else{
            if(\Auth::attempt([
				'email' => $request->email,
				'password' => $request->password
				])) {
					return redirect('dashboard');
			}else {
					return Redirect('/login')->with('failed', 'Username Atau Password Salah');
			}
        }
    }

    public function getDeleteUser($id)
    {
    	$delete = User::where('id',$id)->delete();
    	if($delete){
    		return Redirect()->back()->with('message','Delete data user successfully');
    	}else{
    		return Redirect()->back()->with('failed','Delete data failed');
    	}
    }

    public function getEditUserCms($id)
    {
    	$data = User::where('id',$id)->first();
    	return view('userManagement.editUserCms',compact('data'));
    }

    public function getClientData()
    {
        $jumlahData = 10;
        $data = DB::table('users')->where('hak_access',6)->orderBy('id','desc')->paginate($jumlahData);
        return view('userManagement.dataClient',compact('data','jumlahData'));
    }

    public function getAddUserClient()
    {
        $project = DB::table('project')->where('delete_flag',1)->get();
        return view('userManagement.addClient',compact('project'));
    }


    public function postAddUserClient(Request $request)
    {
        $check = User::where('email',$request->email)->first();
        if($check){
            return redirect('user-client')->with('failed','User Already');
        }else{
            $user = new User();
            $user->name         = $request->name_user;
            $user->email        = $request->email;
            $user->phone_number = $request->phone;
            $user->hak_access   = 6;
            $user->project_id   = $request->group;
            $user->password     = bcrypt($request->password);
            $user->save();
             return redirect('user-client')->with('message','Insert Data User Success');
        }


    }


}
