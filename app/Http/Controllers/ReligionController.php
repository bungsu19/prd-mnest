<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Religion;

class ReligionController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data = Religion::orderBy('id','asc')->get();
        return view('master.religion',compact('data'));
    }

    public function postAddData(Request $request)
    {
    	$data = New Religion();
    	$data->name_religion = $request->name_religion;
    	$data->created_by = 'system';
    	$data->updated_by = 'system';
    	$data->save();

    	return redirect('/religion');
    }

    public function postEditData(Request $request)
    {
    	$data = Religion::where('id', $request->id)->first();
    	$data->name_religion = $request->name_religion;
    	$data->update();

    	return redirect('/religion');
    }

    public function postDeleteData($id)
    {
    	$data = Religion::where('id',$id)->delete();
        return redirect('/religion');
    }


}
