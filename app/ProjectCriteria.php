<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectCriteria extends Model
{
	protected $table ='project_criteria';
	protected $primaryKey = 'id';
}
?>