<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTeamVacancy extends Model
{
	protected $table ='project_team_vacancy';
	protected $primaryKey = 'id';
}
?>