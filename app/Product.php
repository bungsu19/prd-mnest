<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table ='tb_product';
	protected $primaryKey = 'id';

	public function Client()
    {
        return $this->belongsTo('App\Client', 'client_code', 'client_id');
    }
}
?>