<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $table ='tb_client';
	protected $primaryKey = 'client_id';

	public function Branch()
    {
        return $this->belongsTo('App\Branch','branch_id');
    }

    function Product(){
		return $this->hasMany('App\Product','client_id', 'client_code');
	}
}
?>