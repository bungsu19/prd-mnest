<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkPeople extends Model
{
  protected $table ='work_experience_people';
   protected $fillable =[
       'employe_id',
       'company_name',
       'working_period',
       'working_period_end',
       'position',
       'salary',
       'reason_resignation',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'
     ];
}
