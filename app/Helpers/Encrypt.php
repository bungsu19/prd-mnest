<?php
namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class Encrypt {
	
    public static function encrypt($id) {
        return Crypt::encryptString($id);
    }
	
	public static function decrypt($id) {
        return Crypt::decryptString($id);
    }
	
}