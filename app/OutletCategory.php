<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OutletCategory extends Model
{
  protected $table ='outlet_category';
   protected $fillable =[
       'outlet_category',
       'outlet_description',
       'active_flag',
       'delete_flag',
       'created_by',
       'updated_by',
       'created_at',
       'updated_at'

     ];
}
