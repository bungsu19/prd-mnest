<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskLog extends Model
{
    protected $table ='task_log';
	protected $primaryKey = 'id';
	protected $fillable =[
       'task_id',
       'status_flag',
       'received_date',
       'task_opened',
       'task_closed',
       'created_at',
       'updated_at'

     ];
}
